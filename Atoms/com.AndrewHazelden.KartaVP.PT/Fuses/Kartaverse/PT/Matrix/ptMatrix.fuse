-- ============================================================================
-- modules
-- ============================================================================
local matrix = self and require("matrix") or nil
local matrixutils = self and require("vmatrixutils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "ptMatrix"
DATATYPE = "Text"
INP1_MAXSCALE = 5

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = "ScriptVal",
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\PT\\Matrix",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Creates a rotation matrix from a PTS ScriptVal object.",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.Transform3D",
})

function Create()
    InScriptVal = self:AddInput("ScriptVal", "ScriptVal", {
        LINKID_DataType = "ScriptVal",
        LINK_Main = 1
    })

    InIndex = self:AddInput("Index", "Index", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        -- INPID_InputControl = "SliderControl",
        INP_Default = 1,
        INP_Integer = true,
        INP_MinScale = 1,
        INP_MaxScale = 360,
        INP_MinAllowed = 1,
        INP_MaxAllowed = 1e+38,
        -- INP_MaxAllowed = 1000000,
        LINK_Main = 2
    })

    self:BeginControlNest("Rotation", "Rotation", true, {LBLC_PickButton = true})
        InRotOrder = self:AddInput( "Rotation Order", "RotOrder", {
            LINKID_DataType = "Number",
            INPID_InputControl = "MultiButtonControl",
            INP_Default = 0,
            { MBTNC_AddButton = " XYZ " , },
            { MBTNC_AddButton = " XZY " , },
            { MBTNC_AddButton = " YXZ " , },
            { MBTNC_AddButton = " YZX " , },
            { MBTNC_AddButton = " ZXY " , },
            { MBTNC_AddButton = " ZYX " , },
            --INP_DoNotifyChanged = true,
        })

        RotateX = self:AddInput( "X Rotation", "Rotate.X", {
            LINKID_DataType = "Number",
            INPID_InputControl = "ScrewControl",
            INP_Integer = false,
            INP_MaxScale = 360.0,
            IC_Steps = 1,
            IC_NoReset = true,
            INP_Passive = true,
        })

        RotateY = self:AddInput( "Y Rotation", "Rotate.Y", {
            LINKID_DataType = "Number",
            INPID_InputControl = "ScrewControl",
            INP_Integer = false,
            INP_MaxScale = 360.0,
            IC_Steps = 1,
            IC_NoReset = true,
            INP_Passive = true,
        })

        RotateZ = self:AddInput( "Z Rotation", "Rotate.Z", {
            LINKID_DataType = "Number",
            INPID_InputControl = "ScrewControl",
            INP_Integer = false,
            INP_MaxScale = 360.0,
            IC_Steps = 1,
            IC_NoReset = true,
            INP_Passive = true,
        })
    self:EndControlNest()

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })
    
    OutMatrix = self:AddOutput( "Matrix", "Matrix", {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        LINK_Main = 1
    })
end


function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        if param.Value == 1.0 then
            InIndex:SetAttrs({LINK_Visible = true})
        else
            InIndex:SetAttrs({LINK_Visible = false})
        end
    end
end

function get(t, key)
    --[[
        Returns the value of a key in a table.

        :param t: Table to get key value for.
        :type t: table

        :param key: Key to get value of.
        :type key: string

        :rtype: ?
    ]]
    local value = nil
    local found = false

    for k, v in pairs(t) do
        if k == key then
            value = v
            found = true
            break
        end
    end

    if not found then
        error(string.format("no key '%s' found in ScriptVal table", key))
    end

    return value
end

function Process(req)
    local tbl = InScriptVal:GetValue(req):GetValue()
    local key = InIndex:GetValue(req).Value
    
    local rotation_x = 0
    local rotation_y = 0
    local rotation_z = 0

    local value = nil
    local value_str = ""

    -- Extract: project.imagegroups.#.images.filename
    if type(tbl) == "table" and tbl.pts and tbl.pts.project and tbl.pts.project.imagegroups and type(tbl.pts.project.imagegroups) == "table" then
        -- Clamp the index at the maximum range
        max_keys = tonumber(table.getn(tbl.pts.project.imagegroups))
        if key > max_keys then
            key = max_keys
        end

        -- Extract the image number element
        groupTbl = get(tbl.pts.project.imagegroups, key)

        -- Extract the images group
        if type(groupTbl) == "table" then
            -- dump(groupTbl)

            -- Extract the rotation values
            if groupTbl.position and groupTbl.position.params then
                -- dump(groupTbl.position.params)

                rotation_x = math.rad(groupTbl.position.params.pitch)
                rotation_y = math.rad(groupTbl.position.params.yaw)
                rotation_z = math.rad(groupTbl.position.params.roll)

                -- Force the current rotation values into the Inspector ScrewControls
                RotateX:SetSource(Number(math.deg(rotation_x)), 0)
                RotateY:SetSource(Number(math.deg(rotation_y)), 0)
                RotateZ:SetSource(Number(math.deg(rotation_z)), 0)
            end
        end
    end

    local rotation_order = InRotOrder:GetValue(req).Value

    local mx_product = matrix{
        { 1.0 , 0.0 , 0.0 , 0.0 } ,
        { 0.0 , 1.0 , 0.0 , 0.0 } ,
        { 0.0 , 0.0 , 1.0 , 0.0 } ,
        { 0.0 , 0.0 , 0.0 , 1.0 }
    }

-- 3D Math Primer for Graphics and Game Development, Chapter 5. Matrices and Linear Transformations, p140

    local mx_rotation_x = matrix{
        { 1.0 , 0.0                     , 0.0                    , 0.0 } ,
        { 0.0 , math.cos( rotation_x )  , math.sin( rotation_x ) , 0.0 } ,
        { 0.0 , -math.sin( rotation_x ) , math.cos( rotation_x ) , 0.0 } ,
        { 0.0 , 0.0                     , 0.0                    , 1.0 }
    }

    local mx_rotation_y = matrix{
        { math.cos( rotation_y ) , 0.0 , -math.sin( rotation_y ) , 0.0 } ,
        { 0.0                    , 1.0 , 0.0                     , 0.0 } ,
        { math.sin( rotation_y ) , 0.0 , math.cos( rotation_y )  , 0.0 } ,
        { 0.0                    , 0.0 , 0.0                     , 1.0 }
    }

    local mx_rotation_z = matrix{
        { math.cos( rotation_z )  , math.sin( rotation_z ) , 0.0 , 0.0 } ,
        { -math.sin( rotation_z ) , math.cos( rotation_z ) , 0.0 , 0.0 } ,
        { 0.0                     , 0.0                    , 1.0 , 0.0 } ,
        { 0.0                     , 0.0                    , 0.0 , 1.0 }
    }

    -- XYZ
    if rotation_order == 0 then
        mx_product = matrix.mul( matrix.mul( mx_rotation_x , mx_rotation_y ) , mx_rotation_z )
    -- XZY
    elseif rotation_order == 1 then
        mx_product = matrix.mul( matrix.mul( mx_rotation_x , mx_rotation_z ) , mx_rotation_y )
    -- YXZ
    elseif rotation_order == 2 then
        mx_product = matrix.mul( matrix.mul( mx_rotation_y , mx_rotation_x ) , mx_rotation_z )
    -- YZX
    elseif rotation_order == 3 then
        mx_product = matrix.mul( matrix.mul( mx_rotation_y , mx_rotation_z ) , mx_rotation_x )
    -- ZXY
    elseif rotation_order == 4 then
        mx_product = matrix.mul( matrix.mul( mx_rotation_z , mx_rotation_x ) , mx_rotation_y )
    -- ZYX
    elseif rotation_order == 5 then
        mx_product = matrix.mul( matrix.mul( mx_rotation_z , mx_rotation_y ) , mx_rotation_x )
    end

    local output_matrix = matrixutils.matrix_to_string(mx_product)
    OutMatrix:Set(req , Text(output_matrix))
end