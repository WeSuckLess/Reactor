--[[--
Made by Jacob Danell <jacob@emberlight.se>

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
----------------------------------------------------------------------

Todo:
	- Work with RoI

Changelog:
v1.2: 2023-11-01:
- Select the image size, window size and clipping to get the exact image you need
- STMapper can now read the DoD from the stmap and write out data in the outputs DoD correctly
- STMapper now a- dds center-offset metadata to automatically be able to apply the correct center-offset when re-distorting the image
- Grouped the parameters for a cleaner UI
- Changed default tiling mode to Transparent
- Distortion amount now respects the UV channel selection
- Removed cap for Distortion slider. It's not magical and can't reverse your stmap if set to -1 but you can get some fun results.
- Added author text
- Code Cleanup

v1.1: 2022-12-10:
- Now even faster and better quality when viewing with HQ off!
- Rewrote the whole stmap engine to now support native tiling sampler
- Fixed PreCalculation so correct image size information goes to output nodes
- You can now select where to get your metadata from
- The depth is now picked from the texture instead of the STmap
- Now works with DoD bigger than the image/stmap
- Remove premulti for STmap and postmulti for RGB
- Removed Image Frame with DoD crop from Crop selection (I don't remember the use-case really...)
- You can now select the amount of undistortion you want

v1.0.3, 2022-03-17:
- Now works with OpenCL

v1.0.2, 2022-02-25:
- Added Repeat Pivot that lets you select where the pivot point of the repeat is located
- Cleaned up the UX by replacing "Calculate STmap from pixel center" with "Match Render" where you can select between the 3 different types of STMaps.
	Ignore = First pixel is 0, last pixel is 1.
	Image = This is what Custom Node-generated STmaps looks like and what the Texture node expects.
	Mesh = This is what Nuke-generated STmaps looks like.
- Removed Crop to DoD that you could set when using Tile or Mirror (honestly I don't remember what it was there for...)
- Rewrote the Tile and Mirror tiling so it no longer gives weird artifacts when the input images DoD is smaller than the image size.

v1.0.1 2022-02-03:
- STMapper now expects the first pixel to be 0 and the pixel next to the last pixel to be 1. This is how Fusions Texture node works.
- Added "Calculate STmap from pixel center" to read STmaps like Nuke does.

v1.0, 2020-10-12:
- First release!

--]]
--

local version = "1.2"
local authorText = "v" .. version .. ". Made by Jacob Danell, Ember Light"

FuRegisterClass("STMapper", CT_Tool, {
	REGS_Name             = "STMapper",
	REGS_Category         = "Warp",
	REGS_OpIconString     = "SM",
	REGS_OpDescription    = "STMapper",
	REGS_Company          = "Ember Light",
	REGS_URL              = "",
	REGS_HelpTopic        = "",

	REG_Fuse_NoEdit       = false,
	REG_Fuse_NoReload     = false,
	REG_SupportsDoD       = true,
	REG_NoPreCalcProcess  = true,
	REG_Version           = 120,

	REG_NoAutoProxy       = true,
	REG_NoMotionBlurCtrls = true,
	REG_NoObjMatCtrls     = true,
	REG_NoBlendCtrls      = true,
	REG_OpNoMask          = true,
})


function Create()
	InCrop = self:AddInput("Size", "Size", {
		LINKID_DataType    = "Number",
		INPID_InputControl = "MultiButtonControl",
		INP_Default        = 1.0,
		{ MBTNC_AddButton = "Image Frame" },
		{ MBTNC_AddButton = "STMap Frame" },
		MBTNC_StretchToFit  = true,
		MBTNC_ShowName      = true,
		INP_DoNotifyChanged = true,
	})

	InWindow = self:AddInput("Window", "Window", {
		LINKID_DataType    = "Number",
		INPID_InputControl = "MultiButtonControl",
		INP_Default        = 1.0,
		{ MBTNC_AddButton = "Frame" },
		{ MBTNC_AddButton = "DoD" },
		MBTNC_StretchToFit  = true,
		MBTNC_ShowName      = true,
		INP_DoNotifyChanged = true,
	})

	InClipping = self:AddInput("Clipping Mode", "ClippingMode", {
		LINKID_DataType    = "Number",
		INPID_InputControl = "MultiButtonControl",
		INP_Default        = 0.0,
		{ MBTNC_AddButton = "None" },
		{ MBTNC_AddButton = "Frame" },
		MBTNC_StretchToFit  = true,
		MBTNC_ShowName      = true,
		INP_DoNotifyChanged = true,
	})

	InMetadataOffset = self:AddInput("Apply image offset from metadata", "MetadataOffset", {
		LINKID_DataType = "Number",
		INPID_InputControl = "CheckboxControl",
		INP_Default = 1.0,
	})

	self:AddInput(" ", "sep1", {
		INPID_InputControl = "SeparatorControl",
	})

	InUChannel = self:AddInput("U Channel", "UCHannel", {
		LINKID_DataType = "Number",
		INPID_InputControl = "MultiButtonControl",
		INP_Default = 0.0,
		{ MBTNC_AddButton = "Red" },
		{ MBTNC_AddButton = "Green" },
		{ MBTNC_AddButton = "Blue" },
		INP_Integer = true,
	})

	InVChannel = self:AddInput("V Channel", "VCHannel", {
		LINKID_DataType = "Number",
		INPID_InputControl = "MultiButtonControl",
		INP_Default = 1.0,
		{ MBTNC_AddButton = "Red" },
		{ MBTNC_AddButton = "Green" },
		{ MBTNC_AddButton = "Blue" },
		INP_Integer = true,
	})

	InFlipU = self:AddInput("Flip U", "FlipU", {
		LINKID_DataType = "Number",
		INPID_InputControl = "CheckboxControl",
		INP_Default = 0.0,
		ICD_Width = 0.5,
		INP_Integer = true,
	})

	InFlipV = self:AddInput("Flip V", "FlipV", {
		LINKID_DataType = "Number",
		INPID_InputControl = "CheckboxControl",
		INP_Default = 0.0,
		ICD_Width = 0.5,
		INP_Integer = true,
	})

	self:AddInput(" ", "sep2", {
		INPID_InputControl = "SeparatorControl",
	})

	InLockXY = self:AddInput("Lock X/Y", "LockXY", {
		LINKID_DataType = "Number",
		INPID_InputControl = "CheckboxControl",
		INP_Integer = true,
		INP_Default = 1.0,
		INP_DoNotifyChanged = true,
	})

	InDistortionX = self:AddInput("X Distortion", "XDistortion", {
		LINKS_Name = "X Distortion",
		LINKID_DataType = "Number",
		INPID_InputControl = "SliderControl",
		INP_MaxScale = 1.0,
		INP_MinScale = 0.0,
		INP_Default = 1.0,
	})

	InDistortionY = self:AddInput("Y Distortion", "YDistortion", {
		LINKS_Name = "Y Distortion",
		LINKID_DataType = "Number",
		INPID_InputControl = "SliderControl",
		INP_MaxScale = 1.0,
		INP_MinScale = 0.0,
		INP_Default = 1.0,
		INP_Visible = false,
	})

	InOffset = self:AddInput("Offset", "Offset", {
		LINKID_DataType = "Point",
		INPID_InputControl = "OffsetControl",
		INPID_PreviewControl = "CrosshairControl",
	})

	InTiling = self:AddInput("Tiling", "Tiling", {
		LINKID_DataType = "Number",
		INPID_InputControl = "MultiButtonControl",
		INP_Default = 0.0,
		INP_Integer = true,
		{ MBTNC_AddButton = "Transparent" },
		{ MBTNC_AddButton = "Tile" },
		{ MBTNC_AddButton = "Extend" },
		{ MBTNC_AddButton = "Mirror" },
	})

	InRepeatPivot = self:AddInput("Repeat Pivot", "RepeatPivot", {
		LINKID_DataType = "Point",
		INPID_InputControl = "OffsetControl",
		INPID_PreviewControl = "CrosshairControl",
	})

	InHRepeat = self:AddInput("Horizontal Repeat", "HorizontalRepeat", {
		LINKID_DataType = "Number",
		INPID_InputControl = "ScrewControl",
		INP_Default = 1.0,
	})

	InVRepeat = self:AddInput("Vertical Repeat", "VerticalRepeat", {
		LINKID_DataType = "Number",
		INPID_InputControl = "ScrewControl",
		INP_Default = 1.0,
	})

	InCombRepeat = self:AddInput("Combined Repeat", "CombinedRepeat", {
		LINKID_DataType = "Number",
		INPID_InputControl = "ScrewControl",
		INP_Default = 1.0,
	})

	self:AddInput(" ", "sep3", {
		INPID_InputControl = "SeparatorControl",
	})

	InMatchRender = self:AddInput("Match Render", "MatchRender", {
		LINKID_DataType = "Number",
		INPID_InputControl = "MultiButtonControl",
		INP_Default = 1.0,
		{ MBTNC_AddButton = "Ignore (0 to Width)",     MBTNCS_ToolTip = "First pixel is 0, last pixel is 1" },
		{
			MBTNC_AddButton = "Image (0 to Width-1)",
			MBTNCS_ToolTip =
			"This is what Custom Tool-generated STmaps looks like and what the Texture node expects"
		},
		{ MBTNC_AddButton = "Mesh (0.5 to Width-0.5)", MBTNCS_ToolTip = "This is what Nuke-generated STmaps looks like" },
		INP_DoNotifyChanged = true,
		INP_Integer = true,
	})

	InMetadata = self:AddInput("Metadata", "Metadata", {
		LINKID_DataType = "Number",
		INPID_InputControl = "MultiButtonControl",
		INP_Default = 0.0,
		{ MBTNC_AddButton = "Texture" },
		{ MBTNC_AddButton = "STmap" },
		INP_Integer = true,
	})

	self:AddInput(" ", "sep4", {
		INPID_InputControl = "SeparatorControl",
	})

	Author = self:AddInput(authorText, "Author", {
		LINKID_DataType = "Text",
		INPID_InputControl = "LabelControl",
		INP_External = false,
		INP_Passive = true,
		IC_NoLabel = true,
		LBLC_Align = 2,
	})

	InImage = self:AddInput("Texture", "Texture", {
		LINKID_DataType = "Image",
		LINK_Main = 2,
		INP_AcceptsGPUImages = true,
		INP_Priority = -1,
		INP_Required = true,
	})

	InSTMapImage = self:AddInput("Input", "Input", {
		LINKID_DataType = "Image",
		LINK_Main = 1,
		INP_AcceptsGPUImages = true,
		INP_Priority = -2,
		INP_Required = true,
	})

	OutImage = self:AddOutput("Output", "Output", {
		LINKID_DataType = "Image",
		LINK_Main = 1,
	})
end

function NotifyChanged(inp, param, time)
	if inp == InLockXY then
		if param.Value > 0.5 then -- Locked
			InDistortionX:SetAttrs({ LINKS_Name = "Distortion" })
			InDistortionY:SetAttrs({ IC_Visible = false })
		else
			InDistortionX:SetAttrs({ LINKS_Name = "X Distortion" })
			InDistortionY:SetAttrs({ IC_Visible = true })
		end
	elseif inp == InCrop then
		if param.Value < 0.5 then
			InWindow:SetAttrs({ IC_Visible = false })
			--InClipping:SetAttrs({ IC_Visible = false })
		else
			InWindow:SetAttrs({ IC_Visible = true })
			--InClipping:SetAttrs({ IC_Visible = true })
		end
	elseif inp == InWindow then
		if param.Value > 0.5 then
			InClipping:SetAttrs({ IC_Visible = false })
		else
			InClipping:SetAttrs({ IC_Visible = true })
		end
	end
end

-- request the whole DoD from upstream. Without overriding CheckRequest, Fusion would
-- only request what it requested from this tool (the current RoI, that is)
function CheckRequest(req)
	if (req:GetPri() <= -1) and (not req:IsFailed()) then
		local prosImg = nil
		if (req:GetPri() == -1) then
			prosImg = InImage
		else
			prosImg = InSTMapImage
		end
		local inpdod = req:GetInputDoD(prosImg)
		local reqdod = req:GetDoD()
		if (inpdod ~= nil) and (reqdod ~= nil) then
			local reqroi = req:GetRoI()
			if reqroi ~= nil then
				local datawnd = reqroi.ValidWindow
				-- assign new RoI to input
				if tostring(datawnd) == tostring(inpdod) then
					req:SetInputRoI(InImage, inpdod:Intersect(inpdod))
				else
					req:SetInputRoI(InImage, inpdod:Intersect(inpdod))
				end
			end
		end
	end
end

function ClacDod(req)
	local inimg = InImage:GetValue(req)
	local stMapimg = InSTMapImage:GetValue(req)

	local crop = InCrop:GetValue(req).Value
	local window = InWindow:GetValue(req).Value
	local clipping = InClipping:GetValue(req).Value

	-- attributes for output image (to be extended further down)
	local outattrs

	-- calculate output image dimension and output DoD (datawnd)
	if crop == 0 then
		-- Crop to Img
		outattrs = {
			IMG_Like = inimg,
			IMG_NoData = req:IsPreCalc(),
		}
	elseif crop == 1 then
		-- Expand to STMap
		outattrs = {
			IMG_Like = stMapimg,
			IMG_Depth = inimg.Depth,
			IMG_NoData = req:IsPreCalc(),
		}
		if window == 0 then
			if clipping == 1 then
				outattrs.IMG_DataWindow = { 0, 0, stMapimg.Width, stMapimg.Height }
			end
		else -- window = 1
			outattrs.IMG_Width = stMapimg.DataWindow:Width()
			outattrs.IMG_Height = stMapimg.DataWindow:Height()
			outattrs.IMAT_OriginalWidth = stMapimg.DataWindow:Width()
			outattrs.IMAT_OriginalHeight = stMapimg.DataWindow:Height()
		end
	end

	return Image(outattrs)
end

function Process(req)
	local inimg = InImage:GetValue(req)
	local stMapimg = InSTMapImage:GetValue(req)

	local out = ClacDod(req)

	local metadataInput = InMetadata:GetValue(req).Value
	local metadata = {}
	if metadataInput == 0 then
		for name, val in pairs(inimg.Metadata) do
			metadata[name] = val
		end
	else
		metadata = stMapimg.Metadata
	end

	if not req:IsPreCalc() then
		local node = DVIPComputeNode(req, "STMapKernel", STMapKernel, "STMapParams", STMapParams)

		local params = node:GetParamBlock(STMapParams)

		params.outSize[0] = out.DataWindow:Width()
		params.outSize[1] = out.DataWindow:Height()

		params.stmSize[0] = stMapimg.DataWindow:Width()
		params.stmSize[1] = stMapimg.DataWindow:Height()
		params.halfPixel[0] = 0.5 / stMapimg.DataWindow:Width();
		params.halfPixel[1] = 0.5 / stMapimg.DataWindow:Height();

		local stMapDataWidth = stMapimg.DataWindow.right - stMapimg.DataWindow.left
		local stMapDataHeight = stMapimg.DataWindow.top - stMapimg.DataWindow.bottom

		local imgWidth = inimg.Width
		local imgHeight = inimg.Height
		local imgDataWidth = inimg.DataWindow.right - inimg.DataWindow.left
		local imgDataHeight = inimg.DataWindow.top - inimg.DataWindow.bottom
		local imgDataLeft = inimg.DataWindow.left
		local imgDataBottom = inimg.DataWindow.bottom

		params.stmOffset[0] = 0
		params.stmOffset[1] = 0
		params.stmMultiplyer[0] = 1
		params.stmMultiplyer[1] = 1
		if metadata["stmapper"] ~= nil and InMetadataOffset:GetValue(req).Value == 1 then
			params.stmOffset[0] = (metadata["stmapper"]["offsetX"] - (metadata["stmapper"]["orgSizeX"] - imgWidth) / 2) /
				stMapimg.DataWindow:Width()

			params.stmOffset[1] = (metadata["stmapper"]["offsetY"] - (metadata["stmapper"]["orgSizeY"] - imgHeight) / 2) /
				stMapimg.DataWindow:Height()

			params.stmMultiplyer[0] = metadata["stmapper"]["orgSizeX"] / imgWidth
			params.stmMultiplyer[1] = metadata["stmapper"]["orgSizeY"] / imgHeight
		end

		-- Get the difference between the STMap and the texture
		if InCrop:GetValue(req).Value == 0 then -- Image size
			params.imgOffset[0] = imgDataLeft / imgWidth
			params.imgOffset[1] = imgDataBottom / imgHeight
			params.imgMulti[0] = imgWidth / imgDataWidth
			params.imgMulti[1] = imgHeight / imgDataHeight
			params.stmDodOffset[0] = ((-stMapDataWidth + imgWidth) * 0.5)
			params.stmDodOffset[1] = ((-stMapDataHeight + imgHeight) * 0.5)
		else -- STmap size
			params.imgOffset[0] = imgDataLeft / imgWidth
			params.imgOffset[1] = imgDataBottom / imgHeight
			params.imgMulti[0] = imgWidth / imgDataWidth
			params.imgMulti[1] = imgHeight / imgDataHeight
			if InClipping:GetValue(req).Value == 0 or InWindow:GetValue(req).Value == 1 then
				params.stmDodOffset[0] = 0
				params.stmDodOffset[1] = 0
			else
				params.stmDodOffset[0] = stMapimg.DataWindow.left
				params.stmDodOffset[1] = stMapimg.DataWindow.bottom
			end
		end

		local matchRender = InMatchRender:GetValue(req).Value
		params.matchRender = { 0, 0 }
		params.pixelScale[0] = 1
		params.pixelScale[1] = 1
		if matchRender == 0 then
			params.pixelScale[0] = 1 - (1 / imgDataWidth)
			params.pixelScale[1] = 1 - (1 / imgDataHeight)
		elseif matchRender == 2 then
			params.matchRender = { 0.5 / stMapDataWidth, 0.5 / stMapDataHeight }
		end

		params.distAmount[0] = InDistortionX:GetValue(req).Value
		params.distAmount[1] = InDistortionY:GetValue(req).Value
		if InLockXY:GetValue(req).Value == 1 then
			params.distAmount[1] = InDistortionX:GetValue(req).Value
		end

		params.uChannel = InUChannel:GetValue(req).Value
		params.vChannel = InVChannel:GetValue(req).Value
		params.flipU = InFlipV:GetValue(req).Value
		params.flipV = InFlipU:GetValue(req).Value
		params.offset[0] = InOffset:GetValue(req).X - 0.5
		params.offset[1] = InOffset:GetValue(req).Y - 0.5
		params.repeatPivot[0] = InRepeatPivot:GetValue(req).X
		params.repeatPivot[1] = InRepeatPivot:GetValue(req).Y
		params.hRepeat = InHRepeat:GetValue(req).Value + InCombRepeat:GetValue(req).Value - 1
		params.vRepeat = InVRepeat:GetValue(req).Value + InCombRepeat:GetValue(req).Value - 1

		params.compOrder = out:IsMask() and 1 or 15

		node:SetParamBlock(params)

		local edges = InTiling:GetValue(req).Value
		--This gets the value of our input image for us to modify inside the kernel
		if edges == 0 then
			node:AddSampler("RowSampler", TEX_FILTER_MODE_LINEAR, TEX_ADDRESS_MODE_BORDER, TEX_NORMALIZED_COORDS_TRUE)
		elseif edges == 1 then
			node:AddSampler("RowSampler", TEX_FILTER_MODE_LINEAR, TEX_ADDRESS_MODE_WRAP, TEX_NORMALIZED_COORDS_TRUE)
		elseif edges == 2 then
			node:AddSampler("RowSampler", TEX_FILTER_MODE_LINEAR, TEX_ADDRESS_MODE_DUPLICATE, TEX_NORMALIZED_COORDS_TRUE)
		elseif edges == 3 then
			node:AddSampler("RowSampler", TEX_FILTER_MODE_LINEAR, TEX_ADDRESS_MODE_MIRROR, TEX_NORMALIZED_COORDS_TRUE)
		end

		node:AddInput("inimg", inimg)
		node:AddInput("stMapimg", stMapimg)
		node:AddOutput("outimg", out)

		local ok = node:RunSession(req)
		if not ok then
			out = inimg
			dump(node:GetErrorLog())
		end
		collectgarbage()
	end

	if metadata["stmapper"] ~= nil and InMetadataOffset:GetValue(req).Value == 1 then
		metadata["stmapper"] = nil
	else
		local stmapperMetadata = {}
		stmapperMetadata["offsetX"] = stMapimg.DataWindow.left
		stmapperMetadata["offsetY"] = stMapimg.DataWindow.bottom
		stmapperMetadata["orgSizeX"] = stMapimg.Width
		stmapperMetadata["orgSizeY"] = stMapimg.Height
		metadata["stmapper"] = stmapperMetadata
	end
	out.Metadata = metadata

	OutImage:Set(req, out)
	collectgarbage()
end

STMapParams = [[
	int outSize[2];
	float stmSize[2];
	float imgOffset[2];
	float imgMulti[2];
	float distAmount[2];

	float stmDodOffset[2];
	float stmMultiplyer[2];
	float stmOffset[2];

	float matchRender[2];
	float pixelScale[2];
	float halfPixel[2];

	int uChannel;
	int vChannel;
	bool flipU;
	bool flipV;
	float offset[2];
	float repeatPivot[2];
	float hRepeat;
	float vRepeat;

	int compOrder;
]]

STMapKernel = [[
	__KERNEL__ void STMapKernel(
		__CONSTANTREF__ STMapParams *params,
		__TEXTURE2D__ inimg,
		__TEXTURE2D__ stMapimg,
		__TEXTURE2D_WRITE__ outimg
	){
		
		DEFINE_KERNEL_ITERATORS_XY(x, y);
		if (x < params->outSize[0] && y < params->outSize[1])
		{
			//-----------------------
			//DCTL Setup
			//-----------------------
			float2 fragCoord = to_float2(x,y) + to_float2_s(0.5f);
			float2 uvResolution = to_float2_v(params->stmSize);
			float2 stmOffset = to_float2_v(params->stmOffset);
			float2 stMapUV = fragCoord;
			stMapUV /= uvResolution;
			float2 halfPixel = to_float2_v(params->halfPixel);

			//-----------------------
			//STMap Setup
			//-----------------------
			float4 stMappos = _tex2DVecN(stMapimg, stMapUV.x, stMapUV.y, params->compOrder);
			stMappos.x /= stMappos.w;
			stMappos.y /= stMappos.w;
			// Select amount of undistortion
			stMappos.x = (stMappos.x * params->distAmount[0]) + (stMapUV.x * (1-params->distAmount[0]));
			stMappos.y = (stMappos.y * params->distAmount[1]) + (stMapUV.y * (1-params->distAmount[1]));
	
			float2 stMap;
			// CHANNEL SELECT
			if(params->uChannel == 0) // Red
			{
				stMap.x = stMappos.x;
			}
			else if(params->uChannel == 1) // Green
			{
				stMap.x = stMappos.y;
			}
			else if(params->uChannel == 2) // Blue
			{
				stMap.x = stMappos.z;
			}
		
			if(params->vChannel == 0) // Red
			{
				stMap.y = stMappos.x;
			}
			else if(params->vChannel == 1) // Green
			{
				stMap.y = stMappos.y;
			}
			else if(params->vChannel == 2) // Blue
			{
				stMap.y = stMappos.z;
			}

			// Apply offset and offset-scaling
			stMap -= stmOffset;
			stMap = ((stMap - 0.5f) * to_float2_v(params->stmMultiplyer)) + 0.5f;

			//-----------------------
			//STMap Modifications
			//-----------------------

			// Flip the image
			if (params->flipU)
			{
				stMap.x = 1.0f - stMap.x;
			}
			if (params->flipV)
			{
				stMap.y = 1.0f - stMap.y;
			}

			// Apply Repeatage
			stMap -= to_float2_v(params->repeatPivot);
			stMap.x *= params->hRepeat;
			stMap.y *= params->vRepeat;
			stMap += to_float2_v(params->repeatPivot);

			// Offset
			stMap -= to_float2_v(params->offset) * to_float2(params->hRepeat, params->vRepeat);
			
			// DoD Offset
			stMap -= to_float2_v(params->imgOffset);
			stMap *= to_float2_v(params->imgMulti);

			// Correct pixel scale if Render Match is set to "Ignore (0 to Width)"
			stMap.x *= params->pixelScale[0];
			stMap.y *= params->pixelScale[1];

			// Apply shift to STmap depending on Match Render setting
			stMap -= to_float2_v(params->matchRender);

			// Generate image
			float4 fragColor = _tex2DVecN(inimg, stMap.x + halfPixel.x, stMap.y + halfPixel.y, params->compOrder);
		
			_tex2DVec4Write(outimg, x, y, fragColor);
		}
	}
]]
