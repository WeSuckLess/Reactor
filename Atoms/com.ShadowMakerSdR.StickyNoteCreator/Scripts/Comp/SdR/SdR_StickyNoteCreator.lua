--[[--
Colored Sticky Note Creator script by Sander de Regt - ShadowMaker SdR 
based on the *.setting files from Ben The Animator, 
the 'Button with Icon'-script by Andrew Hazelden ,
the Eyeon legacy script 'Switch Motion blur'
and elements from the 'Fuskin Button Toolbar GUI'-script
with inspiration from the code from the Cleanflow scripts from DC Turner and Movalex from Reactor
and of course my very own SdR Node Color Changer which was also based on the above

This script is meant to help with creating colorful Sticky Notes at the click of a button

Version 1.1 fixes sticky notes being created on top of each other
 - now they appear next to each other when adding multiple stick notes in succession
 
 For some reason the viewport does jump a bit all over the place now sometimes. 
 I still need to find a way to keep the flowview 'still' but that's for another version


## Overview ## 

This script creates 16 colorful buttons that when clicked create equally colored Sticky Notes

## Installation ## 

Step 1. Create a new "SdR" folder inside the Fusion user preferences "Scripts/Comp/" folder. 

Step 2. Create a new "IconsSdR" folder inside the "SdR" folder. 

Step 3. Copy the "SdR_StickyNoteCreator.lua" script into the "SdR" folder and all colored icon.pngs images into the "IconsSdR" folder.

Step 4. In Fusion select the "Script > SdR > SdR_StickyNoteCreator" menu item.

--]]--

local ui = fu.UIManager
local disp = bmd.UIDispatcher(ui)
local width,height = 1400,80

-- defining the colors once so I can just type in the name later

local orange = { R = 0.92156862745098, G = 0.431372549019608, B = 0 }
local apricot = { R = 1, G = 0.658823529411765, B = 0.2 }
local yellow = { R = 0.886274509803922, G = 0.662745098039216, B = 0.109803921568627 }
local lime = { R = 0.623529411764706, G = 0.776470588235294, B = 0.0823529411764706 }
local olive = { R = 0.372549019607843, G = 0.6, B = 0.125490196078431 }
local green = { R = 0.266666666666667, G = 0.56078431372549, B = 0.396078431372549 }
local teal = { R = 0, G = 0.596078431372549, B = 0.6 }
local navy = { R = 0.0823529411764706, G = 0.384313725490196, B = 0.517647058823529 }
local blue = { R = 0.474509803921569, G = 0.658823529411765, B = 0.815686274509804 }
local purple = { R = 0.6, G = 0.450980392156863, B = 0.627450980392157 }
local violet = { R = 0.584313725490196, G = 0.294117647058824, B = 0.803921568627451 }
local pink = { R = 0.913725490196078, G = 0.549019607843137, B = 0.709803921568627 }
-- since tan is a reserved word I had to change it here to tanned
local tanned = { R = 0.725490196078431, G = 0.690196078431373, B = 0.592156862745098 }
local beige = { R = 0.776470588235294, G = 0.627450980392157, B = 0.466666666666667 }
local brown = { R = 0.6, G = 0.4, B = 0 }
local chocolate = { R = 0.549019607843137, G = 0.352941176470588, B = 0.247058823529412 }

local flow = comp.CurrentFrame.FlowView


win = disp:AddWindow({
	ID = 'MyWin',
	WindowTitle = 'SdR Sticky Note Creator',
	-- Geometry defines where on the screen the floating window is created
	Geometry = {500, 48, width, height},
	Spacing = 0,
	Margin = 0,
	Weight = 0,

	 ui:VGroup{
		ID = 'root',
		Weight = 0,

		-- Add your GUI elements here:
		ui:VGap(5),
		
		ui:HGroup{
			-- Add a row of 16 buttons with color swatches
			ui:Button{
				ID = 'ColorSwatch1', 
				Flat = true,
				IconSize = {48,48},
				Icon = ui:Icon{
					File = 'Scripts:/Comp/SdR/IconsSdR/1_SNOrange.png',
				},
			},
			ui:Button{
				ID = 'ColorSwatch2', 
				Flat = true,
				IconSize = {48,48},
				Icon = ui:Icon{
					File = 'Scripts:/Comp/SdR/IconsSdR/2_SNApricot.png',
				},
			},
			ui:Button{
				ID = 'ColorSwatch3', 
				Flat = true,
				IconSize = {48,48},
				Icon = ui:Icon{
					File = 'Scripts:/Comp/SdR/IconsSdR/3_SNYellow.png',
				},
			},
			ui:Button{
				ID = 'ColorSwatch4', 
				Flat = true,
				IconSize = {48,48},
				Icon = ui:Icon{
					File = 'Scripts:/Comp/SdR/IconsSdR/4_SNLime.png',
				},
			},
			ui:Button{
				ID = 'ColorSwatch5', 
				Flat = true,
				IconSize = {48,48},
				Icon = ui:Icon{
					File = 'Scripts:/Comp/SdR/IconsSdR/5_SNOlive.png',
				},
			},
			ui:Button{
				ID = 'ColorSwatch6', 
				Flat = true,
				IconSize = {48,48},
				Icon = ui:Icon{
					File = 'Scripts:/Comp/SdR/IconsSdR/6_SNGreen.png',
				},
			},
			ui:Button{
				ID = 'ColorSwatch7', 
				Flat = true,
				IconSize = {48,48},
				Icon = ui:Icon{
					File = 'Scripts:/Comp/SdR/IconsSdR/7_SNTeal.png',
				},
			},
			ui:Button{
				ID = 'ColorSwatch8', 
				Flat = true,
				IconSize = {48,48},
				Icon = ui:Icon{
					File = 'Scripts:/Comp/SdR/IconsSdR/8_SNNavy.png',
				},
			},
			ui:Button{
				ID = 'ColorSwatch9', 
				Flat = true,
				IconSize = {48,48},
				Icon = ui:Icon{
					File = 'Scripts:/Comp/SdR/IconsSdR/9_SNBlue.png',
				},
			},
			ui:Button{
				ID = 'ColorSwatch10', 
				Flat = true,
				IconSize = {48,48},
				Icon = ui:Icon{
					File = 'Scripts:/Comp/SdR/IconsSdR/10_SNPurple.png',
				},
			},
			ui:Button{
				ID = 'ColorSwatch11', 
				Flat = true,
				IconSize = {48,48},
				Icon = ui:Icon{
					File = 'Scripts:/Comp/SdR/IconsSdR/11_SNViolet.png',
				},
			},
			ui:Button{
				ID = 'ColorSwatch12', 
				Flat = true,
				IconSize = {48,48},
				Icon = ui:Icon{
					File = 'Scripts:/Comp/SdR/IconsSdR/12_SNPink.png',
				},
			},
			ui:Button{
				ID = 'ColorSwatch13', 
				Flat = true,
				IconSize = {48,48},
				Icon = ui:Icon{
					File = 'Scripts:/Comp/SdR/IconsSdR/13_SNTan.png',
				},
			},
			ui:Button{
				ID = 'ColorSwatch14', 
				Flat = true,
				IconSize = {48,48},
				Icon = ui:Icon{
					File = 'Scripts:/Comp/SdR/IconsSdR/14_SNBeige.png',
				},
			},
			ui:Button{
				ID = 'ColorSwatch15', 
				Flat = true,
				IconSize = {48,48},
				Icon = ui:Icon{
					File = 'Scripts:/Comp/SdR/IconsSdR/15_SNBrown.png',
				},
			},
			ui:Button{
				ID = 'ColorSwatch16', 
				Flat = true,
				IconSize = {48,48},
				Icon = ui:Icon{
					File = 'Scripts:/Comp/SdR/IconsSdR/16_SNChocolate.png',
				},
			},
		},
		ui:HGroup{	
			ui:Label{
			ID = 'Instructions',
			Text = 'Click the swatches to add a Sticky Note in that color',
			Alignment = {AlignHCenter = true},
			},
		},	
	},
})

-- The window was closed
function win.On.MyWin.Close(ev)
	disp:ExitLoop()
end

-- Add your GUI element based event functions here:
itm = win:GetItems()

-- so here's the thing: the only way (I've found so far) to add a tool and have it be the Active Tool after adding is by using
-- (-32768, -32768) as positions in the addtool command. 
-- If you use variables or values like (0, 10) the added tool will be neither selected nor active, so much more difficult to work with
-- so I had to work around it and I think I found it

function win.On.ColorSwatch1.Clicked(ev)

	local tool = comp.ActiveTool 						-- first I check if there's an Active Tool
	posx, posy = flow:GetPos(tool) 						-- this gets the position of that tool (if there is one)
		if tool == nil then 							-- but if there is no active tool, there is no way to tell Fusion where to put the tool										
				comp:AddTool("Note", -32768, -32768) 	-- so this adds the tool where the last mouseclick was
														-- which is nice if there already tools in the comp otherwise
														-- if there is no tool at all in the comp (unlikely but possible)
														-- the tool gets added at the actual (-32768,-32768) position
			local addedtool = comp.ActiveTool
				flow:SetPos(tool, 0, 0)					-- to prevent this, I manually move the tool at 0,0 in this step
				addedtool.TileColor = orange			-- and change its color
			else
				newposx = posx + 1						-- if there *is* an active tool, then posx and posy have actual values (not just nil)
														-- and can be used in calculations (this moves the value of the posx 1 up)
				comp:AddTool("Note", -32768, -32768)	-- so when its added it won't be added on top of a previous note
				local addedtool = comp.ActiveTool
				flow:SetPos(addedtool, newposx, posy) 	-- but moved over an x value of 1 in the flowview - preventing 90+% of overlapping creations
				addedtool.TileColor = orange
	end

end

function win.On.ColorSwatch2.Clicked(ev)

	local tool = comp.ActiveTool
	posx, posy = flow:GetPos(tool)
		if tool == nil then
				comp:AddTool("Note", -32768, -32768)
			local addedtool = comp.ActiveTool
				flow:SetPos(tool, 0, 0)
				addedtool.TileColor = apricot
			else
				newposx = posx + 1
				comp:AddTool("Note", -32768, -32768)
				local addedtool = comp.ActiveTool
				flow:SetPos(addedtool, newposx, posy)
				addedtool.TileColor = apricot
			end
end

function win.On.ColorSwatch3.Clicked(ev)

	local tool = comp.ActiveTool
	posx, posy = flow:GetPos(tool)
		if tool == nil then
				comp:AddTool("Note", -32768, -32768)
			local addedtool = comp.ActiveTool
				flow:SetPos(tool, 0, 0)
				addedtool.TileColor = yellow
			else
				newposx = posx + 1
				comp:AddTool("Note", -32768, -32768)
				local addedtool = comp.ActiveTool
				flow:SetPos(addedtool, newposx, posy)
				addedtool.TileColor = yellow
	end

end

function win.On.ColorSwatch4.Clicked(ev)

	local tool = comp.ActiveTool
	posx, posy = flow:GetPos(tool)
		if tool == nil then
				comp:AddTool("Note", -32768, -32768)
			local addedtool = comp.ActiveTool
				flow:SetPos(tool, 0, 0)
				addedtool.TileColor = lime
			else
				newposx = posx + 1
				comp:AddTool("Note", -32768, -32768)
				local addedtool = comp.ActiveTool
				flow:SetPos(addedtool, newposx, posy)
				addedtool.TileColor = lime
	end

end

function win.On.ColorSwatch5.Clicked(ev)

	local tool = comp.ActiveTool
	posx, posy = flow:GetPos(tool)
		if tool == nil then
				comp:AddTool("Note", -32768, -32768)
			local addedtool = comp.ActiveTool
				flow:SetPos(tool, 0, 0)
				addedtool.TileColor = olive
			else
				newposx = posx + 1
				comp:AddTool("Note", -32768, -32768)
				local addedtool = comp.ActiveTool
				flow:SetPos(addedtool, newposx, posy)
				addedtool.TileColor = olive
	end

end

function win.On.ColorSwatch6.Clicked(ev)

	local tool = comp.ActiveTool
	posx, posy = flow:GetPos(tool)
		if tool == nil then
				comp:AddTool("Note", -32768, -32768)
			local addedtool = comp.ActiveTool
				flow:SetPos(tool, 0, 0)
				addedtool.TileColor = green
			else
				newposx = posx + 1
				comp:AddTool("Note", -32768, -32768)
				local addedtool = comp.ActiveTool
				flow:SetPos(addedtool, newposx, posy)
				addedtool.TileColor = green
	end
	
end

function win.On.ColorSwatch7.Clicked(ev)

	local tool = comp.ActiveTool
	posx, posy = flow:GetPos(tool)
		if tool == nil then
				comp:AddTool("Note", -32768, -32768)
			local addedtool = comp.ActiveTool
				flow:SetPos(tool, 0, 0)
				addedtool.TileColor = teal
			else
				newposx = posx + 1
				comp:AddTool("Note", -32768, -32768)
				local addedtool = comp.ActiveTool
				flow:SetPos(addedtool, newposx, posy)
				addedtool.TileColor = teal
	end
	
end

function win.On.ColorSwatch8.Clicked(ev)

	local tool = comp.ActiveTool
	posx, posy = flow:GetPos(tool)
		if tool == nil then
				comp:AddTool("Note", -32768, -32768)
			local addedtool = comp.ActiveTool
				flow:SetPos(tool, 0, 0)
				addedtool.TileColor = navy
			else
				newposx = posx + 1
				comp:AddTool("Note", -32768, -32768)
				local addedtool = comp.ActiveTool
				flow:SetPos(addedtool, newposx, posy)
				addedtool.TileColor = navy
	end

end

function win.On.ColorSwatch9.Clicked(ev)

	local tool = comp.ActiveTool
	posx, posy = flow:GetPos(tool)
		if tool == nil then
				comp:AddTool("Note", -32768, -32768)
			local addedtool = comp.ActiveTool
				flow:SetPos(tool, 0, 0)
				addedtool.TileColor = blue
			else
				newposx = posx + 1
				comp:AddTool("Note", -32768, -32768)
				local addedtool = comp.ActiveTool
				flow:SetPos(addedtool, newposx, posy)
				addedtool.TileColor = blue
	end
	
end

function win.On.ColorSwatch10.Clicked(ev)

	local tool = comp.ActiveTool
	posx, posy = flow:GetPos(tool)
		if tool == nil then
				comp:AddTool("Note", -32768, -32768)
			local addedtool = comp.ActiveTool
				flow:SetPos(tool, 0, 0)
				addedtool.TileColor = purple
			else
				newposx = posx + 1
				comp:AddTool("Note", -32768, -32768)
				local addedtool = comp.ActiveTool
				flow:SetPos(addedtool, newposx, posy)
				addedtool.TileColor = purple
	end

end

function win.On.ColorSwatch11.Clicked(ev)

	local tool = comp.ActiveTool
	posx, posy = flow:GetPos(tool)
		if tool == nil then
				comp:AddTool("Note", -32768, -32768)
			local addedtool = comp.ActiveTool
				flow:SetPos(tool, 0, 0)
				addedtool.TileColor = violet
			else
				newposx = posx + 1
				comp:AddTool("Note", -32768, -32768)
				local addedtool = comp.ActiveTool
				flow:SetPos(addedtool, newposx, posy)
				addedtool.TileColor = violet
	end

end

function win.On.ColorSwatch12.Clicked(ev)

	local tool = comp.ActiveTool
	posx, posy = flow:GetPos(tool)
		if tool == nil then
				comp:AddTool("Note", -32768, -32768)
			local addedtool = comp.ActiveTool
				flow:SetPos(tool, 0, 0)
				addedtool.TileColor = pink
			else
				newposx = posx + 1
				comp:AddTool("Note", -32768, -32768)
				local addedtool = comp.ActiveTool
				flow:SetPos(addedtool, newposx, posy)
				addedtool.TileColor = pink
	end

end

function win.On.ColorSwatch13.Clicked(ev)

	local tool = comp.ActiveTool
	posx, posy = flow:GetPos(tool)
		if tool == nil then
				comp:AddTool("Note", -32768, -32768)
			local addedtool = comp.ActiveTool
				flow:SetPos(tool, 0, 0)
				addedtool.TileColor = tanned
			else
				newposx = posx + 1
				comp:AddTool("Note", -32768, -32768)
				local addedtool = comp.ActiveTool
				flow:SetPos(addedtool, newposx, posy)
				addedtool.TileColor = tanned
	end

end

function win.On.ColorSwatch14.Clicked(ev)

	local tool = comp.ActiveTool
	posx, posy = flow:GetPos(tool)
		if tool == nil then
				comp:AddTool("Note", -32768, -32768)
			local addedtool = comp.ActiveTool
				flow:SetPos(tool, 0, 0)
				addedtool.TileColor = beige
			else
				newposx = posx + 1
				comp:AddTool("Note", -32768, -32768)
				local addedtool = comp.ActiveTool
				flow:SetPos(addedtool, newposx, posy)
				addedtool.TileColor = beige
	end

end

function win.On.ColorSwatch15.Clicked(ev)

	local tool = comp.ActiveTool
	posx, posy = flow:GetPos(tool)
		if tool == nil then
				comp:AddTool("Note", -32768, -32768)
			local addedtool = comp.ActiveTool
				flow:SetPos(tool, 0, 0)
				addedtool.TileColor = brown
			else
				newposx = posx + 1
				comp:AddTool("Note", -32768, -32768)
				local addedtool = comp.ActiveTool
				flow:SetPos(addedtool, newposx, posy)
				addedtool.TileColor = brown
	end

end

function win.On.ColorSwatch16.Clicked(ev)

	local tool = comp.ActiveTool
	posx, posy = flow:GetPos(tool)
		if tool == nil then
				comp:AddTool("Note", -32768, -32768)
			local addedtool = comp.ActiveTool
				flow:SetPos(tool, 0, 0)
				addedtool.TileColor = chocolate
			else
				newposx = posx + 1
				comp:AddTool("Note", -32768, -32768)
				local addedtool = comp.ActiveTool
				flow:SetPos(addedtool, newposx, posy)
				addedtool.TileColor = chocolate
	end
	
end		
					


win:Show()
disp:RunLoop()
win:Hide()
