--[[--
GroupValidation.lua - v1.0
A module for validating Pipe groups and managing UI visibility
--]]--

local GroupValidation = {}

-- Validate group data structure
function GroupValidation.validate(groupData)
    if not groupData then return false end
    if type(groupData) ~= "table" then return false end
    if not groupData.name then return false end
    if not groupData.pipes or type(groupData.pipes) ~= "table" then return false end
    
    -- Validate each pipe in the group
    for _, pipe in ipairs(groupData.pipes) do
        if type(pipe) ~= "table" then return false end
        if not pipe.toolID then return false end
        if not pipe.pipeName then return false end
    end
    
    return true
end


-- Validate and update group display
function GroupValidation.updateGroupDisplay(tool, groupData)
    if not tool or not groupData then return end
    
    -- Update group name display if available
    if groupData.name then
        tool.GroupInput:SetSource(Text(groupData.name), 0)
    end
    
    -- Update pipes list
    if groupData.pipes then
        local attrs = {}
        local uniquePipes = {}
        
        for _, pipeInfo in ipairs(groupData.pipes) do
            local displayName = pipeInfo.pipeName:match("Pipe_(.+)") or pipeInfo.pipeName
            if not uniquePipes[displayName] then
                table.insert(attrs, { CCS_AddString = displayName })
                uniquePipes[displayName] = true
            end
        end
        
        tool.InPipes:SetAttrs(attrs)
    end
end

-- Get all pipe groups in the composition
function GroupValidation.getAllPipeGroups(comp)
    if not comp then return {} end
    
    local groups = {}
    local allPipes = comp:GetToolList(false, "Fuse.Pipe")
    
    for _, pipe in ipairs(allPipes) do
        local groupData = pipe:GetData("GroupData")
        if GroupValidation.validate(groupData) then
            if not groups[groupData.name] then
                groups[groupData.name] = {
                    name = groupData.name,
                    pipes = groupData.pipes,
                    rootPath = nil,
                    hasVersioning = false,
                    sample = pipe
                }
            end
        end
    end
    
    return groups
end

return GroupValidation 