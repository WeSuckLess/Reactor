# Xpress It

Version: 0.0009  
By JustCropIt  

## Overview

![GIF](Images/xpressit-A.gif)

This macro is "simply" two text fields (one "small" and one "big") set up for the simple purpose of testing out expressions in. That's it.

Well, practically there's 4 text fields in total but two of those are for optional labeling/commenting. And that's it.

![UI](Images/XpressIt-UI-A.png)

...Well... technically there's 6 text fields but two are hidden (and that really is it) but that's surely only of interest to anyone invested in macro creation hijinks.And if that someone is you then there's certainly (IMO) a fair bit of sneaky macro hijinks stuff going on in this simple macro which I'll ramble on more about in the WSL link below :)

![UI](Images/XpressIt-UI-B.png)

## What's what

Unfold the "B" thing for a monstrous 16 line text box. That's it. Easy peasy.

In the default layout (with the "B nest" closed) the macro takes up pretty much exactly the same amount of Inspector space as a Text+ node that only had the text and expression field. Nice.

## For More Info

Check out the Xpress It thread on the WSL forums:  
[https://www.steakunderwater.com/wesuckless/viewtopic.php?p=50134#p50134](https://www.steakunderwater.com/wesuckless/viewtopic.php?p=50134#p50134)