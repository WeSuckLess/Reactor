--[[--
Spatial Metadata.lua - 2024-10-01 04.15 AM

This is a Resolve based Kartaverse "Media Command" script. The script allows the selected footage that is sourced from a Resolve Media Pool to be automatically encoded as an MV-HEVC video with the required spatial metadata tags injected.

This workflow is useful for immersive media content creators as it allows the SBS or OU formatted stereo 3D video content to play on an Apple Vision Pro HMD.

Note: Pay attention to the Resolve Deliver page encoding settings used for the color profile when you export your stereo 3D video content. The color profile information will determine if your footage can be processed correctly by the Spatial CLI program. If this topic is confusing, don't worry, the Hugh Hou YouTube video tutorials on Apple Vision Pro spatial content creation will cover this topic in more detail. :)


Kartaverse "Media Command" Script Docs:
https://kartaverse.github.io/Kartaverse-Docs/#/mediacommand


Acknowledgements:
Mike Swanson's freeware "Spatial" CLI program is used to power this Lua script. Check out the online documentation for the Spatial tool:

https://blog.mikeswanson.com/spatial_docs/#make
https://blog.mikeswanson.com/spatial_docs/#projection-types

Fraunhofer MV-HEVC:
http://www.hevc.info/mvhevc



Spatial Metadata Script Usage:
This Lua script is accessed from inside the Kartaverse "Media Command" scripting toolset.

1. Switch to the Resolve Media page. Then select the "Workspace > Scripts > Edit > Kartaverse > KartaLink > Media Command" menu item.

2. A "Media Command" window will appear that allows you to select footage from your currently open Media pool "bin" folder. To select an item in the Media Command window you can click on the row of text that describes the individual footage item. If you want to select footage from a different Resolve bin folder, change the active bin in the Media page. Then press the "Refresh" button at the top right corner of the Media Command window.

Note: When using the Media Command window, avoid clicking on the little "X" checkbox on a row as it is used for display purposes only to show the clip selection stage. (The checkbox is not an active control and has no effect if clicked on by the end user.)

3. At the bottom of the Media Command window is a popup menu that lets you select a script you would like to run. Choose the script that has a filename ending in ".../Metadata/Spatial Metadata.lua". Then click the "Go" button to process the selected footage using the Spatial Metadata script.

You can see a textual log output that lists the results of spatial video encoding task by opening the Console window using the "Console" button in the Media Command window, or by selecting the "Workspace > Console" menu item.

4. If the Spatial MV-HEVC video encoding process worked successfully, you should have a newly optimized video file, located next to your source movie file that has a filename ending in "_injected.mov. This is the spatial video encoded content you can playback on an Apple Vision Pro HMD or a Meta Quest HMD.



Spatial Metadata Script Installation:
1. Install the Reactor Package manager for Resolve/Fusion:
https://kartaverse.github.io/Reactor-Docs/#/reactor

2. Open the Reactor package manager for Resolve/Fusion. Click on the category on the left sidebar named "Kartaverse/KartaLink/Scripts/". Then select the "KartaLink | Spatial Metadata" package in the atoms list and click the "Install" button.

3. The "Spatial Metadata" script relies on the (freeware) Spatial CLI program. The Spatial CLI software is installed using the macOS based homebrew package manager:

# Install Homebrew using the macOS terminal:
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

# Install the Spatial CLI app using the brew program in the macOS terminal:
brew install spatial

# The Spatial CLI app now lives on your hard disk at:
/opt/homebrew/bin/spatial

# If you would like to see a quick summary of the Spatial CLI command line flags you can use in the macOS terminal:
/opt/homebrew/bin/spatial

OVERVIEW: A tool to process MV-HEVC spatial photo and video files.

This tool exports from spatial photo/video files to specified formats and makes spatial photo/video
files from formatted inputs. It can also combine existing audio and video files, report spatial
information in a photo/video, and edit spatial video metadata.

Questions and comments: https://blog.mikeswanson.com/contact

USAGE: spatial [--args <args>] <subcommand>

OPTIONS:
  --args <args>           Arguments file.
  --version               Show the version.
  -h, --help              Show help information.

SUBCOMMANDS:
  info                    Show spatial information for an input file.
  export                  Export formatted output from a spatial input file.
  make                    Make spatial output from a formatted input file.
  combine                 Combine/mux multiple inputs into a single video output.
  metadata                Report and modify spatial video metadata.

  See 'spatial help <subcommand>' for detailed help.



Lua Script Sample Output:
[Spatial Metadata]
"/opt/homebrew/bin/spatial" make --input  "$HOME/Desktop/180VR-SBS.mp4" --format sbs --output "$HOME/Desktop/Spatial.mov" --cdist 64 --hfov  180 --hadjust 0 --primary left --projection halfEquirect
 Input: /Users/vfx/Desktop/180VR-SBS.mp4 (sideBySide)
Output: /Users/vfx/Desktop/Spatial.mov (spatial/mv-hevc)
frame=       1 fps=  3.0 size=       0 B time=00:00:00 bitrate=       0 bps speed=  0.3x progress=  2.1%
frame=      13 fps= 23.6 size=   466.7KB time=00:00:00 bitrate=     6.9Mbps speed=  0.8x progress= 27.1%
frame=      28 fps= 29.1 size=     1.0MB time=00:00:01 bitrate=     7.1Mbps speed=  1.0x progress= 58.3%
frame=      37 fps= 16.5 size=     1.4MB time=00:00:01 bitrate=     7.5Mbps speed=  0.9x progress= 77.1%
frame=      47 fps= 16.9 size=     1.9MB time=00:00:01 bitrate=     7.8Mbps speed=  0.9x progress= 97.9%
frame=      47 fps=  0.0 size=     1.9MB time=00:00:02 bitrate=     7.6Mbps speed=  0.9x progress=100.0%

Time: 3.40s

--]]--

-- -------------------------------------------------
-- -------------------------------------------------

-- Spatial program filepath
local spatialProgram = [[/opt/homebrew/bin/spatial]]

-- -------------------------------------------------
-- -------------------------------------------------

-- Find out the current operating system platform. The platform local variable should be set to either "Windows", "Mac", or "Linux".
local platform = (FuPLATFORM_WINDOWS and "Windows") or (FuPLATFORM_MAC and "Mac") or (FuPLATFORM_LINUX and "Linux")

-- -------------------------------------------------
-- -------------------------------------------------
-- parseSequenceFilename()
-- 
-- Process Resolve Media Pool/Media page centric file paths like:
-- D:\Media\Image.[0001-0240].exr
-- D:\Media\Image.0001.exr
-- D:\Media\Image_0001.exr
-- D:\Media\Image.exr
-- D:\Media\Image.mp4
--
-- Code adapted from bmd.scriptlib function "bmd.parseFilename(filename)"
--
-- This is a great function for ripping a filepath into little bits
-- returns a table with the following
--
-- FullPath     : The raw, original path sent to the function
-- Path         : The path, without filename
-- FullName     : The name of the clip w\ extension
-- Name         : The name without extension
-- CleanName    : The name of the clip, without extension or sequence
-- SNumStart    : The original starting frame sequence string, or "" if no sequence
-- SNumEnd      : The original ending frame sequence string, or "" if no sequence
-- FrameStart   : The starting frame sequence as a numeric value, or nil if no sequence
-- FrameEnd     : The ending frame sequence as a numeric value, or nil if no sequence
-- Extension    : The raw extension of the clip
-- Padding      : Amount of padding in the sequence, or nil if no sequence
-- UN           : A true or false value indicating whether the path is a UNC path or not
------------------------------------------------------------------------------
function parseSequenceFilename(filename)
	local seq = {}
	seq.FullPath = filename
	string.gsub(seq.FullPath, "^(.+[/\\])(.+)", function(path, name) seq.Path = path;seq.FullName = name end)
	--string.gsub(seq.FullName, "^(.+)(%..+)%[%..+%]$", function(name, ext) seq.Name = name;seq.Extension = ext end)
	string.gsub(seq.FullName, "^(.+)(%..+)$", function(name, ext) seq.Name = name;seq.Extension = ext end)

	if not seq.Name then -- no extension?
		seq.Name = seq.FullName
	end

	if string.match(seq.Name, "%[.-%-.-%]$") then
		-- Match Video/Video + Audio clip types with image sequence filenames like: "/Path/Name.[####-####].ext"
		string.gsub(seq.Name, "^(.-)%[(%d+)%-(%d+)%]$", function(name, SNumStart, SNumEnd) seq.CleanName = name;seq.SNumStart = SNumStart;seq.SNumEnd = SNumEnd end)
	else
		-- Match Stills/Video/Video + Audio/Geometry/Subtitles clip types with filenames like: "/Path/Name.####.ext" or "/Path/Name.ext"
		string.gsub(seq.Name, "^(.-)(%d+)$", function(name, SNum) seq.CleanName = name;seq.SNumStart = SNum;seq.SNumEnd = SNum end)
	end

	if seq.SNumStart then
		seq.FrameStart = tonumber(seq.SNumStart)
		seq.Padding = string.len(seq.SNumStart)
	else
		seq.SNumStart = ""
		seq.CleanName = seq.Name
	end

	if seq.SNumEnd then
		seq.FrameEnd = tonumber(seq.SNumEnd)
	else
		seq.SNumEnd = ""
	end

	if seq.Extension == nil then seq.Extension = "" end
	seq.UNC = (string.sub(seq.Path, 1, 2) == [[\\]])

	return seq
end


----------------------------------------------------
-- Read a fusion specific preference value. If nothing exists set and return a default value
-- Example: sort = getPreferenceData("KartaLink.SpatialMedia.sort", 1, true)
function getPreferenceData(pref, defaultValue, debugPrint)
	-- Choose if you are saving the preference to the comp or to all of fusion
	-- local newPreference = comp:GetData(pref)
	local newPreference = fu:GetData(pref)

	if newPreference ~= nil then
		-- List the existing preference value
		if (debugPrint == true) or (debugPrint == 1) then
			if newPreference == nil then
				print("[Reading " .. tostring(pref) .. " Preference Data] " .. "nil")
			else
				print("[Reading " .. tostring(pref) .. " Preference Data] " .. tostring(newPreference))
			end
		end
	else
		-- Force a default value into the preference & then list it
		newPreference = defaultValue

		-- Choose if you are saving the preference to the comp or to all of fusion
		-- comp:SetData(pref, defaultValue)
		fu:SetData(pref, defaultValue)

		if (debugPrint == true) or (debugPrint == 1) then
			if newPreference == nil then
				print("[Creating " .. tostring(pref) .. " Preference Data] " .. "nil")
			else
				print("[Creating ".. tostring(pref) .. " Preference Entry] " .. tostring(newPreference))
			end
		end
	end

	return newPreference
end


-------------------------------------------------------------------------------
-- Set a fusion specific preference value
-- Example: setPreferenceData("KartaLink.SpatialMedia.sort", 1, true)
function setPreferenceData(pref, value, debugPrint)
	-- Choose if you are saving the preference to the comp or to all of fusion
	-- comp:SetData(pref, value)
	fu:SetData(pref, value)

	-- List the preference value
	if (debugPrint == true) or (debugPrint == 1) then
		if value == nil then
			print("[Setting " .. tostring(pref) .. " Preference Data] " .. "nil")
		else
			print("[Setting " .. tostring(pref) .. " Preference Data] " .. tostring(value))
		end
	end
end

function GenerateMediaList()
	local mediaTbl = {}

	local tbl = bmd.readstring(args)
	for clipIndex, clipValue in ipairs(tbl) do
		if clipValue["Type"] == "Video" or clipValue["Type"] == "Video + Audio" then
			local path = clipValue["File Path"]
			if path ~= nil and path ~= "" then
				table.insert(mediaTbl, path)
			end
		end
	end

	return mediaTbl
end


function UserSettings()
	local prefsTbl = {}

	local ui = fu.UIManager
	local disp = bmd.UIDispatcher(ui)
	local width, height = 415, 430
	local win = ui.FindWindow("settingsWin")
	if win then
		-- Restore an already open window
		print("[Status] Restoring an existing settings window")
		win.Show()
		win.Raise()
		win.ActivateWindow()
	else
		-- Create a new window
		win = disp:AddWindow({
			ID = "settingsWin",
			TargetID = "settingsWin",
			WindowTitle = "Spatial Metadata",
			Geometry = {0, 30, width, height},
			Spacing = 0,

			ui:VGroup{
				ID = "root",
				-- Add your GUI elements here:
				ui:Button{
					ID = "KartaVRIconButton",
					Weight = 0,
					IconSize = {200,94},
					Icon = ui:Icon{
						File = "Scripts:/Support/Kartaverse/KartaVR_Logo.png"
					},
					MinimumSize = {
						200,
						94,
					},
					Flat = true,
				},
				ui:TextEdit{
					Weight = 1.0,
					ID = "InfoTxt",
					Text = "This script runs a new MV-HEVC spatial video encoding session. The currently selected movies from the Media Command window will be auto-loaded into the Spatial CLI program.",
					StyleSheet = "QTextEdit { border: 0px; }",
					ReadOnly = true,
				},
				ui:VGap(),
				ui:HGroup{
					Weight = 0.01,
					ui:Label{
						Weight = 0.01,
						ID = "ImageProjectionLabel",
						Text = "Image Projection: ",
					},
					ui:ComboBox{
						Weight = 0.01,
						ID = "ImageProjectionCombo",
					},
				},
				ui:HGroup{
					Weight = 0.01,
					ui:Label{
						Weight = 0.01,
						ID = "FormatLabel",
						Text = "Stereo 3D Format: ",
					},
					ui:ComboBox{
						Weight = 0.01,
						ID = "StereoFormatCombo",
					},
				},
				ui:HGroup{
					Weight = 0.01,
					ui:Label{
						Weight = 0.01,
						ID = "PrimaryEyeLabel",
						Text = "Primary Eye: ",
					},
					ui:ComboBox{
						Weight = 0.01,
						ID = "PrimaryEyeCombo",
					},
				},
				ui:HGroup{
					Weight = 0.01,
					ui:Label{
						Weight = 0.01,
						ID = "HFOVLabel",
						Text = "Horizontal Field of View (degrees): ",
					},
					ui:SpinBox{
						Weight = 0.01,
						ID = "HFOVSpinner",
						Value = getPreferenceData("KartaLink.SpatialMedia.HFOV", 180, 0) or 180,
						Minimum = 0,
						Maximum = 360,
					},
				},
				ui:HGroup{
					Weight = 0.01,
					ui:Label{
						Weight = 0.01,
						ID = "InteraxialLensSeparationLabel",
						Text = "Interaxial Lens Separation (mm): ",
					},
					ui:DoubleSpinBox{
						Weight = 0.01,
						ID = "InteraxialLensSeparationSpinner",
						Value = getPreferenceData("KartaLink.SpatialMedia.InteraxialLensSeparation", 60.0, 0) or 60.0,
						Minimum = 0.0,
						Maximum = 6400.0,
					},
				},
				ui:HGroup{
					Weight = 0.01,
					ui:Label{
						Weight = 0.01,
						ID = "HAdjustLabel",
						Text = "Horizontal Disparity Adjustment (-1.0 to 1.0): ",
					},
					ui:DoubleSpinBox{
						Weight = 0.01,
						ID = "HAdjustSpinner",
						Value = getPreferenceData("KartaLink.SpatialMedia.HAdjust", 0, 0) or 0.0,
						Minimum = -1.0,
						Maximum = 1.0,
					},
				},
				ui:CheckBox{
					ID = "OpenFolderCheckbox",
					Text = "Show the Output Folder",
					Checked = getPreferenceData("KartaLink.SpatialMedia.OpenFolder", 0, 0) or false,
				},
				ui:HGroup{
					Weight = 0.01,
					ui:Button{
						Weight = 0.01,
						ID = "CancelButton",
						Text = "Cancel",
						MinimumSize = {60, 24},
					},
					ui:Button{
						Weight = 0.01,
						ID = "ResetButton",
						Text = "Reset Defaults",
						MinimumSize = {60, 24},
					},
					-- OK Button
					ui:Button{
						Weight = 0.01,
						ID = "OKButton",
						Text = "OK",
						MinimumSize = {40, 24},
					},
				},
			},
		})

		-- The window was closed
		function win.On.settingsWin.Close(ev)
			prefsTbl.Cancel = true
			disp:ExitLoop()
		end

		-- Add your GUI element based event functions here:
		itm = win:GetItems()

		-- Add the items to the ComboBox menus
		itm.ImageProjectionCombo:AddItem("180VR")
		itm.ImageProjectionCombo:AddItem("360VR")
		itm.ImageProjectionCombo:AddItem("Fisheye")
		itm.ImageProjectionCombo:AddItem("Flat")
		itm.ImageProjectionCombo.CurrentText = getPreferenceData("KartaLink.SpatialMedia.ImageProjection", "180VR", 0)

		itm.StereoFormatCombo:AddItem("Side By Side")
		itm.StereoFormatCombo:AddItem("Over Under")
		itm.StereoFormatCombo.CurrentText = getPreferenceData("KartaLink.SpatialMedia.StereoFormat", "Side By Side", 0)

		itm.PrimaryEyeCombo:AddItem("Left")
		itm.PrimaryEyeCombo:AddItem("Right")
		itm.PrimaryEyeCombo.CurrentText = getPreferenceData("KartaLink.SpatialMedia.PrimaryEye", "Left", 0)

		-- Force default values into the SpinBox controls
		itm.InteraxialLensSeparationSpinner.Value = getPreferenceData("KartaLink.SpatialMedia.InteraxialLensSeparation", 60.0, 0)
		itm.HFOVSpinner.Value = getPreferenceData("KartaLink.SpatialMedia.HFOV", 180, 0)

		-- Cancel button was pressed
		function win.On.CancelButton.Clicked(ev)
			prefsTbl.Cancel = true
			disp:ExitLoop()
		end
	
		-- Reset the prefs
		function win.On.ResetButton.Clicked(ev)
			setPreferenceData("KartaLink.SpatialMedia.ImageProjection", "180VR", false)
			setPreferenceData("KartaLink.SpatialMedia.StereoFormat",  "Side By Side", false)
			setPreferenceData("KartaLink.SpatialMedia.PrimaryEye", "Left", false)
			setPreferenceData("KartaLink.SpatialMedia.InteraxialLensSeparation", 60.0, false)
			setPreferenceData("KartaLink.SpatialMedia.HFOV", 180, false)
			setPreferenceData("KartaLink.SpatialMedia.HAdjust", 0, false)
			setPreferenceData("KartaLink.SpatialMedia.OpenFolder", 0, false)

			itm.ImageProjectionCombo.CurrentText = getPreferenceData("KartaLink.SpatialMedia.ImageProjection", "180VR", 0)
			itm.StereoFormatCombo.CurrentText = getPreferenceData("KartaLink.SpatialMedia.StereoFormat", "Side By Side", 0)
			itm.PrimaryEyeCombo.CurrentText = getPreferenceData("KartaLink.SpatialMedia.PrimaryEye", "Left", 0)
			itm.InteraxialLensSeparationSpinner.Value = getPreferenceData("KartaLink.SpatialMedia.InteraxialLensSeparation", 60.0, 0)
			itm.HFOVSpinner.Value = getPreferenceData("KartaLink.SpatialMedia.HFOV", 180, 0)
			itm.HAdjustSpinner.Value = getPreferenceData("KartaLink.SpatialMedia.HAdjust", 0, 0)
			itm.OpenFolderCheckbox.Checked = getPreferenceData("KartaLink.SpatialMedia.OpenFolder", 0, 0)
		end

		-- The "OK" button was pressed
		function win.On.OKButton.Clicked(ev)
			disp:ExitLoop()
		end
	end
	-- The app:AddConfig() command that will capture the "Escape", "Control + W" or "Control + F4" hotkeys so they will close the window instead of closing the foreground composite.
	app:AddConfig("settingsWin", {
		Target {
			ID = "settingsWin",
		},

		Hotkeys {
			Target = "settingsWin",
			Defaults = true,

			CONTROL_W = "Execute{cmd = [[app.UIManager:QueueEvent(obj, 'Close', {})]]}",
			CONTROL_F4 = "Execute{cmd = [[app.UIManager:QueueEvent(obj, 'Close', {})]]}",
			ESCAPE = "Execute{cmd = [[app.UIManager:QueueEvent(obj, 'Close', {})]]}",
		},
	})

	win:Show()
	disp:RunLoop()
	win:Hide()

	-- Double Spin Boxes
	-- Interaxial Lens Separation
	prefsTbl.InteraxialLensSeparation = itm.InteraxialLensSeparationSpinner.Value

	-- Horizontal Disparity Adjustment
	prefsTbl.HAdjust = itm.HAdjustSpinner.Value


	-- Integer Spin Boxes
	-- Horizontal Field of View
	prefsTbl.HFOV = itm.HFOVSpinner.Value


	-- Combo Menus
	-- Image Projection
	if itm.ImageProjectionCombo.CurrentText == "" then
		-- Fallback 1
		prefsTbl.ImageProjection = "halfEquirect"
	elseif itm.ImageProjectionCombo.CurrentText == "180VR" then
		prefsTbl.ImageProjection = "halfEquirect"
	elseif itm.ImageProjectionCombo.CurrentText == "360VR" then
		prefsTbl.ImageProjection = "equirect"
	elseif itm.ImageProjectionCombo.CurrentText == "Fisheye" then
		prefsTbl.ImageProjection = "fisheye"
	elseif itm.ImageProjectionCombo.CurrentText == "Flat" then
		prefsTbl.ImageProjection = "rect"
	else
		-- Fallback 2
		prefsTbl.ImageProjection = "halfEquirect"
	end

	-- Stereo 3D Format
	if itm.StereoFormatCombo.CurrentText == "" then
		-- Fallback 1
		prefsTbl.Format = "sbs"
	elseif itm.StereoFormatCombo.CurrentText == "Side By Side" then
		prefsTbl.Format = "sbs"
	elseif itm.StereoFormatCombo.CurrentText == "Over Under" then
		prefsTbl.Format = "ou"
	else
		-- Fallback 2
		prefsTbl.Format = "sbs"
	end

	-- Primary Eye
	prefsTbl.PrimaryEye = string.lower(itm.PrimaryEyeCombo.CurrentText)

	-- Checkbox
	prefsTbl.OpenFolder = itm.OpenFolderCheckbox.Checked

	-- Save the new prefs
	setPreferenceData("KartaLink.SpatialMedia.InteraxialLensSeparation", itm.InteraxialLensSeparationSpinner.Value, false)
	setPreferenceData("KartaLink.SpatialMedia.HFOV", itm.HFOVSpinner.Value, false)
	setPreferenceData("KartaLink.SpatialMedia.HAdjust", itm.HAdjustSpinner.Value, false)
	setPreferenceData("KartaLink.SpatialMedia.ImageProjection", itm.ImageProjectionCombo.CurrentText, false)
	setPreferenceData("KartaLink.SpatialMedia.StereoFormat", itm.StereoFormatCombo.CurrentText, false)
	setPreferenceData("KartaLink.SpatialMedia.PrimaryEye", itm.PrimaryEyeCombo.CurrentText, false)
	setPreferenceData("KartaLink.SpatialMedia.OpenFolder", itm.OpenFolderCheckbox.Checked, false)

	-- Clean out the window close hotkey events
	app:RemoveConfig("settingsWin")
	collectgarbage()

	-- print("  [Prefs]")
	-- dump(prefsTbl)

	return prefsTbl
end

function Main()
	print("[Spatial Metadata]")
	print("Script by Andrew Hazelden <andrew@andrewhazelden>")
	print("Powered by the Spatial CLI tool by Mike Swanson")

	if platform == "Mac" then
		-- macOS

		-- Is the Spatial CLI app installed?
		print("[Status][Passed] Checking if the Spatial CLI program is installed at: " .. tostring(spatialProgram))
		if bmd.fileexists(spatialProgram) then

			-- Check the settings
			local prefs = UserSettings()
			-- dump(prefs)

			-- Debug: Set the prefs to run with an empty (placeholder) Lua table
			-- prefs = {}
			-- prefs.Cancel = false

			-- Check the active selection and return a list of media files
			local MediaTbl = GenerateMediaList(prefs)

			if prefs.Cancel == true then
				print("[Status][Canceled]")
				return
			end

			-- Debug media command clips
			-- dump(MediaTbl)

			if #MediaTbl == 1 then
				print("[Processing Footage] " .. tostring(#MediaTbl) .. " clip found")
			else
				print("[Processing Footage] " .. tostring(#MediaTbl) .. " clips found")
			end

			local prevFolder = ""
			for key, value in pairs(MediaTbl) do
				-- Todo: check if the input movie filename exists on disk, and the output folder name exists
				local inputMovie = value
				if inputMovie and inputMovie ~= "" and bmd.fileexists(inputMovie) then
					local outputTbl = parseSequenceFilename(inputMovie)
					if outputTbl then
						local outputMovie = tostring(outputTbl.Path) .. tostring(outputTbl.Name) .. "_injected.mov"
						print("\n\n[Clip #] " .. tostring(key) .. " [Input Filename] " .. tostring(inputMovie) .. " [Output Filename] " .. tostring(outputMovie) .. "\n")
						-- CLI String
				local options = "make --input \"" .. tostring(inputMovie) .. "\" --format=" .. tostring(prefs.Format) .. " --output \"" .. tostring(outputMovie) .. "\" --cdist=" .. tostring(prefs.InteraxialLensSeparation) .. " --hfov=" .. tostring(prefs.HFOV) .. " --hadjust=" .. tostring(prefs.HAdjust) .. " --primary=" .. tostring(prefs.PrimaryEye) .. " --projection=" .. tostring(prefs.ImageProjection)

						-- Run the Spatial CLI app
						local commands = '"' .. spatialProgram .. '\" ' .. options
						print(commands)

						local i = 0
						local handler = io.popen(commands)
						-- Scan the io.popen output one line at a time
						for line in handler:lines() do
							print(line)
							-- Increment the line number
							i = i + 1
						end
						-- Open the folder where the clip is stored
						if (prevFolder ~= outputTbl.Path) and bmd.direxists(outputTbl.Path) and (prefs.OpenFolder == 1 or prefs.OpenFolder == true) then
							bmd.openfileexternal('Open', outputTbl.Path)
						end
						prevFolder = outputTbl.Path
					else
						print("[Status] Error with Media Pool clip filename: " .. tostring(inputMovie))
					end
				else
					print("[Status] Error with Media Pool clip filename")
				end
			end
		else
			-- Spatial CLI app not installed
			print([[
[Status][Error] The Spatial CLI program was not found. Please install it before running this script again.

The Spatial CLI program can be installed using the macOS based homebrew package manager.

# Install Homebrew using the macOS terminal:
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

# Install the Spatial CLI app using the brew program in the macOS terminal:
brew install spatial

# The Spatial CLI app now lives on your hard disk at:
/opt/homebrew/bin/spatial
]])
		end
	else
		-- Win/Linux Error
		print("[Error] The spatial CLI program is not available for Windows or Linux")
	end
end

Main()
print("[Done]")

