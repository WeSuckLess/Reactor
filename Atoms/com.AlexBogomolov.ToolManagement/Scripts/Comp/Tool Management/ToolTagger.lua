_author = "Alexey Bogomolov <mail@abogomolov.com>"
_date = "2021-01-22"
_VERSION = "1.3"
_LICENSE = "MIT"

--[[
Tool Tagger script
Set tags to a groups of tools and make basic operations such as select, enable/disable, lock/unlock

version history:
v 1.0 - Initial release
v 1.1 - A little bit of bug catching.
      - Prevent adding duplicate data to the Tool Tagger
      - Add Exclude tool from the tag button to the Tool Tagger
v 1.2 - fix buttons
v 1.3 - add refresh button to use with multipe comps
      - select tools on doubleclick
v 1.31 - fix Resolve 17 compatibility (omit __flags data)
]]

local ui = fu.UIManager
local disp = bmd.UIDispatcher(ui)
local width, height = 390, 360

if not comp then comp = fu:GetCurrentComp() end
local data = comp:GetData("ToolManager")
if not data then
    labelMessage = "Type tag name here:"
end



-- disable jit since Fusion has a Lua bug on a Mac with 16.2 
if fu.Version == 16.2 and FuPLATFORM_MAC == true then
    print('JIT disabled for v16.2 on macOS')
    jit.off()
end

app:AddConfig("Tagger", {
    Target {ID = "Tagger"}, Hotkeys {
        Target = "Tagger",
        Defaults = true,
        ESCAPE = "Execute{cmd = [[app.UIManager:QueueEvent(obj, 'Close', {})]]}"
    }
})

mainWin = ui:FindWindow("Tagger")
if mainWin then
    mainWin:Raise()
    mainWin:ActivateWindow()
    return
end

win = disp:AddWindow({
    ID = 'Tagger',
    TargetID = 'Tagger',
    WindowTitle = 'Tool Tagger',
    Geometry = {800, 500, width, height},
    Spacing = 0,

    ui:VGroup{
        ID = 'root',
        ui:HGroup{
        Weight = 0,
        ui:VGroup{
            ui:Button{ID = 'TogglePT', Text = 'Toggle'},
            ui:Button{ID = 'Select', Text = 'Select'},
        },
        ui:VGroup{
            ui:Button{ID = 'Disable', Text = 'Disable'},
            ui:Button{ID = 'ToolLock', Text = 'Lock'},
        },
        ui:VGroup{
            ui:Button{ID = 'Enable', Text = 'Enable'},
            ui:Button{ID = 'ToolUnlock', Text = 'Unlock'},
        },
        ui:VGroup{
            ui:Button{ID = 'RefreshButton', Text = 'Refresh'},
            },
        },
        ui:HGroup{
            Weight = 1,
            ui:Tree{
                Alignment = {AlignHCenter = true, AlignBottom = true},
                ID = 'Tree',
                SortingEnabled = true,
                Events = { ItemClicked = true, ItemDoubleClicked = true, CurrentItemChanged = true, ItemActivated = true }
            }
        },
        ui:VGroup{
            Weight = 0,
            ui:HGroup{
                ui.Label {ID = 'Label', Text = labelMessage, Weight = 0.1},
            },
            ui:HGroup{
                ui.LineEdit {ID = 'Line', Text = '', Events = {ReturnPressed = true, EditingFinished = true}, Weight = .9},
            },
            ui:HGroup {
                ui.Button { ID = 'SetTagButton', Text = 'Set Tag'},
                ui:Button { ID = 'TagInfo', Text = 'Tag Info'},
                ui.Button { ID = 'DeleteTagButton', Text = 'Delete Tag'},
                ui.Button { ID = 'ExcludeButton', Text = 'Exclude Tool'},
            }
        },
    }
})

itm = win:GetItems()

local function GetTreeItems(tree)
	return tree:FindItems("*",
	{
		MatchExactly = false,
		MatchFixedString = false,
		MatchContains = false,
		MatchStartsWith = false,
		MatchEndsWith = false,
		MatchCaseSensitive = false,
		MatchRegExp = false,
		MatchWildcard = true,
		MatchWrap = false,
		MatchRecursive = true,
	}, 0)
end

-- Add a header row.
hdr = itm.Tree:NewItem()

function SetHeader(count)
    if count == 0 then
        hdr.Text[0] = 'Select tools to add tags'
    else
        hdr.Text[0] = 'Select a tag from a list below to manage tools'
    end
end

-- Add clear button
itm.Line:SetClearButtonEnabled(true)

-- Gets all items (useful for getting all items in a nested tree without traversing the tree)

function hasValue(tab, val)
    for i, item in pairs(tab) do
        if item == val then
            return true
        end
    end
    return false
end

function ShowTagInfo()
    local currentTag = itm.Line.Text
    local data = comp:GetData("ToolManager.".. currentTag)
    if not data or #data == 0 then
        print("not tools assigned to the tag")
        return
    end
    if currentTag ~= "" then
        print("tools tagged with [ "..currentTag.." ]:")
        for i, tool in ipairs(data) do
            print("    ".. tool)
        end
    else
        print('select tag from the list or add a new one')
    end
end

function SelectTools()
    local comp = fu:GetCurrentComp()
    flow = comp.CurrentFrame.FlowView
    local selectedTools = comp:GetToolList(true)
    flow:Select()
    local tagSearch = itm.Line.Text

    if tagSearch == "" then
        if #selectedTools == 0 then
            return
        end
        selectedToolId = selectedTools[1].ID
        for i, tool in ipairs(comp:GetToolList(false, selectedToolId)) do
            flow:Select(tool)
        end
    else
        local data = comp:GetData("ToolManager")
        local toolNames = data[tagSearch]
        for _, toolName in ipairs(toolNames) do
            local tool = comp:FindTool(toolName)
            if tool then
                flow:Select(tool)
            end
        end
    end
end

function SetTag(comp)
    sel_tools = comp:GetToolList(true)
    tag = tostring(itm.Line.Text)
    if #sel_tools == 0 then
        print("No tools selected")
    	return
    end
    if tag == "" then
        print("choose the tag from the list or add a new tag")
        return
    end
    data = comp:GetData("ToolManager."..tag) or {}
    for _, tool in pairs(sel_tools) do
        if not hasValue(data, tool.Name) then
            table.insert(data, tool.Name)
        end
    end
    comp:SetData("ToolManager."..tag, data)
    RefreshTable()
end


function RefreshTable()
    local currentTag = itm.Line.Text
    comp = fu:GetCurrentComp()
	itm.Tree:Clear()
    local data = comp:GetData("ToolManager")
    local count = 0
    if data then
        for tag, tools in pairs(data) do
            count = count + 1
            local alreadyAdded = false
            for _, treeItem in ipairs(GetTreeItems(itm.Tree)) do
                if treeItem.Text[0] == tostring(tag) then
                    alreadyAdded = true
                    break
                end
            end
            if alreadyAdded == false then
                itRow = itm.Tree:NewItem();
                itRow.Text[0] = tostring(tag)
                if tag ~= "__flags" then
                    itm.Tree:AddTopLevelItem(itRow)
                    itm.Line.Text = tostring(tag)
                end
            end
        end
    end
    local treeItems = GetTreeItems(itm.Tree)
    SetHeader(#treeItems)
    itm.Line.Text = currentTag
    if count == 0 then
        itm.Label.Text = "Type tag name here:"
        itm.Line.Text = ""
    else
        itm.Label.Text = "Current tag:"
    end
end

function win.On.DeleteTagButton.Clicked(ev)
    local currentTag = itm.Line.Text
    comp:SetData("ToolManager."..currentTag)
    RefreshTable()
    itm.Tree:SetFocus("OtherFocusReason")
end

function win.On.Line.EditingFinished(ev)
    itm.Tree:SetFocus("OtherFocusReason")
end

function win.On.Tree.ItemClicked(ev)
	itm.Line.Text = ev.item.Text[0]
end

function win.On.Tree.ItemActivated(ev)
    itm.Line.Text = ev.item.Text[0]
end

function win.On.Refresh.Clicked(ev)
    RefreshTable()
end

function win.On.Exit.Clicked(ev)
    disp:ExitLoop()
end

function win.On.Line.ReturnPressed(ev)
    local comp = fu:GetCurrentComp()
    SetTag(comp)
end

function win.On.Tree.ItemDoubleClicked(ev)
   SelectTools() 
end

function win.On.TagInfo.Clicked(ev)
    ShowTagInfo()
end

function win.On.RefreshButton.Clicked(ev)
    RefreshTable()
end

function win.On.ExcludeButton.Clicked(ev)
    local tag = itm.Line.Text
    data = comp:GetData("ToolManager.".. tag)
    local selectedTools = comp:GetToolList(true)
    if not data or not selectedTools then
        return
    end

    for k, tool in pairs(selectedTools) do
        for i, name in pairs(data) do
            if tool.Name == name then
                table.remove(data, i)
                print("removed '" ..tool.Name.."' from tag `"..tag.."`")
            end
        end
    end
    comp:SetData("ToolManager."..tag, data)
    RefreshTable()
end

function win.On.SetTagButton.Clicked(ev)
    local comp = fu:GetCurrentComp()
    SetTag(comp)
end

function TogglePassThrough(tool)
    if tool:GetAttrs().TOOLB_PassThrough == true then
        tool:SetAttrs({TOOLB_PassThrough = false})
    else
        tool:SetAttrs({TOOLB_PassThrough = true})
    end
end

function win.On.Tagger.Close(ev)
    disp:ExitLoop()
end

function win.On.Select.Clicked(ev)
    SelectTools()
end

function win.On.TogglePT.Clicked(ev)
    local comp = fu:GetCurrentComp()
    local selectedTools = comp:GetToolList(true)
    local tagSearch = itm.Line.Text

    if tagSearch == "" then
        if #selectedTools == 0 then
            return
        end
        selectedToolId = selectedTools[1].ID
        for i, tool in ipairs(comp:GetToolList(false, selectedToolId)) do
            TogglePassThrough(tool)
        end
    else
         local data = comp:GetData("ToolManager")
         for tag, tools in pairs(data) do
            if tostring(tag) == tagSearch then
                for _, tool in ipairs(tools) do
                    TogglePassThrough(comp:FindTool(tool))
                end
            end
        end
    end
end

function ToolLock(isLocked)
    local comp = fu:GetCurrentComp()
    local selectedTools = comp:GetToolList(true)
    local tagSearch = itm.Line.Text

    if tagSearch == "" then
        if #selectedTools == 0 then
            return
        end
        selectedToolId = selectedTools[1].ID
        for i, tool in ipairs(comp:GetToolList(false, selectedToolId)) do
            tool:SetAttrs({TOOLB_Locked = isLocked})
        end
    else
        local data = comp:GetData("ToolManager")
        for tag, tools in pairs(data) do
            if tostring(tag) == tagSearch then
                for _, tool in ipairs(tools) do
                    tool = comp:FindTool(tool)
                    tool:SetAttrs({TOOLB_Locked = isLocked})
                end
            end
        end
    end
end

function win.On.ToolLock.Clicked(ev)
    ToolLock(true)
end
function win.On.ToolUnlock.Clicked(ev)
    ToolLock(false)
end

function doPassThrough(operation, report)
    local comp = fu:GetCurrentComp()
    local selectedTools = comp:GetToolList(true)
    local tagSearch = itm['Line'].Text
    count = 0

    comp:StartUndo(report .. ' tools')

    if tagSearch == "" then
        if #selectedTools == 0 then
            return
        end
        selectedToolId = selectedTools[1].ID
        for i, tool in ipairs(comp:GetToolList(false, selectedToolId)) do
            tool:SetAttrs({TOOLB_PassThrough = operation})
        end
        comp:EndUndo()
    else
        local data = comp:GetData("ToolManager")
        for tag, tools in pairs(data) do
            if tostring(tag) == tagSearch then
                for _, tool in ipairs(tools) do
                    tool = comp:FindTool(tool)
                    if tool then
                        tool:SetAttrs({TOOLB_PassThrough = operation})
                    end
                    count = count + 1
                end
            end
        end
    end

    comp:EndUndo(true)

    if count == 0 then
        return
    elseif count == 1 then
        multiple_tool = ' tool'
        print(report .. count .. multiple_tool)
    else
        multiple_tool = ' tools'
        print(report .. count .. multiple_tool)
    end
end

function win.On.Disable.Clicked(ev) doPassThrough(true, 'disabled ') end
function win.On.Enable.Clicked(ev) doPassThrough(false, 'enabled ') end

-- Get all nodes
local allTools = comp:GetToolList(false)
RefreshTable(allTools)
itm.Tree:SetHeaderItem(hdr)

win:Show()
disp:RunLoop()
win:Hide()
