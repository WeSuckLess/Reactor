Dual Fisheye Samsung Gear360 Stitching Example
2023-12-22
Prepared by Andrew Hazelden (andrew@andrewhazelden.com)

Overview:
This example composite stitches together footage from a dual circular fisheye lens camera. It expects footage that has a pair of front and back circular fisheye lens images merged into the same frame layout. The settings have been tuned for a Samsung Gear360 camera.

About the Comp:
The kvrStereoCrop node is used to isolate the Samsung Gear360 camera's front and back images from the dual fisheye view layout.

The kvrLensStereo node is used to apply f-theta lens distortion correction to linearize the image content at the far edges of the circular fisheye images.

The ColorCorrector nodes have an Ellipse mask which allows and edge falloff zone color correction adjutment to be applied for lens vingetting correction.

The kvrWarpStitchUltra nodes are used to apply circular fisheye to LatLong image projection conversions. There are mask softness controls that allow feathering the frame border to make it possible to blend the images without a hard border edge

The Merge node is used to combine the front and back LatLong imagery. If you set the Merge "Apply Mode" setting from "Normal" to "Difference" you can do a quality check to validate the edge blending zone is correctly aligned so the two images can be blended seamlessly. If you select the Merge node in the Nodes view and press "Command + T" you are able to toggle the foreground and background input connection ordering which flips the specific view that is on top when doing the blending operation.

Software Required:
- Resolve Free or Resolve Studio or Fusion Studio v17-18.6+
	(https://www.blackmagicdesign.com/support/family/davinci-resolve-and-fusion)

- Open-source Reactor Package Manager + Kartaverse XR Toolset
(https://andrewhazelden.com/projects/kartavr/docs/)

- Kartaverse Fuses Used:
	- kvrCropStereo
	- kvrLensStereo
	- kvrWarpStitchUltra
	- kvrViewer

Effects Template Installation:
- Copy the included "kvrDualFisheye.setting" Effects Template Macro

From:
"Templates/Edit/Effects/KartaVP/Warp/kvrDualFisheye.setting"

To:
Reactor:/Deploy/Templates/Edit/Effects/KartaVP/Warp/kvrDualFisheye.setting

Effects Template Usage:
In the Resolve Edit page, the "kvrDualFisheye" macro can then be dragged from the Effects Library "Effects > KartaVP > Warp" category onto a clip in the timeline. Select the clip and then open the Inspector view to adjust the parameters using the "Effects > Fusion > kvrDualFisheye" item. 

In the inspector view, if you click the little magic wand icon next to the right of the heading "kvrDualFisheye" you can hop into the Fusion page to customize the macro node. Double clicking on the "kvrDualFisheye" node in the Fusion page allows you to expand the group to access the nodes that are stored inside the group object.


