-- "Show Docs Local" menu item

local path = app:MapPath("Reactor:/Deploy/Docs/Kartaverse/Vonk Ultra/Vonk Ultra Data Nodes.pdf")
print("\n[Show Docs Local] ", path)

if bmd.direxists(path) == false then
    print("[Docs Missing] The PDF file could not be found on disk.")
end

bmd.openfileexternal("Open", path)
