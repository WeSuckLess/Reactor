Atom {
	Name = "KartaLink | Comp Sessions",
	Category = "Kartaverse/KartaLink/Scripts",
	Author = "Andrew Hazelden",
	Version = 5.76,
	Date = {2025, 2, 16},
	Description = [[<h2>Comp Session</h2>

<p>The "File &gt; Sessions &gt;" menu allows you to quickly load and save a snapshot of the open .comp tabs that are active in Fusion Studio Standalone. The node selection state is restored for each of the comps as well. This makes it easy to return to an earlier work session.</p>

<p>This approach lets you hop back into a multi-document compositing session and have all your comp work loaded up, just like it was when you saved the previous session.</p>

<h2>Menu Entries</h2>

<p>The "Config:/Menus/Kartaverse Sessions.fu" file adds the following menu entries to Fusion Studio Standalone:</p>

<pre>File > Sessions:
- Restore Session
- Load Session...
- --------------------
- Save Session
- Save Session As...
- --------------------
- Close Session
</pre>

<h2>Usage</h2>

<p>The "Restore Session" menu loads the most recently used session file. Existing comp tabs will be saved and closed first.</p>
<p>The "Load Session..." menu item lets you open up pre-existing .json based session files. Existing comp tabs will be saved and closed first.</p>
<p>The "Save Session" menu item lets you re-save the active .json based session files to disk. This will refresh the active comp tabs and node selection values.</p>
<p>The "Save Sessions As..." menu item lets you save your session to a different .json based session filename on disk.</p>
<p>The "Close Sessions" menu item closes all of the open comp tabs. You are asked to save each .comp file if there are any changes.</p>

<h2>Session Files</h2>

<p>Comp session files are saved in a .json file format. The default comp session document storage path is:<br>
<a href="file://Reactor:/Deploy/Scripts/Support/Kartaverse/Sessions/">Reactor://Deploy/Scripts/Support/Kartaverse/Sessions/</a></p>

]],
	Deploy = {
		"Config/Menus/Kartaverse Sessions.fu",
		"Docs/Kartaverse/KartaLink/Sessions/Images/file_menu.png",
		"Docs/Kartaverse/KartaLink/Sessions/Sessions.html",
		"Docs/Kartaverse/KartaLink/Sessions/Sessions.md",
		"Scripts/Comp/KartaLink/Sessions/Open Sessions Docs Folder.lua",
		"Scripts/Comp/KartaLink/Sessions/Open Sessions Folder.lua",
	},
}
