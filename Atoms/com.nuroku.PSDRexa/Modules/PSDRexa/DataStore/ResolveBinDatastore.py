from pathlib import Path

from Common import Logger
from Service.ResolveService import ResolveService
from Service.SettingFileService import SettingKeys, SettingFileService

logger = Logger.get_logger(__name__)


class ResolveBinDatastore:
    def __init__(self):
        # For Template, create a Datastore class with a name like Template_bin.
        self.image_bin_name = "PSDRexa"
        pass

    # Bin creation
    def get_image_bin(self):
        # Check if there is an existing Bin with the same name
        project_manager = ResolveService.get_resolve().GetProjectManager()
        project = project_manager.GetCurrentProject()
        media_pool = project.GetMediaPool()

        existing_bins = media_pool.GetRootFolder().GetSubFolderList()
        existing_bin = None
        for bin in existing_bins:
            if bin.GetName() == self.image_bin_name:
                existing_bin = bin
                break

        # Create a new Bin if a Bin with the same name does not exist
        if existing_bin is None:
            new_bin = media_pool.AddSubFolder(
                media_pool.GetRootFolder(), self.image_bin_name
            )
            logger.debug(f"New Bin created: {self.image_bin_name}")
            media_pool.SetCurrentFolder(new_bin)
            return new_bin
        else:
            logger.debug(
                f"Did not create a new Bin because a Bin with the same name already exists: {self.image_bin_name}"
            )
            media_pool.SetCurrentFolder(existing_bin)
            return existing_bin

    def get_image_from_bin(self, file_name):
        clips = self.get_image_bin.GetItemList()
        for clip in clips:
            clip_attrs = clip.GetAttrs()
            if clip_attrs["Name"] == file_name:
                return clip

        logger.debug(
            f"An image with the specified file name was not found: {file_name}"
        )
        return None

    def add_image_to_bin(self, image_path: Path):
        # Isn't it simply a naming fraud that added images to the timeline?

        media_storage = ResolveService.get_resolve().GetMediaStorage()
        self.get_image_bin()
        item = media_storage.AddItemListToMediaPool(str(image_path))

        project = ResolveService.get_resolve().GetProjectManager().GetCurrentProject()
        media_pool = project.GetMediaPool()
        timeline = project.GetCurrentTimeline()

        index = int(SettingFileService.read_config(SettingKeys.index_for_output))
        max_right_offset = -1

        video_items = timeline.GetItemListInTrack("video", index)
        for video_item in video_items:
            if max_right_offset < video_item.GetEnd():
                max_right_offset = video_item.GetEnd()

        if max_right_offset == -1:
            max_right_offset = ResolveService.get_startframe()

        res = media_pool.AppendToTimeline(
            [
                {
                    "mediaPoolItem": item[0],
                    "startFrame": 0,
                    "mediaType": 1,
                    "trackIndex": index,
                    "recordFrame": max_right_offset,
                }
            ]
        )
        # return res
