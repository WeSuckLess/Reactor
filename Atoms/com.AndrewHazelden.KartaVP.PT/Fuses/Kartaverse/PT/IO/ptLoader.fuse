-- ============================================================================
-- modules
-- ============================================================================
local jsonutils = self and require("vjsonutils") or nil
local arrayutils = self and require("varrayutils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "ptLoader"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = "ScriptVal",
    -- REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\PT\\IO",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Loads PTGui .pts JSON text into a ScriptVal object.",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.Loader",
})

RefreshPTS = [[
print("[ptLoader] Refresh PTS")
]]

function Create()
    -- [[ Creates the user interface. ]]

    InRefresh = self:AddInput("Refresh File", "Refresh", {
        INPID_InputControl = 'ButtonControl',
        INP_External = false,
        IC_Visible = true,
        BTNCS_Execute = RefreshPTS,
        INP_Passive = false,
    })

    InSeparator = self:AddInput("UISeparator1", "UISeparator1", {
        IC_Visible = true,
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
    })

    InFile = self:AddInput("File" , "File" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "FileControl",
        FC_IsSaver = false,
        FC_ClipBrowse = false,
        FCS_FilterString =  "PTGui (*.pts)|*.pts|JSON Files (*.json)|*.json",
        LINK_Main = 1
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutScriptVal = self:AddOutput("ScriptVal", "ScriptVal", {
        LINKID_DataType = "ScriptVal",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InFile:SetAttrs({LINK_Visible = visible})
    end
end

function read_file(filename)
    local f = assert(io.open(filename, "rb"))
    local content = f:read("*all")
    f:close()

    return content
end


function Process(req)
    -- [[ Creates the output. ]]
    local rel_path = InFile:GetValue(req).Value
    local abs_path = self.Comp:MapPath(rel_path)

    local json_str = read_file(abs_path)
    local tbl = {}

    tbl.file = abs_path
    tbl.pts = jsonutils.decode(json_str)

--    print("[ScriptVal Lua Table]")
--    dump(tbl)

    OutScriptVal:Set(req, ScriptValParam(tbl))
end
