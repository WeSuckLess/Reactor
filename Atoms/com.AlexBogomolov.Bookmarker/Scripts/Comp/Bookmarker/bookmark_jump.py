"""
Jump to stored flow position. Use dropdown menu to switch to stored position
KEY FEATURES:
* Delete single bookmark or all of them
* All bookmarks are alphabetically sorted.
* Add a bookmark from Jump UI with plus button. Press Refresh and new bookmarkk will appear in a list
* To jump the same tool once again just select it again in a list.
v 2.5 update:
* Move selected tools next to bookmark position (enable checkbox 'move selected to bookmark')
* Jump back and forth between previous and current bookmarks
v 2.6:
* Add tool ID next to Bookmark name
* fix move to Bookmark bug
* cach error on JumpBack function
v 2.8:
* Sorting bookmarks by name and tool type
* Button to remove orphaned bookmarks
* prevent multiple windows to launch
* attempt to find tool by ID, if bookmark was not found because tool was renamed
v 2.9
* Use Fusion 17 Bookmark API to restore tool positioning. 
v 3.0
* use TreeView for Bookmark list
* replace orphaned bookmarks (bookmarked tools that was renamed and not found) with flow location bookmark

KNOWN ISSUES:
* depending on complexity if the comp, the nodes in a flow
may temporarily disappear after bookmark jump. As a workaround to this issue
added 0.1 sec delay before jump to the tool.
* the script just finds a tool in a flow and makes it active. It does not center it in the flow.
Possible workaround here:
-- jump to the tool, click on the flow, press CTRL/CMD+F and then hit ENTER
-- right click the flow with node selected, Scale -> Scale to Fit

Alexey Bogomolov mail@abogomolov.com
Requests and issues: https://gitlab.com/WeSuckLess/Reactor/tree/movalex/Atoms/com.AlexBogomolov.Bookmarker
Donations: https://paypal.me/aabogomolov
STU topic and discussion, feature requests and updates: https://www.steakunderwater.com/wesuckless/viewtopic.php?f=33&t=2858

MIT License: https://mit-license.org/
"""
# legacy python reporting compatibility
from __future__ import print_function


comp = fu.GetCurrentComp()
ui = fusion.UIManager
flow = comp.CurrentFrame.FlowView
OPEN_ON_MOUSE_POS = False

regs_dictionary = {
    "Merge": "Mrg",
    "Merge3D": "3Mg",
    "SurfaceAlembicMesh": "ABC",
    "AlphaDivide": "ADv",
    "AlphaMultiply": "AMl",
    "LightAmbient": "3AL",
    "SSAO": "SSAO",
    "Anaglyph": "Ana",
    "Fuse.AnimationEaseModifier": "AEM",
    "AutoDomain": "ADoD",
    "AutoGain": "AG",
    "Background": "BG",
    "Bender3D": "3Bn",
    "Fuse.BezierTaper": "bztp",
    "Dimension.BicubicTest": "BCT",
    "BitmapMask": "Bmp",
    "MtlBlinn": "3Bl",
    "Fuse.Blip": "Blip",
    "Blur": "Blur",
    "BrightnessContrast": "BC",
    "BSplineMask": "BSp",
    "BumpMap": "3Bu",
    "Dimension.CalcOcclusions": "CO",
    "Camera3D": "3Cm",
    "CameraShake": "CShk",
    "Dimension.CameraTracker": "CTra",
    "TexCatcher": "3Ca",
    "ChangeDepth": "CD",
    "MtlChanBool": "3Bol",
    "ChannelBoolean": "Bol",
    "ChromaKeyer": "CKy",
    "Fuse.JD_ChromaticZoom": "crz",
    "CineonLog": "Log",
    "CleanPLate": "CPl",
    "ColorCorrector": "CC",
    "ColorCurves": "CCv",
    "ColorGain": "Clr",
    "CustomColorMatrix": "CMx",
    "ColorSpace": "CS",
    "Combiner": "Com",
    "Fuse.Conditional": "Cndl",
    "Fuse.Convolve": "Cnv",
    "MtlCookTorrance": "3CT",
    "CoordSpace": "CdS",
    "CoordTransform3D": "3CX",
    "Dimension.CopyAux": "CpA",
    "Fuse.FLS_CopyMetadata": "Meta",
    "CornerPositioner": "CPn",
    "CreateBumpMap": "CBu",
    "Crop": "Crp",
    "Fuse.Cryptomatte": "Cryptomatte",
    "Cube3D": "3Cb",
    "CubeMap": "3Cu",
    "CustomFilter": "CFlt",
    "Custom": "CT",
    "CustomVertex3D": "3CV",
    "DaySky": "DS",
    "Dimension.DecomposeOp": "De",
    "Defocus": "Dfo",
    "DeltaKeyer": "DK",
    "Dent": "Dnt",
    "DepthBlur": "DBl",
    "DifferenceKeyer": "DfK",
    "DirectionalBlur": "DrBl",
    "LightDirectional": "3DL",
    "Dimension.Disparity": "Dis",
    "Dimension.DisparityToZ": "D2Z",
    "Displace": "Dsp",
    "Displace3D": "3Di",
    "Dissolve": "DX",
    "Drip": "Drip",
    "Fuse.Duplicate": "Dup",
    "Duplicate3D": "3Dp",
    "Fuse.eLinBC": "lBC",
    "EllipseMask": "Elp",
    "ErodeDilate": "ErDl",
    "Fuse.ExternalMatteSaver": "EMS",
    "FalloffOperator": "3Fa",
    "FastNoiseTexture3D": "3FN",
    "FastNoise": "FN",
    "ExporterFBX": "FBX",
    "SurfaceFBXMesh": "FBX",
    "Dimension.FeatherMaskOp": "FeatherMask",
    "Fields": "Flds",
    "FileLUT": "FLUT",
    "FilmGrain": "FGr",
    "Filter": "Fltr",
    "Dimension.FixOpticalFlow": "FOF",
    "Fuse.FlexyRig": "Lin",
    "Fog": "Fog",
    "Fog3D": "3Fog",
    "Fuse.FractalNoise": "Ftns",
    "Fuse.FrameAverage": "Avg",
    "Fuse.FUI_Circles": "FCT",
    "Fuse.FUI_Grids": "FGT",
    "Dimension.FwdBack": "FwdBack",
    "GamutConvert": "Gmt",
    "Dimension.GlobalAlign": "GA",
    "Glow": "Glo",
    "TexGradient": "3Gd",
    "Grain": "Grn",
    "GridWarp": "Grd",
    "Dimension.GuidedFilterOp": "GF",
    "Highlight": "HiL",
    "HotSpot": "Hot",
    "HueCurves": "HCv",
    "ImagePlane3D": "3Im",
    "KeyStretcher": "KfS",
    "LatLongPatcher": "LLP",
    "LensDistort": "Lens",
    "Letterbox": "Lbx",
    "Fuse.LifeSaver": "LSV",
    "LightTrim": "LT",
    "Fuse.Linger": "Lgr",
    "Loader": "LD",
    "Locator3D": "3Lo",
    "Location": "Location",
    "LumaKeyer": "LKy",
    "Fuse.LUTCubeAnalyzer": "LCA",
    "Fuse.LUTCubeApply": "LCP",
    "Fuse.LUTCubeCreator": "LCC",
    "Mandel": "Man",
    "PaintMask": "PnM",
    "MtlMerge3D": "3MM",
    "MtlSwitch3D": "3MSw",
    "MatteControl": "Mat",
    "MediaIn": "MI",
    "MediaOut": "MO",
    "Dimension.Median3x3": "M3",
    "Dimension.NewEye": "NE",
    "Fuse.Normalize": "fNrl",
    "Note": "Nte",
    "Fuse.Notepad": "Not",
    "Fuse.NumberOperator": "fNOp",
    "OCIOCDLTransform": "OCD",
    "OCIOColorSpace": "OCC",
    "OCIOFileTransform": "OCF",
    "Dimension.OpticalFlow": "OF",
    "Override3D": "3Ov",
    "Paint": "Pnt",
    "PanoMap": "PaM",
    "pAvoid": "pAv",
    "pBounce": "pBn",
    "pChangeStyle": "pCS",
    "pCustom": "pCu",
    "pCustomForce": "pCF",
    "pDirectionalForce": "pDF",
    "pEmitter": "pEm",
    "PerspectivePositioner": "PPn",
    "pFlock": "pFl",
    "pFollow": "pFo",
    "pFriction": "pFr",
    "pGradientForce": "pGF",
    "MtlPhong": "3Ph",
    "pImageEmitter": "pIE",
    "Fuse.PixelSort": "PixS",
    "pKill": "pKl",
    "Dimension.PlanarTracker": "PTra",
    "Dimension.PlanarTransform": "PXf",
    "Plasma": "Plas",
    "pMerge": "pMg",
    "LightPoint": "3PL",
    "PointCloud3D": "3PC",
    "PolylineMask": "Ply",
    "Fuse.PositionHelper": "PHlp",
    "pPointForce": "pPF",
    "pRender": "pRn",
    "Photron.Primatte4": "Pri",
    "Profiler": "Pro",
    "LightProjector": "3Pj",
    "PropagateDepth": "PD",
    "PseudoColor": "PsCl",
    "pSpawn": "pSp",
    "pTangentForce": "pTF",
    "pTurbulence": "pTr",
    "pVortex": "pVt",
    "RangesMask": "Rng",
    "RankFilter": "RFlt",
    "Fuse.OCLRays": "ClR",
    "RectangleMask": "Rct",
    "MtlReflect": "3RR",
    "RemoveNoise": "RN",
    "Renderer3D": "3Rn",
    "Dimension.RepairFrame": "Rep",
    "ReplaceMaterial3D": "3Rpl",
    "ReplaceNormals3D": "3RpN",
    "Replicate3D": "3Rep",
    "BetterResize": "Rsz",
    "Fuse.Retimer": "rt",
    "Ribbon3D": "3Ri",
    "Fuse.RollingShutter": "Rol",
    "RunCommand": "Run",
    "Saver": "SV",
    "Scale": "Scl",
    "Dimension.Seclow": "Sec",
    "SetCanvasColor": "SCv",
    "SetDomain": "DoD",
    "Fuse.SetMetaData": "SMeta",
    "Fuse.SetMetaDataTC": "TCMeta",
    "Shader": "Shd",
    "Shadow": "Sh",
    "Shape3D": "3Sh",
    "Sharpen": "Shrp",
    "Fuse.ShowMetadata": "smd",
    "Dimension.SmoothMotion": "SM",
    "SoftGlow": "SGlo",
    "SoftClip": "3SC",
    "SphereMap": "3SpM",
    "SphericalCamera3D": "3SC",
    "Splitter": "Spl",
    "LightSpot": "3SL",
    "Dimension.StereoAlign": "SA",
    "MtlStereoMix3D": "3SMM",
    "Switch": "Sw",
    "Fuse.Switch": "Sw?",
    "Fuse.SwitchElse": "fSE",
    "Switch3D": "3Sw",
    "Text3D": "3Txt",
    "TextPlus": "Txt+",
    "Texture": "Txr",
    "Texture2DOperator": "3Tx",
    "TextureTransformOperator": "3TT",
    "Fuse.TEXTWRAPPING": "TwR",
    "Fuse.TimeMachine": "TMn",
    "Fuse.TimeMachinePoint": "TMn",
    "Fuse.TimeMachineImage": "TM",
    "Fuse.TimeMachine3D": "TM3D",
    "Fuse.TimeMachineMaterial": "TM3M",
    "TimeSpeed": "TSpd",
    "TimeStretcher": "TSt",
    "Fuse.Time3D": "T3D",
    "TrackerModifier": "Trk",
    "Tracker": "Tra",
    "Trails": "Trls",
    "Transform": "Xf",
    "Transform3D": "3Xf",
    "Triangulate3D": "3Tri",
    "Fuse.True_IK": "IK",
    "TV": "TV",
    "Dimension.Tween": "Tw",
    "TweenOld": "TwO",
    "UltraKeyer": "UKy",
    "Underlay": "Und",
    "UnsharpMask": "USM",
    "UVMap": "3UV",
    "VariBlur": "VBl",
    "Distort": "Dst",
    "VectorMotionBlur": "VBl",
    "VolumeFog": "VlF",
    "VolumeMask": "VlM",
    "Fuse.Voronoi": "vor",
    "Vortex": "Vtx",
    "WandMask": "Wnd",
    "MtlWard": "3Wd",
    "Dimension.WarpColor": "WC",
    "Dimension.WarpDiff": "WarpDiff",
    "Dimension.WarpFlow": "WF",
    "Fuse.WaveModifier": "Wav",
    "Weld3D": "3We",
    "WhiteBalance": "WB",
    "Fuse.WirelessAnything": "Wrless",
    "Fuse.Wireless": "Wire",
    "Fuse.Wireless3D": "Wi3D",
    "Fuse.Wireless3DMAT": "Wi3DMAT",
    "Fuse.WordWriteOn": "WWO",
    "Fuse.XfChroma": "XfC",
    "Dimension.ZToDisparity": "Z2D",
    "ZtoWorldPos": "Z2W",
    "Fuse.ZcovertFuse": "Zconv",
}
# close UI on ESC button
comp.Execute(
    """app:AddConfig("Bookmarker",
{
Target  {ID = "Bookmarker"},
Hotkeys {Target = "Bookmarker",
         Defaults = true,
         ESCAPE = "Execute{cmd = [[app.UIManager:QueueEvent(obj, 'Close', {})]]}" }})
"""
)


def get_abbrev(tool_id):
    abbrev = regs_dictionary.get(tool_id)
    if abbrev:
        return ": ({})".format(abbrev)
    return ""


def refresh_table(data):
    comp = fu.GetCurrentComp()
    itm["Tree"].Clear()
    for k, v in data.items():
        if k == "__flags":
            continue
        bookmark_name = v[1]
        try:
            tool_type = v[5]
            tool_reg_ID = v[4]
        except KeyError:
            tool_type = v[2]
            tool_reg_ID = ""
        regID = regs_dictionary.get(tool_type, "")
        tree_item = itm["Tree"].NewItem()
        # print(dir(itm["Tree"]))
        tree_item["Text"][0] = bookmark_name
        tree_item["Text"][1] = regID
        tree_item["Text"][2] = tool_reg_ID 
        itm["Tree"].AddTopLevelItem(tree_item)


def move_selected_to_bm(target, tools=None):
    if not tools:
        return
    comp.StartUndo("Move tools to Bookmark")
    print("moving currently selected tools to {}".format(target.Name))
    pos_target_x, pos_target_y = flow.GetPosTable(target).values()
    for n, selected_tool in enumerate(tools.values()):
        flow.SetPos(selected_tool, pos_target_x, pos_target_y + (n + 1))
    comp.EndUndo()


def find_tool(regID):
    """
    This function should check whether the bookmarked tool name has changed. If the tool ID is the same, change bookmark data reference to that tool.
    """
    comp = fu.GetCurrentComp()

    # remove Resolve17 __flags data
    data[regID].pop("__flags", None)

    bm_name, tool_name, scale_factor, tool_id_key, ID = data[regID].values()
    target = comp.FindTool(tool_name)
    if not target:
        print("looking for renamed tools")
        tool_dict = {}
        for tool in comp.GetToolList(False).values():
            tool_id = str(int(tool.GetAttrs()["TOOLI_ID"]))
            tool_dict[tool_id] = tool
        parse_key = str(tool_id_key[7:])
        tool_found = tool_dict.get(parse_key)
        if tool_found:
            print("found new name: {}".format(tool_found.Name))
            comp.SetData(
                "BM.{}".format(tool_id_key),
                [bm_name, tool_found.Name, scale_factor, tool_id_key, tool_found.ID,],
            )
            update_data()
            target = comp.FindTool(tool_found.Name)
    if target:
        return target, scale_factor
    else:
        print("Bookmarked tool not found. Setting to location")
        comp.SetData("BM.{}".format(tool_id_key))
        do_delete_native(bm_name)
        flow.InsertBookmark("[BM] : " + bm_name)
        comp.SetData("BM.{}".format(bm_name), [bm_name, "Location"])
        update_data()
        return None, None


def jump_bookmark(ev):
    comp = fu.GetCurrentComp()
    flow = comp.CurrentFrame.FlowView
    bookmark_name = ev["item"].Text[0]
    tool_type = ev["item"].Text[1]
    tool_regID = ev["item"].Text[2]
    if not tool_type == "Location":
        target, scale_factor = find_tool(tool_regID)
        if not target:
            return
        tool_list = comp.GetToolList(True)
        flow.Select()
        if fu.Version < 17:
            flow.SetScale(scale_factor)
        else:
            flow.GoToBookmark("[BM] : {}".format(bookmark_name))
            # tool_list = comp.GetToolList(True).values()
            # if tool_list:
            #     comp.SetData("PrevBM", tool.Name)

        comp.SetActiveTool(target)
        if itm["MoveToolsToBookmarkPosition"].Checked:
            move_selected_to_bm(target, tool_list)
    else:
        flow.GoToBookmark("[BM] : {}".format(bookmark_name))
    itm["MoveToolsToBookmarkPosition"].Checked = False


def jump_back(ev):
    comp = fu.GetCurrentComp()
    previous_tool = comp.FindTool(comp.GetData("PrevBM"))
    current = comp.ActiveTool or previous_tool
    if not current:
        print("no previous tool to jump to")
        return
    if previous_tool:
        comp.SetActiveTool(previous_tool)
        comp.SetData("PrevBM", current.Name)


def close(ev):
    x, y = win.X(), win.Y()
    fu.SetData("BM.Position", [x, y])
    disp.ExitLoop()


def clear_all_bookmarks(ev):
    global data
    if not ev["modifiers"]["AltModifier"]:
        print("to delete all bookmarks press Reset with ALT key")
        return
    comp = fu.GetCurrentComp()
    flow = comp.CurrentFrame.FlowView
    comp.SetData("BM")
    data = {}
    refresh_table(data)
    print("all your bookmarks are belong to us")
    if fu.Version >= 17:
        all_bookmarks = flow.GetBookmarkList()
        for v in all_bookmarks.values():
            if v["Name"].startswith("[BM]"):
                flow.DeleteBookmark(v["Name"])


def remove_with_doubleclick(item):
    bookmark_name = sel.Text[0]
    tool_regID = sel.Text[2]
    selected_item = ev["item"].Text[2]
    print(selected_item)


def do_delete_native(name):
    if fu.Version < 17:
        return
    flow = comp.CurrentFrame.FlowView
    bookmark_name = "[BM] : {}".format(name)
    flow.DeleteBookmark(bookmark_name)


def delete_bookmark(ev):
    selected_items = itm["Tree"].SelectedItems()
    # print(dir(["Tree"]))
    flow = comp.CurrentFrame.FlowView
    for item in selected_items.values():
        bookmark_name = item.Text[0]
        tool_regID = item.Text[2]
        if tool_regID:
            del data[tool_regID]
            bookmark = "BM.{}".format(tool_regID)
        else:
            del data[bookmark_name]
            bookmark = "BM.{}".format(bookmark_name)
        comp.SetData(bookmark)
        do_delete_native(bookmark_name)
        # print("bookmark {} deleted".format(bookmark_name))
    refresh_table(data)
    if len(data) == 0:
        print("all bookmarks deleted")


def update_data(ev=None):
    comp = fu.GetCurrentComp()
    global data
    check_data = comp.GetData("BM")
    if not check_data:
        data = {}
        refresh_table(data)
        return
    if check_data and check_data != data:
        print("bookmarks list updated")
        data = check_data
        refresh_table(data)


def add_bookmark_run(ev):
    comp.RunScript("Scripts:Comp/Bookmarker/bookmark_add.py")


def launch_ui(ui):
    # Main Window
    x, y = fu.GetMousePos().values()
    if y < 90:
        y = 120
    disp = bmd.UIDispatcher(ui)
    button_size = [65, 25]
    win = disp.AddWindow(
        {
            "ID": "Bookmarker",
            "TargetID": "Bookmarker",
            "WindowTitle": "Jump to Bookmark",
            "Geometry": [x, y, 280, 300],
        },
        [
            ui.VGroup(
                {"Weight": 0},
                [

                    ui.HGroup(
                        [
                            ui.Tree(
                                {
                                    "ID": "Tree",
                                    "SortingEnabled": True,
                                    "Text": "Choose preset",
                                    "Events": {
                                        "ItemClicked": True,
                                        "ItemDoubleClicked": True,
                                        "CurrentItemChanged": True,
                                        "ItemActivated": True,
                                    },
                                },
                            ),

                        ]
                    ),

                    ui.HGroup(
                        {"Weight": 0},
                        [
                            # ui.Button(
                            #     {
                            #         "ID": "JumpBackButton",
                            #         "Text": "jump back",
                            #         "MinimumSize": button_size,
                            #     }
                            # ),
                            ui.Button(
                                {
                                    "ID": "AddButton",
                                    "Flat": False,
                                    "IconSize": [12, 12],
                                    "MinimumSize": [20, 12],
                                    "Icon": ui.Icon(
                                        {
                                            "File": "Scripts:Comp/Bookmarker/icons/plus_icon.png"
                                        }
                                    ),
                                }
                            ),
                            ui.Button(
                                {
                                    "ID": "DeleteBookmarkButton",
                                    "Text": "delete",
                                    "MinimumSize": button_size,
                                }
                            ),
                            ui.Button(
                                {
                                    "ID": "RemoveAllBookmarks",
                                    "Text": "reset",
                                    "MinimumSize": button_size,
                                }
                            ),
                            ui.Button(
                                {
                                    "ID": "refreshButton",
                                    "Flat": False,
                                    "IconSize": [12, 12],
                                    "Icon": ui.Icon(
                                        {
                                            "File": "Scripts:Comp/Bookmarker/icons/refresh_icon.png",
                                        }
                                    ),
                                }
                            ),
                        ]
                    ),
                    ui.HGroup(
                        {"Weight": 0},
                        [
                            ui.CheckBox(
                                {
                                    "ID": "MoveToolsToBookmarkPosition",
                                    "Text": "move selected to bookmark?",
                                }
                            )
                        ],
                    ),
                ],
            ),
        ],
    )
    itm = win.GetItems()


    return itm, win, disp


def data_cleanup(data=None):
    """
    remove Resolve17 __flags data
    """
    if not data:
        print("add some bookmarks!")
        data = {}
    else:
        data.pop("__flags", None)
        comp.SetData("BM", data)
    return data


if __name__ == "__main__":
    data = data_cleanup(comp.GetData("BM"))

    if fu.Version == 16.2:
        print("this version of Bookmarker is not compatible with Fusion v16.2")
        disp.ExitLoop()
    win = ui.FindWindow("Bookmarker")

    if win:
        win.Raise()
        win.ActivateWindow()
    else:
        itm, win, disp = launch_ui(ui)

        itm["Tree"].SetSelectionMode("ExtendedSelection")
        # set table headers
        hdr = itm["Tree"].NewItem()
        itm["Tree"].SetHeaderItem(hdr)
        hdr.Text[0] = "Bookmark Name"
        hdr.Text[1] = "Tool Type"
        hdr.Text[2] = "ToolRegID"  # hidden
        # set columns
        itm["Tree"].ColumnCount = 2
        itm["Tree"].ColumnWidth[0] = 160
        itm["Tree"].ColumnWidth[1] = 90
        # win.On.MyCombo.Activated = jump_bookmark
        win.On.DeleteBookmarkButton.Clicked = delete_bookmark
        win.On.RemoveAllBookmarks.Clicked = clear_all_bookmarks
        win.On.JumpBackButton.Clicked = jump_back
        win.On.Bookmarker.Close = close
        win.On.Tree.ItemDoubleClicked = jump_bookmark
        win.On.refreshButton.Clicked = update_data
        win.On.AddButton.Clicked = add_bookmark_run
        try:
            refresh_table(data)
        except AttributeError:
            print(
                "Something is wrong with Bookmarks database."
                "\nDelete all bookmarks to proceed and report this issue"
            )
        win.Show()
        disp.RunLoop()
        win.Hide()
