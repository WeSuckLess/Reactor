-- ============================================================================
-- modules
-- ============================================================================
local yaml = self and require("yaml") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vYAMLViewer"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\YAML\\Utility",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "View YAML in the Inspector.",
    REGS_OpIconString = FUSE_NAME,
    REG_TimeVariant = true, -- required to disable caching of the current time parameter
    REGB_Temporal = true, -- ensures reliability in Resolve 15
    REGS_IconID = "Icons.Tools.Icons.StickyNote",
})

function Create()
    -- [[ Creates the user interface. ]]
    InYAML = self:AddInput("Text", "Text", {
        LINKID_DataType = DATATYPE,
        LINK_Main = 1
    })

    InShowDump = self:AddInput("Show Dump", "ShowDump", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InSort = self:AddInput("Sort List", "Sort", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InUISeparator1 = self:AddInput("UISeparator1", "UISeparator1", {
        IC_Visible = true,
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
    })

    InDisplayLines = self:AddInput("Display Lines", "DisplayLines", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = true,
        INP_MaxScale = 100,
        INP_MinAllowed = 1,
        INP_Default = 25,
        LINK_Visible = true,
        INP_Passive = true,
        INP_DoNotifyChanged  = true,
    })

    InUISeparator2 = self:AddInput("UISeparator2", "UISeparator2", {
        IC_Visible = true,
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
    })

    InScriptVal = self:AddInput("YAML", "YAML", {
        LINKID_DataType = "ScriptVal",
        INPID_InputControl = "ScriptValListControl",
        IC_NoLabel = true,
        LC_Rows = 24,
        INP_Passive = true,
    })

    OutText = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InDisplayLines then
        local lines = InDisplayLines:GetSource(time, REQF_SecondaryTime).Value
        InScriptVal:SetAttrs({LC_Rows = lines})
        -- Toggle the visibility to refresh the inspector view
        InScriptVal:SetAttrs({IC_Visible = false})
        InScriptVal:SetAttrs({IC_Visible = true})
    end
end

function OnConnected(inp, old, new)
    if inp == InYAML and new ~= nil then
        -- New connection
    else
        InScriptVal:SetSource(ScriptValParam({}), 0)
    end
end

function OnAddToFlow()
    InScriptVal:SetSource(ScriptValParam({}), 0)
end

function Process(req)
    -- [[ Creates the output. ]]
    local yaml_str = InYAML:GetValue(req).Value
    local sort = InSort:GetValue(req).Value
    local show_dump = InShowDump:GetValue(req).Value

    local tbl = {}
    if yaml_str ~= "" then
        tbl = yaml.eval(yaml_str)
    else
        error("[YAML] The YAML string was empty.")
        -- print("[YAML] The YAML string was empty.")
    end

    -- Sort the array alphabetically
    if sort == 1.0 then
        table.sort(tbl)
    end

    if show_dump == 1 then
        print("\n----------------------")
        print("[" .. tostring(self.Name) .. "]")
        local txt_str = bmd.writestring(tbl)
        dump(txt_str)
    end

    InScriptVal:SetSource(ScriptValParam(tbl), 0)

    local out = Text(array_str)
    OutText:Set(req, out)
end
