--[[--
----------------------------------------------------------------------------
Open Kartaverse Lens Profiles Folder menu item - v5.74 2024-09-03
by Andrew Hazelden <andrew@andrewhazelden.com>
----------------------------------------------------------------------------
--]]--

local separator = package.config:sub(1,1)
-- Check for a pre-existing PathMap preference
local reactor_existing_pathmap = app:GetPrefs("Global.Paths.Map.Reactor:")
if reactor_existing_pathmap and reactor_existing_pathmap ~= "nil" then
	-- Clip off the "reactor_root" style trailing "Reactor/" subfolder
	reactor_existing_pathmap = string.gsub(reactor_existing_pathmap, "Reactor" .. separator .. "$", "")
end

local reactor_pathmap = os.getenv("REACTOR_INSTALL_PATHMAP") or reactor_existing_pathmap or "AllData:"
local path = app:MapPath(tostring(reactor_pathmap) .. 'Reactor/Deploy/Scripts/Support/Kartaverse/LensProfiles/')
if bmd.direxists(path) == false then
	bmd.createdir(path)
	print('[Created Kartaverse Lens Profiles Folder] ' .. path)
end

print('[Show Kartaverse Lens Profiles Folder] ' .. path)
bmd.openfileexternal('Open', path)
