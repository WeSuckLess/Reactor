Atom {
	Name = "Vonk | FusionCBOR",
	Category = "Kartaverse/Vonk Ultra/Modifiers",
	Author = "Andrew Hazelden",
	Version = 1.9,
	Date = {2024, 10, 12},
	Description = [[<p>FusionCBOR is a node based Concise Binary Object Representation (CBOR) encoding/decoding library for Blackmagic Design Fusion. CBOR is a compact data serialization format that is saved in a binary encoded file. It can be used to represent data structures that come from formats like JSON and ScriptVal (Lua Tables).</p>

<p>Open-Source License<br>
The Vonk fuses are licensed under a GPL v3 license.</p>

<p>The original Spicy Acorn Vonk toolset was created by<br>
<a href="mailto:xmnr0x23@gmail.com">Kristof Indeherberge</a><br>
<a href="mailto:duriau.cedric@live.be">Cédric Duriau</a></p>

<p>The Vonk Ultra fork is maintained by:<br>
<a href="mailto:andrew@andrewhazelden.com">Andrew Hazelden</a></p>

<h2>For More Information</h2>
<p><a href="http://cbor.io/">http://cbor.io/</a></p>]],
	Deploy = {
		"Comps/Kartaverse/Vonk Ultra/Demo CBOR/CBOR/Test.cbor",
		"Comps/Kartaverse/Vonk Ultra/Demo CBOR/CBOR/Test_Write.cbor",
		"Comps/Kartaverse/Vonk Ultra/Demo CBOR/Demo CBOR.comp",
		"Fuses/Kartaverse/Vonk Ultra/CBOR/JSON/vJSONFromCBORFile.fuse",
		"Fuses/Kartaverse/Vonk Ultra/CBOR/JSON/vJSONToCBORFile.fuse",
		"Fuses/Kartaverse/Vonk Ultra/CBOR/ScriptVal/vScriptValFromCBORFile.fuse",
		"Fuses/Kartaverse/Vonk Ultra/CBOR/ScriptVal/vScriptValToCBORFile.fuse",
	},
	Dependencies = {
			"com.Zash.Lua-CBOR",
	},
}
