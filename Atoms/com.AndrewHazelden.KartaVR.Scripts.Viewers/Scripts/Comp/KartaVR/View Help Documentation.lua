--[[--
View Help Documentation - v5.73 2027-07-02
by Andrew Hazelden <andrew@andrewhazelden.com>
https://github.com/kartaverse

Overview:
The View Help Documentation script will open a web browser window to and display the HTML formatted help documentation.

How to use the Script:
Step 1. Start Fusion and open a new comp. Then run the Script > KartaVR > View Help Documentation menu item.

--]]--

-- --------------------------------------------------------
-- Print out extra debugging information
local printStatus = false

-- Open a web browser window up with the help documentation
function openBrowser()
	webpage = "https://github.com/kartaverse"
	bmd.openurl(webpage)
end

-- Open a web browser window up with the help documentation
openBrowser()

-- End of the script
print('[Done]')
