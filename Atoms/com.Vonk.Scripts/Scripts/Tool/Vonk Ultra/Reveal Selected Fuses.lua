--[[--
Reveal Selected Fuses.lua - 1.0 2025-02-24
By Andrew Hazelden <andrew@andrewhazelden.com>

A tool script that reads the active Nodes view selection and then opens a desktop Explorer/Finder/Nautilus file browser window to show the containing folder that holds the selected fuse.

--]]--


-- Check the current operating system platform
platform = (FuPLATFORM_WINDOWS and 'Windows') or (FuPLATFORM_MAC and 'Mac') or (FuPLATFORM_LINUX and 'Linux')

-- Add the platform specific folder slash character
osSeparator = package.config:sub(1,1)

-- Find out the current directory from a file path
-- Example: print(Dirname('/Volumes/Media/image.0000.exr'))
function Dirname(filename)
	return filename:match('(.*' .. tostring(osSeparator) .. ')')
end

-- Open a folder window up using your desktop file browser
function OpenDirectory(mediaDirName)
	path = Dirname(mediaDirName)
	-- print('[Open Containing Folder] ' .. path)
	bmd.openfileexternal('Open', path)
end

if tool and tool.ID:match("^Fuse.") then
    -- List all fuse filenames
    local reg = fusion:GetRegList()
    for k,v in ipairs(reg) do
        if v ~= nil then
            if v.ID:match(tool.ID)  then
                attr = v:GetAttrs()
                fuse = attr.REGS_FileName
                print("[Reveal Selected Fuses] ", fuse)
                OpenDirectory(fuse)
                break
            end
        end
    end
end


