Atom {
	Name = "Make Local Button Script",
	Category = "Scripts",
	Author = "Bryan Ray",
	Version = 2.5,
	Date = {2023, 7, 31},
	Description = [[<p>The "Make Local Button Script" package adds a custom Button Control to your Loader nodes via a Default setting file. You can use the "Make Local" button to copy the media in the Loader into a Comp PathMap managed folder location.</p>
	<p>The script, which will be found in the /Reactor/Deploy/Scripts/Support folder, is easily modified for use in your pipeline. Instructions are in the comments at the beginning of the Loader_MakeLocal.lua file.</p>
	<p>Without customization, the script will copy assets to a sub-folder called 'elements' in the same folder as the .comp file. The composition must be saved before the button will work.</p>
	<p>Because the script is designed to run only from the button, it does not show up in any of the typical script menus within Fusion.</p>
	<p>As a part of the installation process, this Atom will attempt to detect existing Loader default setting files, and if any are found, it will insert the button into those instead of creating a new default. When removed, the button is deleted from all setting files, but any installed Defaults will not be removed, in case you have made additional changes later.]],
	Donation = {
		URL = [[paypal.me/BryanRayVFX]],
		Amount = "$2 USD",
	},

	Deploy = {
		"Scripts/Support/Loader_MakeLocal.lua",
		"Defaults/Loader_Loader.temp",
	},
	InstallScript = {
		[==[
		dprintf("\n[Make Local] Installing Defaults File\n\n")

		osSeparator = package.config:sub(1,1)
		defaultsPath = fu:MapPath("Reactor:")..'Deploy'..osSeparator..'Defaults'..osSeparator
	
		-- Search Defaults pathmaps
		dprintf("[Defaults Folders] Scanning PathMaps\n")

		INSERT_STRING = [=[UserControls = ordered() {
			MakeLocal = {
				LINKS_Name = "Make Local",
				INPID_InputControl = "ButtonControl",
				BTNCS_Execute = [[
				args = { tool = comp.ActiveTool, copyTree = true }
				path = comp:MapPath("Scripts:Support/Loader_MakeLocal.lua")
				comp:RunScript(path, args)
				]],
				IC_ControlPage = 0,
				LINKID_DataType = "Number",
				INP_Default = 0,
				},]=]
			

		function IterateDefaultSettings()
			status = false
			for i, path in ipairs(fu:MapPathSegments("defaults:")) do
				for j, file in ipairs(bmd.readdir(path .. "*.setting")) do
					if file.Name == "Loader_Loader.setting" then
						status = true
						file_path = path .. file.Name
						AddUserControls(file_path)
					end
				end
			end
			return status
		end


		function AddUserControls(filePath)
			-- UserControl to be inserted into Loader_Loader.setting
			readFile = io.open(filePath, "r")
			if not readFile then
				dprintf("file not found")
				return
			end
			data = readFile:read("*all")
			-- Scan the file for a UserControls section
			findString = "UserControls = ordered%(%) {"
			k, l = string.find(data, findString)
			if l then
				-- Insert button into existing UserControls table
				dprintf("[Loader_Loader.setting] UserControls detected at " .. filePath .."\n")
				newData = string.gsub(data, findString, INSERT_STRING)
			else
				-- Insert new UserControls table
				dprintf("[Loader_Loader.setting] No UserControls.\n")
				newData = string.gsub(data, "CtrlWZoom = false,", "CtrlWZoom = false,\n			" .. INSERT_STRING .. "          },")
			end
	
			-- Close the file, then reopen to overwrite
			readFile:close()
	
			rewriteFile = io.open(filePath, "wb")
			if rewriteFile then
				io.output(rewriteFile)
				io.write(newData)
				io.close(rewriteFile)
			end
		end


		function RenameTempFile(tempSetting)
			if not bmd.fileexists(tempSetting) then
				return
			end
			dprintf("[Loader_Loader.setting] No existing default files found. Settings created from temp file.\n")
			-- Rename Loader_Loader.temp to Loader_Loader.setting
			os.rename(tempSetting, defaultsPath .. 'Loader_Loader.setting')
		end


		function DeleteTempFile(tempSetting)
			if not bmd.fileexists(tempSetting) then
				return
			end
			dprintf("[Loader_Loader.temp] Button insertion successful. Temp file deleted.\n")
			-- Delete Loader_Loader.temp
			os.remove(tempSetting)
		end


		defaultFound = IterateDefaultSettings()
		tempSetting = defaultsPath .. 'Loader_Loader.temp'

		if defaultFound then
		-- If setting file was successfully modified, delete the temp file in the Reactor folder. 
			DeleteTempFile(tempSetting)
		else
		-- Otherwise, rename it.
			RenameTempFile(tempSetting)
		end
		dprintf("Install script run successfully\n")

		]==],
	},
	UninstallScript = {

		[==[
			
		dprintf("\n[Make Local] Uninstalling Defaults File\n\n")
		local osSeparator = package.config:sub(1,1)

		-- Find all existing Loader default setting files
		dprintf("[Defaults Folders] Scanning PathMaps\n")

		function RemoveUserControls(filePath)
			dprintf("[Setting file found] "..filePath .. "\n")

			-- read file to memory
			settingsFile = io.open(filePath, "r")

			if settingsFile then
				data = settingsFile:read("*all")
				-- Delete the MakeLoader button
				startIndex, _ = string.find(data, "MakeLocal = ")
				if not startIndex then
					dprintf("[Make Local] user contols not found\n")
					return
                end
				newdata = data:sub(1, startIndex-1) .. data:sub(startIndex+356, #data)

				-- Close the file, then reopen to overwrite
				settingsFile:close()

				settingsRewrite = io.open(filePath, "wb")
				if settingsRewrite then
					io.output(settingsRewrite)
					io.write(newdata)
					io.close(settingsRewrite)
                end
				dprintf("[Make Local] button removed from Loader User Controls\n")
            end
		end

		function IterateDefaultSettings()
			status = false
			for i, path in ipairs(fu:MapPathSegments("defaults:")) do
				for j, file in ipairs(bmd.readdir(path .. "*.setting")) do
					if file.Name == "Loader_Loader.setting" then
						status = true
						file_path = path .. file.Name
						RemoveUserControls(file_path)
					end
				end
			end
			return status
		end

		defaultFound = IterateDefaultSettings()
		if not defaultFound then
			dprintf("No files processed by Uninstall script\n")
		else
			dprintf("Uninstall script run successfully\n")
		end

		]==],
	},
}