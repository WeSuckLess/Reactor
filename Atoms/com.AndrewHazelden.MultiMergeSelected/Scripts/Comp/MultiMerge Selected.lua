--[[--
MultiMerge Selected - 2024-07-04 03.30 AM
By Andrew Hazelden (andrew@andrewhazelden.com)

Overview:
Inserts a new MultiMerge node and auto-connects the selected nodes as input connections.

Installation:
Copy the "MultiMerge Selected.lua" script to the "Scripts:/Comp" PathMap folder.

Usage:
1. Select several nodes in the flow area.
2. Launch the "Scripts > MultiMerge Selected" menu item. 
3. In the "MultiMerge Selected" window use the Sort Order control to choose if you would like to sort the node connections by "Vertical Position", "Horizontal Position", or "Node Name". 

The "Alpha Gain Zero" checkbox allows you to disable the MultiMerge node's gain setting. This helps with light select pass compositing.

Click the OK button to continue.

4. The script will add a new MultiMerge node. The selected nodes in the flow area will be automatically connected to the MultiMerge node input connections.

Known Issue:
Using a Fusion window layout that doesn't use a flow area on the main monitor can result in an error message of:

This script can't find the Fusion flow view. Try using a single monitor layout if you want to sort the node input connections by "Vertical Position" or "Horizontal Position".

--]]--



-------------------------------------------------------------------------------
-- Read a fusion specific preference value. If nothing exists set and return a default value
-- Example: splitDirection = getPreferenceData("MMrgSelected.sort", 1, true)
function getPreferenceData(pref, defaultValue, debugPrint)
	-- Choose if you are saving the preference to the comp or to all of fusion
	-- local newPreference = comp:GetData(pref)
	local newPreference = fu:GetData(pref)

	if newPreference ~= nil then
		-- List the existing preference value
		if (debugPrint == true) or (debugPrint == 1) then
			if newPreference == nil then
				print("[Reading " .. tostring(pref) .. " Preference Data] " .. "nil")
			else
				print("[Reading " .. tostring(pref) .. " Preference Data] " .. tostring(newPreference))
			end
		end
	else
		-- Force a default value into the preference & then list it
		newPreference = defaultValue

		-- Choose if you are saving the preference to the comp or to all of fusion
		-- comp:SetData(pref, defaultValue)
		fu:SetData(pref, defaultValue)

		if (debugPrint == true) or (debugPrint == 1) then
			if newPreference == nil then
				print("[Creating " .. tostring(pref) .. " Preference Data] " .. "nil")
			else
				print("[Creating ".. tostring(pref) .. " Preference Entry] " .. tostring(newPreference))
			end
		end
	end

	return newPreference
end


-------------------------------------------------------------------------------
-- Set a fusion specific preference value
-- Example: setPreferenceData("MMrgSelected.sort", 1, true)
function setPreferenceData(pref, value, debugPrint)
	-- Choose if you are saving the preference to the comp or to all of fusion
	-- comp:SetData(pref, value)
	fu:SetData(pref, value)

	-- List the preference value
	if (debugPrint == true) or (debugPrint == 1) then
		if value == nil then
			print("[Setting " .. tostring(pref) .. " Preference Data] " .. "nil")
		else
			print("[Setting " .. tostring(pref) .. " Preference Data] " .. tostring(value))
		end
	end
end

function Main()
	local sort = getPreferenceData("MMrgSelected.sort", 1, 0)
	local autoNameLayers = getPreferenceData("MMrgSelected.autoNameLayers", 1, 0)
	local alphaGain = getPreferenceData("MMrgSelected.alphaGain", 1, 0)

	local ui = fu.UIManager
	local disp = bmd.UIDispatcher(ui)
	local width,height = 300,125

	win = disp:AddWindow({
		ID = "MultiMerge",
		TargetID = "MultiMerge",
		WindowTitle = "MultiMerge Selected",
		Geometry = {100, 100, width, height},
		Spacing = 10,
	
		ui:VGroup{
			ID = "root",
	
			ui:HGroup{
				Weight = 0.01,
				ui:Label{
					ID = "SortLabel",
					Text = "Sort Order",
				},
				ui:ComboBox{
					ID = "SortMenu",
					Text = "Sort Order",
				},
			},
			ui:CheckBox{
				ID = "AutoNameLayers",
				Text = "Auto Name Layers",
				Checked = autoNameLayers or true,
			},
			ui:CheckBox{
				ID = "AlphaGain",
				Text = "Alpha Gain Zero",
				Checked = alphaGain or true,
			},
			ui:Button{
				Weight = 0.01,
				ID = "OKButton",
				Text = "OK",
			},
		},
	})
	
	-- The window was closed
	function win.On.MultiMerge.Close(ev)
		disp:ExitLoop()
	end
	
	-- Add your GUI element based event functions here:
	itm = win:GetItems()
	
	-- Add the items to the ComboBox menu
	itm.SortMenu:AddItem("Vertical Position")
	itm.SortMenu:AddItem("Horizontal Position")
	itm.SortMenu:AddItem("Node Name")
	-- Restore the SortMenu preference
	itm.SortMenu.CurrentIndex = sort

	-- The app:AddConfig() command that will capture the "Control + W" or "Control + F4" hotkeys so they will close the window instead of closing the foreground composite.
	app:AddConfig("MultiMerge", {
		Target {
			ID = "MultiMerge",
		},
	
		Hotkeys {
			Target = "MultiMerge",
			Defaults = true,
	
			CONTROL_W = "Execute{cmd = [[app.UIManager:QueueEvent(obj, 'Close', {})]]}",
			CONTROL_F4 = "Execute{cmd = [[app.UIManager:QueueEvent(obj, 'Close', {})]]}",
			ESCAPE = "Execute{cmd = [[app.UIManager:QueueEvent(obj, 'Close', {})]]}"
		},
	})
	
	function win.On.OKButton.Clicked(ev)
		if itm.SortMenu.CurrentIndex == 0 then
			sort = "Vertical Position"
		elseif itm.SortMenu.CurrentIndex == 1 then
			sort = "Horizontal Position"
		elseif itm.SortMenu.CurrentIndex == 2 then
			sort = "Node Name"
		end
		
		autoNameLayers = itm.AutoNameLayers.Checked
		alphaGain = itm.AlphaGain.Checked

		disp:ExitLoop()
	end
	
	win:Show()
	disp:RunLoop()
	win:Hide()

	print("MultiMerge Selected")
	print("[Sort by] ", sort)
	print("[Auto Name Layers] ", autoNameLayers)
	print("[Alpha Gain Zero] ", alphaGain)

	setPreferenceData("MMrgSelected.sort", itm.SortMenu.CurrentIndex, VERBOSE)
	setPreferenceData("MMrgSelected.autoNameLayers", itm.AutoNameLayers.Checked, VERBOSE)
	setPreferenceData("MMrgSelected.alphaGain", itm.AlphaGain.Checked, VERBOSE)

	-- Read the selection
	local tools = comp:GetToolList(true)
	local selectedTool = tool or comp.ActiveTool or tools and tools[1]
	if selectedTool then
		-- Check the selected node"s output type
		toolOutput = selectedTool:FindMainOutput(1)
		if toolOutput ~= nil then
			toolType = toolOutput:GetAttrs().OUTS_DataType
	
			-- Lock the comp flow area
			comp:Lock()
	
			-- Add the correct MultiMerge node
			local mmrg
			if toolType == "Image" then
				if comp and comp.CurrentFrame and comp.CurrentFrame.FlowView then
					if sort == "Vertical Position" then
					table.sort(tools, function(a,b) return select(2, comp.CurrentFrame.FlowView:GetPos(a)) > select(2, comp.CurrentFrame.FlowView:GetPos(b)) end)
					elseif sort == "Horizontal Position" then
						table.sort(tools, function(a,b) return select(1, comp.CurrentFrame.FlowView:GetPos(a)) < select(1, comp.CurrentFrame.FlowView:GetPos(b)) end)
					elseif sort == "Node Name" then
						table.sort(tools, function(a,b) return a:GetAttrs().TOOLS_Name < b:GetAttrs().TOOLS_Name end)
					end
				else
					print("[Error] This script can't find the Fusion flow view. Try using a single monitor layout if you want to sort the node input connections by \"Vertical Position\" or \"Horizontal Position\".")
				end
				
				mmrg = comp:AddTool("MultiMerge", -32768, -32768)
				print("[Added Node] ", mmrg.Name)
				
				-- Connect the inputs
				for k,v in pairs(tools) do
					if k == 1 then
						-- Connect the Background Input
						mmrg:ConnectInput("Background", tools[k])
						print(string.format("[%03d][Connection] %30s -> %s", k, tostring(tools[k].Name), tostring(mmrg.Name) .. ".Background"))
					else
						-- Connect the "Layer#.Foreground" Inputs
						mmrg:ConnectInput("Layer" .. (k-1)  .. ".Foreground", tools[k])
						if itm.AutoNameLayers.Checked == true then
							mmrg["LayerName" .. (k-1)][fu.TIME_UNDEFINED] = tools[k].Name
						end
						if itm.AlphaGain.Checked == true then
							mmrg["Layer" .. (k-1)  .. ".Gain"][fu.TIME_UNDEFINED] = 0
						end
						print(string.format("[%03d][Connection] %30s -> %s", k, tostring(tools[k].Name), tostring(mmrg.Name)  .. ".Layer" .. (k-1)  .. ".Foreground"))
					end
				end
			end
	
			-- Unlock the comp flow area
			comp:Unlock()
		end
	end
end

Main()
print("[Done]")
