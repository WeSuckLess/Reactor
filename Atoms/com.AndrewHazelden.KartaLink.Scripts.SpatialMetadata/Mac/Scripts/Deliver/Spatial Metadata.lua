--[[--
Spatial Metadata 2024-10-01 04.15 AM
By Andrew Hazelden <andrew@andrewhazelden.com>


Overview:
A Resolve Deliver page script can be used to automatically create an MV-HEVC encoded video for the current render job.

Script Usage:
1. Switch to the Deliver page.

2. In the Render Settings dialog, click on the "Video" tab. Expand the "Advanced Settings" section.

3. Enable the "[x] Trigger script at" checkbox and set it to "[End] of render job". In the Script combobox menu select the "Spatial Metadata" entry.

4. On the Deliver page, press the "Add to Render Queue" button.


Console Output:
[Spatial Metadata]
Deliver Page Script by Andrew Hazelden <andrew@andrewhazelden>
Powered by the Spatial CLI tool by Mike Swanson

[Job Details]
table: 0x0104417280
	MarkIn = 108000
	MarkOut = 108286
	AudioBitDepth = 24
	AudioSampleRate = 48000
	ExportAlpha = false
	TargetDir = /Users/vfx/Desktop
	RenderMode = Single clip
	OutputFilename = DF ProRes.mov
	PresetName = Custom
	VideoFormat = QuickTime
	VideoCodec = Apple ProRes 422 HQ
	AudioCodec = lpcm
	NetworkOptimization = false
	RenderJobName = Job 1
	TimelineName = DF ProRes
	IsExportVideo = true
	IsExportAudio = true
	FormatWidth = 7680
	FormatHeight = 4320
	JobId = 929d5a8b-6362-4634-8245-31d87afacd28
	FrameRate = "29.97"
	PixelAspectRatio = 1

[Job Status]
table: 0x01043d6488
	CompletionPercentage = 100
	JobStatus = Complete
	TimeTakenToRenderInMs = 4130

[Spatial Metadata] Resolve Render Completed in 00:4.13 (MM:SS.SS)

[Status] Checking if the Spatial CLI program is installed at: /opt/homebrew/bin/spatial

[Fusion Prefs]
table: 0x0104401078
	HAdjust = 0
	HFOV = 180
	ImageProjection = halfEquirect
	Format = sbs
	PrimaryEye = left
	InteraxialLensSeparation = 60

[Media]
table: 0x0104415768
	1 = /Users/vfx/Desktop/DF ProRes.mov

[Processing Footage] 1 clip found


[Clip #] 1 [Input Filename] /Users/vfx/Desktop/DF ProRes.mov [Output Filename] /Users/vfx/Desktop/DF ProRes_injected.mov

"/opt/homebrew/bin/spatial" make --input "/Users/vfx/Desktop/DF ProRes.mov" --format=sbs --output "/Users/vfx/Desktop/DF ProRes_injected.mov" --cdist=60 --hfov=180 --hadjust=0 --primary=left --projection=halfEquirect
 Input: /Users/vfx/Desktop/DF ProRes.mov (sideBySide)
Output: /Users/vfx/Desktop/DF ProRes_injected.mov (spatial/mv-hevc)
Overwrite /Users/vfx/Desktop/DF ProRes_injected.mov? (y/n) frame=       1 fps=  5.5 size=   524.3KB time=00:00:00 bitrate=       0 bps speed=  0.0x progress=  0.0%
frame=      21 fps= 36.7 size=     1.2MB time=00:00:00 bitrate=    14.6Mbps speed=  1.2x progress=  7.0%
frame=      28 fps= 13.6 size=     1.6MB time=00:00:00 bitrate=    14.1Mbps speed=  0.8x progress=  9.4%
frame=      35 fps= 13.6 size=     2.3MB time=00:00:01 bitrate=    16.0Mbps speed=  0.7x progress= 11.8%
frame=      42 fps= 13.6 size=     2.7MB time=00:00:01 bitrate=    15.8Mbps speed=  0.7x progress= 14.3%
frame=      49 fps= 13.7 size=     3.3MB time=00:00:01 bitrate=    16.3Mbps speed=  0.6x progress= 16.7%
frame=      56 fps= 13.7 size=     3.7MB time=00:00:01 bitrate=    16.1Mbps speed=  0.6x progress= 19.2%
frame=      63 fps= 13.6 size=     4.4MB time=00:00:02 bitrate=    17.0Mbps speed=  0.6x progress= 21.6%
frame=      70 fps= 13.4 size=     5.2MB time=00:00:02 bitrate=    18.2Mbps speed=  0.6x progress= 24.0%
frame=      77 fps= 13.7 size=     6.7MB time=00:00:02 bitrate=    21.1Mbps speed=  0.5x progress= 26.5%
frame=      84 fps= 13.6 size=     7.3MB time=00:00:02 bitrate=    21.0Mbps speed=  0.5x progress= 28.9%
frame=      91 fps= 13.6 size=     7.8MB time=00:00:03 bitrate=    20.9Mbps speed=  0.5x progress= 31.4%
frame=      98 fps= 13.6 size=     8.5MB time=00:00:03 bitrate=    21.0Mbps speed=  0.5x progress= 33.8%
frame=     105 fps= 13.8 size=     9.0MB time=00:00:03 bitrate=    20.7Mbps speed=  0.5x progress= 36.2%
frame=     112 fps= 13.6 size=     9.6MB time=00:00:03 bitrate=    20.7Mbps speed=  0.5x progress= 38.7%
frame=     119 fps= 13.5 size=    10.4MB time=00:00:03 bitrate=    21.1Mbps speed=  0.5x progress= 41.1%
frame=     126 fps= 13.5 size=    11.1MB time=00:00:04 bitrate=    21.3Mbps speed=  0.5x progress= 43.6%
frame=     133 fps= 13.7 size=    11.7MB time=00:00:04 bitrate=    21.3Mbps speed=  0.5x progress= 46.0%
frame=     140 fps= 13.6 size=    13.2MB time=00:00:04 bitrate=    22.8Mbps speed=  0.5x progress= 48.4%
frame=     147 fps= 13.6 size=    13.7MB time=00:00:04 bitrate=    22.6Mbps speed=  0.5x progress= 50.9%
frame=     154 fps= 13.6 size=    14.3MB time=00:00:05 bitrate=    22.4Mbps speed=  0.5x progress= 53.3%
frame=     161 fps= 13.6 size=    14.8MB time=00:00:05 bitrate=    22.2Mbps speed=  0.5x progress= 55.7%
frame=     168 fps= 13.4 size=    16.1MB time=00:00:05 bitrate=    23.1Mbps speed=  0.5x progress= 58.2%
frame=     175 fps= 13.5 size=    17.5MB time=00:00:05 bitrate=    24.1Mbps speed=  0.5x progress= 60.6%
frame=     182 fps= 13.5 size=    18.6MB time=00:00:06 bitrate=    24.6Mbps speed=  0.5x progress= 63.1%
frame=     189 fps= 13.6 size=    19.8MB time=00:00:06 bitrate=    25.3Mbps speed=  0.5x progress= 65.5%
frame=     196 fps= 13.6 size=    22.0MB time=00:00:06 bitrate=    27.0Mbps speed=  0.5x progress= 67.9%
frame=     203 fps= 13.3 size=    23.2MB time=00:00:06 bitrate=    27.6Mbps speed=  0.5x progress= 70.4%
frame=     210 fps= 13.6 size=    24.4MB time=00:00:06 bitrate=    28.0Mbps speed=  0.5x progress= 72.8%
frame=     217 fps= 13.6 size=    25.8MB time=00:00:07 bitrate=    28.6Mbps speed=  0.5x progress= 75.3%
frame=     224 fps= 13.5 size=    26.9MB time=00:00:07 bitrate=    28.9Mbps speed=  0.5x progress= 77.7%
frame=     231 fps= 13.5 size=    28.1MB time=00:00:07 bitrate=    29.3Mbps speed=  0.5x progress= 80.1%
frame=     238 fps= 13.5 size=    29.1MB time=00:00:07 bitrate=    29.4Mbps speed=  0.5x progress= 82.6%
frame=     245 fps= 13.6 size=    30.1MB time=00:00:08 bitrate=    29.6Mbps speed=  0.5x progress= 85.0%
frame=     252 fps= 13.5 size=    31.1MB time=00:00:08 bitrate=    29.7Mbps speed=  0.5x progress= 87.5%
frame=     259 fps= 13.5 size=    33.0MB time=00:00:08 bitrate=    30.7Mbps speed=  0.5x progress= 89.9%
frame=     266 fps= 13.4 size=    34.0MB time=00:00:08 bitrate=    30.8Mbps speed=  0.5x progress= 92.3%
frame=     273 fps= 13.6 size=    34.9MB time=00:00:09 bitrate=    30.8Mbps speed=  0.5x progress= 94.8%
frame=     280 fps= 13.5 size=    35.9MB time=00:00:09 bitrate=    30.8Mbps speed=  0.5x progress= 97.2%
frame=     287 fps= 13.5 size=    36.7MB time=00:00:09 bitrate=    30.8Mbps speed=  0.5x progress= 99.7%
frame=     287 fps=  0.0 size=    36.8MB time=00:00:09 bitrate=    30.8Mbps speed=  0.5x progress=100.0%

Time: 21.55s
[Done]

--]]--


-- Spatial program filepath
local spatialProgram = [[/opt/homebrew/bin/spatial]]

-- Find out the current operating system platform. The platform local variable should be set to either "Windows", "Mac", or "Linux".
local platform = (FuPLATFORM_WINDOWS and "Windows") or (FuPLATFORM_MAC and "Mac") or (FuPLATFORM_LINUX and "Linux")

function GetProject()
	-- Get the current Resolve timeline
	local res = app:GetResolve()
	if res then
		local pm = res:GetProjectManager()
		local project = pm:GetCurrentProject()
		local mp = project:GetMediaPool()
		local folder = mp:GetCurrentFolder()
		if project then
			return project
		else
			print("\n[Spatial Metadata][Error] No Resolve project is open at this time.")
			return nil
		end
	end
end

function GetTimeline()
	local project = GetProject()
	local timeline = project:GetCurrentTimeline()

	-- Fallback timeline hunting
	if not timeline then
		if project:GetTimelineCount() > 0 then
			timeline = project:GetTimelineByIndex(1)
			project:SetCurrentTimeline(timeline)
		end
	end

	-- We have a timeline
	if timeline then
		return timeline
	else
		print("\n[Spatial Metadata][Error]] No Resolve timeline is open at this time.")
		return nil
	end
end

function GetRenderJob(project, jobId)
	if project then
		jobList = project:GetRenderJobList()
		for i, jobDetail in ipairs(jobList) do
			if jobDetail["JobId"] == jobId then
				return jobDetail
			end
		end
	end

	print("\n[Spatial Metadata][Error] No Resolve project is open at this time.")
	return ""
end

-- -------------------------------------------------
-- -------------------------------------------------
-- parseSequenceFilename()
-- 
-- Process Resolve Media Pool/Media page centric file paths like:
-- D:\Media\Image.[0001-0240].exr
-- D:\Media\Image.0001.exr
-- D:\Media\Image_0001.exr
-- D:\Media\Image.exr
-- D:\Media\Image.mp4
--
-- Code adapted from bmd.scriptlib function "bmd.parseFilename(filename)"
--
-- This is a great function for ripping a filepath into little bits
-- returns a table with the following
--
-- FullPath     : The raw, original path sent to the function
-- Path         : The path, without filename
-- FullName     : The name of the clip w\ extension
-- Name         : The name without extension
-- CleanName    : The name of the clip, without extension or sequence
-- SNumStart    : The original starting frame sequence string, or "" if no sequence
-- SNumEnd      : The original ending frame sequence string, or "" if no sequence
-- FrameStart   : The starting frame sequence as a numeric value, or nil if no sequence
-- FrameEnd     : The ending frame sequence as a numeric value, or nil if no sequence
-- Extension    : The raw extension of the clip
-- Padding      : Amount of padding in the sequence, or nil if no sequence
-- UN           : A true or false value indicating whether the path is a UNC path or not
------------------------------------------------------------------------------
function parseSequenceFilename(filename)
	local seq = {}
	seq.FullPath = filename
	string.gsub(seq.FullPath, "^(.+[/\\])(.+)", function(path, name) seq.Path = path;seq.FullName = name end)
	--string.gsub(seq.FullName, "^(.+)(%..+)%[%..+%]$", function(name, ext) seq.Name = name;seq.Extension = ext end)
	string.gsub(seq.FullName, "^(.+)(%..+)$", function(name, ext) seq.Name = name;seq.Extension = ext end)

	if not seq.Name then -- no extension?
		seq.Name = seq.FullName
	end

	if string.match(seq.Name, "%[.-%-.-%]$") then
		-- Match Video/Video + Audio clip types with image sequence filenames like: "/Path/Name.[####-####].ext"
		string.gsub(seq.Name, "^(.-)%[(%d+)%-(%d+)%]$", function(name, SNumStart, SNumEnd) seq.CleanName = name;seq.SNumStart = SNumStart;seq.SNumEnd = SNumEnd end)
	else
		-- Match Stills/Video/Video + Audio/Geometry/Subtitles clip types with filenames like: "/Path/Name.####.ext" or "/Path/Name.ext"
		string.gsub(seq.Name, "^(.-)(%d+)$", function(name, SNum) seq.CleanName = name;seq.SNumStart = SNum;seq.SNumEnd = SNum end)
	end

	if seq.SNumStart then
		seq.FrameStart = tonumber(seq.SNumStart)
		seq.Padding = string.len(seq.SNumStart)
	else
		seq.SNumStart = ""
		seq.CleanName = seq.Name
	end

	if seq.SNumEnd then
		seq.FrameEnd = tonumber(seq.SNumEnd)
	else
		seq.SNumEnd = ""
	end

	if seq.Extension == nil then seq.Extension = "" end
	seq.UNC = (string.sub(seq.Path, 1, 2) == [[\\]])

	return seq
end

----------------------------------------------------
-- Read a fusion specific preference value. If nothing exists set and return a default value
-- Example: sort = getPreferenceData("KartaLink.SpatialMedia.sort", 1, true)
function getPreferenceData(pref, defaultValue, debugPrint)
	-- Choose if you are saving the preference to the comp or to all of fusion
	-- local newPreference = comp:GetData(pref)
	local newPreference = fu:GetData(pref)

	if newPreference ~= nil then
		-- List the existing preference value
		if (debugPrint == true) or (debugPrint == 1) then
			if newPreference == nil then
				print("[Reading " .. tostring(pref) .. " Preference Data] " .. "nil")
			else
				print("[Reading " .. tostring(pref) .. " Preference Data] " .. tostring(newPreference))
			end
		end
	else
		-- Force a default value into the preference & then list it
		newPreference = defaultValue

		-- Choose if you are saving the preference to the comp or to all of fusion
		-- comp:SetData(pref, defaultValue)
		fu:SetData(pref, defaultValue)

		if (debugPrint == true) or (debugPrint == 1) then
			if newPreference == nil then
				print("[Creating " .. tostring(pref) .. " Preference Data] " .. "nil")
			else
				print("[Creating ".. tostring(pref) .. " Preference Entry] " .. tostring(newPreference))
			end
		end
	end

	return newPreference
end

-------------------------------------------------------------------------------
-- Set a fusion specific preference value
-- Example: setPreferenceData("KartaLink.SpatialMedia.sort", 1, true)
-------------------------------------------------------------------------------
function setPreferenceData(pref, value, debugPrint)
	-- Choose if you are saving the preference to the comp or to all of fusion
	-- comp:SetData(pref, value)
	fu:SetData(pref, value)

	-- List the preference value
	if (debugPrint == true) or (debugPrint == 1) then
		if value == nil then
			print("[Setting " .. tostring(pref) .. " Preference Data] " .. "nil")
		else
			print("[Setting " .. tostring(pref) .. " Preference Data] " .. tostring(value))
		end
	end
end

function GenerateMediaList(details)
	local mediaTbl = {}
	local path = tostring(details["TargetDir"]) .. "/" .. tostring(details["OutputFilename"])
	table.insert(mediaTbl, path)

	return mediaTbl
end

function UserSettings()
	local prefsTbl = {}

	local StereoFormatCombo = getPreferenceData("KartaLink.SpatialMedia.StereoFormat", "Side By Side", 0)
	local PrimaryEyeCombo = getPreferenceData("KartaLink.SpatialMedia.PrimaryEye", "Left", 0)
	local InteraxialLensSeparationSpinner = getPreferenceData("KartaLink.SpatialMedia.InteraxialLensSeparation", 60.0, 0)
	local HFOVSpinner = getPreferenceData("KartaLink.SpatialMedia.HFOV", 180, 0)
	local ImageProjectionCombo = getPreferenceData("KartaLink.SpatialMedia.ImageProjection", "180VR", 0)
	local HAdjustSpinner = getPreferenceData("KartaLink.SpatialMedia.HAdjust", 0, 0)

	-- Interaxial Lens Separation
	prefsTbl.InteraxialLensSeparation = InteraxialLensSeparationSpinner

	-- Horizontal Disparity Adjustment
	prefsTbl.HAdjust = HAdjustSpinner
	
	-- Horizontal Field of View
	prefsTbl.HFOV = HFOVSpinner

	-- Image Projection
	if ImageProjectionCombo == "" then
		-- Fallback 1
		prefsTbl.ImageProjection = "halfEquirect"
	elseif ImageProjectionCombo == "180VR" then
		prefsTbl.ImageProjection = "halfEquirect"
	elseif ImageProjectionCombo == "360VR" then
		prefsTbl.ImageProjection = "equirect"
	elseif ImageProjectionCombo == "Fisheye" then
		prefsTbl.ImageProjection = "fisheye"
	elseif ImageProjectionCombo== "Flat" then
		prefsTbl.ImageProjection = "rect"
	else
		-- Fallback 2
		prefsTbl.ImageProjection = "halfEquirect"
	end

	-- Stereo 3D Format
	if StereoFormatCombo == "" then
		-- Fallback 1
		prefsTbl.Format = "sbs"
	elseif StereoFormatCombo == "Side By Side" then
		prefsTbl.Format = "sbs"
	elseif StereoFormatCombo == "Over Under" then
		prefsTbl.Format = "ou"
	else
		-- Fallback 2
		prefsTbl.Format = "sbs"
	end

	-- Primary Eye
	prefsTbl.PrimaryEye = string.lower(PrimaryEyeCombo)

	-- Checkbox
	prefsTbl.OpenFolder = OpenFolderCheckbox

	-- print("  [Prefs]")
	-- dump(prefsTbl)

	return prefsTbl
end

function Main()
	print("[Spatial Metadata]")
	print("Deliver Page Script by Andrew Hazelden <andrew@andrewhazelden>")
	print("Powered by the Spatial CLI tool by Mike Swanson")

	if platform == "Mac" then
		-- The script is running on macOS
		local project = GetProject()
		-- Read the current job in the render queue
		-- Note: "job" is a global variable available in the trigger script context
		local jobDetails = GetRenderJob(project, job)
		local jobStatus = project:GetRenderJobStatus(job)

		if jobDetails and jobDetails ~= "" then
			print("\n[Job Details]")
			dump(jobDetails)

			print("\n[Job Status]")
			dump(jobStatus)

			-- if jobStatus["JobStatus"] == "Complete" then
			if math.floor(tonumber(jobStatus["CompletionPercentage"])) == 100 then
				local timeRawSeconds = tonumber(jobStatus["TimeTakenToRenderInMs"] or 0) * 0.001
				local timeSeconds = timeRawSeconds % 60
				local timeMinutes = math.floor(timeSeconds / 60)
				print(string.format("\n[Spatial Metadata] Resolve Render Completed in %02d:%02.02f (MM:SS.SS)", tonumber(timeMinutes or 0), tonumber(timeSeconds or 0)))

				-- Is the Spatial CLI app installed?
				print("\n[Status] Checking if the Spatial CLI program is installed at: " .. tostring(spatialProgram))
				if bmd.fileexists(spatialProgram) then
					-- Check the settings
					local prefs = UserSettings()
					print("\n[Fusion Prefs]")
					dump(prefs)

					-- Check the active selection and return a list of media files
					local MediaTbl = GenerateMediaList(jobDetails)

					-- Debug media command clips
					print("\n[Media]")
					dump(MediaTbl)

					if #MediaTbl == 1 then
						print("\n[Processing Footage] " .. tostring(#MediaTbl) .. " clip found")
					else
						print("\n[Processing Footage] " .. tostring(#MediaTbl) .. " clips found")
					end

					local prevFolder = ""
					for key, value in pairs(MediaTbl) do
						-- Todo: check if the input movie filename exists on disk, and the output folder name exists
						local inputMovie = value
						if inputMovie and inputMovie ~= "" and bmd.fileexists(inputMovie) then
							local outputTbl = parseSequenceFilename(inputMovie)
							if outputTbl then
								local outputMovie = tostring(outputTbl.Path) .. tostring(outputTbl.Name) .. "_injected.mov"
								print("\n\n[Clip #] " .. tostring(key) .. " [Input Filename] " .. tostring(inputMovie) .. " [Output Filename] " .. tostring(outputMovie) .. "\n")
								-- CLI String
						local options = "make --input \"" .. tostring(inputMovie) .. "\" --format=" .. tostring(prefs.Format) .. " --output \"" .. tostring(outputMovie) .. "\" --cdist=" .. tostring(prefs.InteraxialLensSeparation) .. " --hfov=" .. tostring(prefs.HFOV) .. " --hadjust=" .. tostring(prefs.HAdjust) .. " --primary=" .. tostring(prefs.PrimaryEye) .. " --projection=" .. tostring(prefs.ImageProjection)

								-- Run the Spatial CLI app
								local commands = '"' .. spatialProgram .. '\" ' .. options
								print(commands)

								local i = 0
								local handler = io.popen(commands)
								-- Scan the io.popen output one line at a time
								for line in handler:lines() do
									print(line)
									-- Increment the line number
									i = i + 1
								end
								-- Open the folder where the clip is stored
								if (prevFolder ~= outputTbl.Path) and bmd.direxists(outputTbl.Path) and (prefs.OpenFolder == 1 or prefs.OpenFolder == true) then
									bmd.openfileexternal('Open', outputTbl.Path)
								end
								prevFolder = outputTbl.Path
							else
								print("\n[Status] Error with Media Pool clip filename: " .. tostring(inputMovie))
							end
						else
							print("\n[Status] Error with Media Pool clip filename")
						end
					end
				else
					-- Spatial CLI app not installed
					print([[
	
	[Status][Error] The Spatial CLI program was not found. Please install it before running this script again.
	
	The Spatial CLI program can be installed using the macOS based homebrew package manager.
	
	# Install Homebrew using the macOS terminal:
	/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
	
	# Install the Spatial CLI app using the brew program in the macOS terminal:
	brew install spatial
	
	# The Spatial CLI app now lives on your hard disk at:
	/opt/homebrew/bin/spatial
	]])
				end
			else
				print(string.format("\n[Spatial Metadata] Render Cancelled After %02d:%02.02f (MM:SS.SS)", tonumber(timeMinutes or 0), tonumber(timeSeconds or 0)))
			end
		else
			print("\n[Error] This script needs to be run as the Deliver page end of render job \"Trigger Script\".")
		end
	else
		-- Win/Linux Error
		print("\n[Error] The spatial CLI program is not available for Windows or Linux")
	end
end

Main()
print("[Done]")
