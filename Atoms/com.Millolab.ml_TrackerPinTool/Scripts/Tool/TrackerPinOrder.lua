-- Get the current composition
comp = fusion:GetCurrentComp()

-- Get the currently selected tool
node = comp.ActiveTool

-- Check if a node is selected
if node then
    print("Node selected: " .. node.Name)
    
    -- Define input names and corresponding expressions
    inputNames = {
        {id = "TopLeftID", expressionInput = "TL"},
        {id = "TopRightID", expressionInput = "TR"},
        {id = "BottomLeftID", expressionInput = "BL"},
        {id = "BottomRightID", expressionInput = "BR"}
    }
    
    -- Loop through each input and set the corresponding expression
    for _, input in ipairs(inputNames) do
        local idInputName = input.id
        local expressionInputName = input.expressionInput
        
        -- Ensure the ID input exists and get its value
        local idInput = node[idInputName]
        
        if idInput then
            -- Get the current value of the ID (assumed to be a numeric value)
            local idValue = idInput[1]  -- Indexing to get the first value from the input
            
            -- If the value is a number, proceed to build the expression
            if idValue then
                -- Build the expression dynamically using the ID value
                local expression = 'self:GetSourceTool("Tracker").TrackedCenter' .. tostring(idValue)
                
                -- Assign the expression to the corresponding input (TL, TR, BL, or BR)
                node[expressionInputName]:SetExpression(expression)
                print("Expression assigned to input '" .. expressionInputName .. "': " .. expression)
            else
                print("The value of '" .. idInputName .. "' is not a valid number.")
            end
        else
            print("Input '" .. idInputName .. "' not found on the selected node.")
        end
    end
else
    print("No node selected. Please select a node and try again.")
end
