--[[--
View Immersive Pipeline Integration Guide - v5.73 2027-07-02
by Andrew Hazelden <andrew@andrewhazelden.com>
https://github.com/kartaverse

Overview:
The View Immersive Pipeline Integration Guide script will open a web browser window to and display the HTML formatted developer documentation.

How to use the Script:
Step 1. Start Fusion and open a new comp. Then run the "Script > KartaVR > View Immersive Pipeline Integration Guide" menu item.

--]]--

-- --------------------------------------------------------
-- Print out extra debugging information
local printStatus = false

-- Open a web browser window up with the developer documentation
function openBrowser()
	webpage = "https://docs.google.com/document/d/1tewIaHZh8mWI8x5BzlpZBkF8eXhK2b_XhTWiU_93HBA/edit?usp=sharing"
	bmd.openurl(webpage)
end

-- Open a web browser window up with the help documentation
openBrowser()

-- End of the script
print('[Done]')
