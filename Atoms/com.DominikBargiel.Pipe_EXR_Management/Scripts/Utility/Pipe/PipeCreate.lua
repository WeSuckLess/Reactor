--[[--
Pipe v1.0
by Dominik Bargiel dominikbargiel97@gmail.com
-------------------------------------------------------------------------------
Copyright Notice:
-------------------------------------------------------------------------------
This script is part of WSL (We Suck Less) tools collection.

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, subject to
the following conditions:

1. The above copyright notice and this permission notice shall be included
   in all copies or substantial portions of the Software.
   
2. Distribution is restricted to:
   - WSL Reactor package manager
   - We Suck Less forum (www.steakunderwater.com)

Any other distribution or sharing of this script outside of these channels
requires explicit permission from the author.

Copyright © 2025 Dominik Bargiel
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
Files Included:
-------------------------------------------------------------------------------
- Fuses:/Flow/Pipe.fuse
- Fuses:/Matte/PipeCryptomatte.fuse
- Scripts:/Utility/Pipe/PipeManager.lua
- Scripts:/Utility/Pipe/PipeCreate.lua
- Modules:/Lua/Pipe/PipeCryptomatte_utilities.lua
- Modules:/Lua/Pipe/ErrorHandler.lua	
- Modules:/Lua/Pipe/PathUtils.lua
- Modules:/Lua/Pipe/Pipetest_cryptomatte_utilities.lua
- Config:/Pipe/Pipe_shortcuts.fu
- Config:/Pipe/Pipe_tile_and_color.fu
- Config:/Pipe/cryptomatte_shortcuts.fu
-------------------------------------------------------------------------------
Version History:
-------------------------------------------------------------------------------
v1.0 - 2024-01-20
    - Initial release
    - Basic pipe functionality with Pipe groups and AOV selection
    - Automatic renaming from tools and loaders
	- Pipe Manager - Version control and path managment
	- EXR Workflow Enhancements
	- Pipe Cryptomatte - Added support for multipart EXR files
--]]--


-- Add at the top after initial comments
local PathUtils = require("Pipe.PathUtils")
local GroupValidation = require("Pipe.GroupValidation")
local ErrorHandler = require("Pipe.ErrorHandler")

-- Configure ErrorHandler
ErrorHandler.configure({
    logToConsole = true,
    logToFile = true,
    logFile = "PipeCreate.log",
    minLevel = ErrorHandler.Level.INFO  -- Show all messages
})

-- Get all selected nodes
local selected = comp:GetToolList(true)

-- Check if we have any selected nodes
if #selected == 0 then
    ErrorHandler.error("PipeCreate", "No nodes selected")
    return
end

local groupName = ""  -- default
local createSinglePipe = true  -- default

-- Start undo event
comp:StartUndo("PipeCreate")


-- Create UI
local ui = fu.UIManager
local disp = bmd.UIDispatcher(ui)

-- Get mouse position for UI placement
local mouseX = fu:GetMousePos()[1]
local mouseY = fu:GetMousePos()[2]

-- Adjust position if too close to screen edge
local window_dimensions = fusion:GetPrefs("Global.Main.Window")
if window_dimensions.Width - mouseX < 300 then
    mouseX = mouseX - 300
end
if window_dimensions.Height - mouseY < 105 then  -- Adjusted for new height
    mouseY = mouseY - 105
end

local win = disp:AddWindow({
    ID = "PipeGroup",
    WindowTitle = "Create Pipe Group",
    Geometry = { mouseX+20, mouseY, 300, 105 },
    
    ui:VGroup{
        ui:HGroup{
            ui:Label{ Text = "Group Name:", Weight = 0.3 },
            ui:LineEdit {
                ID = "GroupName",
                Text = "Group1",
                Events = {ReturnPressed = true},
                Weight = 0.7,
            },
        },
        ui:HGroup{
            ui:CheckBox{
                ID = "SinglePipe",
                Text = "Create Single Pipe for Group",
                Checked = true,
            },
        },
        ui:HGroup{
            ui:VGap(20),
            ui:Button{ ID = "Cancel", Text = "Cancel", Weight = 0.5 },
            ui:Button{ ID = "OK", Text = "Create", Weight = 0.5 },
        },
    },
})

local itm = win:GetItems()
itm.GroupName:SelectAll()

-- Handle window events
function win.On.GroupName.ReturnPressed(ev)
    groupName = itm.GroupName:GetText()
    createSinglePipe = itm.SinglePipe.Checked
    disp:ExitLoop()
end

function win.On.OK.Clicked(ev)
    groupName = itm.GroupName:GetText()
    createSinglePipe = itm.SinglePipe.Checked
    disp:ExitLoop()
end

function win.On.Cancel.Clicked(ev)
    groupName = nil
    disp:ExitLoop()
end

function win.On.PipeGroup.Close(ev)
    groupName = nil
    disp:ExitLoop()
end

win:Show()
disp:RunLoop()
win:Hide()
    

-- If user cancelled, abort
if groupName == nil then
    ErrorHandler.info("PipeCreate", "User cancelled pipe creation")
    comp:EndUndo()
    return
end

-- Check for duplicate group names if a group name was provided
if groupName ~= "" then
    -- Get all Pipe tools directly
    local allPipes = comp:GetToolList(false, "Fuse.Pipe")
    
    -- Check each pipe for matching group name
    local existingGroup = false
    local existingPipes = {}
    for _, pipe in ipairs(allPipes) do
        local groupData = pipe:GetData("GroupData")
        if GroupValidation.validate(groupData) and groupData.name == groupName then
            existingGroup = true
            table.insert(existingPipes, pipe)
            ErrorHandler.info("PipeCreate", "Found existing pipe in group: " .. pipe:GetAttrs().TOOLS_Name)
        end
    end

    if existingGroup then
        ErrorHandler.info("PipeCreate", "Group '" .. groupName .. "' already exists with " .. #existingPipes .. " pipes")
        -- Create UI for group options
        local ui = fu.UIManager
        local disp = bmd.UIDispatcher(ui)
        
        -- Get mouse position for UI placement
        local mouseX = fu:GetMousePos()[1]
        local mouseY = fu:GetMousePos()[2]
        
        -- Adjust position if too close to screen edge
        local window_dimensions = fusion:GetPrefs("Global.Main.Window")
        if window_dimensions.Width - mouseX < 300 then
            mouseX = mouseX - 300
        end
        if window_dimensions.Height - mouseY < 150 then
            mouseY = mouseY - 150
        end
        
        local win = disp:AddWindow({
            ID = "GroupOptions",
            WindowTitle = "Group Already Exists",
            Geometry = { mouseX+20, mouseY, 390, 120 },
            
            ui:VGroup{
                ui:Label{ 
                    Text = "A Pipe group named '" .. groupName .. "' already exists.\nWhat would you like to do?",
                    WordWrap = true,
                    Weight = 0.4
                },
                ui:VGap(10),
                ui:HGroup{
                    ui:Button{ ID = "Override", Text = "Override Group", Weight = 0.3 },
                    ui:Button{ ID = "Append", Text = "Append to Group", Weight = 0.3 },
                    ui:Button{ ID = "Cancel", Text = "Cancel", Weight = 0.3 },
                },
            },
        })
        
        local result = "cancel"
        
        -- Handle window events
        function win.On.Override.Clicked(ev)
            result = "override"
            disp:ExitLoop()
        end
        
        function win.On.Append.Clicked(ev)
            result = "append"
            disp:ExitLoop()
        end
        
        function win.On.Cancel.Clicked(ev)
            result = "cancel"
            disp:ExitLoop()
        end
        
        function win.On.GroupOptions.Close(ev)
            result = "cancel"
            disp:ExitLoop()
        end
        
        win:Show()
        disp:RunLoop()
        win:Hide()
        
        if result == "cancel" then
            comp:EndUndo()
            return
        elseif result == "override" then
            -- Create new group data with just the newly selected tools
            groupData = {
                name = groupName,
                pipes = {}
            }
            
            -- Add new tools to group data and sort them alphabetically
            for _, tool in ipairs(selected) do
                local toolID = tool:GetAttrs().TOOLS_Name
                local nameInfo = PathUtils.getPartOrChannelNameFromLoader(tool)
                local displayName = nameInfo and nameInfo.name or toolID
                
                table.insert(groupData.pipes, {
                    toolID = toolID,
                    pipeName = "Pipe_" .. displayName
                })
            end
            
            -- Sort pipes alphabetically by pipeName
            table.sort(groupData.pipes, function(a, b) 
                return a.pipeName:lower() < b.pipeName:lower() 
            end)
            
            
            -- Update all existing pipes with new group data and refresh their UI
            for _, pipe in ipairs(existingPipes) do
                -- Reset initialization state
                pipe:SetData("WasOnceInitialized", false)
                pipe:SetData("GroupData", groupData)

                -- Force reload of the Fuse to refresh UI
                pipe.ScriptReload[fu.TIME_UNDEFINED] = 1
                pipe.TileColor = {R = 0.6, G = 0.3, B = 0.803921568627451}
            end
            
            comp:EndUndo()
            
            -- Don't create new pipes since we're reusing existing ones
            return
            
        elseif result == "append" then
            -- Get existing group data to append to
            local existingGroupData = existingPipes[1]:GetData("GroupData")
            
            -- Create a lookup table for existing tool IDs to avoid duplicates
            local existingToolIDs = {}
            for _, pipeInfo in ipairs(existingGroupData.pipes) do
                existingToolIDs[pipeInfo.toolID] = true
            end
            
            -- Only append tools that aren't already in the group
            local newTools = {}
            for _, tool in ipairs(selected) do
                local toolID = tool:GetAttrs().TOOLS_Name
                if not existingToolIDs[toolID] then
                    local nameInfo = PathUtils.getPartOrChannelNameFromLoader(tool)
                    local displayName = nameInfo and nameInfo.name or toolID
                    
                    table.insert(existingGroupData.pipes, {
                        toolID = toolID,
                        pipeName = "Pipe_" .. displayName
                    })
                    table.insert(newTools, tool)
                end
            end
            
            -- Sort pipes alphabetically by pipeName
            table.sort(existingGroupData.pipes, function(a, b) 
                return a.pipeName:lower() < b.pipeName:lower() 
            end)
            
            -- Update group data on all existing pipes and refresh their UI
            for _, pipe in ipairs(existingPipes) do
                -- Reset initialization state
                pipe:SetData("WasOnceInitialized", false)
                pipe:SetAttrs({
                    TOOLB_ControlsExpanded = true,  -- Ensure controls are expanded to update
                    TOOLB_PassThrough = false       -- Force update
                })
                pipe:SetData("GroupData", existingGroupData)
                -- Force reload of the Fuse to refresh UI
                pipe.ScriptReload[fu.TIME_UNDEFINED] = 1
                -- Wait for reload to complete
                bmd.wait(0.1)
                -- Set color after reload
                pipe:SetAttrs({TOOLS_TileColor = {R = 0.584313725490196, G = 0.294117647058824, B = 0.803921568627451}})
            end
            
            -- Only create new pipes for new tools
            if #newTools > 0 then
                -- Use the updated group data for new pipes
                groupData = existingGroupData
                -- Update selected to only include new tools
                selected = newTools
            else
                -- If no new tools to add, just end here
                comp:EndUndo()
                return
            end
            comp:EndUndo()
        end
    end
end

-- Create group data for both single and multiple selections
if not groupData then
    groupData = {
        name = groupName,
        pipes = {}
    }
end

-- Check if all selected tools are of the same type
local firstTool = selected[1]
local sourceType = firstTool:GetAttrs().TOOLS_RegID
local commonPath = nil
local confirmedRoot = nil

-- For loaders, get the first path to compare against
if sourceType == "Loader" then
    -- Collect all loader paths
    local loaderPaths = {}
    for _, tool in ipairs(selected) do
        local toolType = tool:GetAttrs().TOOLS_RegID
        if toolType ~= sourceType then
            ErrorHandler.error("PipeCreate", "All selected tools must be of the same type")
            comp:EndUndo()
            return
        end
        
        local currentPath = tool.Clip[fu.TIME_UNDEFINED]
        if currentPath and currentPath ~= "" then
            table.insert(loaderPaths, currentPath)
        else
            ErrorHandler.warning("PipeCreate", "Empty path found for loader: " .. tool:GetAttrs().TOOLS_Name)
        end
    end

    -- Detect common root using PathUtils
    local commonRoot = PathUtils.findCommonRoot(loaderPaths)
    print("Common root: " .. commonRoot)
    if not commonRoot then
        print("No common root found, using first loader's parent directory")
        commonRoot = PathUtils.getParentDirectory(loaderPaths[1]) or ""
        ErrorHandler.info("PipeCreate", "Using first loader's parent directory as root: " .. commonRoot)
    end

    -- Create root path confirmation UI
    local rootWin = disp:AddWindow({
        ID = "RootConfirm",
        WindowTitle = "Confirm Root Path",
        Geometry = { mouseX+20, mouseY, 500, 150 },
        
        ui:VGroup{
            ui:Label{ 
                Text = "Please confirm or modify the root path for the Pipe group:",
                WordWrap = true 
            },
            ui:LineEdit{ 
                ID = "RootPath",
                Text = commonRoot,
                PlaceholderText = "Enter root path..."
            },
            ui:HGroup{
                ui:Button{ ID = "Browse", Text = "Browse", Weight = 0.3 },
                ui:Button{ ID = "Cancel", Text = "Cancel", Weight = 0.35 },
                ui:Button{ ID = "OK", Text = "Confirm", Weight = 0.35 }
            }
        }
    })

    local rootItm = rootWin:GetItems()

    -- Handle window events
    function rootWin.On.Browse.Clicked(ev)
        local selectedPath = fu:RequestDir(rootItm.RootPath.Text)
        if selectedPath then
            selectedPath = PathUtils.normalizePath(selectedPath)
            selectedPath = PathUtils.ensureTrailingSeparator(selectedPath)
            rootItm.RootPath.Text = selectedPath
            ErrorHandler.info("PipeCreate", "Selected new root path: " .. selectedPath)
        end
    end

    function rootWin.On.OK.Clicked(ev)
        confirmedRoot = rootItm.RootPath.Text
        confirmedRoot = PathUtils.normalizePath(confirmedRoot)
        confirmedRoot = PathUtils.ensureTrailingSeparator(confirmedRoot)
        
        if not PathUtils.validatePathExists(confirmedRoot) then
            ErrorHandler.warning("PipeCreate", "Selected root path does not exist: " .. confirmedRoot)
        end
        
        disp:ExitLoop()
    end

    function rootWin.On.Cancel.Clicked(ev)
        confirmedRoot = nil
        disp:ExitLoop()
    end

    function rootWin.On.RootConfirm.Close(ev)
        confirmedRoot = nil
        disp:ExitLoop()
    end

    rootWin:Show()
    disp:RunLoop()
    rootWin:Hide()

    -- If user cancelled, abort
    if not confirmedRoot then
        ErrorHandler.info("PipeCreate", "User cancelled root path selection")
        comp:EndUndo()
        return
    end

    -- Store the confirmed root path for use in pipe creation
    commonPath = confirmedRoot
end

-- Pre-populate group data with all selected tools
local pipesTable = {}

for _, tool in ipairs(selected) do
    local toolID = tool:GetAttrs().TOOLS_Name
    local displayName = nil
    local toolPath = nil
    local toolRelativePath = nil
    
    -- First check if it's a loader
    if tool:GetAttrs().TOOLS_RegID == "Loader" then
        toolPath = tool.Clip[fu.TIME_UNDEFINED]
        if toolPath and toolPath ~= "" then
            -- Calculate relative path (should include filename)
            toolRelativePath = PathUtils.getRelativePath(toolPath, confirmedRoot)
        end
        
        -- Get display name using PathUtils
        local nameInfo = PathUtils.getPartOrChannelNameFromLoader(tool)
        if nameInfo then
            displayName = nameInfo.name
        end
    end
    
    -- If we couldn't get a display name, use the toolID
    if not displayName or displayName == "" then
        displayName = toolID
    end
    
    -- Add to pipes table with path information
    table.insert(pipesTable, {
        toolID = toolID,
        pipeName = "Pipe_" .. displayName,
        relativePath = toolRelativePath,
        sourcePath = toolPath  -- Keep full path for compatibility
    })
end

-- Sort pipes alphabetically by pipeName
table.sort(pipesTable, function(a, b) 
    return a.pipeName:lower() < b.pipeName:lower() 
end)

-- Add sorted pipes to group data
groupData.pipes = pipesTable

if createSinglePipe and #selected > 1 then
    -- Create single pipe connected to first tool
    local pipe = comp:AddTool("Fuse.Pipe")
    
    -- Position the pipe below the first tool
    local flow = comp.CurrentFrame.FlowView
    local x, y = flow:GetPos(selected[1])
    flow:SetPos(pipe, x, y+3)
    
    -- Get the first tool's info from the pipes table
    local firstPipeInfo = pipesTable[1]
    local displayName = firstPipeInfo.pipeName:match("^Pipe_(.+)") or firstPipeInfo.pipeName
    
    pipe:SetAttrs({TOOLS_Name = "Pipe_" .. displayName})
    
    -- Set color and tile settings
    pipe.TileColor = {R = 0.584313725490196, G = 0.294117647058824, B = 0.803921568627451}
    pipe.Flags = {ShowPic = true}
    
    -- Store data about the first source tool
    local pipeData = {
        sourceTool = firstPipeInfo.toolID,
        sourceType = sourceType,
        rootPath = confirmedRoot,  -- Store root path only in PipeData
        relativePath = firstPipeInfo.relativePath,
        sourcePath = firstPipeInfo.sourcePath  -- Keep for backward compatibility
    }
    pipe:SetData("PipeData", pipeData)
    
    -- Store group data with all selected tools
    pipe:SetData("GroupData", groupData)
    
    -- Connect to first tool's output
    local sourceOutput = selected[1].Output
    if not sourceOutput then
        sourceOutput = selected[1].Mask
    end
    
    if sourceOutput then
        pipe.Input:ConnectTo(sourceOutput)
        local outputType = sourceOutput:GetAttrs().OUTS_DataType
        if outputType then
            pipe:SetAttrs({TOOLB_PassThrough = outputType})
            pipe:SetData("connectedOutputs", {
                [4] = firstPipeInfo.toolID
            })
        end
    end
    
else
    -- Create pipes for each selected tool
    for i, tool in ipairs(selected) do
        -- Create a Pipe node
        local pipe = comp:AddTool("Fuse.Pipe")
        
        -- Position the Pipe below the tool
        local flow = comp.CurrentFrame.FlowView
        local x, y = flow:GetPos(tool)
        flow:SetPos(pipe, x, y+3)
        
        -- Get the pipe info from the table
        local pipeInfo = pipesTable[i]
        
        -- Set the pipe name
        pipe:SetAttrs({TOOLS_Name = pipeInfo.pipeName})
        ErrorHandler.info("PipeCreate", "Created pipe: " .. pipeInfo.pipeName)
        
        -- Set color and tile settings
        pipe.TileColor = {R = 0.584313725490196, G = 0.294117647058824, B = 0.803921568627451}
        pipe.Flags = {ShowPic = true}
        
        -- Store information about the source tool
        local pipeData = {
            sourceTool = pipeInfo.toolID,
            sourceType = sourceType,
            rootPath = confirmedRoot,  -- Store root path only in PipeData
            relativePath = pipeInfo.relativePath,
            sourcePath = pipeInfo.sourcePath  -- Keep for backward compatibility
        }
        
        -- Store the data in the pipe
        pipe:SetData("PipeData", pipeData)
        
        -- Store group data for all pipes
        pipe:SetData("GroupData", groupData)
        
        -- Find the appropriate output to connect to
        local sourceOutput = tool.Output
        if not sourceOutput then
            sourceOutput = tool.Mask
        end

        -- Connect using the found output
        if sourceOutput then
            pipe.Input:ConnectTo(sourceOutput)
            -- Set the pass through mode based on the output type
            local outputType = sourceOutput:GetAttrs().OUTS_DataType
            if outputType then
                pipe:SetAttrs({TOOLB_PassThrough = outputType})
                
                -- Store connection information
                pipe:SetData("connectedOutputs", {
                    [4] = pipeInfo.toolID
                })
                ErrorHandler.info("PipeCreate", "Connected pipe to " .. pipeInfo.toolID)
            end
        else
            ErrorHandler.warning("PipeCreate", "No valid output found for " .. pipeInfo.toolID)
        end
    end 
end

-- End undo event
comp:EndUndo()
ErrorHandler.info("PipeCreate", "Pipe creation completed successfully")