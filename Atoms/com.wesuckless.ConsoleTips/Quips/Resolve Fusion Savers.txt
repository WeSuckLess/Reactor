{[[Savers in the Fusion page of Resolve Studio don't allow you to set the format for anything other than .exr in the dialog. But if you manually change the extension to the format you want, Fusion will recognize the format and expose the controls in the format tab and you can render that out.

-- Chad Capeland]]}
