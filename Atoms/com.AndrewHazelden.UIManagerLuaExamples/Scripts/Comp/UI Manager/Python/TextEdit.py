ui = fu.UIManager
disp = bmd.UIDispatcher(ui)

dlg = disp.AddWindow({ "WindowTitle": "My First Window", "ID": "MyWin", "Geometry": [100, 100, 600, 800], "Spacing": 10, "Margin": 10,},[
	ui.VGroup({ "ID": "root",},[
		# Add your GUI elements here:
		ui.TextEdit({ 
			"ID": "MyTxt",
			"Text": "Hello",
			"PlaceholderText": "Please Enter a few words.",
			"Lexer": "fusion",
		}),
	]),
])

itm = dlg.GetItems()

# The window was closed
def _func(ev):
	disp.ExitLoop()
dlg.On.MyWin.Close = _func

# Add your GUI element based event functions here:

def _func(ev):
	print(itm['MyTxt'].PlainText)
dlg.On.MyTxt.TextChanged = _func

dlg.Show()
disp.RunLoop()
dlg.Hide()
