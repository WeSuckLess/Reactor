-- Open a webpage URL in the default web browser
if bmd.openurl then
	url = 'https://documents.blackmagicdesign.com/UserManuals/Fusion_Fuse_SDK.pdf?_v=1687158010000'
	bmd.openurl(url)
	print('[Opening URL] ' .. url .. '\n')
end
