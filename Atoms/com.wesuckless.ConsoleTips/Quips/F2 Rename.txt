{[[Pressing F2 hotkey makes it easy to rename nodes in the Flow view. If you have several nodes selected at the same time, a "Rename Tools" dialog will appear so you can rename each item individually.

-- Andrew Hazelden]]}
