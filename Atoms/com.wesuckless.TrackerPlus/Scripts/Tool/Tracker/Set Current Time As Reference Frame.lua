--[[

Tracker Plus - Set Current Time As Reference Frame.lua
http://www.steakunderwater.com/wesuckless/viewtopic.php?f=6&t=1192
20171123 - Pieter Van Houte (pieter[at]secondman[dot]com)
20240803 - Bug fixes, code cleanup and updated for new Tracker types in Fusion 19
--]]

--[[ 

original script by Bartos P. - info[at]talmai[dot]de
syntax adjustments for Fu9 by Michael Vorberg

]]--

-- THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
-- THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND
-- DISTRIBUTORS HAVE NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
-- UPDATES, ENHANCEMENTS, OR MODIFICATIONS. 

--some variables
current = comp.CurrentTime

k = 1
l = 1
m = 1
inputlist = nil

local comp = fusion.CurrentComp

if not #comp:GetToolList(true) == 1 then
	print ("Please make sure Tracker is selected...")
	return
end
local trackertool = comp:GetToolList(true)[1]
if trackertool:GetAttrs().TOOLS_RegID ~= "Tracker" then
	print ("Please select a Tracker...")
	return
end

function findLowestNumber(strings, targetString)
	local lowestNumber = nil
	for i, str in ipairs(strings) do
		local matchedString, number = str:match("^(" .. targetString .. ") (%d+)$")
		if matchedString and number then
			number = tonumber(number)
			if lowestNumber == nil or (number ~= nil and number < lowestNumber) then
				lowestNumber = number
			end
		end
	end
	
	if lowestNumber == nil then
--		print("number for " .. targetString .. " equals 1")
		return 1
	else
--		print("number for " .. targetString .. " equals " .. lowestNumber)
		return lowestNumber
	end
end

--search for a specific input and return its ID
function SearchInput(t,s)
	none = false
	if inputlist == nil then
		print("Please wait - caching InputList...")	--or doing something absolutely useless 
		inputlist = {}
		for n,m in ipairs(t:GetInputList()) do
			inputlist[n] = m
		end
	end
	for i, inp in ipairs(inputlist) do
		local inpid = inp:GetAttrs().INPS_ID
		if tostring(inpid) == tostring(s) then
--			print (i)
--			print (s)
			return i
		else
			none = true
		end
	end
	if none == true then
		return nil
	end
end

function UpdateTracker(trackertool,trackertype,counter)
	print ("There be "..trackertype.."s")
	local trackers = {}
	repeat	--get all tracker
		local count = SearchInput(trackertool,trackertype..counter)
--		print (count)
		trackers[counter] = SearchInput(trackertool,trackertype..counter)
		counter = counter + 1
	until count == nil
	for i, t in pairs(trackers) do
--		print (i)
--		print (t)
		if IsEnabled(trackertool,i) == true then	--check if enabled
			pcenter = SearchInput(trackertool,"PatternCenter"..i)	--get PatternCenter input ID
			tcenter = SearchInput(trackertool,"TrackedCenter"..i)	--get TrackedCenter input ID
			trackertool:GetInputList()[pcenter][current] = trackertool:GetInputList()[tcenter][current]	--move PatterCenter to TrackedCenter
			print("Moving PatternCenter"..i.." to TrackedCenter"..i)
		else
			print("Skipping disabled "..trackertype..": "..trackertool:GetInputList()[t]:GetAttrs().INPS_Name)
		end
	end
end

--check if tracker is disabled
function IsEnabled(t,id)
	c = id
	enabled = {}
	repeat
		count = SearchInput(t,"Enabled"..c)
		enabled[c] = SearchInput(t,"Enabled"..c)
		c = c + 1
	until count == nil
	--if t:GetInputList()[enabled[id]][current] == 1 or t:GetInputList()[enabled[id]][current] == 2 then
	if t:GetInputList()[enabled[id]][current] == 1 then	
		return true
	end
	return false
end

if trackertool:GetAttrs().TOOLS_RegID == "Tracker" then
	if trackertool.Reference[1] == 0 then	--Reference must be set to "Select time"
			local inputs = trackertool:GetInputList()
			local inputnames = {}
			for i, inp in ipairs(inputs) do
				inputnames[i]=(inp:GetAttrs().INPS_Name)
			end
--			dump (inputnames)
			k = findLowestNumber(inputnames, "Tracker")
			l = findLowestNumber(inputnames, "Point")
			m = findLowestNumber(inputnames, "IntelliTrack")
--			print("k: "..k)
--			print("l: "..l)
--			print("m: "..m)
		if SearchInput(trackertool,"Tracker"..k) == nil and SearchInput(trackertool,"Point"..l) == nil and SearchInput(trackertool,"IntelliTrack"..m) == nil then
			print("No Tracker present. Please add a Tracker")
		else
			comp:StartUndo("update pattern")
			print("Update pattern")
			if SearchInput(trackertool,"Tracker"..k) ~= nil then
				UpdateTracker(trackertool,"Tracker",k)
			end
			if SearchInput(trackertool,"Point"..l) ~= nil then
				UpdateTracker(trackertool,"Point",l)
			end
			if SearchInput(trackertool,"IntelliTrack"..m) ~= nil then
				UpdateTracker(trackertool,"IntelliTrack",m)
			end
			trackertool.ReferenceFrame = current
			trackertool.Comments = "Reference Frame: "..current
			print("Reference Frame is "..current)
			comp:EndUndo(true)
		end
	else
		print("Will only run  when \"Reference\" is set to \"Select time\"")
	end
else
	print("This is not a Tracker")
end