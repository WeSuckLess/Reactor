-- Import the UI library
local ui = fu.UIManager
local disp = bmd.UIDispatcher(ui)

-- Function to create a BetterResize node with KeepAspect option
function createResizeNode(comp, source, width, height, x, y)
    local resize = comp:AddTool("BetterResize", x, y)
    resize.Width = width
    resize.Height = height
    resize.KeepAspect = 1
    resize.HiQOnly = 0
    resize.PixelAspect = {1, 1}
    resize.Input = source
    return resize
end

-- Function to create a background of appropriate size
function createBackgroundNode(comp, width, height, x, y)
    local background = comp:AddTool("Background", x, y)
    background.Width = width
    background.Height = height
    return background
end

-- Function to create a TextPlus node displaying only the file name
function createTextPlusNode(comp, loader, x, y, tileWidth, tileHeight, textColor)
    local fullFilename = loader.Clip[fu.TIME_UNDEFINED]
    -- Extract only the filename from the full path
    local filename = string.match(fullFilename, "([^\\/]*)$")
    local textPlus = comp:AddTool("TextPlus", x, y)
    textPlus.Width = tileWidth
    textPlus.Height = tileHeight
    textPlus.Depth = 1
    textPlus.Center = {0.5, 0.01}
    textPlus:SetInput("StyledText", filename) -- Set the filename without path
    textPlus.Font = "Open Sans"
    textPlus.Style = "Bold"
    textPlus.Size = 0.0373
    textPlus.VerticalTopCenterBottom = 1
    textPlus.VerticalJustificationNew = 3
    textPlus.HorizontalJustificationNew = 3
    
    -- Set the text color
    textPlus.Red1 = textColor[1]
    textPlus.Green1 = textColor[2]
    textPlus.Blue1 = textColor[3]
    textPlus.Alpha1 = textColor[4]
    
    return textPlus
end


-- Function to create a Merge node with specific settings
function createTextMergeNode(comp, background, foreground, x, y)
    local merge = comp:AddTool("Merge", x, y)
    merge.Background = background.Output
    merge.Foreground = foreground.Output
    merge.PerformDepthMerge = 0
    return merge
end

-- Function to arrange layers in a grid by merge nodes.
function createImagesMergeNodes(comp, sources, rows, cols, background, x, y, hSpacing, vSpacing, tileWidth, tileHeight)
    local idx = 1
    local mergedimages = {}
    
    -- Calculate total dimensions including spacing
    local totalWidth = background.Width[fu.TIME_UNDEFINED]
    local totalHeight = background.Height[fu.TIME_UNDEFINED]
    
    -- Calculate cell dimensions including spacing
    local cellWidth = tileWidth / totalWidth
    local cellHeight = tileHeight / totalHeight
    
    -- Calculate spacing as a fraction of the total size
    local hSpacingNorm = hSpacing / totalWidth
    local vSpacingNorm = vSpacing / totalHeight
    
    for row = 0, rows - 1 do
        for col = 0, cols - 1 do
            if idx <= #sources then
                local merge = comp:AddTool("Merge", x + idx, y)
                table.insert(mergedimages, merge)
                
                -- Calculate center position with spacing
                local centerX = (col * (cellWidth + hSpacingNorm))
                centerX = centerX + (cellWidth / 2)
                
                local centerY = 1 - ((row * (cellHeight + vSpacingNorm)) + (cellHeight / 2))
                
                -- Adjust for edge cases
                centerX = math.min(math.max(centerX, 0), 1)
                centerY = math.min(math.max(centerY, 0), 1)
                
                if idx == 1 then
                    merge.Background = background.Output
                else 
                    merge.Background = mergedimages[idx-1].Output
                end

                merge.Foreground = sources[idx].Output
                merge.Center = {centerX, centerY}

                idx = idx + 1
            end
        end
    end
    return mergedimages[#mergedimages]
end


-- Create the user interface dialog
function createDialog()
    -- Get the number of selected loader nodes
    local comp = fu:GetCurrentComp()
    local loaderNodes = comp:GetToolList(true, "Loader")
    local numLoaders = #loaderNodes
    
    -- Calculate default rows and columns based on number of loaders
    local defaultRows = 1
    local defaultCols = numLoaders
    
    if numLoaders >= 6 and numLoaders < 12 then
        defaultRows = 2
        defaultCols = math.ceil(numLoaders / 2)
    elseif numLoaders >= 12 and numLoaders < 20 then
        defaultRows = 3
        defaultCols = math.ceil(numLoaders / 3)
    elseif numLoaders >= 20 then
        defaultRows = 4
        defaultCols = math.ceil(numLoaders / 4)
    end
    
    -- Create the dialog
    local dlg = disp:AddWindow({
        ID = "TileGridDialog",
        WindowTitle = "Tile and Grid Settings",
        Geometry = {100, 100, 400, 450},
        ui:VGroup{
            ID = "root",
            ui:VGap(10),
            ui:Label{ID = "selectedNodesInfo", Text = numLoaders .. " loader nodes selected"},
            ui:Label{ID = "labelTileWidth", Text = "Enter tile width:"},
            ui:LineEdit{ID = "tileWidth", Text = "500"},
            ui:Label{ID = "labelGridRows", Text = "Enter number of grid rows:"},
            ui:LineEdit{ID = "gridRows", Text = tostring(defaultRows)},
            ui:Label{ID = "labelGridCols", Text = "Enter number of grid columns:"},
            ui:LineEdit{ID = "gridCols", Text = tostring(defaultCols)},
            ui:VGap(5),
            ui:HGroup{
                ui:Label{Text = "Spacing:", Weight = 0.3},
                ui:VGroup{
                    Weight = 0.7,
                    ui:HGroup{
                        ui:Label{Text = "Horizontal:", Weight = 0.5},
                        ui:LineEdit{ID = "hSpacing", Weight = 0.5, Text = "0"}
                    },
                    ui:HGroup{
                        ui:Label{Text = "Vertical:", Weight = 0.5},
                        ui:LineEdit{ID = "vSpacing", Weight = 0.5, Text = "0"}
                    }
                }
            },
            ui:VGap(5),
            ui:CheckBox{ID = "showNames", Text = "Show file names", Checked = true},
            ui:HGroup{
                ui:Label{
                    Weight = 0.3,
                    Text = "Text Color: ",
                },
                ui:ComboBox{
                    Weight = 0.7,
                    ID = "textColor",
                }
            },
            ui:HGroup{
                ui:Label{
                    Weight = 0.3,
                    Text = "Sort Order: ",
                },
                ui:ComboBox{
                    Weight = 0.7,
                    ID = "sortOrder",
                }
            },
            ui:Label{ID = "totalSize", Text = "Final Size: calculating...", WordWrap = true},
            ui:HGroup{
                ui:Button{ID = "okButton", Text = "OK"},
                ui:Button{ID = "cancelButton", Text = "Cancel"}
            }
        }
    })

    -- Get the ComboBox item and add the options
    local items = dlg:GetItems()
    items.sortOrder:AddItem("Original Order")
    items.sortOrder:AddItem("Alphabetical")
    items.sortOrder:AddItem("Reverse Alphabetical")
    items.sortOrder:SetCurrentIndex(0)  -- Set default selection
    
    -- Add color options to the color dropdown
    items.textColor:AddItem("White")
    items.textColor:AddItem("Black")
    items.textColor:AddItem("Red")
    items.textColor:AddItem("Green")
    items.textColor:AddItem("Blue")
    items.textColor:AddItem("Yellow")
    items.textColor:AddItem("Cyan")
    items.textColor:AddItem("Magenta")
    items.textColor:AddItem("Orange")
    items.textColor:AddItem("Purple")
    items.textColor:SetCurrentIndex(0)  -- Default to White

    -- Add event handler for ComboBox changes
    function dlg.On.sortOrder.CurrentIndexChanged(ev)
        -- The value will be automatically passed to main() through the okButton handler
    end

    -- Update the event handlers
    function dlg.On.TileGridDialog.Close(ev)
        disp:ExitLoop()
    end

    -- Add preview calculation for total size
    function updateTotalSize()
        local items = dlg:GetItems()
        local tileWidth = tonumber(items.tileWidth.Text)
        local gridRows = tonumber(items.gridRows.Text)
        local gridCols = tonumber(items.gridCols.Text)
        local hSpacing = tonumber(items.hSpacing.Text) or 0
        local vSpacing = tonumber(items.vSpacing.Text) or 0
        
        if tileWidth and gridRows and gridCols then
            local comp = fu:GetCurrentComp()
            local loaderNodes = comp:GetToolList(true, "Loader")
            
            if #loaderNodes > 0 then
                local maxAspectRatio = 0
                for _, loader in ipairs(loaderNodes) do
                    -- Get image dimensions using Input
                    local img = loader.Output:GetValue()
                    bmd.wait(0.02)
                    if img then
                        local imageWidth = img.Width
                        local imageHeight = img.Height
                        if imageWidth and imageHeight and imageHeight > 0 then
                            local aspectRatio = imageWidth / imageHeight
                            if aspectRatio > maxAspectRatio then
                                maxAspectRatio = aspectRatio
                            end
                        end
                    end
                end
                
                if maxAspectRatio > 0 then
                    local tileHeight = math.floor(tileWidth / maxAspectRatio)
                    -- Add spacing to total dimensions
                    local totalWidth = (gridCols * tileWidth) + (hSpacing * (gridCols - 1))
                    local totalHeight = (gridRows * tileHeight) + (vSpacing * (gridRows - 1))
                    
                    items.totalSize.Text = string.format("Final Size: %dx%d pixels", totalWidth, totalHeight)
                else
                    items.totalSize.Text = "Final Size: calculating... (waiting for image data)"
                end
            end
        end
    end

    function dlg.On.tileWidth.TextChanged(ev)
        updateTotalSize()
    end

    function dlg.On.gridRows.TextChanged(ev)
        updateTotalSize()
    end

    function dlg.On.gridCols.TextChanged(ev)
        updateTotalSize()
    end

    function dlg.On.hSpacing.TextChanged(ev)
        local value = tonumber(ev.Text)
        if value then
            updateTotalSize()
        end
    end

    function dlg.On.vSpacing.TextChanged(ev)
        local value = tonumber(ev.Text)
        if value then
            updateTotalSize()
        end
    end

    function dlg.On.okButton.Clicked(ev)
        local items = dlg:GetItems()
        local tileWidth = tonumber(items.tileWidth.Text)
        local gridRows = tonumber(items.gridRows.Text)
        local gridCols = tonumber(items.gridCols.Text)
        local showNames = items.showNames.Checked
        local sortOrder = items.sortOrder:GetCurrentIndex()
        local hSpacing = tonumber(items.hSpacing.Text) or 0
        local vSpacing = tonumber(items.vSpacing.Text) or 0
        
        -- Get color selection from dropdown
        local colorIndex = items.textColor:GetCurrentIndex()
        local textColor
        
        -- Map color index to RGB values
        if colorIndex == 0 then      -- White
            textColor = {1, 1, 1, 1}
        elseif colorIndex == 1 then  -- Black
            textColor = {0, 0, 0, 1}
        elseif colorIndex == 2 then  -- Red
            textColor = {1, 0, 0, 1}
        elseif colorIndex == 3 then  -- Green
            textColor = {0, 1, 0, 1}
        elseif colorIndex == 4 then  -- Blue
            textColor = {0, 0, 1, 1}
        elseif colorIndex == 5 then  -- Yellow
            textColor = {1, 1, 0, 1}
        elseif colorIndex == 6 then  -- Cyan
            textColor = {0, 1, 1, 1}
        elseif colorIndex == 7 then  -- Magenta
            textColor = {1, 0, 1, 1}
        elseif colorIndex == 8 then  -- Orange
            textColor = {1, 0.5, 0, 1}
        elseif colorIndex == 9 then  -- Purple
            textColor = {0.5, 0, 1, 1}
        else                         -- Default to White
            textColor = {1, 1, 1, 1}
        end

        if not tileWidth or not gridRows or not gridCols then
            print("Invalid input. Please enter valid numbers.")
            return
        end

        dlg:Hide()
        main(tileWidth, gridRows, gridCols, showNames, sortOrder, hSpacing, vSpacing, textColor)
    end

    function dlg.On.cancelButton.Clicked(ev)
        dlg:Hide()
        disp:ExitLoop()
    end

    -- Calculate initial total size
    updateTotalSize()

    return dlg
end

-- Main function to execute the script logic
function main(tileWidth, gridRows, gridCols, showNames, sortOrder, hSpacing, vSpacing, textColor)
    local comp = fu:GetCurrentComp()
    
    -- Get the selected tools in the composition
    local loaderNodes = comp:GetToolList(true, "Loader")

    -- Check if there are any loaders selected
    if #loaderNodes == 0 then
        print("No Loader nodes selected.")
        return
    end

    -- Find the maximum aspect ratio among all images
    local maxAspectRatio = 0
    for _, loader in ipairs(loaderNodes) do
        -- Get image dimensions using Input
        local img = loader.Output:GetValue()
        if img then
            local imageWidth = img.Width
            local imageHeight = img.Height
            if imageWidth and imageHeight and imageHeight > 0 then
                local aspectRatio = imageWidth / imageHeight
                if aspectRatio > maxAspectRatio then
                    maxAspectRatio = aspectRatio
                end
            end
        end
    end
    
    -- Calculate tile height based on the maximum aspect ratio
    local tileHeight = math.floor(tileWidth / maxAspectRatio)
    
    -- If we couldn't determine aspect ratio, use tileWidth as fallback
    if maxAspectRatio == 0 then
        tileHeight = math.floor(tileWidth)
        print("Warning: Could not determine aspect ratio, using square tiles.")
    end

    -- Convert loaderNodes to a regular table we can sort
    local sortableNodes = {}
    for i = 1, #loaderNodes do
        sortableNodes[i] = loaderNodes[i]
    end

    -- Sort loaders if needed
    if sortOrder == 1 then  -- Alphabetical
        print("Sorting alphabetically...")
        table.sort(sortableNodes, function(a, b)
            local fileA = a.Clip[fu.TIME_UNDEFINED]
            local fileB = b.Clip[fu.TIME_UNDEFINED]
            local nameA = string.match(fileA, "([^\\/%s]+)%.%w+$") or fileA
            local nameB = string.match(fileB, "([^\\/%s]+)%.%w+$") or fileB
            print("Comparing files: " .. nameA .. " with " .. nameB)
            return string.lower(nameA) < string.lower(nameB)
        end)
    elseif sortOrder == 2 then  -- Reverse Alphabetical
        print("Sorting reverse alphabetically...")
        table.sort(sortableNodes, function(a, b)
            local fileA = a.Clip[fu.TIME_UNDEFINED]
            local fileB = b.Clip[fu.TIME_UNDEFINED]
            local nameA = string.match(fileA, "([^\\/%s]+)%.%w+$") or fileA
            local nameB = string.match(fileB, "([^\\/%s]+)%.%w+$") or fileB
            print("Comparing files: " .. nameA .. " with " .. nameB)
            return string.lower(nameA) > string.lower(nameB)
        end)
    end

    -- Print the final order for debugging
    print("\nFinal order after sorting:")
    for i, node in ipairs(sortableNodes) do
        local filename = string.match(node.Clip[fu.TIME_UNDEFINED], "([^\\/%s]+)%.%w+$") or node.Clip[fu.TIME_UNDEFINED]
        print(i .. ": " .. filename)
    end

    -- Start the composition changes
    comp:StartUndo("Create Grid Collage")

    -- Get positions for nodes using the sorted order
    local positions = {}
    for i, loader in ipairs(sortableNodes) do
        local x, y = comp.CurrentFrame.FlowView:GetPos(loader)
        table.insert(positions, {x = x, y = y})
    end

    -- Create background with spacing included in dimensions
    local totalWidth = (gridCols * tileWidth) + (hSpacing * (gridCols - 1))
    local totalHeight = (gridRows * tileHeight) + (vSpacing * (gridRows - 1))
    local bgpos = positions[1]
    local background = createBackgroundNode(comp, math.floor(totalWidth + 0.5), math.floor(totalHeight + 0.5), bgpos.x - 3, bgpos.y + 3)

    -- Create resize, text, and merge nodes in the sorted order
    local mergedNodes = {}
    for i, loader in ipairs(sortableNodes) do
        local pos = positions[i]
        local resize = createResizeNode(comp, loader, tileWidth, tileHeight, pos.x, pos.y + 2)
        
        if showNames then
            local textPlus = createTextPlusNode(comp, loader, pos.x - 1, pos.y + 1, tileWidth, tileHeight, textColor)
            local merge = createTextMergeNode(comp, resize, textPlus, pos.x, pos.y + 3)
            table.insert(mergedNodes, merge)
        else
            table.insert(mergedNodes, resize)
        end
    end

    -- Create MultiMerge and arrange layers in a grid
    local pos = positions[1]
    local multiMerge = createImagesMergeNodes(comp, mergedNodes, gridRows, gridCols, background, 
        pos.x-1, pos.y + 4, hSpacing, vSpacing, tileWidth, tileHeight)

    comp:EndUndo()
end

-- Show the dialog and start the event loop
local dlg = createDialog()
dlg:Show()
disp:RunLoop()
dlg:Hide()
