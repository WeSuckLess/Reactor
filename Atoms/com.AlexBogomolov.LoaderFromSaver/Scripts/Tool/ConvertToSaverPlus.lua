    --[[
    This tool script will disable currently selected saver, disconnect it and add a new Saver Plus (must be saved as default)
    Old saver will be renamed to <saver-name>_old.
    New saver will be reconnected and inherit the name of the old saver.

    Copyright © 2022 Alexey Bogomolov
    
    Permission is hereby granted, free of charge, to any person obtaining
    a copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
    IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
    TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ]]

function ConvertToSaverPlus(settings)
    comp:Lock()
    saver_plus = comp:AddTool('Saver', -32768, -32768)

    tool_name = tool.Name
    tool:SetAttrs({TOOLB_PassThrough = true})
    tool:SetAttrs({TOOLB_NameSet = true, TOOLS_Name = tool_name .. "_old"})
    
    inp = tool:FindMainInput(1)
    connected = inp:GetConnectedOutput()

    if connected then
        new_tool_inp = saver_plus:FindMainInput(1)
        new_tool_inp:ConnectTo(connected:GetTool().Output)
    end
    saver_plus:LoadSettings(settings)
    saver_plus:SetAttrs({TOOLB_NameSet = true, TOOLS_Name = tool_name})
    comp:Unlock()
    inp:ConnectTo()
end

if not tool then
    print('This is a tool script.')
end

if not tool.ID == "Saver" then
    print("This tool is not a Saver.")
end

settings = comp:CopySettings(tool)
defaultSaverSettingsFound = bmd.fileexists(comp:MapPath("Defaults:Saver_Saver.setting"))
if defaultSaverSettingsFound then
    ConvertToSaverPlus(settings)
else
    print("Default settings for Saver Plus not found. Create a new Saver Plus with CreateSaverPlus script, and save it as default:\nRight click Saver Plus -> Settings -> Save Default")
end
