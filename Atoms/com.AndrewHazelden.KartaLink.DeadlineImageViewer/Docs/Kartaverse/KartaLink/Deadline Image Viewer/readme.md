# Deadline Image Viewer | AddLoader.lua
By Andrew Hazelden <andrew@andrewhazelden.com>

## Overview:

Allows you to add new footage to the currently open Fusion Studio foreground composite using the command prompt. This makes Fusion Studio usable as a media viewer in Deadline.

## Install:

### 1. Copy the Script:

Copy the "AddLoader.lua" script to a "Scripts/Support/" PathMap location:

**macOS:**  

	/Library/Application Support/Blackmagic Design/Fusion/Reactor/Deploy/Scripts/Support/Deadline/AddLoader.lua

**Windows:**  

	C:\ProgramData\Blackmagic Design\Fusion\Reactor\Deploy\Scripts\Support\Deadline\AddLoader.lua

**Linux:**  

	/opt/Reactor/Deploy/Scripts/Support/Deadline/AddLoader.lua

### 2. Set up Deadline Monitor:

In the Deadline Monitor program we are going to assign the "AddLoader.lua" script as one of the custom viewer entries. In Super User mode, select the "Tools > Options..." menu item.

![Tool Options](Images/1.png)

In the Options window navigate to the "Configure Monitor Options > Image Viewers > " section. We are going to define a "Custom Image Viewer 1:" entry.

**Executable: (macOS)**  

	/Applications/Blackmagic Fusion 18/Fusion.app/Contents/MacOS/fuscript

**Executable: (Windows)**  

	C:\Program Files\Blackmagic Design\Fusion 18\fuscript.exe

**Executable: (Linux)**  

	/opt/BlackmagicDesign/Fusion18/fuscript


**Arguments: (macOS)**  

	"/Library/Application Support/Blackmagic Design/Fusion/Reactor/Deploy/Scripts/Support/Deadline/AddLoader.lua" "{FRAME}"

**Arguments: (Windows)**  

	"C:\ProgramData\Blackmagic Design\Fusion\Reactor\Deploy\Scripts\Support\Deadline\AddLoader.lua" "{FRAME}"

**Arguments: (Linux)**  

	"/opt/Reactor/Deploy/Scripts/Support/Deadline/AddLoader.lua" "{FRAME}"

**Name:**  

	Fusion

![Image Viewers](Images/2.png)

Then in the Options window navigate to the "Configure Monitor Options > Monitor Options > " section. Set the "Task List > Completed Task Double-Click Behavior" entry to "View Image".

![Monitor Options](Images/3.png)

## Usage:

After rendering an image sequence in Deadline, if the submitter supports filename detection in the output log you will be able to load that footage into Fusion Studio by double-clicking on the entry in the Tasks view.

You can also right-click on a task and select the "View Output" menu item.

![View Output](Images/4.png)

* * *

## Lua Script Customization:

### OpenColorIO:
The "ocio.OCIOConfig" line in the script can be commented out to skip defining a custom OCIO config file path when the "OCIOColorSpace" node is added to the comp. You can also pre-define your preferred SourceSpace/OutputSpace/

### Viewer:
The DoAction "Tool_ViewOn" lines in the script can be used to specify if you want the media to be loaded automatically in the left or right viewer window context.

### Resolve:
If you really wanted to, one could modify this script so it adds the new Deadline renderings to the Resolve Media Pool, or as Loader node based media in the currently active Fusion page composite.

* * *

## CLI Syntax:

	fuscript <addLoader.lua> [/path/to/image.exr]

**macOS:**  

	"/Applications/Blackmagic Fusion 18/Fusion.app/Contents/MacOS/fuscript" "/Library/Application Support/Blackmagic Design/Fusion/Reactor/Deploy/Scripts/Support/Deadline/AddLoader.lua" "/Volumes/Path/To/Image.0001.exr"

**Windows:**  

	"C:\Program Files\Blackmagic Design\Fusion 18\fuscript.exe" "C:\ProgramData\Blackmagic Design\Fusion\Reactor\Deploy\Scripts\Support\Deadline\AddLoader.lua" "F:\Path\To\Image.0001.exr"

**Linux:**  

	"/opt/BlackmagicDesign/Fusion18/fuscript" "/opt/Reactor/Deploy/Scripts/Support/Deadline/AddLoader.lua" "/Path/To/Image.0001.exr"


* * *

fuscript CLI Syntax:

	Usage: fuscript [opts] <script> [args]
	   -i                   - Enter interactive mode
	   -v                   - Print version information
	   -s                   - Run as script server
	   -S                   - Run as script server with no timeout
	   -p [appname]         - Ping script apps
	   -P [appname]         - Ping script apps (longer timeout)
	   -q                   - Be quiet
	   -b                   - Run with commandline debugger
	   -l <lang>            - Explicitly set language, from:
	      lua
	      py2 or python2
	      py3 or python3
	   -x <string>          - Execute string


