# KartaVP kvrGuides

By Andrew Hazelden [&lt;andrew@andrewhazelden.com&gt;](mailto:andrew@andrewhazelden.com)

A set of Fusion viewer window guide overlays that allow you to check the alignment of XR imagery. 

The custom guides are selectable in the Fusion viewer window under the "Guides > " contextual menu where they act as alignment overlays. After you save or modify a guide file, you can view the updated guide in Fusion without needing to restart the program. The custom guide files have the .guide extension and are saved in the "Guides:/" PathMap folder.

![Fusion Viewer Guides Menu](Images/fusion_viewer_guides_menu.png)

## Included Guides

- kvrGuides_SBS.guide
- kvrGuides_OU.guide

The guide grids are installed in this folder:

		Reactor:/Deploy/Guides/

When this atom package is installed you can explore the example Fusion composites in this folder:

		Reactor:/Deploy/Comps/Kartaverse/KartaVP/kvrGuides/

## Example Comp Screenshot

![Example Comps](Images/kvrguides_example_comp.png)

