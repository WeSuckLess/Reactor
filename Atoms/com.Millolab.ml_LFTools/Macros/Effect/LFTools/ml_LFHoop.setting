--[[-- 
Copyright (c) 2020,  Emilio Sapia
https://emiliosapia.myportfolio.com
Written by : Emilio Sapia 
Written on : Aug, 2020
version 1.0

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--]]--

{
	Tools = ordered() {
		ml_LFHoop = MacroOperator {
			CtrlWZoom = false,
			Inputs = ordered() {
				MainInput1 = InstanceInput {
					SourceOp = "PipeRouter",
					Source = "Input",
					Name = "Background",
				},
				Controls = InstanceInput {
					SourceOp = "Hoop",
					Source = "Controls",
				},
				Blank10 = InstanceInput {
					SourceOp = "Hoop",
					Source = "Blank1",
				},
				Blend = InstanceInput {
					SourceOp = "Merge5",
					Source = "Blend",
					Name = "Main Blend",
					Default = 1,
				},
				Size = InstanceInput {
					SourceOp = "AUTO_R",
					Source = "n5",
					Name = "Size",
					MinScale = 1,
					MaxScale = 5,
					Default = 2.5,
				},
				Aspect = InstanceInput {
					SourceOp = "Transform",
					Source = "Aspect",
					MaxScale = 1,
					Default = 1,
				},
				Angle = InstanceInput {
					SourceOp = "Transform",
					Source = "Angle",
					Default = 0,
				},
				RotationOn = InstanceInput {
					SourceOp = "Hoop",
					Source = "EnableAutoRotation",
					Default = 1,
				},
				OffsetOn = InstanceInput {
					SourceOp = "Hoop",
					Source = "EnableOffset",
					Default = 1,
				},
				Blank1 = InstanceInput {
					SourceOp = "Hoop",
					Source = "Blank1",
				},
				Thickness = InstanceInput {
					SourceOp = "Background",
					Source = "Thickness",
					MaxScale = 5,
					Default = 1,
				},
				Lenght = InstanceInput {
					SourceOp = "Rectangle",
					Source = "Size",
					Name = "Lenght",
					Page = "Controls",
					Default = 0.5,
				},
				SoftEdge = InstanceInput {
					SourceOp = "Blur",
					Source = "XBlurSize",
					Name = "SoftEdge",
					Default = 60,
				},
				Blank3 = InstanceInput {
					SourceOp = "Hoop",
					Source = "Blank1",
				},
				LockXY = InstanceInput {
					SourceOp = "Blur3",
					Source = "LockXY",
					Default = 1,
				},
				XBlurSize = InstanceInput {
					SourceOp = "Blur3",
					Source = "XBlurSize",
					Default = 60,
				},
				YBlurSize = InstanceInput {
					SourceOp = "Blur3",
					Source = "YBlurSize",
					Default = 4,
				},
				Blank15 = InstanceInput {
					SourceOp = "Hoop",
					Source = "Blank1",
				},
				Gradient = InstanceInput {
					SourceOp = "Background",
					Source = "Gradient",
				},
				Blank6 = InstanceInput {
					SourceOp = "Hoop",
					Source = "Blank1",
				},
				Spectrum = InstanceInput {
					SourceOp = "Background",
					Source = "Offset",
					Name = "Spectrum",
					Default = 0,
				},
				Repeat = InstanceInput {
					SourceOp = "Background",
					Source = "Repeat",
				},
				InvertSpectrum = InstanceInput {
					SourceOp = "Background",
					Source = "Invert",
					Name = "Invert Spectrum",
					Default = 0,
				},
				Blank2 = InstanceInput {
					SourceOp = "Hoop",
					Source = "Blank1",
				},
				NoiseMix = InstanceInput {
					SourceOp = "FastNoise",
					Source = "Detail",
					Name = "Noise Mix",
					Default = 10,
				},
				NoiseScale = InstanceInput {
					SourceOp = "FastNoise",
					Source = "XScale",
					Name = "Noise Scale",
					Default = 100,
				},
				Seethe = InstanceInput {
					SourceOp = "FastNoise",
					Source = "Seethe",
					Default = 0,
				},
				SeetheRate = InstanceInput {
					SourceOp = "FastNoise",
					Source = "SeetheRate",
					Default = 0.15,
				}
			},
			Outputs = {
				MainOutput1 = InstanceOutput {
					SourceOp = "SetDomain",
					Source = "Output",
				}
			},
			ViewInfo = GroupInfo {
				Pos = { 410, -192.637 },
				Flags = {
					Expanded = true,
					AllowPan = false,
					ForceModes = false,
					ConnectedSnap = true,
					AutoSnap = true,
					ShowInstance = false,
					Thumbnails = false,
					RemoveRouters = true
				},
				Size = { 1380.63, 448.493, 531.318, 24.2424 },
				Direction = "Vertical",
				PipeStyle = "Direct",
				Scale = 1,
				Offset = { 8.85215, 0 }
			},
			Tools = ordered() {
				PipeRouter = PipeRouter {
					CtrlWShown = false,
					NameSet = true,
					ViewInfo = PipeRouterInfo { Pos = { 364.173, 29.8986 } },
				},
				PipeRouter1 = PipeRouter {
					CtrlWShown = false,
					Inputs = {
						Input = Input {
							SourceOp = "Scale",
							Source = "Output",
						},
					},
					ViewInfo = PipeRouterInfo { Pos = { 534.525, 68.856 } },
				},
				FastNoise = FastNoise {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Width = Input {
							Value = 960,
							Expression = "PipeRouter1.Input.OriginalWidth",
						},
						Height = Input {
							Value = 960,
							Expression = "Width",
						},
						["Gamut.SLogVersion"] = Input { Value = FuID { "SLog2" }, },
						Detail = Input { Value = 10, },
						Contrast = Input { Value = 0.417, },
						LockXY = Input { Value = 0, },
						XScale = Input { Value = 100, },
						SeetheRate = Input { Value = 0.15, },
						Color1Red = Input { Value = 1, },
						Color1Green = Input { Value = 1, },
						Color1Blue = Input { Value = 1, },
						Color1Alpha = Input { Value = 1, },
						Color2Red = Input { Value = 0, },
						Color2Green = Input { Value = 0, },
						Color2Blue = Input { Value = 0, },
					},
					ViewInfo = OperatorInfo { Pos = { 131.354, 178.051 } },
				},
				CoordinateSpace = CoordSpace {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Shape = Input { Value = 1, },
						Input = Input {
							SourceOp = "FastNoise",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 297.682, 178.051 } },
				},
				Background3 = Background {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Width = Input {
							Value = 960,
							Expression = "PipeRouter1.Input.OriginalWidth",
						},
						Height = Input {
							Value = 960,
							Expression = "Width",
						},
						Depth = Input { Value = 4, },
						["Gamut.SLogVersion"] = Input { Value = FuID { "SLog2" }, },
						TopLeftAlpha = Input { Value = 0, },
						Start = Input { Value = { 0.5, 1 }, },
						End = Input { Value = { 0.5, 0 }, },
						Gradient = Input {
							Value = Gradient {
								Colors = {
									[0] = { 1, 0, 0, 1 },
									[0.16666666] = { 0.9999994635582, 1, 2.644956e-07, 1 },
									[0.333333333333] = { 0, 1, 4.377216e-07, 1 },
									[0.5] = { 0, 1, 1, 1 },
									[0.66666666] = { 0, 0, 1, 1 },
									[0.866666666] = { 1, 0, 1, 1 },
									[1] = { 1, 0, 0, 1 }
								}
							},
						},
						Repeat = Input { Value = FuID { "Repeat" }, },
					},
					ViewInfo = OperatorInfo { Pos = { -175.312, 179.76 } },
				},
				Blur = Blur {
					NameSet = true,
					Inputs = {
						Filter = Input {
							Value = FuID { "Fast Gaussian" },
							Expression = "tonumber(bmd._VERSION:sub(1,2)) >= 15 and \"Fast Gaussian\" or \"Gaussian\"",
						},
						LockXY = Input { Value = 0, },
						XBlurSize = Input { Value = 60, },
						Input = Input {
							SourceOp = "Background",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -312.984, 114.557 } },
				},
				Merge = Merge {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Background = Input {
							SourceOp = "Background3",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "Blur",
							Source = "Output",
						},
						PerformDepthMerge = Input { Value = 0, },
					},
					ViewInfo = OperatorInfo { Pos = { -175.312, 114.557 } },
				},
				Background = Background {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						EffectMask = Input {
							SourceOp = "Rectangle",
							Source = "Mask",
						},
						Width = Input {
							Value = 960,
							Expression = "PipeRouter1.Input.OriginalWidth",
						},
						Height = Input {
							Value = 32,
							Expression = "PipeRouter1.Input.OriginalWidth/30*Thickness",
						},
						Depth = Input { Value = 4, },
						["Gamut.SLogVersion"] = Input { Value = FuID { "SLog2" }, },
						Type = Input { Value = FuID { "Gradient" }, },
						TopLeftRed = Input { Value = 1, },
						TopLeftGreen = Input { Value = 1, },
						TopLeftBlue = Input { Value = 1, },
						Start = Input {
							Value = { 0.5, 1 },
							Expression = "Point(0.5, 1.0*(1-Invert))",
						},
						End = Input {
							Value = { 0.5, 0 },
							Expression = "Point(0.5, 1.0*Invert)",
						},
						Gradient = Input {
							Value = Gradient {
								Colors = {
									[0] = { 0, 0, 1, 1 },
									[0.166666666] = { 1, 0, 1, 1 },
									[0.333333333] = { 1, 0, 0, 1 },
									[0.5] = { 0.9999994635582, 1, 2.644956e-07, 1 },
									[0.6666666666] = { 0, 1, 4.377216e-07, 1 },
									[0.83333333333] = { 0, 1, 1, 1 },
									[1] = { 0, 0, 1, 1 }
								}
							},
						},
						GradientInterpolationMethod = Input { Value = FuID { "HLS" }, },
						Repeat = Input { Value = FuID { "Repeat" }, },
					},
					ViewInfo = OperatorInfo { Pos = { -471.258, 114.557 } },
					UserControls = ordered() {
						Invert = {
							CBC_TriState = false,
							INP_Integer = false,
							LINKID_DataType = "Number",
							INPID_InputControl = "CheckboxControl",
							LINKS_Name = "Invert",
						},
						Thickness = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "SliderControl",
							INP_MaxScale = 10,
							INP_Default = 1,
							INP_MinScale = 1,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							LINKS_Name = "Thickness",
						}
					}
				},
				Rectangle = RectangleMask {
					NameSet = true,
					Inputs = {
						Filter = Input {
							Value = FuID { "Fast Gaussian" },
							Expression = "tonumber(bmd._VERSION:sub(1,2)) >= 15 and \"Fast Gaussian\" or \"Gaussian\"",
						},
						MaskWidth = Input { Value = 1920, },
						MaskHeight = Input { Value = 1080, },
						PixelAspect = Input { Value = { 1, 1 }, },
						ClippingMode = Input { Value = FuID { "None" }, },
						Width = Input { Expression = "Size", },
						Height = Input { Value = 1, },
					},
					ViewInfo = OperatorInfo { Pos = { -471.258, 58.6709 } },
					UserControls = ordered() {
						Size = {
							LINKS_Name = "Size",
							INP_Integer = false,
							LINKID_DataType = "Number",
							ICS_ControlPage = "Controls",
							INPID_InputControl = "SliderControl",
							INP_MinScale = 0,
							INP_MaxScale = 1,
							INP_Default = 0.5
						}
					}
				},
				Scale = Scale {
					NameSet = true,
					Inputs = {
						XSize = Input { Value = 0.5, },
						HiQOnly = Input { Value = 0, },
						PixelAspect = Input { Value = { 1, 1 }, },
						Input = Input {
							SourceOp = "PipeRouter",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 534.525, 29.8986 } },
				},
				Hoop = Merge {
					NameSet = true,
					Inputs = {
						Background = Input {
							SourceOp = "PipeRouter1",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "Transform",
							Source = "Output",
						},
						Center = Input { Expression = "(self:GetSourceTool\"Controls\".HoRig)*EnableOffset+(self:GetSourceTool\"Controls\".PointIn2)*(1-EnableOffset)", },
						Size = Input {
							SourceOp = "AUTO_R",
							Source = "NumberResult",
						},
						Angle = Input {
							Value = -25.3344255473814,
							Expression = "AutoRotation*EnableAutoRotation",
						},
						Operator = Input { Value = FuID { "In" }, },
						FilterMethod = Input { Value = 1, },
						FlattenTransform = Input { Value = 1, },
						PerformDepthMerge = Input { Value = 0, },
						SizeCopy = Input { Value = 1.25, },
						AutoRotation = Input {
							SourceOp = "Calculation1_1",
							Source = "Result",
						},
						EnableAutoRotation = Input { Value = 1, },
					},
					ViewInfo = OperatorInfo { Pos = { 534.525, 114.557 } },
					UserControls = ordered() {
						Controls = {
							LINKID_DataType = "Image",
							INP_Default = 0,
							IC_ControlPage = 0,
							INPID_InputControl = "ImageControl",
							LINKS_Name = "Controls",
						},
						SizeCopy = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "SliderControl",
							INP_MaxScale = 10,
							INP_Default = 7,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							ICD_Center = 1,
							LINKS_Name = "SizeCopy",
						},
						EnableOffset = {
							CBC_TriState = false,
							INP_Integer = false,
							LINKID_DataType = "Number",
							INPID_InputControl = "CheckboxControl",
							LINKS_Name = "Enable Offset",
						},
						AutoRotation = {
							INPID_InputControl = "ScrewControl",
							INP_Integer = false,
							LINKID_DataType = "Number",
							LINKS_Name = "AutoRotation",
						},
						EnableAutoRotation = {
							CBC_TriState = false,
							INP_Integer = false,
							LINKID_DataType = "Number",
							INPID_InputControl = "CheckboxControl",
							LINKS_Name = "Enable Auto Rotation",
						}
					}
				},
				AUTO_R = Expression {
					CtrlWZoom = false,
					NameSet = true,
					Inputs = {
						n1 = Input {
							Value = 0.283068617344534,
							Expression = "Hoop:GetSourceTool\"Controls\".PointIn1.X",
						},
						n2 = Input {
							Value = 0.383952517618714,
							Expression = "(Hoop:GetSourceTool\"Controls\".PointIn1.Y)/(PipeRouter1.Input.OriginalWidth/PipeRouter1.Input.OriginalHeight)",
						},
						n3 = Input {
							Value = 0.5,
							Expression = "Hoop:GetSourceTool\"Controls\".PointIn2.X",
						},
						n4 = Input {
							Value = 0.28125,
							Expression = "(Hoop:GetSourceTool\"Controls\".PointIn2.Y)/(PipeRouter1.Input.OriginalWidth/PipeRouter1.Input.OriginalHeight)",
						},
						n5 = Input { Value = 2.5, },
						NumberExpression = Input { Value = "dist(n1,n2,n3,n4)*n5+n6", },
					},
				},
				SetDomain = SetDomain {
					NameSet = true,
					Inputs = {
						Mode = Input { Value = FuID { "Set" }, },
						Input = Input {
							SourceOp = "Merge5",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 534.525, 339.908 } },
				},
				Merge5 = Merge {
					NameSet = true,
					Inputs = {
						Background = Input {
							SourceOp = "PipeRouter",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "Resize",
							Source = "Output",
						},
						Gain = Input { Value = 0, },
						FilterMethod = Input { Value = 1, },
						FlattenTransform = Input { Value = 1, },
						PerformDepthMerge = Input { Value = 0, },
					},
					ViewInfo = OperatorInfo { Pos = { 534.525, 270.078 } },
				},
				Resize = BetterResize {
					NameSet = true,
					Inputs = {
						Width = Input {
							Value = 1920,
							Expression = "Scale.Input.OriginalWidth",
						},
						Height = Input {
							Value = 1080,
							Expression = "Scale.Input.OriginalHeight",
						},
						HiQOnly = Input { Value = 0, },
						PixelAspect = Input { Value = { 1, 1 }, },
						Input = Input {
							SourceOp = "Hoop",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 534.525, 190.41 } },
				},
				Transform = Transform {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						EffectMask = Input {
							SourceOp = "CoordinateSpace",
							Source = "Output",
						},
						MultiplyByMask = Input { Value = 1, },
						MaskChannel = Input { Value = 5, },
						FlattenTransform = Input { Value = 1, },
						Input = Input {
							SourceOp = "Blur3",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 297.682, 114.557 } },
				},
				Blur3 = Blur {
					NameSet = true,
					Inputs = {
						Filter = Input {
							Value = FuID { "Fast Gaussian" },
							Expression = "tonumber(bmd._VERSION:sub(1,2)) >= 15 and \"Fast Gaussian\" or \"Gaussian\"",
						},
						LockXY = Input { Value = 0, },
						XBlurSize = Input { Value = 60, },
						YBlurSize = Input { Value = 4, },
						Input = Input {
							SourceOp = "CoordinateSpace2",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 123.789, 114.557 } },
				},
				CoordinateSpace2 = CoordSpace {
					NameSet = true,
					Inputs = {
						Shape = Input { Value = 1, },
						Input = Input {
							SourceOp = "Merge",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -24.1698, 114.557 } },
				}
			},
		},
		Calculation1_1 = Calculation {
			CtrlWZoom = false,
			Inputs = {
				FirstOperand = Input {
					SourceOp = "Offset1_2",
					Source = "Angle",
				},
				Operator = Input { Value = 2, },
				SecondOperand = Input { Value = -1, },
			},
		},
		Offset1_2 = Offset {
			CtrlWZoom = false,
			Inputs = {
				Position = Input {
					Value = { 0.283068617344534, 0.68258225354438 },
					Expression = "Hoop:GetSourceTool\"Controls\".PointIn1",
				},
				Offset = Input {
					Value = { 0.5, 0.5 },
					Expression = "Hoop:GetSourceTool\"Controls\".PointIn2",
				},
				ImageAspect = Input { Value = 1.777778, },
			},
		}
	},
	ActiveTool = "ml_LFHoop"
}