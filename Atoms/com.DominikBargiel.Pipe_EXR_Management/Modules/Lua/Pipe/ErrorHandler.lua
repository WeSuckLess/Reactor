--[[--
ErrorHandler.lua - v1.0
A module for structured error logging and handling in the Pipe system
--]]--

local ErrorHandler = {}

-- Error levels
ErrorHandler.Level = {
    INFO = "INFO",
    WARNING = "WARNING",
    ERROR = "ERROR",
    CRITICAL = "CRITICAL"
}

-- Default configuration
local config = {
    logToConsole = true,
    logToFile = false,
    logFile = "PipeErrors.log",
    minLevel = ErrorHandler.Level.WARNING
}

-- Configure error handler
function ErrorHandler.configure(options)
    if type(options) ~= "table" then return end
    
    for k, v in pairs(options) do
        config[k] = v
    end
end

-- Format error message
local function formatMessage(source, message, level)
    -- Remove timestamp for cleaner logs
    return string.format("[%s][%s] %s", level, source, message)
end

-- Write to log file
local function writeToLog(formattedMessage)
    if not config.logToFile then return end
    
    local file = io.open(config.logFile, "a")
    if file then
        file:write(formattedMessage .. "\n")
        file:close()
    end
end

-- Main error capture function
function ErrorHandler.capture(source, message, level)
    level = level or ErrorHandler.Level.ERROR
    
    -- Skip if below minimum level
    local levels = {
        INFO = 1,
        WARNING = 2,
        ERROR = 3,
        CRITICAL = 4
    }
    
    if levels[level] < levels[config.minLevel] then
        return
    end
    
    local formattedMessage = formatMessage(source, message, level)
    
    -- Log to console if enabled
    if config.logToConsole then
        print(formattedMessage)
    end
    
    -- Write to log file if enabled
    writeToLog(formattedMessage)
    
    -- For critical errors, show dialog
    if level == ErrorHandler.Level.CRITICAL then
        if fu then
            fu:ShowError(message)
        end
    end
end

-- Helper functions for different error levels
function ErrorHandler.info(source, message)
    ErrorHandler.capture(source, message, ErrorHandler.Level.INFO)
end

function ErrorHandler.warning(source, message)
    ErrorHandler.capture(source, message, ErrorHandler.Level.WARNING)
end

function ErrorHandler.error(source, message)
    ErrorHandler.capture(source, message, ErrorHandler.Level.ERROR)
end

function ErrorHandler.critical(source, message)
    ErrorHandler.capture(source, message, ErrorHandler.Level.CRITICAL)
end

return ErrorHandler 