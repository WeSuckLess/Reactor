--[[--
Edit Selected Fuses.lua - 1.0 2025-02-18
By Andrew Hazelden <andrew@andrewhazelden.com>

A tool script to quickly open the selected fuse nodes in an external text editor.

--]]--

if tool and tool.ID:match("^Fuse.") then
    -- Edit code based upon work by Kristof Indeherberge
    tool.ScriptEdit[fu.TIME_UNDEFINED] = 1
    tool.ScriptEdit[fu.TIME_UNDEFINED] = 0
end
