--[[-- 
https://emiliosapia.myportfolio.com
Written by : Emilio Sapia
Written on : Mar, 2023
version 1.0

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--]]--

{
	Tools = ordered() {
		ml_TrackerTool1 = MacroOperator {
			CtrlWZoom = false,
			CustomData = {
				Path = {
					Map = {
						["Setting:"] = "Macros:/"
					}
				},
			},
			Inputs = ordered() {
				CommentsNest = Input { Value = 0, },
				MainInput1 = InstanceInput {
					SourceOp = "Transform11",
					Source = "Input",
				},
				Blank = InstanceInput {
					SourceOp = "Transform11",
					Source = "Blank1",
				},
				Tracker = InstanceInput {
					SourceOp = "Transform11",
					Source = "Tracker",
					Default = 0,
				},
				Mode = InstanceInput {
					SourceOp = "Transform11",
					Source = "Mode",
					Page = "Controls",
				},
				Blank0 = InstanceInput {
					SourceOp = "Transform11",
					Source = "Blank1",
				},
				CenterValue = InstanceInput {
					SourceOp = "Transform11",
					Source = "Value",
					Name = "Center Value",
					Page = "Controls",
					Default = 1,
				},
				AngleValue = InstanceInput {
					SourceOp = "Transform11",
					Source = "AngleValue",
					Page = "Controls",
					Default = 1,
				},
				SizeValue = InstanceInput {
					SourceOp = "Transform11",
					Source = "SizeValue",
					Page = "Controls",
					Default = 1,
				},
				Blank100 = InstanceInput {
					SourceOp = "Transform11",
					Source = "Blank1",
				},
				Edges = InstanceInput {
					SourceOp = "Transform11",
					Source = "Edges",
					Page = "Controls",
					Default = 0,
				},
				Filter = InstanceInput {
					SourceOp = "Transform11",
					Source = "FilterMethod",
					Page = "Controls",
				},
				Window = InstanceInput {
					SourceOp = "Transform11",
					Source = "WindowMethod",
					Page = "Controls",
				},
				Flatten = InstanceInput {
					SourceOp = "Transform11",
					Source = "FlattenTransform",
					Page = "Controls",
				},
				Blend = InstanceInput {
					SourceOp = "Transform11",
					Source = "Blend",
					Page = "Common",
					Default = 1,
				},
				Input9 = InstanceInput {
					SourceOp = "Transform11",
					Source = "ProcessWhenBlendIs00",
					Default = 0,
				},
				Input10 = InstanceInput {
					SourceOp = "Transform11",
					Source = "ProcessRed",
					Name = "Process",
					ControlGroup = 11,
					Default = 1,
				},
				Input11 = InstanceInput {
					SourceOp = "Transform11",
					Source = "ProcessGreen",
					ControlGroup = 11,
					Default = 1,
				},
				Input12 = InstanceInput {
					SourceOp = "Transform11",
					Source = "ProcessBlue",
					ControlGroup = 11,
					Default = 1,
				},
				Input13 = InstanceInput {
					SourceOp = "Transform11",
					Source = "ProcessAlpha",
					ControlGroup = 11,
					Default = 1,
				},
				Input14 = InstanceInput {
					SourceOp = "Transform11",
					Source = "Blank1",
				},
				Input15 = InstanceInput {
					SourceOp = "Transform11",
					Source = "ApplyMaskInverted",
					Default = 0,
				},
				Input16 = InstanceInput {
					SourceOp = "Transform11",
					Source = "MultiplyByMask",
					Default = 0,
				},
				Input17 = InstanceInput {
					SourceOp = "Transform11",
					Source = "FitMask",
				},
				Input18 = InstanceInput {
					SourceOp = "Transform11",
					Source = "Blank2",
				},
				Input19 = InstanceInput {
					SourceOp = "Transform11",
					Source = "MaskChannel",
					Default = 3,
				},
				Input20 = InstanceInput {
					SourceOp = "Transform11",
					Source = "MaskLow",
					ControlGroup = 18,
					Default = 0,
				},
				Input21 = InstanceInput {
					SourceOp = "Transform11",
					Source = "MaskHigh",
					ControlGroup = 18,
					Default = 1,
				},
				Input22 = InstanceInput {
					SourceOp = "Transform11",
					Source = "MaskClipBlack",
					Name = "Black",
					Width = 0.5,
					Default = 1,
				},
				Input23 = InstanceInput {
					SourceOp = "Transform11",
					Source = "MaskClipWhite",
					Name = "White",
					Width = 0.5,
					Default = 1,
				},
				Input24 = InstanceInput {
					SourceOp = "Transform11",
					Source = "Blank4",
				},
				Input25 = InstanceInput {
					SourceOp = "Transform11",
					Source = "UseObject",
					Default = 0,
				},
				Input26 = InstanceInput {
					SourceOp = "Transform11",
					Source = "UseMaterial",
					Default = 0,
				},
				Input27 = InstanceInput {
					SourceOp = "Transform11",
					Source = "CorrectEdges",
				},
				Input28 = InstanceInput {
					SourceOp = "Transform11",
					Source = "ObjectID",
					ControlGroup = 25,
					Default = 0,
				},
				Input29 = InstanceInput {
					SourceOp = "Transform11",
					Source = "MaterialID",
					ControlGroup = 25,
					Default = 0,
				},
				Input30 = InstanceInput {
					SourceOp = "Transform11",
					Source = "Blank5",
				},
				Input31 = InstanceInput {
					SourceOp = "Transform11",
					Source = "MotionBlur",
					Default = 0,
				},
				Input32 = InstanceInput {
					SourceOp = "Transform11",
					Source = "Quality",
					Default = 2,
				},
				Input33 = InstanceInput {
					SourceOp = "Transform11",
					Source = "ShutterAngle",
					Default = 180,
				},
				Input34 = InstanceInput {
					SourceOp = "Transform11",
					Source = "CenterBias",
					Default = 0,
				},
				Input35 = InstanceInput {
					SourceOp = "Transform11",
					Source = "SampleSpread",
					Default = 1,
				},
				Input36 = InstanceInput {
					SourceOp = "Transform11",
					Source = "Blank6",
				},
				Input37 = InstanceInput {
					SourceOp = "Transform11",
					Source = "UseGPU",
					Default = 1,
				},
				Version = InstanceInput {
					SourceOp = "Transform11",
					Source = "Version",
					Name = "<p style='color:#555555; text-align: center;'>TrackerTool v1.0 © Emilio Sapia 2024</p>",
					Page = "Controls",
				},
				EffectMask = InstanceInput {
					SourceOp = "Transform11",
					Source = "EffectMask",
				}
			},
			Outputs = {
				MainOutput1 = InstanceOutput {
					SourceOp = "Transform11",
					Source = "Output",
				}
			},
			ViewInfo = GroupInfo {
				Pos = { 110, 676.5 },
				Flags = {
					AllowPan = false,
					GridSnap = true,
					ConnectedSnap = true,
					AutoSnap = true,
					ShowInstance = false,
					Thumbnails = false,
					RemoveRouters = true
				},
				Size = { 126, 66.3636, 63, 24.2424 },
				Direction = "Vertical",
				PipeStyle = "Direct",
				Scale = 1,
				Offset = { 0, 0 }
			},
			Tools = ordered() {
				Transform11 = Transform {
					NameSet = true,
					Inputs = {
						TransformNest = Input { Value = 0, },
						Center = Input { Expression = "XY_Time", },
						Pivot = Input { Expression = "self:GetSourceTool(\"Tracker\").SteadyAxis", },
						Size = Input { Expression = "Size_Time", },
						Angle = Input { Expression = "Angle_Time", },
						InvertTransform = Input {
							Value = 1,
							Expression = "Mode",
						},
						ReferenceSize = Input { Value = 1, },
						XY = Input { Expression = "self:GetSourceTool(\"Tracker\").SteadyPosition", },
						XY_Time = Input { Expression = "Point(0.5-(((self:GetValue(\"XY\", ReferenceFrame).X)-XY.X)*Value), 0.5-(((self:GetValue(\"XY\", ReferenceFrame).Y)-XY.Y)*Value))", },
						ReferenceFrame = Input { Expression = "ml_TrackerTool1.ReferenceFrame", },
						Angle_Ref = Input { Expression = "self:GetSourceTool(\"Tracker\").SteadyAngle", },
						Angle_Time = Input { Expression = "0-(self:GetValue(\"Angle_Ref\", ReferenceFrame)-Angle_Ref)*AngleValue", },
						Size_Ref = Input { Expression = "self:GetSourceTool(\"Tracker\").SteadySize", },
						Size_Time = Input { Expression = "1-(self:GetValue(\"Size_Ref\", ReferenceFrame)-Size_Ref)*SizeValue", },
						ActivateTranslation = Input { Value = 0, },
						ActivateRotation = Input { Value = 0, },
						ActivateSize = Input { Value = 0, }
					},
					ViewInfo = OperatorInfo { Pos = { 0, 16.5 } },
					UserControls = ordered() {
						Tracker = {
							LINKID_DataType = "Image",
							INP_Default = 0,
							IC_ControlPage = 0,
							INPID_InputControl = "ImageControl",
							LINKS_Name = "Tracker",
						},
						XY = {
							ICS_ControlPage = "Controls",
							INPID_InputControl = "OffsetControl",
							LINKID_DataType = "Point",
							LINKS_Name = "XY",
						},
						XY_Time = {
							ICS_ControlPage = "Controls",
							INPID_InputControl = "OffsetControl",
							LINKID_DataType = "Point",
							LINKS_Name = "XY_Time",
						},
						ReferenceFrame = {
							INP_Integer = true,
							LINKID_DataType = "Number",
							INP_Default = 1001,
							ICS_ControlPage = "Controls",
							INPID_InputControl = "ScrewControl",
							LINKS_Name = "Reference Frame",
						},
						Value = {
							INP_Default = 1,
							INP_Integer = false,
							ICS_ControlPage = "Controls",
							LINKID_DataType = "Number",
							INP_MinScale = 0,
							INPID_InputControl = "SliderControl",
							INP_MaxScale = 1,
							LINKS_Name = "Value",
						},
						Mode = {
							{ MBTNC_AddButton = "Stabilize" },
							{ MBTNC_AddButton = "Matchmove" },
							INP_Integer = false,
							INPID_InputControl = "MultiButtonControl",
							MBTNC_ShowBasicButton = true,
							ICS_ControlPage = "Controls",
							LINKID_DataType = "Number",
							MBTNC_ShowName = true,
							MBTNC_StretchToFit = true,
							MBTNC_ShowToolTip = true,
							LINKS_Name = "Mode",
						},
						Angle_Ref = {
							INP_Integer = false,
							LINKID_DataType = "Number",
							INP_Default = 0,
							ICS_ControlPage = "Controls",
							INPID_InputControl = "ScrewControl",
							LINKS_Name = "Angle_Ref",
						},
						Angle_Time = {
							INP_Integer = false,
							LINKID_DataType = "Number",
							ICS_ControlPage = "Controls",
							INPID_InputControl = "ScrewControl",
							LINKS_Name = "Angle_Time",
						},
						Size_Ref = {
							INP_Integer = false,
							LINKID_DataType = "Number",
							INP_Default = 1,
							ICS_ControlPage = "Controls",
							INPID_InputControl = "SliderControl",
							LINKS_Name = "Size_Ref",
						},
						Size_Time = {
							INP_Integer = false,
							LINKID_DataType = "Number",
							ICS_ControlPage = "Controls",
							INPID_InputControl = "SliderControl",
							LINKS_Name = "Size_Time",
						},
						ActivateTranslation = {
							CBC_TriState = false,
							INP_Integer = false,
							LINKID_DataType = "Number",
							INP_Default = 1,
							ICS_ControlPage = "Controls",
							INPID_InputControl = "CheckboxControl",
							LINKS_Name = "Activate Translation",
						},
						ActivateRotation = {
							CBC_TriState = false,
							INP_Integer = false,
							INP_External = false,
							LINKID_DataType = "Number",
							INP_Default = 1,
							ICS_ControlPage = "Controls",
							INPID_InputControl = "CheckboxControl",
							LINKS_Name = "Activate Rotation",
						},
						ActivateSize = {
							CBC_TriState = false,
							INP_Integer = false,
							LINKID_DataType = "Number",
							INP_Default = 1,
							ICS_ControlPage = "Controls",
							INPID_InputControl = "CheckboxControl",
							LINKS_Name = "Activate Size",
						},
						AngleValue = {
							INP_Default = 1,
							INP_Integer = false,
							ICS_ControlPage = "Controls",
							LINKID_DataType = "Number",
							INP_MinScale = 0,
							INPID_InputControl = "SliderControl",
							INP_MaxScale = 1,
							LINKS_Name = "Angle Value",
						},
						SizeValue = {
							INP_Default = 1,
							INP_Integer = false,
							ICS_ControlPage = "Controls",
							LINKID_DataType = "Number",
							INP_MinScale = 0,
							INPID_InputControl = "SliderControl",
							INP_MaxScale = 1,
							LINKS_Name = "Size Value",
						},
						Axis_Ref = {
							LINKS_Name = "Axis_Ref",
							LINKID_DataType = "Point",
							INPID_InputControl = "OffsetControl",
							ICS_ControlPage = "Controls",
						},
						Axis_Time = {
							LINKS_Name = "Axis_Time",
							LINKID_DataType = "Point",
							INPID_InputControl = "OffsetControl",
							ICS_ControlPage = "Controls",
						},
						Version = {
							INP_External = false,
							IC_ControlPage = 0,
							LBLC_DropDownButton = false,
							LINKID_DataType = "Number",
							LBLC_NumInputs = 1,
							INP_Passive = true,
							INPID_InputControl = "NestControl",
							LINKS_Name = "<p style='color:#555555; text-align: center;'>Macro name β 0.0</p>",
						}
					}
				}
			},
			UserControls = ordered() {
				Controls = ControlPage {
					CT_Visible = true,
					CTID_DIB_ID = "Icons.Tools.Tabs.Transform",
					CTS_FullName = "Tooltip for Page1"
				},
				ReferenceFrame = {
					INP_MaxAllowed = 1000000,
					INP_Integer = true,
					INPID_InputControl = "ScrewControl",
					INP_MaxScale = 1001,
					INP_Default = 1001,
					INP_MinScale = 0,
					INP_MinAllowed = -1000000,
					LINKID_DataType = "Number",
					ICS_ControlPage = "Controls",
					LINKS_Name = "Reference Frame"
				},
				SetReferenceFrame = {
					INP_MaxAllowed = 1000000,
					INP_Integer = false,
					INPID_InputControl = "ButtonControl",
					BTNCS_Execute = "                                    tool:SetInput('ReferenceFrame', comp.CurrentTime, fu.TIME_UNDEFINED)",
					INP_MaxScale = 1,
					INP_MinScale = 0,
					INP_MinAllowed = -1000000,
					LINKID_DataType = "Number",
					ICS_ControlPage = "Controls",
					ICD_Width = 0.5,
					LINKS_Name = "Set Reference Frame"
				},
				GoToFrame = {
					INP_MaxAllowed = 1000000,
					INP_Integer = false,
					INPID_InputControl = "ButtonControl",
					BTNCS_Execute = "comp.CurrentTime = tool.ReferenceFrame[fu.TIME_UNDEFINED]",
					INP_MaxScale = 1,
					INP_MinScale = 0,
					INP_MinAllowed = -1000000,
					LINKID_DataType = "Number",
					ICS_ControlPage = "Controls",
					ICD_Width = 0.5,
					LINKS_Name = "Go To Frame"
				}
			}
		}
	},
	ActiveTool = "ml_TrackerTool1"
}