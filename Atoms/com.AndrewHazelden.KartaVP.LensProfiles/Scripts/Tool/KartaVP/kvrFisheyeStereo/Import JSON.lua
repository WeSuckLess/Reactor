dump("[" .. tostring(tool.Name) .. "] Import JSON Script")

local dir = "Scripts:/Support/Kartaverse/LensProfiles/"
if not bmd.direxists(comp:MapPath(dir)) then
	bmd.createdir(comp:MapPath(dir))
	print("[Created Folder] " .. tostring(comp:MapPath(dir)) .. "\n")
end

-- Import JSON data
local json_str = ""
local tbl = {}
selectedFilePath = comp:MapPath(fu:RequestFile(dir, file))
if selectedFilePath then
	-- Import the JSON file
	print("[Import JSON File] " .. tostring(selectedFilePath) .. "\n")
	
	-- Code from Vonk Ultra vJSON "vjsonutils.lua" Lua Module
	local json = require("dkjson")
	local fp = io.open(selectedFilePath, "r")
	if fp == nil then
		error(string.format("file does not exist: %s", selectedFilePath))
	end
	local json_str = fp:read("*all")
	fp:close()
	tbl = json.decode(json_str)

	if type(tbl) == "table" then
		table.sort(tbl)
		dump(tbl)
	
		-- Add the input control data
		tool:SetInput("Camera", tbl.Params.Camera, comp.CurrentTime)
		tool:SetInput("Lens", tbl.Params.Lens, comp.CurrentTime)
		tool:SetInput("ViewMode", tbl.Params.ViewMode, comp.CurrentTime)
		tool:SetInput("Anaglyph", tbl.Params.Anaglyph, comp.CurrentTime)
		tool:SetInput("FieldOfView", tbl.Params.FieldOfView, comp.CurrentTime)
		tool:SetInput("Convergence", tbl.Params.Convergence, comp.CurrentTime)
		tool:SetInput("CircularMaskDiameter", tbl.Params.CircularMaskDiameter, comp.CurrentTime)
		tool:SetInput("CircularMaskSoftness", tbl.Params.CircularMaskSoftness, comp.CurrentTime)
		tool:SetInput("Exposure1", tbl.Params.Exposure1, comp.CurrentTime)
		tool:SetInput("Gamma1", tbl.Params.Gamma1, comp.CurrentTime)
		tool:SetInput("Saturation1", tbl.Params.Saturation1, comp.CurrentTime)
		tool:SetInput("Vibrance1", tbl.Params.Vibrance1, comp.CurrentTime)
		tool:SetInput("Exposure2", tbl.Params.Exposure2, comp.CurrentTime)
		tool:SetInput("Gamma2", tbl.Params.Gamma2, comp.CurrentTime)
		tool:SetInput("Saturation2", tbl.Params.Saturation2, comp.CurrentTime)
		tool:SetInput("Vibrance2", tbl.Params.Vibrance2, comp.CurrentTime)
		tool:SetInput("Center1", tbl.Params.Center1, comp.CurrentTime)
		tool:SetInput("Width1", tbl.Params.Width1, comp.CurrentTime)
		tool:SetInput("Height1", tbl.Params.Height1, comp.CurrentTime)
		tool:SetInput("Angle1", tbl.Params.Angle1, comp.CurrentTime)
		tool:SetInput("Center2", tbl.Params.Center2, comp.CurrentTime)
		tool:SetInput("Width2", tbl.Params.Width2, comp.CurrentTime)
		tool:SetInput("Height2", tbl.Params.Height2, comp.CurrentTime)
		tool:SetInput("Angle2", tbl.Params.Angle2, comp.CurrentTime)
		tool:SetInput("a", tbl.Params.a, comp.CurrentTime)
		tool:SetInput("b", tbl.Params.b, comp.CurrentTime)
		tool:SetInput("c", tbl.Params.c, comp.CurrentTime)
		tool:SetInput("cx", tbl.Params.cx, comp.CurrentTime)
		tool:SetInput("cy", tbl.Params.cy, comp.CurrentTime)
		tool:SetInput("XShift", tbl.Params.XShift, comp.CurrentTime)
		tool:SetInput("YShift", tbl.Params.YShift, comp.CurrentTime)
		tool:SetInput("STMapWidth", tbl.Params.STMapWidth, comp.CurrentTime)
		tool:SetInput("STMapHeight", tbl.Params.STMapHeight, comp.CurrentTime)
	
		-- Rename the node
		tool:SetAttrs({TOOLS_Name = tbl.Name})
	else
		print("[Import JSON] There was a json formatting issue with the Lua table output.\n")
	end
else
	print("[Import JSON File] Skipped. No Filename specified.\n")
end

