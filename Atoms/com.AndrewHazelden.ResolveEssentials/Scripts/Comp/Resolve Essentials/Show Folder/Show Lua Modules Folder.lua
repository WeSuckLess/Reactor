-- Show Lua Modules Folder menu item
-- v4 - 2024-08-15

local path = app:MapPath("LuaModules:/")
if bmd.direxists(path) == false then
	bmd.createdir(path)
	print("[Created Lua Modules Folder] " .. path)
end

print("[Show Lua Modules Folder] " .. path)
bmd.openfileexternal("Open", path)
