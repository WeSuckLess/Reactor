dump("[" .. tostring(tool.Name) .. "] Export JSON Script")

-- Add the input control data
local tbl = {}
tbl["Camera"] = tool:GetInput("Camera", comp.CurrentTime)
tbl["Lens"] = tool:GetInput("Lens", comp.CurrentTime)
tbl["ViewMode"] = tool:GetInput("ViewMode", comp.CurrentTime)
tbl["Anaglyph"] = tool:GetInput("Anaglyph", comp.CurrentTime)
tbl["FieldOfView"] = tool:GetInput("FieldOfView", comp.CurrentTime)
tbl["Convergence"] = tool:GetInput("Convergence", comp.CurrentTime)
tbl["CircularMaskDiameter"] = tool:GetInput("CircularMaskDiameter", comp.CurrentTime)
tbl["CircularMaskSoftness"] = tool:GetInput("CircularMaskSoftness", comp.CurrentTime)
tbl["Exposure1"] = tool:GetInput("Exposure1", comp.CurrentTime)
tbl["Gamma1"] = tool:GetInput("Gamma1", comp.CurrentTime)
tbl["Saturation1"] = tool:GetInput("Saturation1", comp.CurrentTime)
tbl["Vibrance1"] = tool:GetInput("Vibrance1", comp.CurrentTime)
tbl["Exposure2"] = tool:GetInput("Exposure2", comp.CurrentTime)
tbl["Gamma2"] = tool:GetInput("Gamma2", comp.CurrentTime)
tbl["Saturation2"] = tool:GetInput("Saturation2", comp.CurrentTime)
tbl["Vibrance2"] = tool:GetInput("Vibrance2", comp.CurrentTime)
tbl["Center1"] = tool:GetInput("Center1", comp.CurrentTime)
tbl["Width1"] = tool:GetInput("Width1", comp.CurrentTime)
tbl["Height1"] = tool:GetInput("Height1", comp.CurrentTime)
tbl["Angle1"] = tool:GetInput("Angle1", comp.CurrentTime)
tbl["Center2"] = tool:GetInput("Center2", comp.CurrentTime)
tbl["Width2"] = tool:GetInput("Width2", comp.CurrentTime)
tbl["Height2"] = tool:GetInput("Height2", comp.CurrentTime)
tbl["Angle2"] = tool:GetInput("Angle2", comp.CurrentTime)
tbl["a"] = tool:GetInput("a", comp.CurrentTime)
tbl["b"] = tool:GetInput("b", comp.CurrentTime)
tbl["c"] = tool:GetInput("c", comp.CurrentTime)
tbl["cx"] = tool:GetInput("cx", comp.CurrentTime)
tbl["cy"] = tool:GetInput("cy", comp.CurrentTime)
tbl["XShift"] = tool:GetInput("XShift", comp.CurrentTime)
tbl["YShift"] = tool:GetInput("YShift", comp.CurrentTime)
tbl["STMapWidth"] = tool:GetInput("STMapWidth", comp.CurrentTime)
tbl["STMapHeight"] = tool:GetInput("STMapHeight", comp.CurrentTime)
table.sort(tbl)

local exportTBL = {}
exportTBL["Export"] = "kvrFisheyeStereo"
exportTBL["Name"] = tool.Name
exportTBL["Purpose"] = "Lens Profile"
exportTBL["Version"] = tool:GetInput("Version", comp.CurrentTime)
exportTBL["TOOLS_RegID"] = "GroupOperator"
exportTBL["Params"] = tbl
dump(exportTBL)

-- Export JSON data from a Lua Table
if type(exportTBL) == "table" then
	-- -------------------------------------------------------
	-- Code from Vonk Ultra vJSON "vjsonutils.lua" Lua Module
	-- -------------------------------------------------------
	local json = require("dkjson")
	local json_str = json.encode(exportTBL, {indent = true})
	dump(json_str)
	-- -------------------------------------------------------

	local file = (tool.Name or "kvrFisheyeStereo") .. ".json"
	local dir = "Scripts:/Support/Kartaverse/LensProfiles/"
	if not bmd.direxists(comp:MapPath(dir)) then
		bmd.createdir(comp:MapPath(dir))
		print("[Created Folder] " .. tostring(comp:MapPath(dir)) .. "\n")
	end
	
	-- Write the data to disk
	selectedFilePath = comp:MapPath(fu:RequestFile(dir, file, {FReqB_Saving = true}))
	if selectedFilePath then
		print("[Export JSON File] " .. tostring(selectedFilePath) .. "\n")
		-- -------------------------------------------------------
		-- Code from Vonk Ultra vJSON "vjsonutils.lua" Lua Module
		-- -------------------------------------------------------
		local directory = selectedFilePath:match("(.*[/\\])")
		if bmd.fileexists(directory) == false then
			bmd.createdir(directory)
			-- print('[Created Directory]', folder)
		end
		fp = io.open(selectedFilePath, "w")
		if fp == nil then
			error(string.format("[Export JSON File][Write Error] File or directory does not exist: %s", directory))
		end
		fp:write(json_str)
		fp:close()
		-- -------------------------------------------------------
	else
		print("[Export JSON File] Skipped. No Filename specified.\n")
	end
else
	print("[Export JSON] There was a json formatting issue with the Lua table output.\n")
end