-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vTextCreateArch"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Text\\Create",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Create a unique Fusion Text object per CPU architecture.",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]
    Inx86 = self:AddInput("x86" , "x86" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 1
    })

    Inx64 = self:AddInput("x64" , "x64" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 1
    })

    InARM64 = self:AddInput("ARM64" , "ARM64" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 1
    })

    OutText = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end


function GetArch()
    -- Find out the current CPU architecture
    return jit.arch
end


function Process(req)
    -- [[ Creates the output. ]]
    local x86 = Inx86:GetValue(req).Value
    local x64 = Inx64:GetValue(req).Value
    local arm64 = InARM64:GetValue(req).Value

    local out = ""

    local arch = GetArch()
    if arch == "x86" then
        -- Running on x86
        out = Text(x86)
    elseif arch == "x64" then
        -- Running on x64
        out = Text(x64)
    elseif arch == "arm64" then
        -- Running on Apple Silicon or Windows on ARM
        out = Text(arm64)
    else
        error("[TextCreateArch] There is an invalid CPU architecture detected")
    end

    OutText:Set(req, out)
end
