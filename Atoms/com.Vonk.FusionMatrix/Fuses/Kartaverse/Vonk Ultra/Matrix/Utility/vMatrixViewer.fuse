-- ============================================================================
-- modules
-- ============================================================================
local matrix = self and require("matrix") or nil
local matrixutils = self and require("vmatrixutils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vMatrixViewer"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Matrix\\Utility",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "View matrix content in the Inspector.",
    REGS_OpIconString = FUSE_NAME,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.Transform3D",
})

function Create()
    -- [[ Creates the user interface. ]]
    InTranspose = self:AddInput("Transpose Matrix", "Transpose", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InShowDump = self:AddInput("Show Dump", "ShowDump", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

--    InShowInput = self:AddInput("Show Input", "ShowInput", {
--        LINKID_DataType = "Number",
--        INPID_InputControl = "CheckboxControl",
--        INP_Integer = true,
--        INP_Default = 1.0,
--        INP_External = false,
--        INP_DoNotifyChanged = true
--    })

    InUISeparator2 = self:AddInput("UISeparator2", "UISeparator2", {
        IC_Visible = true,
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
    })

    InMatrix1 = self:AddInput("Matrix1", "Matrix1", {
        LINKID_DataType = "Text",
        --INPID_InputControl = "TextEditControl",
        --TEC_Wrap = true,
        LINK_Main = 1
    })

    InScriptVal = self:AddInput("Transform", "Transform", {
        LINKID_DataType = "ScriptVal",
        INPID_InputControl = "ScriptValListControl",
        IC_NoLabel = true,
        LC_Rows = 14,
        INP_Passive = true,
    })

    -- The output node connection where data is pushed out of the fuse
    OutMatrix = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end


function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
--    if inp == InDisplayLines then
--        local lines = InDisplayLines:GetSource(time, REQF_SecondaryTime).Value
--        InScriptVal:SetAttrs({LC_Rows = lines})
--        -- Toggle the visibility to refresh the inspector view
--        InScriptVal:SetAttrs({IC_Visible = false})
--        InScriptVal:SetAttrs({IC_Visible = true})
--    elseif inp == InShowInput then
--        local visible
--        if param.Value == 1.0 then visible = true else visible = false end
--        InMatrix1:SetAttrs({LINK_Visible = visible})
--    end
end

function OnConnected(inp, old, new)
    if inp == InMatrix1 and new ~= nil then
        -- New connection
    else
        InScriptVal:SetSource(ScriptValParam({}), 0)
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local mx_1_str = InMatrix1:GetValue(req).Value
    local show_dump = InShowDump:GetValue(req).Value
    local transpose = InTranspose:GetValue(req).Value

    -- Transpose matrix
    local mx_orig = matrixutils.matrix_from_string(mx_1_str)
    local mx = mx_orig
    if transpose == 1 then
        mx = matrix.transpose(mx_orig)
    end

    -- Translate
    local tx = mx[4][1]
    local ty = mx[4][2]
    local tz = mx[4][3]

    -- Rotate
    local rx = 0
    local ry = 0
    local rz = 0

    if math.cos(-math.asin(mx[1][3])) ~= 0 then
        rx = math.deg(math.atan2(mx[2][3], mx[3][3]))
    else
        rx = math.deg(math.atan2(mx[2][1], mx[2][2]))
    end

    ry = math.deg(-math.asin(mx[1][3]))

    if math.cos(-math.asin(mx[1][3])) ~= 0 then
        rz = math.deg(math.atan2(mx[1][2], mx[1][1]))
    end

    -- Scale
    local sx = math.sqrt(mx[1][1]^2 + mx[1][2]^2 + mx[1][3]^2)
    local sy = math.sqrt(mx[2][1]^2 + mx[2][2]^2 + mx[2][3]^2)
    local sz = math.sqrt(mx[3][1]^2 + mx[3][2]^2 + mx[3][3]^2)

    local tbl = {
        Translate = {
            X = tx,
            Y = ty,
            Z = tz,
        },
        Rotate = {
            X = rx,
            Y = ry,
            Z = rz,
        },
        Scale = {
            X = sx,
            Y = sy,
            Z = sz,
        },
    }

    if show_dump == 1 then
        print("\n----------------------")
        print("[" .. tostring(self.Name) .. "]")
        local txt_str = bmd.writestring(tbl)
        dump(txt_str)
    end

    InScriptVal:SetSource(ScriptValParam(tbl), 0)

    local out = Text(mx_1_str)
    OutMatrix:Set(req, out)
end
