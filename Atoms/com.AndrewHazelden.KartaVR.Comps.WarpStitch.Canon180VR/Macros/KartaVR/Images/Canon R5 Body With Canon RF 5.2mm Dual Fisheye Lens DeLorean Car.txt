The Canon EOS 180VR media used in this example composite features Keeley Ann Turner sitting in a DeLorean car at CES 2022. The footage was photographed by Hugh Hou (https://www.youtube.com/hughhoufilm). 

Media (c) Copyright 2022 Hugh Hou.

The media was made available for use in Resolve/Fusion/Kartaverse based immersive tutorials and example project files under a Creative Commons Attribution-ShareAlike 4.0 International License (https://creativecommons.org/licenses/by-sa/4.0/).

