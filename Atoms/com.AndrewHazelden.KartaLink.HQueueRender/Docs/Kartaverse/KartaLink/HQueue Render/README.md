# KartaLink HQueue Render - v5.73 2024-07-05
By Andrew Hazelden <andrew@andrewhazelden.com>

## Overview

This Kartaverse comp script allows Fusion Studio to submit compositing jobs for rendering via Houdini HQueue.


![KartaLink HQueue Render](Images/1-KartaLink-Submit-Fusion-Comps-to-Houdini-HQueue.png)

## Open Source Software License

- LGPL 3.0 License

## Known Issues

The script currently works with macOS and Linux based HQueue Client render nodes.

Windows support is currently under development. On Windows the following error is reported by HQueue when a job is run:

	[WinError 2] The system cannot find the file specified 
	ERROR: Could not run command.

## Installation

Copy the "HQueue Render.py" script into the Fusion Studio centric "Reactor:/Deploy/Scripts/Comp/KartaLink" PathMap based folder. (Create the needed sub-folders if required.)

## Usage

Step 1. Make sure you have defined the Reactor PathMap settings in your Fusion Render Node preferences. This will allow Reactor downloaded fuses and plugins to work correctly.

Step 2. Open the HQueue management webpage. (Typically this defaults to "http://localhost:5000"). Use the HQueue webUI to create an HQueue "Client Group" called "Fusion".

![HQueue Groups 1](Images/HQueue-Groups-1.png)
![HQueue Groups 2](Images/HQueue-Groups-2.png)

Step 3. Open a Fusion comp. Select the "Scripts > KartaLink > HQueue Render" menu item to submit a Fusion composite to your render farm.

Set the "Frames Per Task" value to define how large of a frame chunk you want each job task to use. A value of zero sets the job to render as a single job task.


![KartaLink HQueue Render 2](Images/2-HQueue-WebUI.png)

![KartaLink HQueue Render 3](Images/3-HQueue-WebUI.png)

![KartaLink HQueue Render 4](Images/4-HQueue-WebUI.png)

![KartaLink HQueue Render 5](Images/5-HQueue-WebUI.png)