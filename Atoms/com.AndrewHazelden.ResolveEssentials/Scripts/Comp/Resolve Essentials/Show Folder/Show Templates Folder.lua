-- Show Templates Folder menu item
-- v4 - 2024-08-15

local path = app:MapPath("Templates:/")
if bmd.direxists(path) == false then
	bmd.createdir(path)
	print("[Created Templates Folder] " .. path)
end

print("[Show Templates Folder] " .. path)
bmd.openfileexternal("Open", path)
