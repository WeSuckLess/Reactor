--[[--
Node Color Changer button script by Sander de Regt - ShadowMaker SdR 
based on the *.setting files from Ben The Animator, 
the 'Button with Icon'-script by Andrew Hazelden ,
the Eyeon legacy script 'Switch Motion blur'
and elements from the 'Fuskin Button Toolbar GUI'-script
with inspiration from Asher Roland's macro creator for showing me the proper use of stylesheets

This was originally thought up as a way to create colorful Sticky Notes at the click of a button
but re-imagined and Frankensteined together as a way to change the node color of all selected nodes at the click of a button

Version 1.01 - adds the option to return the colors to their default state
Version 2 - completely revamps the underlying system so no icons are needed - this is all buttons all the time
Added bonus is that the window (and its buttons) are completely responsive

## Overview ## 

This script creates 16 buttons to make it easy to change the color of selected nodes in the Fusion flow area

## Installation ## 

Step 1. Create a new "SdR" folder inside the Fusion user preferences "Scripts/Comp/" folder. 

Step 2. Copy the "SdRNodeColorChanger.lua" script into the "SdR" folder

Step 3. In Fusion select the "Script > SdR > SdR_NodeColorChanger" menu item.

--]]--

local ui = fu.UIManager
local disp = bmd.UIDispatcher(ui)
local width,height = 200,240


-- defining the colors once so I can just type in the name later

local orange = { R = 0.92156862745098, G = 0.431372549019608, B = 0 }
local apricot = { R = 1, G = 0.658823529411765, B = 0.2 }
local yellow = { R = 0.886274509803922, G = 0.662745098039216, B = 0.109803921568627 }
local lime = { R = 0.623529411764706, G = 0.776470588235294, B = 0.0823529411764706 }
local olive = { R = 0.372549019607843, G = 0.6, B = 0.125490196078431 }
local green = { R = 0.266666666666667, G = 0.56078431372549, B = 0.396078431372549 }
local teal = { R = 0, G = 0.596078431372549, B = 0.6 }
local navy = { R = 0.0823529411764706, G = 0.384313725490196, B = 0.517647058823529 }
local blue = { R = 0.474509803921569, G = 0.658823529411765, B = 0.815686274509804 }
local purple = { R = 0.6, G = 0.450980392156863, B = 0.627450980392157 }
local violet = { R = 0.584313725490196, G = 0.294117647058824, B = 0.803921568627451 }
local pink = { R = 0.913725490196078, G = 0.549019607843137, B = 0.709803921568627 }
-- since tan is a reserved word I had to change it here
local tanned = { R = 0.725490196078431, G = 0.690196078431373, B = 0.592156862745098 }
local beige = { R = 0.776470588235294, G = 0.627450980392157, B = 0.466666666666667 }
local brown = { R = 0.6, G = 0.4, B = 0 }
local chocolate = { R = 0.549019607843137, G = 0.352941176470588, B = 0.247058823529412 }



local OrangeButton = [[
    QPushButton {
        color:rgb(255,255,255);
        padding:5px;
        margin:3px;
        border: 1px solid rgb(125,125,125);
        background-color: rgb(235,110,0);
    }
    QPushButton:hover {
        font-weight: bold;
        border: 3px solid rgb(31,31,31);
		}
]]

local ApricotButton = [[
    QPushButton {
        color:rgb(255,255,255);
        padding:5px;
        margin:3px;
        border: 1px solid rgb(125,125,125);
        background-color: rgb(255,168,51);
    }
    QPushButton:hover {
        font-weight: bold;
        border: 3px solid rgb(31,31,31);
		}
]]

local YellowButton = [[
    QPushButton {
        color:rgb(255,255,255);
        padding:5px;
        margin:3px;
        border: 1px solid rgb(125,125,125);
        background-color: rgb(226,169,28);
    }
    QPushButton:hover {
        font-weight: bold;
        border: 3px solid rgb(31,31,31);
		}
]]

local LimeButton = [[
    QPushButton {
        color:rgb(255,255,255);
        padding:5px;
        margin:3px;
        border: 1px solid rgb(125,125,125);
        background-color: rgb(159,198,21);
    }
    QPushButton:hover {
        font-weight: bold;
        border: 3px solid rgb(31,31,31);
		}
]]

local OliveButton = [[
    QPushButton {
        color:rgb(255,255,255);
        padding:5px;
        margin:3px;
        border: 1px solid rgb(125,125,125);
        background-color: rgb(95,153,32);
    }
    QPushButton:hover {
        font-weight: bold;
        border: 3px solid rgb(31,31,31);
		}
]]

local GreenButton = [[
    QPushButton {
        color:rgb(255,255,255);
        padding:5px;
        margin:3px;
        border: 1px solid rgb(125,125,125);
        background-color: rgb(68,143,101);
    }
    QPushButton:hover {
        font-weight: bold;
        border: 3px solid rgb(31,31,31);
		}
]]

local TealButton = [[
    QPushButton {
        color:rgb(255,255,255);
        padding:5px;
        margin:3px;
        border: 1px solid rgb(125,125,125);
        background-color: rgb(0,152,153);
    }
    QPushButton:hover {
        font-weight: bold;
        border: 3px solid rgb(31,31,31);
		}
]]

local NavyButton = [[
    QPushButton {
        color:rgb(255,255,255);
        padding:5px;
        margin:3px;
        border: 1px solid rgb(125,125,125);
        background-color: rgb(21,98,132);
    }
    QPushButton:hover {
        font-weight: bold;
        border: 3px solid rgb(31,31,31);
		}
]]

local BlueButton = [[
    QPushButton {
        color:rgb(255,255,255);
        padding:5px;
        margin:3px;
        border: 1px solid rgb(125,125,125);
        background-color: rgb(121,168,208);
    }
    QPushButton:hover {
        font-weight: bold;
        border: 3px solid rgb(31,31,31);
		}
]]

local PurpleButton = [[
    QPushButton {
        color:rgb(255,255,255);
        padding:5px;
        margin:3px;
        border: 1px solid rgb(125,125,125);
        background-color: rgb(153,115,160);
    }
    QPushButton:hover {
        font-weight: bold;
        border: 3px solid rgb(31,31,31);
		}
]]

local VioletButton = [[
    QPushButton {
        color:rgb(255,255,255);
        padding:5px;
        margin:3px;
        border: 1px solid rgb(125,125,125);
        background-color: rgb(149,75,205);
    }
    QPushButton:hover {
        font-weight: bold;
        border: 3px solid rgb(31,31,31);
		}
]]

local PinkButton = [[
    QPushButton {
        color:rgb(255,255,255);
        padding:5px;
        margin:3px;
        border: 1px solid rgb(125,125,125);
        background-color: rgb(233,140,181);
    }
    QPushButton:hover {
        font-weight: bold;
        border: 3px solid rgb(31,31,31);
		}
]]

local TannedButton = [[
    QPushButton {
        color:rgb(255,255,255);
        padding:5px;
        margin:3px;
        border: 1px solid rgb(125,125,125);
        background-color: rgb(185,176,151);
    }
    QPushButton:hover {
        font-weight: bold;
        border: 3px solid rgb(31,31,31);
		}
]]

local BeigeButton = [[
    QPushButton {
        color:rgb(255,255,255);
        padding:5px;
        margin:3px;
        border: 1px solid rgb(125,125,125);
        background-color: rgb(198,160,119);
    }
    QPushButton:hover {
        font-weight: bold;
        border: 3px solid rgb(31,31,31);
		}
]]

local BrownButton = [[
    QPushButton {
        color:rgb(255,255,255);
        padding:5px;
        margin:3px;
        border: 1px solid rgb(125,125,125);
        background-color: rgb(153,102,0);
    }
    QPushButton:hover {
        font-weight: bold;
        border: 3px solid rgb(31,31,31);
		}
]]

local ChocolateButton = [[
    QPushButton {
        color:rgb(255,255,255);
        padding:5px;
        margin:3px;
        border: 1px solid rgb(125,125,125);
        background-color: rgb(140,90,63);
    }
    QPushButton:hover {
        font-weight: bold;
        border: 3px solid rgb(31,31,31);
		}
]]


win = disp:AddWindow({
	ID = 'MyWin',
	WindowTitle = 'SdR Node Color Changer',
	Geometry = {32, 160, width, height},
	Spacing = 0,

	ui:VGroup{
		ID = 'root',

		-- Add your GUI elements here:
		
		ui:HGroup{	
			Weight = 0,
			ui:Label{
			ID = 'Instructions',
			Text = 'Click swatches to change color',
			Alignment = {AlignHCenter = true},
			WordWrap = true,
			},
		},
	
		ui:HGroup{
			ui:HGap(1,0),
			Weight = 0.2,
			-- Add first row of buttons with color swatches
			ui:Button{
				ID = 'ColorSwatch1', 
				Flat = true,
				MinimumSize = {16,16},
				StyleSheet = OrangeButton,
			},
			ui:Button{
				ID = 'ColorSwatch2', 
				Flat = true,
				MinimumSize = {16,16},
				StyleSheet = ApricotButton,
			},
			ui:Button{
				ID = 'ColorSwatch3', 
				Flat = true,
				MinimumSize = {16,16},
				StyleSheet = YellowButton,
			},
			ui:Button{
				ID = 'ColorSwatch4', 
				Flat = true,
				MinimumSize = {16,16},
				StyleSheet = LimeButton,
			},
		},
		ui:VGap(1),
		ui:HGroup{
			ui:HGap(1,0),
			Weight = 0.2,
			-- Add second row of four buttons with color swatches
			ui:Button{
				ID = 'ColorSwatch5', 
				Flat = true,
				MinimumSize = {16,16},
				StyleSheet = OliveButton,
			},
			ui:Button{
				ID = 'ColorSwatch6', 
				Flat = true,
				MinimumSize = {16,16},
				StyleSheet = GreenButton,
			},
			ui:Button{
				ID = 'ColorSwatch7', 
				Flat = true,
				MinimumSize = {16,16},
				StyleSheet = TealButton,
			},
			ui:Button{
				ID = 'ColorSwatch8', 
				Flat = true,
				MinimumSize = {16,16},
				StyleSheet = NavyButton,
			},
		},
		ui:VGap(1),
		ui:HGroup{
			ui:HGap(1,0),
			Weight = 0.2,
			-- Add third row of four buttons with color swatches
			ui:Button{
				ID = 'ColorSwatch9', 
				Flat = true,
				MinimumSize = {16,16},
				StyleSheet = BlueButton,
			},
			ui:Button{
				ID = 'ColorSwatch10', 
				Flat = true,
				MinimumSize = {16,16},
				StyleSheet = PurpleButton,
			},
			ui:Button{
				ID = 'ColorSwatch11', 
				Flat = true,
				MinimumSize = {16,16},
				StyleSheet = VioletButton,
			},
			ui:Button{
				ID = 'ColorSwatch12', 
				Flat = true,
				MinimumSize = {16,16},
				StyleSheet = PinkButton,
			},
		},
		ui:VGap(1),
		ui:HGroup{
			-- Add last row of four buttons with color swatches
			ui:HGap(0.1,0),
			Weight = 0.2,
			ui:Button{
				ID = 'ColorSwatch13', 
				Flat = true,
				MinimumSize = {16,16},
				StyleSheet = TannedButton,
			},
			ui:Button{
				ID = 'ColorSwatch14', 
				Flat = true,
				MinimumSize = {16,16},
				StyleSheet = BeigeButton,
			},
			ui:Button{
				ID = 'ColorSwatch15', 
				Flat = true,
				MinimumSize = {16,16},
				StyleSheet = BrownButton,
			},
			ui:Button{
				ID = 'ColorSwatch16', 
				Flat = true,
				MinimumSize = {16,16},
				StyleSheet = ChocolateButton,
			},
		},
		ui:HGroup{	
			Weight = 0,
			ui:Button{
				ID = 'ResetColor', 
				Text = 'reset colors',
				Flat = false,
			},
		},
	};
})

-- Add your GUI element based event functions here:
itm = win:GetItems()

function win.On.ColorSwatch1.Clicked(ev)
	-- the next line will get an overview of selected tools
	local selectedtools = composition:GetToolList(true)
	-- the next line will dump the result of the previous line to the console - but it's no longer needed once it works
	-- dump(selectedtools)
	-- the next line will loop through the selected nodes and change its color to the values corresponding with Orange, Apricot etc
	for i,tool in pairs(selectedtools) do
		tool.TileColor = orange -- look at that! It actually works. I can type in the color names and they are applied. Good times!
	end
end

-- all that needs to be done now is copy/paste/changing the function 15 times and we're done!

function win.On.ColorSwatch2.Clicked(ev)
	local selectedtools = composition:GetToolList(true)
		for i,tool in pairs(selectedtools) do
		tool.TileColor = apricot
	end
end

function win.On.ColorSwatch3.Clicked(ev)
	local selectedtools = composition:GetToolList(true)
		for i,tool in pairs(selectedtools) do
		tool.TileColor = yellow
	end
end

function win.On.ColorSwatch4.Clicked(ev)
	local selectedtools = composition:GetToolList(true)
		for i,tool in pairs(selectedtools) do
		tool.TileColor = lime
	end
end

function win.On.ColorSwatch5.Clicked(ev)
	local selectedtools = composition:GetToolList(true)
		for i,tool in pairs(selectedtools) do
		tool.TileColor = olive
	end
end

function win.On.ColorSwatch6.Clicked(ev)
	local selectedtools = composition:GetToolList(true)
		for i,tool in pairs(selectedtools) do
		tool.TileColor = green
	end
end

function win.On.ColorSwatch7.Clicked(ev)
	local selectedtools = composition:GetToolList(true)
		for i,tool in pairs(selectedtools) do
		tool.TileColor = teal
	end
end

function win.On.ColorSwatch8.Clicked(ev)
	local selectedtools = composition:GetToolList(true)
		for i,tool in pairs(selectedtools) do
		tool.TileColor = navy
	end
end

function win.On.ColorSwatch9.Clicked(ev)
	local selectedtools = composition:GetToolList(true)
		for i,tool in pairs(selectedtools) do
		tool.TileColor = blue
	end
end

function win.On.ColorSwatch10.Clicked(ev)
	local selectedtools = composition:GetToolList(true)
		for i,tool in pairs(selectedtools) do
		tool.TileColor = purple
	end
end

function win.On.ColorSwatch11.Clicked(ev)
	local selectedtools = composition:GetToolList(true)
		for i,tool in pairs(selectedtools) do
		tool.TileColor = violet
	end
end

function win.On.ColorSwatch12.Clicked(ev)
	local selectedtools = composition:GetToolList(true)
		for i,tool in pairs(selectedtools) do
		tool.TileColor = pink
	end
end

function win.On.ColorSwatch13.Clicked(ev)
	local selectedtools = composition:GetToolList(true)
		for i,tool in pairs(selectedtools) do
		tool.TileColor = tanned
	end
end

function win.On.ColorSwatch14.Clicked(ev)
	local selectedtools = composition:GetToolList(true)
		for i,tool in pairs(selectedtools) do
		tool.TileColor = beige
	end
end

function win.On.ColorSwatch15.Clicked(ev)
	local selectedtools = composition:GetToolList(true)
		for i,tool in pairs(selectedtools) do
		tool.TileColor = brown
	end
end

function win.On.ColorSwatch16.Clicked(ev)
	local selectedtools = composition:GetToolList(true)
		for i,tool in pairs(selectedtools) do
		tool.TileColor = chocolate
	end
end

function win.On.ResetColor.Clicked(ev)
	local selectedtools = composition:GetToolList(true)
		for i,tool in pairs(selectedtools) do
		tool.TileColor = nil
	end
end

-- The window was closed
function win.On.MyWin.Close(ev)
		print('The window was closed')
		disp:ExitLoop()
end

win:Show()
disp:RunLoop()
win:Hide()
