comp = fu.CurrentComp

# Stop Loader/Saver node file dialogs from showing
comp.Lock()

# Read the macro into a variable
macroContents = bmd.readstring("""
{
	Tools = ordered() {
		PipeRouter1 = PipeRouter {
		}
	},
	ActiveTool = "PipeRouter1"
}
""")

print('[Macro Contents]\n' + str(macroContents))

# Add the macro to your foreground comp
comp.Paste(macroContents)

# Allow Loader/Saver node file dialogs to show up again
comp.Unlock()
