--[[--
Atomz Expand DragDrop.fu - v1.7 2024-07-28 11.01 PM (ADT)
By Andrew Hazelden <andrew@andrewhazelden.com>

Overview
---------
The "Atomz Expand" script allows you to import zipped Reactor Atomz Packages by dragging them into the Fusion Nodes view from a desktop Explorer/Finder/Linux folder browsing window.

The DragDrop file supports dragging in multiple zip elements at the same time, and each item will be imported. This allows for easier NAS-based offline Reactor installation use.

You also have the option of importing Atomz package list files (atomz.lst). An atomz list text file works with an IFL image-file-list like document that is stored in the same folder as a collection of atomz zip files. The list is just a text file with a single atomz package name per line that has the .lst file extension added to the filename. Using lst files let you define a custom selection list of packages to be installed at once. This technique can help with bootstrapping a local set of zipped offline usable Atomz packages in a bundle that get bulk loaded in fast.

Atomz.lst Document Example
--------------------------
com.MuseVFX.XGlow.zip
com.PieterVanHoute.SuckLessAudio.zip
com.PieterVanHoute.SuckLessWriteOn.zip
com.Protean.ConvertToRelativePaths.zip
com.Psyop.Cryptomatte.SampleImages.zip
com.RogerMagnusson.ClassBrowser.zip
com.StefanIhringer.srgb_rec709_viewshaders.zip
com.StefanIhringer.XfChroma.zip
com.TomBerakis.SlashFor.zip
com.wesuckless.SlashCommand.zip

Requirements
------------
- Python v3.6 to v3.10 64-bit

Script Usage
------------
Step 1. After you install the "Atomz Expand DragDrop.fu", you will need to restart the Fusion program once so the new .fu file is loaded during Fusion's startup phase.

Step 2. Select one or more Atomz packaged .zip files or	 Atomz List .lst files in an Explorer (Win), Finder (macOS), or Linux desktop folder browsing window.

Step 3. Drag the .zip/.lst file(s) to the Fusion/Resolve Nodes view. The packages will be automatically imported into your "Reactor:" PathMap folder with blazing-fast speed.

The Console window displays the atoms package installation process. This diagnostic information is useful for testing and validating content prepared by the Atomizer GUI.

A file missing entry will be listed in the Console window if an atomz package asks for a file in the .atom deploy textfield that is not present in the zipped folder.

Todo
-----
- Add support for InstallScripts
- Add support for Atom Dependency tags


Version History
----------------
v1.7 2024-07-29 

Added support for expanding GitLab created folder zips of individual atom packages. 

These atomz packages are typically accessed from the Reactor-Docs webpage: https://kartaverse.github.io/Reactor-Docs/

--]]--

{
	Language = "Python3",
	Event {
		-- Add a new event that intercepts the Action
		Action = "Drag_Drop",
		Targets = {
			FuView = {
				Execute = _Python [=[
import os, shutil, re, csv, datetime, math, zipfile, platform

zipFound = False

def GetValue(key, dct, default):
	if key in dct:
		return dct[key]
	else:
		return default

def ImportText(file):
	parentFolder = os.path.dirname(app.MapPath(file))
	textItems = []
	with open(app.MapPath(file), encoding = "utf-8-sig", newline = "") as f:
		reader = csv.reader(f)
		for row in reader:
			if len(row) >= 1:
				atom = os.path.join(parentFolder, row[0])
				textItems.append(atom)
	return textItems

def AtomzExpand(file):
	if app:
		app.DoAction("Console_Show", {"show" : True})

	print("-------------------------------------------------------------------------------------------------------")
	# Create the Reactor Deploy folder
	reactor = app.MapPath("Reactor:/Deploy/")
	if not os.path.exists(reactor):
		try:
			print("\t[Atomz][Make Reactor Directory]", reactor)
			os.makedirs(reactor)
		except OSError as error:
			print("\t[Atomz][Make Reactor Directory Error]", reactor)
	else:
		print("\t[Atomz][Reactor Directory]", reactor)

	if file.endswith(".zip") and not file.startswith("."):
		if not os.path.exists(file):
			print("[Atomz][Source File Missing]", file)
		else:
			if os.path.basename(file).startswith("Reactor-master-Atoms-"):
				# This is a GitLab live folder download built zip
				fileBaseName = os.path.basename(file)
				dirtyAtomBaseName = str(os.path.splitext(fileBaseName)[0])
				cleanAtomBaseName = str(os.path.splitext(fileBaseName)[0]).replace("Reactor-master-Atoms-", "")
				atomName = str(cleanAtomBaseName + ".atom")
				fileSubFolder = dirtyAtomBaseName + "/Atoms/" + cleanAtomBaseName + "/"
				print("[Atomz][GitLab Download][Source File]", file)
				print("[Atomz][GitLab Download][Atom Name]", atomName)
				if not zipfile.is_zipfile(file):
					print("[Atomz][Zip File Contents Error]")
				else:
					zipFP = zipfile.ZipFile(file, mode = "r")
					if zipFP is None:
						print("[Atomz][Zip File Contents Error]")
					else:
						try:
							zipContents = zipFP.read(fileSubFolder + atomName).decode(encoding = "utf-8")
							#print(zipContents)
						except Exception as error:
							zipFP.close()
							print("\t[Unzipping Error]", error)
							return
						atomDict = bmd.readstring(zipContents)
						if atomDict is None:
							print("[Atomz][Atom Parsing Error]")
						else:
							name = GetValue("Name", atomDict, "")
							version = GetValue("Version", atomDict, 0.0)
							author = GetValue("Author", atomDict, "")
							print("[Atomz][Info]", name, "v" + str(version), "by", author)

							print("[Atomz][Dict Contents]")
							print(atomDict)
							# allFiles
							# winFiles
							# macFiles
							# linuxFiles

							# Reactor
							atomsDir = os.path.join(reactor, "Atoms", "Reactor")
							src = fileSubFolder + str(atomName)
							dest = str(atomName)
							result = zipFP.extract(src, path = atomsDir)
							print("\n[Install] [Atom]", result)

							# Check the active operating system (Darwin, Windows, Linux)
							hostOS = platform.system()

							print("\n[Deploy][All]")
							allDeploy = GetValue("Deploy", atomDict, {})
							for key in allDeploy:
								if type(key) is float:
									file = allDeploy[key]
									try:
										src = fileSubFolder + str(file)
										dest = str(file)
										zipFP.getinfo(src).filename = dest
										result = zipFP.extract(src, path = reactor)
										print("\t[Install]", result)
									except Exception as error:
										print("\t[Install Error]", error)

							deployLinux = GetValue("Linux", allDeploy, {})
							deployMac = GetValue("Mac", allDeploy, {})
							deployWindows = GetValue("Windows", allDeploy, {})

							if hostOS == "Linux":
								print("\n[Deploy][Linux]")
								for key in deployLinux:
									file = deployLinux[key]
									try:
										src = fileSubFolder + "Linux/" + str(file)
										dest = str(file)
										zipFP.getinfo(src).filename = dest
										result = zipFP.extract(src, path = reactor)
										print("\t[Install]", result, "\t\t[Src]", src)
									except Exception as error:
										print("\t[Install Error]", error)

							if hostOS == "Darwin":
								print("\n[Deploy][Mac]")
								for key in deployMac:
									file = deployMac[key]
									try:
										src = fileSubFolder + "Mac/" + str(file)
										dest = str(file)
										zipFP.getinfo(src).filename = dest
										result = zipFP.extract(src, path = reactor)
										print("\t[Install]", result, "\t\t[Src]", src)
									except Exception as error:
										print("\t[Install Error]", error)

							if hostOS == "Windows":
								print("\n[Deploy][Windows]")
								for key in deployWindows:
									file = deployWindows[key]
									try:
										src = fileSubFolder + "Windows/" + str(file)
										dest = str(file)
										zipFP.getinfo(src).filename = dest
										result = zipFP.extract(src, path = reactor)
										print("\t[Install]", result, "\t\t[Src]", src)
									except Exception as error:
										print("\t[Install Error]", error)

							zipFP.close()
			else:
				# This is an Atomz Create style .zip file hierarchy
				fileBaseName = os.path.basename(file)
				atomName = str(os.path.splitext(fileBaseName)[0]) + ".atom"
				print("[Atomz][Source File]", file)
				print("[Atomz][Atom Name]", atomName)
				if not zipfile.is_zipfile(file):
					print("[Atomz][Zip File Contents Error]")
				else:
					zipFP = zipfile.ZipFile(file, mode = "r")
					if zipFP is None:
						print("[Atomz][Zip File Contents Error]")
					else:
						try:
							zipContents = zipFP.read(atomName).decode(encoding = "utf-8")
							#print(zipContents)
						except Exception as error:
							zipFP.close()
							print("\t[Unzipping Error]", error)
							return
						atomDict = bmd.readstring(zipContents)
						if atomDict is None:
							print("[Atomz][Atom Parsing Error]")
						else:
							name = GetValue("Name", atomDict, "")
							version = GetValue("Version", atomDict, 0.0)
							author = GetValue("Author", atomDict, "")
							print("[Atomz][Info]", name, "v" + str(version), "by", author)

							print("[Atomz][Dict Contents]")
							print(atomDict)
							# allFiles
							# winFiles
							# macFiles
							# linuxFiles

							# Reactor
							atomsDir = os.path.join(reactor, "Atoms", "Reactor")
							result = zipFP.extract(atomName, path = atomsDir)
							print("\n[Install] [Atom]", result)

							# Check the active operating system (Darwin, Windows, Linux)
							hostOS = platform.system()

							print("\n[Deploy][All]")
							allDeploy = GetValue("Deploy", atomDict, {})
							for key in allDeploy:
								if type(key) is float:
									file = allDeploy[key]
									try:
										result = zipFP.extract(file, path = reactor)
										print("\t[Install]", result)
									except Exception as error:
										print("\t[Install Error]", error)

							deployLinux = GetValue("Linux", allDeploy, {})
							deployMac = GetValue("Mac", allDeploy, {})
							deployWindows = GetValue("Windows", allDeploy, {})

							if hostOS == "Linux":
								print("\n[Deploy][Linux]")
								for key in deployLinux:
									file = deployLinux[key]
									try:
										src = "Linux/" + str(file)
										dest = str(file)
										zipFP.getinfo(src).filename = dest
										result = zipFP.extract(src, path = reactor)
										print("\t[Install]", result, "\t\t[Src]", src)
									except Exception as error:
										print("\t[Install Error]", error)

							if hostOS == "Darwin":
								print("\n[Deploy][Mac]")
								for key in deployMac:
									file = deployMac[key]
									try:
										src = "Mac/" + str(file)
										dest = str(file)
										zipFP.getinfo(src).filename = dest
										result = zipFP.extract(src, path = reactor)
										print("\t[Install]", result, "\t\t[Src]", src)
									except Exception as error:
										print("\t[Install Error]", error)

							if hostOS == "Windows":
								print("\n[Deploy][Windows]")
								for key in deployWindows:
									file = deployWindows[key]
									try:
										src = "Windows\\" + str(file)
										dest = str(file)
										zipFP.getinfo(src).filename = dest
										result = zipFP.extract(src, path = reactor)
										print("\t[Install]", result, "\t\t[Src]", src)
									except Exception as error:
										print("\t[Install Error]", error)

							zipFP.close()

rets = self.Default(ctx, args)
if rets is not None:
	if "urilist" in args:
		for file in args["urilist"].values():
			if (file.endswith(".zip") or file.endswith(".lst")) and not file.startswith("."):
				zipFound = True
				#print(file)

	if zipFound == True:
		print("[Atomz Expand]")

		# Process the drag and dropped items
		if "urilist" in args:
			for file in args["urilist"].values():
				print("[Atomz List]")
				print("-------------------------------------------------------------------------------------------------------")
				if file.endswith(".lst") and not file.startswith("."):
					# Process the drag and dropped items
					print("\t[List] " + str(file))
					atomList = ImportText(file)
					for atmFile in atomList:
						print("\t[Zip] " + str(atmFile))
						if atmFile.endswith(".zip") and not atmFile.startswith("."):
							AtomzExpand(app.MapPath(atmFile))
				elif file.endswith(".zip") and not file.startswith("."):
					AtomzExpand(app.MapPath(file))
]=],
			},
		},
	},
}
