Atom {
	Name = "Visual C++ Redistributable",
	Category = "Bin",
	Author = "Microsoft",
	Version = 2019,
	Date = {2019, 10, 5},
	Description = [[<h2>Overview</h2>

<p>This atom package provides the vcredist (Microsoft Visual C++ redistributable library) installers needed to use the Reactor BIN category atoms on a freshly reloaded Windows bare-metal system. Microsoft vc-redist files are needed to run programs that were compiled using Microsoft's Visual Studio development environment.</p>

<p>Also included are the Microsoft .NET v3.5 and v4.5 framework installers.</p>

<h2>More Info</h2>
<p><a href="https://support.microsoft.com/en-us/help/2977003/the-latest-supported-visual-c-downloads">https://support.microsoft.com/en-us/help/2977003/the-latest-supported-visual-c-downloads</a></p>

<h2>Microsoft .NET Development Center</h2>
<p><a href="https://dotnet.microsoft.com/">https://dotnet.microsoft.com/</a></p>

<h2>Microsoft .NET Docs</h2>
<p><a href="https://docs.microsoft.com/en-us/dotnet/framework/">https://docs.microsoft.com/en-us/dotnet/framework/</a></p>

<h2>Windows Installer 3.1</h2>
<p><a href="https://support.microsoft.com/en-gb/help/893803/windows-installer-3-1-v2-3-1-4000-2435-is-available">https://support.microsoft.com/en-gb/help/893803/windows-installer-3-1-v2-3-1-4000-2435-is-available</a></p>]],
	Deploy = {

		Windows = {
			"Bin/Windows/Visual C++ Redistributable/dotNetFx35_ReadMe.htm",
			"Bin/Windows/Visual C++ Redistributable/dotNetFx35_setup.exe",
			"Bin/Windows/Visual C++ Redistributable/dotNetFx45_ReadMe.htm",
			"Bin/Windows/Visual C++ Redistributable/dotNetFx45_setup.exe",
			"Bin/Windows/Visual C++ Redistributable/vcredist_vs2010_x64.exe",
			"Bin/Windows/Visual C++ Redistributable/vcredist_vs2010_x86.exe",
			"Bin/Windows/Visual C++ Redistributable/vcredist_vs2013_x64.exe",
			"Bin/Windows/Visual C++ Redistributable/vcredist_vs2013_x86.exe",
			"Bin/Windows/Visual C++ Redistributable/vcredist_vs2015_x64.exe",
			"Bin/Windows/Visual C++ Redistributable/vcredist_vs2015_x86.exe",
			"Bin/Windows/Visual C++ Redistributable/vcredist_vs2017_x64.exe",
			"Bin/Windows/Visual C++ Redistributable/vcredist_vs2017_x86.exe",
			"Bin/Windows/Visual C++ Redistributable/vcredist_vs2019_x64.exe",
			"Bin/Windows/Visual C++ Redistributable/vcredist_vs2019_x86.exe",
		},
	},
}
