{
	Tools = ordered() {
		kvrDualFisheye = GroupOperator {
			CtrlWZoom = false,
			CustomData = {
				Path = {
					Map = {
						["Setting:"] = "Templates:/Edit/Effects/KartaVR/Warp/"
					}
				},
				HelpPage = "https://github.com/kartaverse",
			},
			Inputs = ordered() {
				MainInput1 = InstanceInput {
					SourceOp = "DualFisheyeIn",
					Source = "Input",
				},
				InputLogo = InstanceInput {
					SourceOp = "Front_kvrWarpStitchUltra",
					Source = "Label",
					Page = "Controls",
				},
				FieldOfViewInput = InstanceInput {
					SourceOp = "Front_kvrWarpStitchUltra",
					Source = "FieldOfView",
					Page = "Controls",
					Default = 195,
				},
				AInput = InstanceInput {
					SourceOp = "DualFisheye_kvrLensStereo",
					Source = "a",
					Name = "Panotools Distort A",
					Page = "Controls",
					Default = 0,
				},
				BInput = InstanceInput {
					SourceOp = "DualFisheye_kvrLensStereo",
					Source = "b",
					Name = "Panotools Distort B",
					Page = "Controls",
					Default = 0,
				},
				CInput = InstanceInput {
					SourceOp = "DualFisheye_kvrLensStereo",
					Source = "c",
					Name = "Panotools Distort C",
					Page = "Controls",
					Default = 0,
				},
				CXInput = InstanceInput {
					SourceOp = "DualFisheye_kvrLensStereo",
					Source = "cx",
					Name = "Distort Center X",
					Page = "Controls",
					Default = 0,
				},
				CYInput = InstanceInput {
					SourceOp = "DualFisheye_kvrLensStereo",
					Source = "cy",
					Name = "Distort Center Y",
					Page = "Controls",
					Default = 0,
				},
				Center1Input = InstanceInput {
					SourceOp = "DualFisheye_kvrCropStereo",
					Source = "Center1",
					Name = "Crop Center1",
					Page = "Controls",
					DefaultX = 0.75,
					DefaultY = 0.5,
				},
				Center2Input = InstanceInput {
					SourceOp = "DualFisheye_kvrCropStereo",
					Source = "Center2",
					Name = "Crop Center2",
					Page = "Controls",
					DefaultX = 0.25,
					DefaultY = 0.5,
				},
				SoftEdgeInput = InstanceInput {
					SourceOp = "EllipseGradingMask",
					Source = "SoftEdge",
					Name = "Vignette SoftEdge",
					Page = "Controls",
					Default = 0.04,
				},
				BorderWidthInput = InstanceInput {
					SourceOp = "EllipseGradingMask",
					Source = "BorderWidth",
					Name = "Vignette Border Width",
					Page = "Controls",
					Default = -0.04,
				},
				Input1 = InstanceInput {
					SourceOp = "FrontColorCorrector",
					Source = "MasterRGBGain",
					Name = "Vignette Gain",
					Page = "Controls",
					Default = 1.06,
				},
				Input2 = InstanceInput {
					SourceOp = "FrontColorCorrector",
					Source = "MasterRGBGamma",
					Name = "Vignette Gamma",
					Page = "Controls",
					Default = 1,
				},
				ApplyModeInput = InstanceInput {
					SourceOp = "SphericalMerge",
					Source = "ApplyMode",
					Name = "Merge Apply Mode",
					Page = "Controls",
				}
			},
			Outputs = {
				MainOutput1 = InstanceOutput {
					SourceOp = "SphericalOut",
					Source = "Output",
				}
			},
			ViewInfo = GroupInfo {
				Pos = { -495, 379.5 },
				Flags = {
					AllowPan = false,
					AutoSnap = true
				},
				Size = { 1180.96, 546.179, 525.183, 48.1908 },
				Direction = "Horizontal",
				PipeStyle = "Direct",
				Scale = 1,
				Offset = { 0, 0 }
			},
			Tools = ordered() {
				DualFisheyeNote1 = Note {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Comments = Input { Value = "This example composite stitches together footage from a dual circular fisheye lens camera. It expects footage that has a pair of front and back circular fisheye lens images merged into the same frame layout. The settings have been tuned for a Samsung Gear360 camera.\n\nAbout the Comp:\nThe kvrStereoCrop node is used to isolate the Samsung Gear360 camera's front and back images from the dual fisheye view layout.\n\nThe kvrLensStereo node is used to apply f-theta lens distortion correction to linearize the image content at the far edges of the circular fisheye images.\n\nThe ColorCorrector nodes have an Ellipse mask which allows and edge falloff zone color correction adjutment to be applied for lens vingetting correction.\n\nThe kvrWarpStitchUltra nodes are used to apply circular fisheye to LatLong image projection conversions. There are mask softness controls that allow feathering the frame border to make it possible to blend the images without a hard border edge\n\nThe Merge node is used to combine the front and back LatLong imagery. If you set the Merge \"Apply Mode\" setting from \"Normal\" to \"Difference\" you can do a quality check to validate the edge blending zone is correctly aligned so the two images can be blended seamlessly. If you select the Merge node in the Nodes view and press \"Command + T\" you are able to toggle the foreground and background input connection ordering which flips the specific view that is on top when doing the blending operation.", }
					},
					ViewInfo = StickyNoteInfo {
						Pos = { -330, 49.5 },
						Flags = {
							Expanded = true
						},
						Size = { 820.647, 264.101 }
					},
					Colors = { TileColor = { R = 0.266666666666667, G = 0.56078431372549, B = 0.396078431372549 }, }
				},
				DualFisheye_kvrCropStereo = Fuse.kvrCropStereo {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Center1 = Input { Value = { 0.75, 0.5 }, },
						Center2 = Input { Value = { 0.25, 0.5 }, },
						InputStereo = Input { Value = 1, },
						Image1 = Input {
							SourceOp = "DualFisheyeIn",
							Source = "Output",
						},
						Image2 = Input {
							SourceOp = "DualFisheyeIn",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -330, 346.5 } },
					Colors = { TileColor = { R = 0.266666666666667, G = 0.56078431372549, B = 0.396078431372549 }, },
					Version = 500
				},
				FontPipeRouter = PipeRouter {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Input = Input {
							SourceOp = "DualFisheye_kvrCropStereo",
							Source = "Output1",
						},
					},
					ViewInfo = PipeRouterInfo { Pos = { -220, 346.5 } },
					Colors = { TileColor = { R = 0.266666666666667, G = 0.56078431372549, B = 0.396078431372549 }, }
				},
				DualFisheyeIn = PipeRouter {
					CtrlWShown = false,
					NameSet = true,
					CustomData = {
						Path = {
							Map = {
								["Setting:"] = "Macros:/KartaVR/Miscellaneous/"
							}
						}
					},
					ViewInfo = PipeRouterInfo { Pos = { -440, 346.5 } },
					Colors = { TileColor = { R = 0.266666666666667, G = 0.56078431372549, B = 0.396078431372549 }, }
				},
				DualFisheye_kvrLensStereo = Fuse.kvrLensStereo {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						LensModel = Input { Value = 1, },
						a = Input { Value = 0.017, },
						b = Input { Value = -0.085, },
						c = Input { Value = -0.034, },
						InputStereo = Input { Value = 1, },
						Image1 = Input {
							SourceOp = "FontPipeRouter",
							Source = "Output",
						},
						Image2 = Input {
							SourceOp = "BackPipeRouter",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -110, 346.5 } },
					Colors = { TileColor = { R = 0.266666666666667, G = 0.56078431372549, B = 0.396078431372549 }, },
					Version = 500
				},
				BackPipeRouter = PipeRouter {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Input = Input {
							SourceOp = "DualFisheye_kvrCropStereo",
							Source = "Output2",
						},
					},
					ViewInfo = PipeRouterInfo { Pos = { -220, 379.5 } },
					Colors = { TileColor = { R = 0.266666666666667, G = 0.56078431372549, B = 0.396078431372549 }, }
				},
				BackColorCorrector = ColorCorrector {
					CtrlWShown = false,
					NameSet = true,
					SourceOp = "FrontColorCorrector",
					Inputs = {
						EffectMask = Input {
							SourceOp = "EllipseGradingMask",
							Source = "Mask",
						},
						SettingsNest = Input { },
						SnapshotMatch = Input { },
						ReleaseMatch = Input { },
						MatchNest = Input { },
						ResetAllHistogramChanges = Input { },
						ResetAllColorChanges = Input { },
						ResetAllLevels = Input { },
						ResetAllSuppression = Input { },
						PresetSimpleRanges = Input { },
						PresetSmoothRanges = Input { },
						Input = Input {
							SourceOp = "DualFisheye_kvrLensStereo",
							Source = "Output2",
						},
						MatchReference = Input { },
						MatchMask = Input { },
						CommentsNest = Input { },
						FrameRenderScriptNest = Input { },
						StartRenderScripts = Input { },
						EndRenderScripts = Input { },
					},
					ViewInfo = OperatorInfo { Pos = { 0, 412.5 } },
					Colors = { TileColor = { R = 0.266666666666667, G = 0.56078431372549, B = 0.396078431372549 }, }
				},
				FrontColorCorrector = ColorCorrector {
					CtrlWShown = false,
					NameSet = true,
					CustomData = {
						Settings = {
							[1] = {
								Tools = ordered() {
									FrontColorCorrector = ColorCorrector {
										Inputs = {
											HistogramIgnoreTransparent = Input { Value = 1 },
											MasterRGBGain = Input { Value = 1.06 },
											Input = Input {
												SourceOp = "kvrLensStereo1",
												Source = "Output1"
											},
											ColorRanges = Input {
												Value = ColorCurves {
													Curves = {
														{
															Points = {
																{ 0, 1 },
																{ 0.4, 0.2 },
																{ 0.6, 0 },
																{ 1, 0 }
															}
														},
														{
															Points = {
																{ 0, 0 },
																{ 0.4, 0 },
																{ 0.6, 0.2 },
																{ 1, 1 }
															}
														}
													}
												}
											},
											EffectMask = Input {
												SourceOp = "Ellipse1",
												Source = "Mask"
											},
											PreDividePostMultiply = Input { Value = 1 }
										},
										Colors = { TileColor = { R = 0.266666666666667, G = 0.56078431372549, B = 0.396078431372549 } },
										CtrlWZoom = false,
										NameSet = true,
										ViewInfo = OperatorInfo { Pos = { 824.333, -16.5 } },
										CustomData = {
										}
									}
								}
							}
						}
					},
					Inputs = {
						EffectMask = Input {
							SourceOp = "EllipseGradingMask",
							Source = "Mask",
						},
						MasterRGBGain = Input { Value = 1.14, },
						MasterRGBBrightness = Input { Value = 0.02, },
						ColorRanges = Input {
							Value = ColorCurves {
								Curves = {
									{
										Points = {
											{ 0, 1 },
											{ 0.4, 0.2 },
											{ 0.6, 0 },
											{ 1, 0 }
										}
									},
									{
										Points = {
											{ 0, 0 },
											{ 0.4, 0 },
											{ 0.6, 0.2 },
											{ 1, 1 }
										}
									}
								}
							},
						},
						PreDividePostMultiply = Input { Value = 1, },
						HistogramIgnoreTransparent = Input { Value = 1, },
						Input = Input {
							SourceOp = "DualFisheye_kvrLensStereo",
							Source = "Output1",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 0, 346.5 } },
					Colors = { TileColor = { R = 0.266666666666667, G = 0.56078431372549, B = 0.396078431372549 }, }
				},
				EllipseGradingMask = EllipseMask {
					CtrlWZoom = false,
					CtrlWShown = false,
					NameSet = true,
					CustomData = {
						Settings = {
							[2] = {
								Tools = ordered() {
									Ellipse1 = EllipseMask {
										Inputs = {
											MaskWidth = Input { Value = 1920 },
											PixelAspect = Input { Value = { 1, 1 } },
											BorderWidth = Input { Value = -0.08 },
											ClippingMode = Input { Value = FuID { "None" } },
											MaskHeight = Input { Value = 1080 },
											Height = Input {
												Value = 0.989,
												Expression = "Width"
											},
											Width = Input { Value = 0.989 },
											Invert = Input { Value = 1 },
											SoftEdge = Input { Value = 0.05 }
										},
										Colors = { TileColor = { R = 0.266666666666667, G = 0.56078431372549, B = 0.396078431372549 } },
										CtrlWZoom = false,
										ViewInfo = OperatorInfo { Pos = { 823.667, 17.1061 } },
										CustomData = {
										}
									}
								}
							}
						}
					},
					Inputs = {
						SoftEdge = Input { Value = 0.04, },
						BorderWidth = Input { Value = -0.04, },
						Invert = Input { Value = 1, },
						MaskWidth = Input { Value = 1920, },
						MaskHeight = Input { Value = 1080, },
						PixelAspect = Input { Value = { 1, 1 }, },
						ClippingMode = Input { Value = FuID { "None" }, },
						Width = Input { Value = 0.989, },
						Height = Input {
							Value = 0.989,
							Expression = "Width",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 0, 379.5 } },
					Colors = { TileColor = { R = 0.266666666666667, G = 0.56078431372549, B = 0.396078431372549 }, }
				},
				Back_kvrWarpStitchUltra = Fuse.kvrWarpStitchUltra {
					CtrlWShown = false,
					NameSet = true,
					SourceOp = "Front_kvrWarpStitchUltra",
					Inputs = {
						SettingsNest = Input { },
						ScriptEdit = Input { },
						ScriptReload = Input { },
						InReset = Input { Value = 1, },
						LensDistortionCorrectionNest = Input { },
						["Lens Distortion Model"] = Input { Disabled = true, },
						["F-Theta"] = Input { },
						ShowFThetaSource = Input { },
						InitialCropNest = Input { },
						["Image OrientationNest"] = Input { },
						VectorMaskingNest = Input { },
						CircularFisheyeMaskingNest = Input { },
						RectangularMaskingNest = Input { },
						SplitViewMaskingNest = Input { },
						WarpedImageNest = Input { },
						RotateSphereNest = Input { },
						Pan = Input { Value = 180, },
						ColorCorrectedImageNest = Input { },
						WhiteBalanceNest = Input { },
						InApplyMetadata = Input { Disabled = true, },
						InInputSwapEyes = Input { Disabled = true, },
						InStereo = Input { Disabled = true, },
						InOutputSwapEyes = Input { Disabled = true, },
						StereoEyeView = Input { Disabled = true, },
						ImageFramebufferNest = Input { },
						ImageMetadataNest = Input { },
						EdgeSettingsNest = Input { },
						EditNest = Input { },
						EditScript = Input { },
						ReloadScript = Input { },
						HelpNest = Input { },
						Help_script = Input { },
						Image = Input {
							SourceOp = "BackColorCorrector",
							Source = "Output",
						},
						GarbageMatte = Input { },
						CommentsNest = Input { },
						FrameRenderScriptNest = Input { },
						StartRenderScripts = Input { },
						EndRenderScripts = Input { },
					},
					ViewInfo = OperatorInfo { Pos = { 110, 412.5 } },
					Colors = { TileColor = { R = 0.266666666666667, G = 0.56078431372549, B = 0.396078431372549 }, },
					Version = 503
				},
				Front_kvrWarpStitchUltra = Fuse.kvrWarpStitchUltra {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						View = Input { Value = 7, },
						InReset = Input { Value = 1, },
						["Lens Distortion Model"] = Input { Disabled = true, },
						CircularMaskDiameter = Input { Value = 0.99, },
						CircularMaskSoftness = Input { Value = 0.03, },
						FieldOfView = Input { Value = 195, },
						InApplyMetadata = Input { Disabled = true, },
						InInputSwapEyes = Input { Disabled = true, },
						InStereo = Input { Disabled = true, },
						InOutputSwapEyes = Input { Disabled = true, },
						StereoEyeView = Input { Disabled = true, },
						Image = Input {
							SourceOp = "FrontColorCorrector",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 110, 346.5 } },
					Colors = { TileColor = { R = 0.266666666666667, G = 0.56078431372549, B = 0.396078431372549 }, },
					Version = 503
				},
				SphericalMerge = Merge {
					CtrlWShown = false,
					NameSet = true,
					CustomData = {
						Settings = {
						}
					},
					Inputs = {
						UseGPU = Input { Value = 0, },
						Background = Input {
							SourceOp = "Back_AutoDomain",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "Front_AutoDomain",
							Source = "Output",
						},
						FlattenTransform = Input { Value = 1, },
						PerformDepthMerge = Input { Value = 0, },
						ReferenceSize = Input { Value = 1, },
						Width = Input { Value = 1920, },
						Height = Input { Value = 1080, },
						UseFrameFormatSettings = Input { Value = 1, },
					},
					ViewInfo = OperatorInfo { Pos = { 385, 346.5 } },
					Colors = { TileColor = { R = 0.266666666666667, G = 0.56078431372549, B = 0.396078431372549 }, }
				},
				SphericalOut = PipeRouter {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Input = Input {
							SourceOp = "SphericalMerge",
							Source = "Output",
						},
					},
					ViewInfo = PipeRouterInfo { Pos = { 495, 346.5 } },
					Colors = { TileColor = { R = 0.266666666666667, G = 0.56078431372549, B = 0.396078431372549 }, }
				},
				DualFisheyeUnderlay = Underlay {
					CtrlWShown = false,
					NameSet = true,
					ViewInfo = UnderlayInfo {
						Pos = { 55, 16.5 },
						Size = { 1049.76, 454.169 }
					},
					Colors = { TileColor = { R = 0.266666666666667, G = 0.56078431372549, B = 0.396078431372549 }, }
				},
				Front_AutoDomain = AutoDomain {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Input = Input {
							SourceOp = "Front_kvrWarpStitchUltra",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 220, 346.5 } },
				},
				Back_AutoDomain = AutoDomain {
					NameSet = true,
					SourceOp = "Front_AutoDomain",
					Inputs = {
						Input = Input {
							SourceOp = "Back_kvrWarpStitchUltra",
							Source = "Output",
						},
						CommentsNest = Input { },
						FrameRenderScriptNest = Input { },
						StartRenderScripts = Input { },
						EndRenderScripts = Input { },
					},
					ViewInfo = OperatorInfo { Pos = { 220, 412.5 } },
				}
			},
			Colors = { TileColor = { R = 0.266666666666667, G = 0.56078431372549, B = 0.396078431372549 }, }
		}
	},
	ActiveTool = "kvrDualFisheye"
}