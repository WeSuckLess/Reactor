# Display a syntax highlighted version of the active Fusion composite in a UI Manager based TextEdit view.

ui = fu.UIManager
disp = bmd.UIDispatcher(ui)

dlg = disp.AddWindow({ "WindowTitle": "Fusion Composite Viewer", "ID": "EditWin", "Geometry": [100, 100, 800, 1024], "Spacing": 10, "Margin": 10,},[
	ui.VGroup({ "ID": "root",},[
		# Add your GUI elements here:
		ui.TextEdit({
			"ID": "TextEdit",
			"LineWrapMode": "NoWrap",
			"AcceptRichText": False,
			"ReadOnly": True,
			"Font": ui.Font({
				"Family": "Droid Sans Mono",
				"StyleName": "Regular",
				"PixelSize": 12,
				"MonoSpaced": True,
				
			}),
			# Use the Fusion hybrid lexer module to add syntax highlighting
			"Lexer": "fusion",
			"Weight": 1,
		}),
		ui.Button({
			"ID": "RefeshButton",
			"Text": "Refresh Document",
			"Weight": 0.01,
		}),
	]),
])

itm = dlg.GetItems()

# The window was closed
def _func(ev):
	disp.ExitLoop()
dlg.On.EditWin.Close = _func

# Load the current Fusion composite source contents into the viewer:
def RefeshDocument():
	compFile = comp.GetAttrs("COMPS_FileName")
	print("[Viewing File] ", compFile)

	if not compFile:
		itm['TextEdit'].PlainText = "Please save the current Fusion composite to disk before running this script again."
	else:
		# Read in the active .comp as a file from disk then trim off the final null character from the file
		file = open(compFile, mode = "rt", encoding = "UTF-8")
		rawDocument = file.read()
		file.close()
	
		document = rawDocument[:-2]

		# Print the file contents to the console
		# print("[Fusion Composite Contents]")
		# print(document)

		# Update the TextEdit field contents
		itm['TextEdit'].PlainText = document

		# Update the window title caption with the filename
		itm['EditWin'].WindowTitle = "Fusion Composite Viewer: " + str(compFile)

# Add your GUI element based event functions here:

# The "Refresh" button was pressed.
def _func(ev):
	print("[Update] Refreshing the view.")
	RefeshDocument()
dlg.On.RefeshButton.Clicked = _func

RefeshDocument()

dlg.Show()
disp.RunLoop()
dlg.Hide()