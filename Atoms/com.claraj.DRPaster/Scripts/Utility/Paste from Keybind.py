import os
import sys
from pathlib import Path
import subprocess
import time
import json

VERSIONSTRING = "0.9.00-b"

print(f"\n--[ Init. DRPaster v{VERSIONSTRING} ]-------------\n")

def load_source(module_name, file_path):
    if sys.version_info[0] >= 3 and sys.version_info[1] >= 5:
        import importlib.util

        module = None
        spec = importlib.util.spec_from_file_location(module_name, file_path)
        if spec:
            module = importlib.util.module_from_spec(spec)
        if module:
            sys.modules[module_name] = module
            spec.loader.exec_module(module)
        return module
    else:
        import imp
        return imp.load_source(module_name, file_path)

def init_bmd():
    """
    Initialize the DaVinciResolveScript module and assign to the global variable 'bmd'.
    This replaces the previous GetResolve() fallback logic and makes the module available
    for both Resolve and Fusion functionality.
    """
    global bmd
    try:
        import DaVinciResolveScript as bmd
    except ImportError:
        if sys.platform.startswith("darwin"):
            expectedPath = "/Library/Application Support/Blackmagic Design/DaVinci Resolve/Developer/Scripting/Modules/"
        elif sys.platform.startswith("win") or sys.platform.startswith("cygwin"):
            expectedPath = os.path.join(os.getenv('PROGRAMDATA'), "Blackmagic Design", "DaVinci Resolve", "Support", "Developer", "Scripting", "Modules")
        elif sys.platform.startswith("linux"):
            expectedPath = "/opt/resolve/Developer/Scripting/Modules/"

        print("Unable to find DaVinciResolveScript module from PYTHONPATH - trying default locations")
        try:
            load_source('DaVinciResolveScript', os.path.join(expectedPath, "DaVinciResolveScript.py"))
            import DaVinciResolveScript as bmd
            print("Successfully loaded DaVinciResolveScript")
        except Exception as ex:
            print("Unable to find DaVinciResolveScript. Please ensure that it is discoverable by python.")
            print(ex)
            sys.exit(1)

# Initialize the DaVinci Resolve module (bmd) globally.
init_bmd()

# Global UI and Resolve/Fusion objects initialization
resolve = bmd.scriptapp("Resolve")
fusion = bmd.scriptapp("Fusion")
ui = fusion.UIManager
disp = bmd.UIDispatcher(ui)

# Global configuration variable for saved directory
save_directory = None

# Get the directory where the script is located
try:
    SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
except NameError:
    # When running in Resolve's script environment
    if sys.platform.startswith("win"):
        SCRIPT_DIR = os.path.join(os.getenv('PROGRAMDATA'), "Blackmagic Design", "DaVinci Resolve", "Fusion", "Scripts", "Utility")
    elif sys.platform.startswith("darwin"):
        SCRIPT_DIR = os.path.expanduser("~/Library/Application Support/Blackmagic Design/DaVinci Resolve/Fusion/Scripts/Utility")
    else:  # Linux
        SCRIPT_DIR = os.path.expanduser("~/.local/share/DaVinciResolve/Fusion/Scripts/Utility")

CONFIG_PATH = os.path.join(SCRIPT_DIR, "drpaster_config.json")

def load_config():
    """Load configuration file if it exists."""
    global save_directory
    try:
        if os.path.exists(CONFIG_PATH):
            with open(CONFIG_PATH, 'r') as f:
                config = json.load(f)
                save_directory = config.get("SaveDirectory")
                if save_directory and os.path.exists(save_directory):
                    print(f"Loaded save location: {save_directory}")
                    return True
    except Exception as e:
        print(f"Error loading config: {e}")
    return False

def initial_setup():
    """Perform initial setup if no configuration exists."""
    global save_directory

    setup_window = disp.AddWindow({
        'ID': 'setupWindow',
        'WindowTitle': 'DRPaster Initial Setup',
        'Spacing': 10,
        'FixedSize': [400, 100],
    }, [
        ui.VGroup({'Weight': 0}, [
            ui.Label({'Text': 'Please select a directory to save clipboard images', 'WordWrap': True}),
            ui.HGroup([
                ui.TextEdit({'ID': 'pathEdit', 'Text': os.path.expanduser("~/Pictures"), 'ReadOnly': True}),
                ui.Button({'ID': 'browseButton', 'Text': 'Browse...'})
            ])
        ])
    ])

    setup_items = setup_window.GetItems()

    def on_browse(ev):
        global save_directory
        picked_path = fusion.RequestDir()
        if picked_path:
            save_directory = picked_path
            setup_items['pathEdit'].Text = picked_path
            try:
                with open(CONFIG_PATH, 'w') as f:
                    json.dump({"SaveDirectory": save_directory}, f)
                setup_window.Hide()
                disp.ExitLoop()
            except Exception as e:
                print(f"Error saving config: {e}")

    setup_window.On.browseButton.Clicked = on_browse
    setup_window.On.setupWindow.Close = lambda ev: sys.exit()

    setup_window.Show()
    disp.RunLoop()
    setup_window.Hide()

def get_clipboard_image():
    """Get an image from the clipboard and save it to the configured location."""
    global save_directory
    if not save_directory or not os.path.exists(save_directory):
        load_config()

    os.makedirs(save_directory, exist_ok=True)
    image_path = os.path.join(save_directory, f"paste_image_{int(time.time())}.png")

    try:
        if sys.platform == "darwin":  # macOS
            script = f'''
            try
                set myFile to "{image_path}"
                set myData to the clipboard as «class PNGf»
                set fileRef to open for access myFile with write permission
                write myData to fileRef
                close access fileRef
                return true
            on error
                try
                    close access myFile
                end try
                return false
            end try
            '''
            result = subprocess.run(['osascript', '-e', script], capture_output=True)
            if result.returncode != 0:
                raise Exception("Failed to get image from clipboard")
        elif sys.platform.startswith("win"):  # Windows
            ps_script = f'''
            Add-Type -AssemblyName System.Windows.Forms
            $image = [Windows.Forms.Clipboard]::GetImage()
            if ($image -ne $null) {{
                $image.Save("{image_path.replace('\\', '\\\\')}", [System.Drawing.Imaging.ImageFormat]::Png)
                $image.Dispose()
                exit 0
            }}
            exit 1
            '''
            result = subprocess.run(["powershell", "-command", ps_script], capture_output=True)
            if result.returncode != 0:
                raise Exception("Failed to get image from clipboard on Windows")
        else:  # Linux
            os.makedirs(os.path.dirname(image_path), exist_ok=True)
            with open(image_path, 'wb') as outfile:
                result = subprocess.run(
                    ["xclip", "-selection", "clipboard", "-t", "image/png", "-o"],
                    stdout=outfile
                )
            if result.returncode != 0:
                raise Exception("Failed to get image from clipboard using xclip")
    except Exception as e:
        print(f"Error saving clipboard image: {str(e)}")
        raise

    if not os.path.exists(image_path) or os.path.getsize(image_path) < 100:
        raise Exception("Failed to save valid image from clipboard")

    return image_path

def import_to_resolve(image_path):
    """Import the image into DaVinci Resolve's media pool."""
    mediastorage = resolve.GetMediaStorage()
    print(f"Attempting to import {image_path}")
    result = mediastorage.AddItemListToMediaPool([image_path])
    print(result)

def trigger(ev):
    try:
        image_path = get_clipboard_image()
        print(f"Image saved to: {image_path}")
        print(f"Image size: {os.path.getsize(image_path)} bytes")

        if not os.path.exists(image_path):
            return

        import_to_resolve(image_path)
        print("\n--[Finished. Image should be pasted.]-----")
    except Exception as e:
        error_message = str(e)
        print(f"Error: {error_message}")

# Check if configuration exists; if not, run initial setup.
if not load_config():
    initial_setup()

if not save_directory:
    print("No save directory selected. Exiting.")
    sys.exit()

print(f"Save location: {save_directory}")

trigger(None)