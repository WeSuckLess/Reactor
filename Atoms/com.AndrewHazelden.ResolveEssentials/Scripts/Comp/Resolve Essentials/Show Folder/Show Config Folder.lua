-- Show Config Folder menu item
-- v4 - 2024-08-15

local path = app:MapPath("Config:/")
if bmd.direxists(path) == false then
	bmd.createdir(path)
	print("[Created Config Folder] " .. path)
end

print("[Show Config Folder] " .. path)
bmd.openfileexternal("Open", path)
