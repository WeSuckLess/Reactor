# Kartaverse Sessions
v5.75 2024-10-08  
By Andrew Hazelden <andrew@andrewhazelden.com>  

## Overview

The "File &gt; Sessions &gt; " menu allows you to quickly load and save a snapshot of the open .comp tabs that are active in Fusion Studio Standalone. The node selection state is restored for each of the comps as well. This makes it easy to return to an earlier work session.

![Menu](Images/file_menu.png)

This approach lets you hop back into a multi-document compositing session and have all your comp work loaded up, just like it was when you saved the previous session.

## Installation

Step 1. Move the "Kartaverse Sessions.fu" file into the Fusion user prefs "Config:/" folder.

Step 2. Restart Fusion so the config file is loaded.

## Menu Entries

The "Kartaverse Sessions.fu" file adds the following menu entries to Fusion Studio Standalone:

File > Sessions:
- Restore Session
- Load Session...
- --------------------
- Save Session
- Save Session As...
- --------------------
- Close Session

## Usage

The "Restore Session" menu loads the most recently used session file. Existing comp tabs will be saved and closed first.

The "Load Session..." menu item lets you open up pre-existing .json based session files. Existing comp tabs will be saved and closed first.

The "Save Session" menu item lets you re-save the active .json based session files to disk. This will refresh the active comp tabs and node selection values.

The "Save Sessions As..." menu item lets you save your session to a different .json based session filename on disk.

The "Close Sessions" menu item closes all of the open comp tabs. You are asked to save each .comp file if there are any changes.

## Session Files

Comp session files are saved in a .json file format. The default comp session document storage path is:

	Reactor://Deploy/Scripts/Support/Kartaverse/Sessions/

Example session.json File Syntax:

	{
	  "Array":[{
		  "ActiveTool":"TinyPlanet_kvrViewer1",
		  "CurrentTime":1,
		  "LeftView":"TinyPlanet_kvrViewer1",
		  "RightView":"TinyPlanet_kvrViewer1",
		  "Filename":"Reactor:/Deploy/Comps/Kartaverse/WarpStitch/Dual Fisheye/Ricoh Theta v001.comp"
		},{
		  "ActiveTool":"TinyPlanet_kvrViewer1",
		  "CurrentTime":1,
		  "LeftView":"TinyPlanet_kvrViewer1",
		  "RightView":"TinyPlanet_kvrViewer1",
		  "Filename":"Reactor:/Deploy/Comps/Kartaverse/WarpStitch/Dual Fisheye/Samsung Gear360 v001.comp"
		},{
		  "ActiveTool":"Flat_kvrViewer1",
		  "CurrentTime":1,
		  "LeftView":"Flat_kvrViewer1",
		  "RightView":"Flat_kvrViewer1",
		  "Filename":"Reactor:/Deploy/Comps/Kartaverse/WarpStitch/Dual Fisheye/YI 360 VR Office Interior v001.comp"
		},{
		  "ActiveTool":"Flat_kvrViewer1",
		  "CurrentTime":1,
		  "LeftView":"Flat_kvrViewer1",
		  "RightView":"Flat_kvrViewer1",
		  "Filename":"Reactor:/Deploy/Comps/Kartaverse/WarpStitch/Dual Fisheye/YI 360 VR Parking Lot v001.comp"
		}],
	  "Version":"5.75",
	  "Purpose":"Comp Session",
	  "Name":"Sessions"
	}

## Prefs

If you want to clear out the old sessions preference you can run:

	app:SetData("Kartaverse.Sessions.Default", nil)

