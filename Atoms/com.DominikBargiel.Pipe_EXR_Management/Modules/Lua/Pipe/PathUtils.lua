--[[--
PathUtils.lua - v1.0
A utility module for path manipulation and validation in the Pipe system.
--]]--

local ErrorHandler = require("Pipe.ErrorHandler")
local PathUtils = {}

-- Extract version information from a path
function PathUtils.extractVersionInfo(path)
    if not path then return nil end
    
    -- Try to find version with underscore (e.g., V001_3)
    local mainNum = path:match("MAIN(%d+)")
    local versionNum, suffixNum = path:match("[vV](%d+)_(%d+)")
    
    if versionNum and suffixNum then
        return {
            number = tonumber(versionNum),
            suffix = tonumber(suffixNum),
            format = "%03d",  -- Always use 3 digits for version
            prefix = path:match("([vV])%d+"),  -- Capture v or V
            mainVersion = mainNum and tonumber(mainNum) or nil
        }
    end
    
    -- Fallback to simple version pattern
    local simpleNum = path:match("[vV](%d+)")
    if simpleNum then
        return {
            number = tonumber(simpleNum),
            format = "%03d",
            prefix = path:match("([vV])%d+"),
            mainVersion = mainNum and tonumber(mainNum) or nil
        }
    end
    
    return nil
end

-- Parse a path into its components
function PathUtils.parsePath(fullPath)
    if not fullPath then return nil end
    
    -- Find the last directory separator
    local lastSlash = fullPath:match("^.*()[/\\]")
    if not lastSlash then return nil end
    
    return {
        root = fullPath:sub(1, lastSlash),
        rel_path = fullPath:sub(lastSlash + 1),
        full_path = fullPath
    }
end

-- Get folder from path
function PathUtils.getFolder(path)
    if not path then return nil end
    return path:match("(.+[\\/])[^\\/]+$")
end

-- Get filename from path
function PathUtils.getFilename(path)
    if not path then return nil end
    return path:match("[^\\/]+$")
end

-- Validate if a path exists
function PathUtils.validatePath(path)
    if not path or path == "" then return false end
    return bmd.fileexists(path)
end

-- Update loader path
function PathUtils.updateLoaderPath(loader, newPath)
    if not loader or not newPath then return false end
    loader.Clip[fu.TIME_UNDEFINED] = newPath
    return true
end

-- Get relative path from root
function PathUtils.getRelativePath(fullPath, rootPath)
    if not fullPath or not rootPath then return nil end
    
    -- Normalize paths to use same separator
    fullPath = fullPath:gsub("\\", "/")
    rootPath = rootPath:gsub("\\", "/")
    
    -- Ensure root path ends with separator
    if not rootPath:match("/$") then
        rootPath = rootPath .. "/"
    end
    
    -- Remove root path to get relative path
    if fullPath:sub(1, #rootPath) == rootPath then
        return fullPath:sub(#rootPath + 1)
    end
    
    -- If root path doesn't match exactly, try to find common structure
    local pattern = "(.-)[\\/][^\\/]*$"
    local currentPath = fullPath
    while currentPath do
        local nextPath = currentPath:match(pattern)
        if not nextPath or nextPath == currentPath then break end
        currentPath = nextPath
        
        -- Check if this is a valid root
        if fullPath:sub(1, #currentPath) == currentPath then
            return fullPath:sub(#currentPath + 1)
        end
    end
    
    return fullPath
end

-- Find common root path among multiple paths
function PathUtils.findCommonRoot(paths)
    if #paths == 0 then return nil end
    if #paths == 1 then return PathUtils.getParentDirectory(paths[1]) end
    
    -- Split all paths into segments
    local segments = {}
    for _, path in ipairs(paths) do
        local pathSegments = {}
        for segment in path:gmatch("[^/\\]+") do
            table.insert(pathSegments, segment)
        end
        table.insert(segments, pathSegments)
    end
    
    -- Find common prefix
    local commonSegments = {}
    local minLen = math.huge
    for _, seg in ipairs(segments) do
        minLen = math.min(minLen, #seg)
    end
    
    for i = 1, minLen do
        local segment = segments[1][i]
        local isCommon = true
        for j = 2, #segments do
            if segments[j][i] ~= segment then
                isCommon = false
                break
            end
        end
        if isCommon then
            table.insert(commonSegments, segment)
        else
            break
        end
    end
    
    -- Reconstruct path
    if #commonSegments > 0 then
        -- Check if we're on Windows or Unix
        local separator = paths[1]:match("[\\/]")
        local commonPath = table.concat(commonSegments, separator)
        
        -- If common root is a file or files, return its directory
        if #commonSegments == #segments[1] then
            return PathUtils.getParentDirectory(commonPath)
        else
            return commonPath .. separator
        end
    end
    
    return nil
end

-- Swap version in a path
function PathUtils.swapVersion(path, newVersion)
    if not path or not newVersion then return path end
    
    -- Get current version info
    local versionInfo = PathUtils.extractVersionInfo(path)
    if not versionInfo then return path end
    
    -- Format new version number according to original format
    local formattedVersion
    if versionInfo.suffix then
        formattedVersion = string.format("V%03d_%d", newVersion, versionInfo.suffix)
    else
        formattedVersion = string.format("V%03d", newVersion)
    end
    
    -- Replace version in path
    local result = path
    local currentVersion = path:match("[vV]%d+_%d+") or path:match("[vV]%d+")
    if currentVersion then
        result = path:gsub(currentVersion, formattedVersion)
    end
    
    return result
end

-- Construct full path from PipeData
function PathUtils.constructFullPath(pipeData)
    if not pipeData then return nil end
    
    -- If we have both root and relative paths, use them
    if pipeData.rootPath and pipeData.relativePath then
        -- Normalize paths
        local root = pipeData.rootPath:gsub("\\", "/")
        local rel = pipeData.relativePath:gsub("\\", "/")
        
        -- Ensure root path ends with separator
        if not root:match("/$") then
            root = root .. "/"
        end
        
        -- Remove any leading separator from relative path
        rel = rel:gsub("^[/\\]+", "")
        
        -- Combine paths
        return root .. rel
    end
    
    -- Fallback to sourcePath for backward compatibility
    return pipeData.sourcePath
end


-- Get display name from loader
function PathUtils.getPartOrChannelNameFromLoader(tool)
    if not tool then return nil end
    
    -- First check if it's a loader
    if tool:GetAttrs().TOOLS_RegID == "Loader" then
        -- First try to get the Part name
        local part = tool:GetInput("Clip1.OpenEXRFormat.Part")
        if part then
            local partName = tostring(part)
            -- Check if it's a generic subimageXX pattern
            if partName:match("^subimage%d+$") then
                -- If generic, use the RedName
                local redName = tool:GetInput("Clip1.OpenEXRFormat.RedName")
                if redName then
                    return {
                        name = tostring(redName):gsub("%.R$", ""),
                        type = "channel",
                        sourcePartName = partName
                    }
                end
            else
                -- Otherwise use it as a part name
                return {
                    name = partName,
                    type = "part"
                }
            end
        end
    end
    return nil
end

-- Function to safely get parts from a loader
function PathUtils.getLoaderParts(loader)
    -- Check if we have access to OpenEXR format
    if not loader.Clip1 or not loader.Clip1.OpenEXRFormat then
        ErrorHandler.warning("PipeManager", "No OpenEXR format available for loader")
        return nil
    end
    
    -- Try to get parts
    local parts = loader.Clip1.OpenEXRFormat.Part
    if not parts then
        ErrorHandler.warning("PipeManager", "No parts found in OpenEXR. Probablly not multipart.")
        return nil
    end
    
    -- Get the parts table
    local partsTable = parts:GetAttrs().INPIDT_ComboControl_ID
    if not partsTable then
        ErrorHandler.warning("PipeManager", "Could not get parts table from OpenEXR")
        return nil
    end
    
    return partsTable
end

-- Add this function in the Utility section
function PathUtils.GetPartMappings(loader, partsTable)
    local mappings = {}
    local originalPart = loader:GetInput("Clip1.OpenEXRFormat.Part")
    
    for _, partName in pairs(partsTable) do
        loader:SetInput("Clip1.OpenEXRFormat.Part", partName)
        bmd.wait(0.05)  -- Uncomment if needed for stability
        
        local partInfo = PathUtils.getPartOrChannelNameFromLoader(loader)
        if partInfo then
            --mappings[partName] = partInfo.name
            mappings[partInfo.name] = partName
            mappings["part:" .. partName] = partInfo.name
            --[[ ErrorHandler.info("PipeManager", string.format(" EXR - Part '%s' contains '%s'", 
                             partName, partInfo.name))  ]]
        end
    end
    
    loader:SetInput("Clip1.OpenEXRFormat.Part", originalPart)
    return mappings
end

function PathUtils.findNewParts(oldMappings, newMappings)
    local newParts = {}
    
    -- Create lookup from old mappings (display names)
    local oldLookup = {}
    for displayName, partName in pairs(oldMappings) do
        if not displayName:match("^part:") then  -- Skip the part: prefix entries
            oldLookup[displayName] = true
        end
    end

    -- Check new mappings for new display names
    for displayName, partName in pairs(newMappings) do
        if not displayName:match("^part:") then  -- Skip the part: prefix entries
            if not oldLookup[displayName] then
                table.insert(newParts, {
                    displayName = displayName,
                    partID = partName  -- This is already the part name (e.g. subimage01)
                })
                ErrorHandler.info("PipeManager", string.format("Found new part: %s (ID: %s)", displayName, partName))
            end
        end
    end
    
    return newParts
end

-- Scan directory for sequences
function PathUtils.scanForSequences(rootPath)
    -- Normalize path for Fusion
    rootPath = rootPath:gsub("\\", "/")
    if not rootPath:match("/$") then
        rootPath = rootPath .. "/"
    end
    
    local results = {
        sequences = {},  -- Will store sequence paths with their folder structure
        folders = {}    -- Will store folder information
    }
    
    -- Function to find first EXR in a directory
    local function findFirstEXR(dirPath, folderName)
        local exrFiles = bmd.readdir(dirPath .. "*.exr")
        if exrFiles then
            for _, info in pairs(exrFiles) do
                if type(info) == "table" and info.Name then
                    -- Make path relative to input rootPath
                    local relativePath = dirPath:gsub("^" .. rootPath, "")
                    -- Store the folder structure and file info
                    local sequence = {
                        folder = folderName,  -- The folder name (empty for root)
                        relativePath = relativePath .. info.Name,  -- Full relative path
                        filename = info.Name   -- Just the filename
                    }
                    return sequence
                end
            end
        end
        return nil
    end
    
    -- Check current directory for EXR files
    local mainSeq = findFirstEXR(rootPath, "")
    if mainSeq then
        table.insert(results.sequences, mainSeq)
        results.folders["iffolders"] = true
    end
    
    -- Scan subdirectories
    local entries = bmd.readdir(rootPath .. "*")
    if entries then
        for _, info in pairs(entries) do
            if type(info) == "table" and info.IsDir then
                if info.Name and info.Name ~= "." and info.Name ~= ".." then
                    local folderPath = rootPath .. info.Name .. "/"
                    local seq = findFirstEXR(folderPath, info.Name)
                    if seq then
                        table.insert(results.sequences, seq)
                        
                    end
                end
            end
        end
    end
    
    return results
end

-- Move loader to specific frame
function PathUtils.moveLoader(tool, startFrom, comp)
    if not tool or not startFrom then 
        ErrorHandler.warning("PathUtils", "Tool or startFrom is nil")
        return 
    end
    
    -- Make sure we have a valid frame number
    local frameNumber = tonumber(startFrom)
    if not frameNumber then 
        ErrorHandler.warning("PathUtils", "Invalid frame number: " .. tostring(startFrom))
        return 
    end
    
    -- Move the clip
    bmd.MoveClip(tool, comp:GetAttrs().COMPN_GlobalStart, frameNumber)
end

-- Get parent directory from path
function PathUtils.getParentDirectory(path)
    if not path then return nil end
    return path:match("^(.+)[/\\][^/\\]*$") or ""
end

-- Ensure path ends with correct separator
function PathUtils.ensureTrailingSeparator(path)
    if not path then return nil end
    path = path:gsub("\\", "/")  -- Normalize to forward slashes first
    if not path:match("/$") then
        return path .. "/"
    end
    return path
end

-- Normalize path separators and format
function PathUtils.normalizePath(path)
    if not path then return nil end
    path = path:gsub("\\", "/")  -- Convert backslashes to forward slashes
    path = path:gsub("//+", "/") -- Remove multiple consecutive slashes
    return path
end

-- Enhanced path validation
function PathUtils.validatePathExists(path)
    if not path or path == "" then return false end
    path = PathUtils.normalizePath(path)
    return bmd.fileexists(path)
end

-- Get sequence info from path
function PathUtils.getSequenceInfo(path)
    if not path then return nil end
    
    local info = {}
    local parsedName = bmd.parseFilename(path)
    if parsedName then
        info.number = parsedName.Number
        info.extension = parsedName.Extension
        info.name = parsedName.Name
        info.path = parsedName.Path
    end
    return info
end

-- Construct path from components
function PathUtils.joinPath(...)
    local parts = {...}
    local normalized = {}
    
    for _, part in ipairs(parts) do
        if part and part ~= "" then
            -- Remove leading/trailing separators
            part = part:gsub("^[/\\]+", ""):gsub("[/\\]+$", "")
            if part ~= "" then
                table.insert(normalized, part)
            end
        end
    end
    
    return table.concat(normalized, "/")
end

-- Get parent folder name from path (just the name of the immediate parent directory)
function PathUtils.getParentFolder(path)
    if not path then return nil end
    path = PathUtils.normalizePath(path)
    
    -- Get the full parent directory path
    local parentDir = PathUtils.getParentDirectory(path)
    if not parentDir or parentDir == "" then return "" end
    
    -- Extract just the folder name
    return parentDir:match("([^/\\]+)$") or ""
end

-- Get base name from filename (strips version and frame numbers)
function PathUtils.getBaseName(filename)
    if not filename then return nil end
    
    -- Remove directory path if present
    local name = filename:match("([^/]+)$") or filename
    
    -- Extract the base part before frame number
    -- Pattern: captures everything up to the last dot-number-dot-extension
    -- e.g. "FLWR_010_V004_2.MAIN2" from "FLWR_010_V004_2.MAIN2.0030.exr"
    local baseName = name:match("(.+)%.[0-9]+%.%w+$")
    if not baseName then
        -- If no frame number found, just remove the extension
        baseName = name:match("(.+)%.%w+$") or name
    end
    
    return baseName
end

-- Get sequence base name (for matching across different versions)
function PathUtils.getSequenceBaseName(filename)
    if not filename then return nil end
    
    local baseName = PathUtils.getBaseName(filename)
    if not baseName then return nil end
    
    -- Remove version and main version patterns
    -- e.g. "FLWR_010" from "FLWR_010_V004_2.MAIN2"
    local sequenceName = baseName:match("(.-)_[vV]%d+") or 
                        baseName:match("(.-)_[vV]%d+_%d+") or 
                        baseName
                        
    -- Remove any trailing MAIN patterns
    sequenceName = sequenceName:gsub("%.MAIN%d*$", "")
    
    return sequenceName
end

return PathUtils 