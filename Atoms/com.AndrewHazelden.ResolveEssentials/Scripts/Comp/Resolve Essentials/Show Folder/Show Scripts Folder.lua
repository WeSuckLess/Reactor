-- Show Scripts Folder menu item
-- v4 - 2024-08-15

local path = app:MapPath("Scripts:/")
if bmd.direxists(path) == false then
	bmd.createdir(path)
	print("[Created Scripts Folder] " .. path)
end

print("[Show Scripts Folder] " .. path)
bmd.openfileexternal("Open", path)
