--[[--
Deadline Image Viewer | AddLoader.lua v5.73 2024-06-27
By Andrew Hazelden <andrew@andrewhazelden.com>

Overview:
Allows you to add new footage to the currently open Fusion Studio foreground composite using the command prompt. This makes Fusion Studio usable as a media viewer in Deadline.

Install:
1. Copy the Script:
Copy the "AddLoader.lua" script to a "Scripts/Support/" PathMap location:

macOS:
/Library/Application Support/Blackmagic Design/Fusion/Reactor/Deploy/Scripts/Support/Deadline/AddLoader.lua

Windows:
C:\ProgramData\Blackmagic Design\Fusion\Reactor\Deploy\Scripts\Support\Deadline\AddLoader.lua

Linux:
/opt/Reactor/Deploy/Scripts/Support/Deadline/AddLoader.lua

2. Set up Deadline Monitor:

In the Deadline Monitor program we are going to assign the "AddLoader.lua" script as one of the custom viewer entries.

In Super User mode, select the "Tools > Options..." menu item. In the Options window navigate to the "Configure Monitor Options > Image Viewers > " section.

We are going to define a "Custom Image Viewer 1:" entry.

Executable: (macOS)
/Applications/Blackmagic Fusion 18/Fusion.app/Contents/MacOS/fuscript

Executable: (Windows)
C:\Program Files\Blackmagic Design\Fusion 18\fuscript.exe

Executable: (Linux)
/opt/BlackmagicDesign/Fusion18/fuscript

Arguments: (macOS)
"/Library/Application Support/Blackmagic Design/Fusion/Reactor/Deploy/Scripts/Support/Deadline/AddLoader.lua" "{FRAME}"

Arguments: (Windows)
"C:\ProgramData\Blackmagic Design\Fusion\Reactor\Deploy\Scripts\Support\Deadline\AddLoader.lua" "{FRAME}"

Arguments: (Linux)
"/opt/Reactor/Deploy/Scripts/Support/Deadline/AddLoader.lua" "{FRAME}"

Name:
Fusion


Then in the Options window navigate to the "Configure Monitor Options > Monitor Options > " section. Set the "Task List > Completed Task Double-Click Behavior" entry to "View Image".

* * *

Usage:

After rendering an image sequence in Deadline, if the submitter supports filename detection in the output log you will be able to load that footage into Fusion Studio by double-clicking on the entry in the Tasks view.

You can also right-click on a task and select the "View Output" menu item.

* * *

Lua Script Customization:

OpenColorIO:
The "ocio.OCIOConfig" line in the script can be commented out to skip defining a custom OCIO config file path when the "OCIOColorSpace" node is added to the comp. You can also pre-define your preferred SourceSpace/OutputSpace/

Viewer:
The DoAction "Tool_ViewOn" lines in the script can be used to specify if you want the media to be loaded automatically in the left or right viewer window context.

Resolve:
If you really wanted to, one could modify this script so it adds the new Deadline renderings to the Resolve Media Pool, or as Loader node based media in the currently active Fusion page composite.

* * *

CLI Syntax:
fuscript <addLoader.lua> [/path/to/image.exr]

macOS:
"/Applications/Blackmagic Fusion 18/Fusion.app/Contents/MacOS/fuscript" "/Library/Application Support/Blackmagic Design/Fusion/Reactor/Deploy/Scripts/Support/Deadline/AddLoader.lua" "/Volumes/Path/To/Image.0001.exr"

Windows:
"C:\Program Files\Blackmagic Design\Fusion 18\fuscript.exe" "C:\ProgramData\Blackmagic Design\Fusion\Reactor\Deploy\Scripts\Support\Deadline\AddLoader.lua" "F:\Path\To\Image.0001.exr"

Linux:
"/opt/BlackmagicDesign/Fusion18/fuscript" "/opt/Reactor/Deploy/Scripts/Support/Deadline/AddLoader.lua" "/Path/To/Image.0001.exr"


* * *

fuscript CLI Syntax:
Usage: fuscript [opts] <script> [args]
   -i                   - Enter interactive mode
   -v                   - Print version information
   -s                   - Run as script server
   -S                   - Run as script server with no timeout
   -p [appname]         - Ping script apps
   -P [appname]         - Ping script apps (longer timeout)
   -q                   - Be quiet
   -b                   - Run with commandline debugger
   -l <lang>            - Explicitly set language, from:
      lua
      py2 or python2
      py3 or python3
   -x <string>          - Execute string

--]]--

print("[AddLoader]")

function ListArgs()
	print("[Args]")
	for i, v in ipairs(arg) do
		print("[" .. i .. "] ", v)
	end
end

function Connect()
	fusion = bmd.scriptapp([[Fusion]], [[localhost]], 0.0, 0, [[Interactive]]);
	if fusion ~= nil then
		fu = fusion
		app = fu

		composition = fu.CurrentComp
		comp = composition

		if comp ~= nil then
			SetActiveComp(comp)
			return comp
		end
	else
		print([[FuScript Error] Please open up the Fusion Studio GUI before running this menu item.]])
	end

	return nil
end

function AddMedia(comp)
	if comp ~= nil and arg[1] ~= nil and arg[1] ~= "" then
		loaderFile = arg[1]
		if bmd.fileexists(loaderFile) == true then
			print('  [Media] "' .. tostring(loaderFile) .. '"')

			-- Add a new undo history item
			comp:StartUndo("Deadline Loader")

			-- Deselect the nodes
			comp.CurrentFrame.FlowView:Select()

			-- Disable the file browser dialog
			AutoClipBrowse = app:GetPrefs("Global.UserInterface.AutoClipBrowse")
			app:SetPrefs("Global.UserInterface.AutoClipBrowse", false)
	
			-- Add the Loader node to the comp
			local ldr = comp:AddTool("Loader", -32768, -32768)
	
			-- Update the loader's clip filename
			ldr.Clip[fu.TIME_UNDEFINED] = fusion:MapPath(loaderFile)
	
			-- Re-enable the file browser dialog
			app:SetPrefs("Global.UserInterface.AutoClipBrowse", AutoClipBrowse)
	
			-- Loop
			ldr:SetAttrs({TOOLBT_Clip_Loop = true})
	
			-- Hold on missing frames
			ldr.MissingFrames = 1
	
			-- Enable HiQ mode
			comp:SetAttrs{COMPB_HiQ = true}

			-- Add an OCIO node
			local ocio = comp:AddTool("OCIOColorSpace", -32768, -32768)
			if jit.os == "OSX" then
				ocio.OCIOConfig[fu.TIME_UNDEFINED] = [[/Applications/aces_1.0.3/config.ocio]]
			elseif jit.os == "Linux" then
				ocio.OCIOConfig[fu.TIME_UNDEFINED] = [[/opt/aces_1.0.3/config.ocio]]
			else
				ocio.OCIOConfig[fu.TIME_UNDEFINED] = [[C:\aces_1.0.3\config.ocio]]
			end

			-- ACEScg Input
			ocio.SourceSpace[fu.TIME_UNDEFINED] = "ACES - ACEScg"

			-- sRGB Output
			ocio.OutputSpace[fu.TIME_UNDEFINED] = "Output - sRGB"

			-- Linear Output
			-- ocio.OutputSpace[fu.TIME_UNDEFINED] = "lin_srgb"

			-- Display in the left viewer window
			comp:DoAction("Tool_ViewOn", {index = 1})

			-- Display in the right viewer window
			-- comp:DoAction("Tool_ViewOn", {index = 2})

			-- Close off the undo history item block
			comp:EndUndo(true)
		else
			print("  [Media] Footage not found: \"" .. tostring(loaderFile) .. "\"")
		end
	end
end

function Main()
	-- List the CLI arguments
	-- ListArgs()

	-- Connect to Fusion
	comp = Connect()

	-- Add the footage
	AddMedia(comp)

	print("[Done]")
end

Main()


