--[[--
Pipe v1.0
by Dominik Bargiel dominikbargiel97@gmail.com
-------------------------------------------------------------------------------
Copyright Notice:
-------------------------------------------------------------------------------
This script is part of WSL (We Suck Less) tools collection.

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, subject to
the following conditions:

1. The above copyright notice and this permission notice shall be included
   in all copies or substantial portions of the Software.
   
2. Distribution is restricted to:
   - WSL Reactor package manager
   - We Suck Less forum (www.steakunderwater.com)

Any other distribution or sharing of this script outside of these channels
requires explicit permission from the author.

Copyright © 2025 Dominik Bargiel
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
Files Included:
-------------------------------------------------------------------------------
- Fuses:/Flow/Pipe.fuse
- Fuses:/Matte/PipeCryptomatte.fuse
- Scripts:/Utility/Pipe/PipeManager.lua
- Scripts:/Utility/Pipe/PipeCreate.lua
- Modules:/Lua/Pipe/PipeCryptomatte_utilities.lua
- Modules:/Lua/Pipe/ErrorHandler.lua	
- Modules:/Lua/Pipe/PathUtils.lua
- Modules:/Lua/Pipe/Pipetest_cryptomatte_utilities.lua
- Config:/Pipe/Pipe_shortcuts.fu
- Config:/Pipe/Pipe_tile_and_color.fu
- Config:/Pipe/cryptomatte_shortcuts.fu
-------------------------------------------------------------------------------
Version History:
-------------------------------------------------------------------------------
v1.0 - 2024-01-20
    - Initial release
    - Basic pipe functionality with Pipe groups and AOV selection
    - Automatic renaming from tools and loaders
	- Pipe Manager - Version control and path managment
	- EXR Workflow Enhancements
	- Pipe Cryptomatte - Added support for multipart EXR files
--]]--


-- Add at the top after initial comments
local PathUtils = require("Pipe.PathUtils")
local GroupValidation = require("Pipe.GroupValidation")
local ErrorHandler = require("Pipe.ErrorHandler")

-- Configure ErrorHandler
ErrorHandler.configure({
    logToConsole = true,
    logToFile = true,
    logFile = "PipeManager.log",
    minLevel = ErrorHandler.Level.INFO  -- Show all messages
})

-- Initialize UI Manager
local ui = fu.UIManager
local disp = bmd.UIDispatcher(ui)
local width, height = 1500, 800

-- Constants
local VERSION_PATTERNS = {
    DEFAULT = {pattern = "V%d+", format = "V%03d"}  -- Simplified to handle any number of digits
}

-- At the top of the file, after the constants, add:
local initialRefreshDone = false

-- Optimized Status Updates
local StatusUtils = {
    log = function(msg)
        -- Only log important status messages through ErrorHandler
        if msg:match("^===") then
            ErrorHandler.info("PipeManager", msg)
        end
    end,

    updateStatus = function(statusBar, msg)
        if statusBar then
            statusBar.Text = msg
        end
        -- Only log final status updates
        if msg:match("^Paths updated") or msg:match("^Error") then
            ErrorHandler.info("PipeManager", "[STATUS] " .. msg)
        end
    end
}

-- Optimized Selection Handling
local SelectionUtils = {
    getSelectedItems = function(tree)
        if not tree then 
            ErrorHandler.warning("PipeManager", "No tree provided for selection")
            return {}, 0 
        end
        
        local selected = {}
        local count = 0
        
        -- Get selected items directly from the tree
        for i = 0, tree:TopLevelItemCount() - 1 do
            local item = tree:TopLevelItem(i)
            if item and item.Selected then  -- Use direct property access
                local displayName = item.Text[0]
                if displayName then
                    selected[displayName] = true
                    selected["Pipe_" .. displayName] = true
                    count = count + 1
                    StatusUtils.log("Selected item: " .. displayName)
                end
            end
        end
        
        return selected, count
    end,
    
    createLookupTable = function(items)
        local lookup = {}
        for _, item in ipairs(items) do
            lookup[item.name] = true
        end
        return lookup
    end
}

-- Helper Functions
function ParsePath(fullPath)
    return PathUtils.parsePath(fullPath)
end

-- Replace GetAllPipeGroups function with GroupValidation version
function GetAllPipeGroups()
    return GroupValidation.getAllPipeGroups(fu:GetCurrentComp())
end

function ExtractGroupData(group)
    if not group then 
        ErrorHandler.error("PipeManager", "No group provided to extract data from")
        return nil 
    end
    
    local data = {
        name = group.name or "Unnamed Group",
        rootPath = group.rootPath or "",
        hasVersioning = group.hasVersioning or false,
        loaders = {},
        commonPatterns = {},
        fileTypes = {},
        pipes = group.pipes or {}
    }
    
    if not GroupValidation.validate(data) then
        ErrorHandler.error("PipeManager", "Invalid group data structure")
        return nil
    end
    
    for _, pipe in ipairs(data.pipes) do
        if type(pipe) == "table" and pipe.toolID then
            local tool = comp:FindTool(pipe.toolID)
            if tool then
                local path = ""
                if tool:GetAttrs().TOOLS_RegID == "Loader" then
                    path = tool.Clip[fu.TIME_UNDEFINED]
                end
                
                if path and path ~= "" then
                    local pathInfo = PathUtils.parsePath(path)
                    local fileType = path:match("%.([^%.]+)$")
                    table.insert(data.loaders, {
                        name = tool:GetAttrs().TOOLS_Name,
                        path = path,
                        pathInfo = pathInfo,
                        fileType = fileType
                    })
                    
                    if fileType then
                        data.fileTypes[fileType] = true
                    end
                end
            end
        end
    end
    
    return data
end

-- Helper function to get table keys
function table.keys(t)
    local keys = {}
    for k, _ in pairs(t) do
        table.insert(keys, k)
    end
    return keys
end

-- UI Creation
win = disp:AddWindow({
    ID = 'PipeManagerWin',
    WindowTitle = 'Pipe Manager',
    Geometry = {100, 100, width, height},
    Spacing = 0,
    Margin = 5,
    MinimumSize = {800, 600},
    Resize = true,

    ui:VGroup{
        ID = 'root',
        
        -- Group Selection
        ui:HGroup{
            Weight = 0,
            ui:Label{
                ID = 'GroupLabel',
                Text = 'Pipe Group:',
                Weight = 0.2
            },
            ui:ComboBox{
                ID = 'GroupSelect',
                Weight = 0.8
            }
        },
        
        -- Group Info
        ui:Tree{
            ID = 'GroupInfo',
            SortingEnabled = true,
            Events = {
                ItemDoubleClicked = true,
                ItemClicked = true
            },
            Weight = 1
        },
        
        -- Project Root Path
        ui:HGroup{
            Weight = 0,
            ui:Label{
                ID = 'RootLabel',
                Text = 'Root Path:',
                Weight = 0.2
            },
            ui:LineEdit{
                ID = 'RootPath',
                Weight = 0.6,
                PlaceholderText = 'Enter root path...'
            },
            ui:Button{
                ID = 'BrowseRoot',
                Text = 'Browse',
                Weight = 0.2
            }
        },
        
        -- Version Control (only shown for versioned groups)
        ui:HGroup{
            ID = 'VersionControl',
            Weight = 0,
            ui:Label{
                ID = 'VersionLabel',
                Text = 'Version:',
                Weight = 0.2
            },
            ui:SpinBox{
                ID = 'Version',
                Value = 1,
                Minimum = 1,
                Maximum = 999,
                Weight = 0.5
            },
            ui:Button{
                ID = 'SwapAll',
                Text = 'Swap Version',
                Weight = 0.5
            }
        },
        
        -- Pipe List
        ui:Tree{
            ID = 'PipeList',
            SortingEnabled = true,
            SelectionMode = "ExtendedSelection",
            SelectionBehavior = "SelectRows",
            Events = {
                ItemDoubleClicked = true,
                ItemClicked = true,
                SelectionChanged = true
            },
            Weight = 3
        },
        
        -- Action Buttons
        ui:HGroup{
            Weight = 0,
            ui:Button{ID = 'UpdateAll', Text = 'Update All Paths', Weight = 1},
            ui:Button{ID = 'ValidatePaths', Text = 'Validate Paths', Weight = 1},
            ui:Button{ID = 'Refresh', Text = 'Refresh', Weight = 1}
        },
        
        -- Status Bar
        ui:Label{
            ID = 'StatusBar',
            Text = 'Ready',
            Weight = 0,
            Alignment = {
                AlignHCenter = true,
                AlignVCenter = true
            }
        }
    }
})

-- Initialize Tree Headers
local itm = win:GetItems()

-- Group Info headers
local infoHdr = itm.GroupInfo:NewItem()
infoHdr.Text[0] = 'Property'
infoHdr.Text[1] = 'Value'
itm.GroupInfo:SetHeaderItem(infoHdr)

-- Set Group Info column widths
itm.GroupInfo.ColumnCount = 2
itm.GroupInfo.ColumnWidth[0] = 200
itm.GroupInfo.ColumnWidth[1] = 750

-- Pipe List headers
local hdr = itm.PipeList:NewItem()
hdr.Text[0] = 'Pipe Name'
hdr.Text[1] = 'Relative Path'
hdr.Text[2] = 'Full Path'
hdr.Text[3] = 'Status'
itm.PipeList:SetHeaderItem(hdr)

-- Set Pipe List column widths
itm.PipeList.ColumnCount = 4
itm.PipeList.ColumnWidth[0] = 150
itm.PipeList.ColumnWidth[1] = 300
itm.PipeList.ColumnWidth[2] = 400
itm.PipeList.ColumnWidth[3] = 100

-- Configure selection mode explicitly after creation
itm.PipeList:SetSelectionMode("ExtendedSelection")
itm.PipeList:SetSelectionBehavior("SelectRows")

-- Event Handlers
function win.On.PipeManagerWin.Close(ev)
    ErrorHandler.info("PipeManager", "Window closing")
    disp:ExitLoop()
end

function win.On.GroupSelect.CurrentIndexChanged(ev)
    ErrorHandler.info("PipeManager", "Group selection changed")
    ErrorHandler.info("PipeManager", "Selected Group: " .. itm.GroupSelect.CurrentText)
    UpdateGroupDisplay(itm.GroupSelect.CurrentText)
end

function win.On.BrowseRoot.Clicked(ev)
    ErrorHandler.info("PipeManager", "Browse root button clicked")
    local currentPath = itm.RootPath.Text
    ErrorHandler.info("PipeManager", "Current Path: " .. (currentPath or ""))
    
    local startPath = currentPath
    
    if currentPath and currentPath ~= "" then
        if PathUtils.validatePathExists(currentPath) then
            startPath = currentPath
            ErrorHandler.info("PipeManager", "Using current path as start path")
        else
            local parentPath = PathUtils.getParentDirectory(currentPath)
            if parentPath and PathUtils.validatePathExists(parentPath) then
                startPath = parentPath
                ErrorHandler.info("PipeManager", "Using parent path as start path: " .. parentPath)
            end
        end
    end
    
    ErrorHandler.info("PipeManager", "Opening directory browser at: " .. (startPath or "default location"))
    local selectedPath = fu:RequestDir(startPath or "")
    
    if selectedPath then
        ErrorHandler.info("PipeManager", "Selected new path: " .. selectedPath)
        selectedPath = PathUtils.normalizePath(selectedPath)
        selectedPath = PathUtils.ensureTrailingSeparator(selectedPath)
        itm.RootPath.Text = selectedPath
        ErrorHandler.info("PipeManager", "Updated root path to: " .. selectedPath)
    else
        ErrorHandler.info("PipeManager", "No path selected")
    end
end

function win.On.UpdateAll.Clicked(ev)
    ErrorHandler.info("PipeManager", "Update All button clicked")
    local groupName = itm.GroupSelect.CurrentText
    ErrorHandler.info("PipeManager", "Current Group: " .. (groupName or "none"))
    
    local groups = GetAllPipeGroups()
    local group = groups[groupName]
    
    if not group then
        ErrorHandler.error("PipeManager", "No group selected")
        itm.StatusBar.Text = 'No group selected'
        return
    end
    
    ErrorHandler.info("PipeManager", "Updating all paths for group")
    UpdateGroupPaths(group)
end

function win.On.ValidatePaths.Clicked(ev)
    ErrorHandler.info("PipeManager", "Validate Paths button clicked")
    ValidateAllPaths()
end

function win.On.Refresh.Clicked(ev)
    ErrorHandler.info("PipeManager", "Refresh button clicked")
    RefreshDisplay()
end

function win.On.PipeList.ItemClicked(ev)
    -- Get the clicked item
    local item = ev.item
    if not item then
        -- If no item was clicked (clicked on empty space), clear all selections
        for i = 0, itm.PipeList:TopLevelItemCount() - 1 do
            local treeItem = itm.PipeList:TopLevelItem(i)
            if treeItem then
                treeItem.Selected = false
            end
        end
        ErrorHandler.info("PipeManager", "Cleared all selections")
        return
    end
    
    -- If an item was clicked, just log it
    ErrorHandler.info("PipeManager", "Clicked item: " .. item.Text[0])
end

function win.On.PipeList.ItemDoubleClicked(ev)
    ErrorHandler.info("PipeManager", "Pipe List item double clicked")
    ErrorHandler.info("PipeManager", "Double Clicked Item: " .. ev.item.Text[0])
    
    local fullPath = ev.item.Text[2]
    ErrorHandler.info("PipeManager", "Full Path: " .. fullPath)
    
    bmd.setclipboard(fullPath)
    StatusUtils.updateStatus(itm.StatusBar, 'Copied path: ' .. fullPath)
    
    local folder = PathUtils.getParentDirectory(fullPath)
    if folder and folder ~= "" then
        ErrorHandler.info("PipeManager", "Opening folder: " .. folder)
        if FuPLATFORM_WINDOWS then
            folder = folder:gsub("/", "\\")
            if not folder:match('^".*"$') then
                folder = '"' .. folder .. '"'
            end
            local cmd = string.format('explorer.exe %s', folder)
            ErrorHandler.info("PipeManager", "Running command: " .. cmd)
            os.execute(cmd)
        elseif FuPLATFORM_MAC then
            ErrorHandler.info("PipeManager", "Running command: open " .. folder)
            os.execute('open "' .. folder .. '"')
        else
            ErrorHandler.info("PipeManager", "Running command: xdg-open " .. folder)
            os.execute('xdg-open "' .. folder .. '"')
        end
    else
        ErrorHandler.error("PipeManager", "Could not determine folder from path: " .. fullPath)
    end
end

function win.On.Version.ValueChanged(ev)
    ErrorHandler.info("PipeManager", "Version value changed")
    ErrorHandler.info("PipeManager", "New Version Value: " .. itm.Version.Value)
end

function win.On.SwapAll.Clicked(ev)
    ErrorHandler.info("PipeManager", "Swap Version button clicked")
    local groupName = itm.GroupSelect.CurrentText
    ErrorHandler.info("PipeManager", "Current Group: " .. (groupName or "none"))
    
    local groups = GetAllPipeGroups()
    local group = groups[groupName]
    
    if not group then
        ErrorHandler.error("PipeManager", "No group selected")
        itm.StatusBar.Text = 'No group selected'
        return
    end
    
    local newVersion = itm.Version.Value
    ErrorHandler.info("PipeManager", "Swapping to version: " .. newVersion)
    SwapGroupVersion(group, newVersion)
end

-- Core Functions
function RefreshDisplay()
    StatusUtils.log("\n=== Starting Display Refresh ===")
    
    -- Clear and repopulate group selector
    StatusUtils.log("Clearing group selector...")
    itm.GroupSelect:Clear()
    
    StatusUtils.log("Getting all pipe groups...")
    local groups = GetAllPipeGroups()
    for name, _ in pairs(groups) do
        StatusUtils.log("Adding group: " .. name)
        itm.GroupSelect:AddItem(name)
    end
    
    -- Update current group display if any selected, but ONLY if not during initial load
    local currentGroup = itm.GroupSelect.CurrentText
    if currentGroup and currentGroup ~= "" and initialRefreshDone then
        StatusUtils.log("Updating display for current group: " .. currentGroup)
        UpdateGroupDisplay(currentGroup)
    else
        StatusUtils.log("Skipping initial group display update")
        initialRefreshDone = true
    end
    
    StatusUtils.updateStatus(itm.StatusBar, 'Display refreshed')
    StatusUtils.log("=== Display Refresh Complete ===\n")
end

function UpdateGroupDisplay(groupName)
    ErrorHandler.info("PipeManager", "\n=== Updating Group Display ===")
    ErrorHandler.info("PipeManager", "Group Name: " .. groupName)
    
    local groups = GetAllPipeGroups()
    local group = groups[groupName]
    
    if not group then
        ErrorHandler.error("PipeManager", "Group not found")
        itm.StatusBar.Text = 'Group not found'
        return
    end
    
    ErrorHandler.info("PipeManager", "Extracting group data...")
    local groupData = ExtractGroupData(group)
    if not groupData then
        ErrorHandler.error("PipeManager", "Failed to extract group data")
        itm.StatusBar.Text = 'Failed to extract group data'
        return
    end
    
    ErrorHandler.info("PipeManager", "\nGroup Info:")
    ErrorHandler.info("PipeManager", "Name: " .. groupData.name)
    ErrorHandler.info("PipeManager", "Total Pipes: " .. #(groupData.pipes or {}))
    ErrorHandler.info("PipeManager", "Total Loaders: " .. #(groupData.loaders or {}))
    
    -- Update Group Info display
    itm.GroupInfo:Clear()
    
    -- Add basic info
    local groupNameItem = itm.GroupInfo:NewItem()
    groupNameItem.Text[0] = "Group Name"
    groupNameItem.Text[1] = groupData.name
    itm.GroupInfo:AddTopLevelItem(groupNameItem)
    
    -- Get path info from all loaders to find common root
    local allPaths = {}
    local version = nil
    local versionNum = nil
    ErrorHandler.info("PipeManager", "\nGathering all loader paths for group: " .. groupData.name)
    for i, loader in ipairs(groupData.loaders) do
        if loader.path and loader.path ~= "" then
            -- Normalize path and add to array
            local normalizedPath = loader.path:gsub("\\", "/")
            table.insert(allPaths, normalizedPath)
            -- Try to find version pattern from any path
            if not version then
                local versionInfo = PathUtils.extractVersionInfo(loader.path)
                if versionInfo then
                    version = versionInfo.prefix .. string.format(versionInfo.format, versionInfo.number)
                    -- Store the version number for the spinner
                    versionNum = versionInfo.number
                end
            end
        end
    end
    
    -- Show/hide version control based on whether we found a version
    if version then
        itm.VersionControl.Visible = true
        if versionNum then
            itm.Version.Value = versionNum
        end
    else
        itm.VersionControl.Visible = false
    end
    
    -- Find the common root path
    local rootPath = nil
    if #allPaths > 0 then
        -- Split all paths into segments
        local segments = {}
        for _, path in ipairs(allPaths) do
            local pathSegments = {}
            for segment in path:gmatch("[^/]+") do
                table.insert(pathSegments, segment)
            end
            table.insert(segments, pathSegments)
        end
        
        -- Find common prefix
        local commonSegments = {}
        local minLen = math.huge
        for _, seg in ipairs(segments) do
            minLen = math.min(minLen, #seg)
        end
        
        for i = 1, minLen do
            local segment = segments[1][i]
            local isCommon = true
            for j = 2, #segments do
                if segments[j][i] ~= segment then
                    isCommon = false
                    break
                end
            end
            if isCommon then
                table.insert(commonSegments, segment)
            else
                break
            end
        end
        
        -- Reconstruct root path
        if #commonSegments > 0 then
            rootPath = table.concat(commonSegments, "/")
            -- Check if the last segment is a file (ends with an extension)
            local lastSegment = commonSegments[#commonSegments]
            if not lastSegment:match("%.%w+$") then
                -- Ensure it ends with a separator if it's not a file
                if not rootPath:match("/$") then
                    rootPath = rootPath .. "/"
                end
            else
                -- If it's a file, remove the last segment
                rootPath = table.concat(commonSegments, "/", 1, #commonSegments - 1)
            end
            ErrorHandler.info("PipeManager", "\nFound common root path: " .. rootPath)
        end
    end
    
    if rootPath then
        local rootPathItem = itm.GroupInfo:NewItem()
        rootPathItem.Text[0] = "Root Path"
        rootPathItem.Text[1] = rootPath
        itm.GroupInfo:AddTopLevelItem(rootPathItem)
        
        if version then
            local versionItem = itm.GroupInfo:NewItem()
            versionItem.Text[0] = "Current Version"
            versionItem.Text[1] = version
            itm.GroupInfo:AddTopLevelItem(versionItem)
        end
    end
    
    -- Add total counts
    local totalPipesItem = itm.GroupInfo:NewItem()
    totalPipesItem.Text[0] = "Total Pipes"
    totalPipesItem.Text[1] = tostring(#(groupData.pipes or {}))
    itm.GroupInfo:AddTopLevelItem(totalPipesItem)
    
    local totalLoadersItem = itm.GroupInfo:NewItem()
    totalLoadersItem.Text[0] = "Total Loaders"
    totalLoadersItem.Text[1] = tostring(#(groupData.loaders or {}))
    itm.GroupInfo:AddTopLevelItem(totalLoadersItem)
    
    -- Update root path control
    if rootPath then
        itm.RootPath.Text = rootPath
        itm.RootPath.Visible = true
        itm.RootLabel.Visible = true
        itm.BrowseRoot.Visible = true
    else
        itm.RootPath.Visible = false
        itm.RootLabel.Visible = false
        itm.BrowseRoot.Visible = false
    end
    
    -- Update pipe list
    UpdatePipeList(group)
    
    -- Adjust column widths
    AdjustColumnWidths()
    
    itm.StatusBar.Text = 'Group display updated'
end

function UpdatePipeList(group)
    itm.PipeList:Clear()
    
    if not group or not group.pipes or type(group.pipes) ~= "table" then
        itm.StatusBar.Text = 'No valid pipes to display'
        return
    end
    
    local comp = fu:GetCurrentComp()
    for _, pipe in ipairs(group.pipes) do
        if type(pipe) == "table" and pipe.toolID then
            local tool = comp:FindTool(pipe.toolID)
            if tool then
                local item = itm.PipeList:NewItem()
                -- Remove "Pipe_" prefix from pipe name
                local displayName = pipe.pipeName:match("^Pipe_(.+)") or pipe.pipeName
                item.Text[0] = displayName
                
                -- Get path info from PipeData
                local pipeData = tool:GetData("PipeData")
                if pipeData then
                    -- Get relative path directly from PipeData
                    item.Text[1] = pipeData.relativePath or ""
                    
                    -- Get full path using GetFullPath function
                    local fullPath = GetFullPath(pipeData)
                    item.Text[2] = fullPath or ""
                    
                    -- Validate the path using PathUtils
                    item.Text[3] = PathUtils.validatePathExists(fullPath) and 'Valid' or 'Missing'
                else
                    -- If no PipeData, try to get path from loader directly
                    if tool:GetAttrs().TOOLS_RegID == "Loader" then
                        local loaderPath = tool.Clip[fu.TIME_UNDEFINED]
                        if loaderPath and loaderPath ~= "" then
                            item.Text[1] = pipe.relativePath or ""  -- Use pipe's relative path if available
                            item.Text[2] = loaderPath
                            item.Text[3] = PathUtils.validatePathExists(loaderPath) and 'Valid' or 'Missing'
                        else
                            item.Text[1] = ""
                            item.Text[2] = ""
                            item.Text[3] = 'Unknown'
                        end
                    else
                        item.Text[1] = ""
                        item.Text[2] = ""
                        item.Text[3] = 'Unknown'
                    end
                end
                
                -- Add the item to the tree
                itm.PipeList:AddTopLevelItem(item)
            end
        end
    end
    
    -- Adjust column widths based on content
    AdjustColumnWidths()
    
    -- Update status
    itm.StatusBar.Text = string.format('Found %d pipes', itm.PipeList:TopLevelItemCount())
end

function SwapGroupVersion(group, newVersion)
    ErrorHandler.info("PipeManager", "\n=== Swapping Group Version ===")
    ErrorHandler.info("PipeManager", "Group: " .. group.name)
    ErrorHandler.info("PipeManager", "New Version: " .. newVersion)
    
    if not group then 
        ErrorHandler.error("PipeManager", "No group provided")
        StatusUtils.updateStatus(itm.StatusBar, "No group provided")
        return 
    end
    
    local comp = fu:GetCurrentComp()
    local success = true
    local updatedCount = 0
    local undoGroup = {}
    
    StatusUtils.updateStatus(itm.StatusBar, "Collecting paths to update...")
    
    -- First pass: collect all changes
    for _, pipe in ipairs(group.pipes) do
        local loader = comp:FindTool(pipe.toolID)
        if loader and loader:GetAttrs().TOOLS_RegID == "Loader" then
            local currentPath = loader.Clip[fu.TIME_UNDEFINED]
            if currentPath and currentPath ~= "" then
                local startFrame = loader.GlobalIn[fu.TIME_UNDEFINED]
                local newPath = PathUtils.swapVersion(currentPath, newVersion)
                if newPath ~= currentPath then
                    table.insert(undoGroup, {
                        loader = loader,
                        oldPath = currentPath,
                        newPath = newPath,
                        startFrame = startFrame
                    })
                end
            end
        end
    end
    
    -- If we have changes to make, do them all in one undo group
    if #undoGroup > 0 then
        comp:StartUndo("Swap Group Version")
        
        for i, change in ipairs(undoGroup) do
            StatusUtils.updateStatus(itm.StatusBar, string.format("Updating path %d of %d...", i, #undoGroup))
            
            comp:Lock()
            if PathUtils.updateLoaderPath(change.loader, change.newPath) then
                bmd.MoveClip(change.loader, comp:GetAttrs().COMPN_GlobalStart, tonumber(change.startFrame))
                -- Force reload
                change.loader:SetAttrs({TOOLB_PassThrough = true})
                change.loader:SetAttrs({TOOLB_PassThrough = false})
                updatedCount = updatedCount + 1
            else
                success = false
            end
            comp:Unlock()
        end
        
        comp:EndUndo()
        
        -- After version swap, update all paths to refresh PipeData
        if success then
            StatusUtils.updateStatus(itm.StatusBar, "Refreshing pipe data...")
            -- Store current root path
            local currentRoot = itm.RootPath.Text
            
            -- Update the display to get the new paths
            UpdateGroupDisplay(group.name)
            
            -- Now update all paths which will update PipeData
            UpdateGroupPaths(group)
            
            -- Restore the root path
            itm.RootPath.Text = currentRoot
        end
    end
    
    -- Final status update
    local statusMsg = success 
        and string.format("Version swap completed: %d paths updated", updatedCount)
        or "Some paths failed to update"
    
    StatusUtils.updateStatus(itm.StatusBar, statusMsg)

    UpdateGroupDisplay(group.name)
    ErrorHandler.info("PipeManager", "Display refresh completed")
end

function UpdateGroupPaths(group)
    if not group then 
        ErrorHandler.error("PipeManager", "No group provided to update paths")
        return 
    end
    
    local rootPath = itm.RootPath.Text
    if not rootPath or rootPath == '' then
        ErrorHandler.error("PipeManager", "Root path not set")
        itm.StatusBar.Text = 'Please set root path first'
        return
    end
    
    ErrorHandler.info("PipeManager", "Starting path update")
    ErrorHandler.info("PipeManager", "New Root Path: " .. rootPath)
    
    StatusUtils.updateStatus(itm.StatusBar, "Starting path update...")
    
    -- Start updating paths
    comp:StartUndo("Update Group Paths")
    
    -- Clean up and normalize the path
    rootPath = PathUtils.normalizePath(rootPath)
    
    -- Check if path ends with a file
    if rootPath:match("%.exr/?$") then
        rootPath = PathUtils.getParentDirectory(rootPath)
        if not rootPath then
            ErrorHandler.error("PipeManager", "Invalid path format")
            itm.StatusBar.Text = 'Invalid path format'
            return
        end
    end
    
    -- Ensure path ends with slash
    rootPath = PathUtils.ensureTrailingSeparator(rootPath)
    
    StatusUtils.updateStatus(itm.StatusBar, "Scanning for sequences...")
    -- Scan for sequences in the new location
    local scanResults = PathUtils.scanForSequences(rootPath)
    --[[ scanResults = {
            sequences = {},  -- Will store sequence paths with their folder structure
            folders = {}    -- Will store if subfolders exist
        }
        sequence = {
            folder = folderName,  -- The folder name (empty for root)
            relativePath = relativePath .. info.Name,  -- Full relative path
            filename = info.Name   -- Just the filename
        } ]]

    
    StatusUtils.updateStatus(itm.StatusBar, "Building part mappings...")
    -- Create a lookup table for quick path matching based on folder structure
    local pathLookup = {}
    local partMappings = {}  -- Will store part mappings for each sequence/folder
    
    -- Store original part information for all loaders first
    local originalParts = {}
    for _, pipe in ipairs(group.pipes) do
        if type(pipe) == "table" and pipe.toolID then
            local tool = comp:FindTool(pipe.toolID)
            if tool and tool:GetAttrs().TOOLS_RegID == "Loader" then
                originalParts[tool:GetAttrs().TOOLS_Name] = PathUtils.getPartOrChannelNameFromLoader(tool)
            end
        end
    end
    
    for _, seq in ipairs(scanResults.sequences) do
        pathLookup[seq.folder] = seq.relativePath
        ErrorHandler.info("PipeManager", string.format("Found sequence: %s -> %s (Full path: %s)", 
            seq.folder == "" and "root" or seq.folder,
            seq.relativePath,
            PathUtils.joinPath(rootPath, seq.relativePath)))
        
        -- Find first loader for this sequence/folder
        local firstLoaderInSeq = nil
        for _, pipe in ipairs(group.pipes) do
            if type(pipe) == "table" and pipe.toolID then
                local tool = comp:FindTool(pipe.toolID)
                if tool and tool:GetAttrs().TOOLS_RegID == "Loader" then
                    local currentPath = PathUtils.normalizePath(tool.Clip[fu.TIME_UNDEFINED])
                    local currentFolder = PathUtils.getParentFolder(currentPath) or ""
                    
                    if seq.folder == "" then
                        -- For root sequence, check if this is a main sequence loader by:
                        -- 1. Not being in a special folder (data, cryptomatte)
                        -- 2. Having the most parts (main sequence typically has all AOVs)
                        -- 3. Having standard part names (not just cryptomatte parts)
                        if not currentPath:match("/cryptomatte/") and not currentPath:match("/data/") then
                            -- Get the parts for this loader
                            local partsTable = PathUtils.getLoaderParts(tool)
                            if partsTable then
                                local partMappings = PathUtils.GetPartMappings(tool, partsTable)
                                local hasCryptoPartsOnly = true
                                
                                -- Check if this loader has any non-cryptomatte parts
                                for displayName, _ in pairs(partMappings) do
                                    if not displayName:match("^part:") and not displayName:match("crypto") then
                                        hasCryptoPartsOnly = false
                                        break
                                    end
                                end
                                
                                -- Use this loader if it has non-cryptomatte parts
                                if not hasCryptoPartsOnly then
                                    firstLoaderInSeq = tool
                                    ErrorHandler.info("PipeManager", string.format("Found main sequence loader: %s (Path: %s)", 
                                        tool:GetAttrs().TOOLS_Name, currentPath))
                                    break
                                end
                            end
                        end
                    else
                        -- Match using folder from PathUtils
                        if currentFolder == seq.folder then
                            firstLoaderInSeq = tool
                            ErrorHandler.info("PipeManager", string.format("Found matching folder: %s with loader: %s (Path: %s)", 
                                seq.folder, tool:GetAttrs().TOOLS_Name, currentPath))
                            break
                        end
                    end
                end
            end
        end
        
        -- If we found a loader for this sequence, build part mapping
        if firstLoaderInSeq then
            comp:Lock()  
            local originalPath = firstLoaderInSeq.Clip[fu.TIME_UNDEFINED]
            ErrorHandler.info("PipeManager", string.format("Processing sequence with loader: %s (Path: %s)", 
                firstLoaderInSeq:GetAttrs().TOOLS_Name, originalPath))

            -- Get old parts mapping
            local oldPartsMapping = {}
            local oldPartsTable = PathUtils.getLoaderParts(firstLoaderInSeq)

            if oldPartsTable then
                ErrorHandler.info("PipeManager", string.format("Found %d parts in original loader: %s", 
                    #oldPartsTable, firstLoaderInSeq:GetAttrs().TOOLS_Name))
                oldPartsMapping = PathUtils.GetPartMappings(firstLoaderInSeq, oldPartsTable)
            
                -- Set new path and get new parts mapping
                local newFullPath = PathUtils.joinPath(rootPath, seq.relativePath)
                ErrorHandler.info("PipeManager", string.format("Checking new path: %s", newFullPath))
                
                -- Temporarily set new path to get parts
                local success = PathUtils.updateLoaderPath(firstLoaderInSeq, newFullPath)
                if success then
                    -- Force a reload and wait for the loader to update
                    firstLoaderInSeq:SetAttrs({TOOLB_PassThrough = true})
                    bmd.wait(0.1)
                    firstLoaderInSeq:SetAttrs({TOOLB_PassThrough = false})
                    
                    local newPartsTable = PathUtils.getLoaderParts(firstLoaderInSeq)
                    if newPartsTable then
                        ErrorHandler.info("PipeManager", string.format("Found %d parts in new sequence at %s", 
                            #newPartsTable, seq.folder == "" and "root" or seq.folder))
                        local newPartsMapping = PathUtils.GetPartMappings(firstLoaderInSeq, newPartsTable)
                        
                        -- Store mappings for this sequence
                        partMappings[seq.folder] = {
                            old = oldPartsMapping,
                            new = newPartsMapping,
                            oldPath = originalPath,
                            newPath = newFullPath
                        }
                        
                        -- If this is the root sequence, also store as "root"
                        if seq.folder == "" then
                            partMappings["root"] = partMappings[seq.folder]
                            ErrorHandler.info("PipeManager", "Stored root sequence mappings")
                        end
                    end
                end
                
                -- Restore original path and force reload
                PathUtils.updateLoaderPath(firstLoaderInSeq, originalPath)
                firstLoaderInSeq:SetAttrs({TOOLB_PassThrough = true})
                bmd.wait(0.1)
                firstLoaderInSeq:SetAttrs({TOOLB_PassThrough = false})
            end
            
            comp:Unlock()
        else
            ErrorHandler.warning("PipeManager", string.format("No loader found for sequence: %s", 
                seq.folder == "" and "root" or seq.folder))
        end
    end

    -- Store the mappings in scanResults for use during updates
    scanResults.partMappings = partMappings
    
        ------------------------------------------------------------

    -- First, collect all the updated paths for GroupData
    local updatedPipes = {}
    
    -- Check frame range change with first loader
    local shouldAdjustFrames = false
    local firstLoader = nil
    local oldFrameNumber = nil
    local newFrameNumber = nil
    
    -- Find first loader to check frame range
    for _, pipe in ipairs(group.pipes) do
        if type(pipe) == "table" and pipe.toolID then
            local tool = comp:FindTool(pipe.toolID)
            if tool and tool:GetAttrs().TOOLS_RegID == "Loader" then
                firstLoader = tool
                break
            end
        end
    end
    
    -- Check frame range difference if we found a loader
    if firstLoader then
        comp:Lock()
        oldFrameNumber = PathUtils.getSequenceInfo(firstLoader.Clip[fu.TIME_UNDEFINED]).number
        -- Temporarily set new path to check frame number
        local folder = firstLoader.Clip[fu.TIME_UNDEFINED]:match(".*/([^/]+)/[^/]+%.exr$") or ""
        local newRelativePath = pathLookup[folder] or pathLookup[""]
        if newRelativePath then
            local newFullPath = PathUtils.joinPath(rootPath, newRelativePath)
            local originalPath = firstLoader.Clip[fu.TIME_UNDEFINED]
            PathUtils.updateLoaderPath(firstLoader, newFullPath)
            newFrameNumber = PathUtils.getSequenceInfo(newFullPath).number
            PathUtils.updateLoaderPath(firstLoader, originalPath)
            
            -- Force reload after path change
            firstLoader:SetAttrs({TOOLB_PassThrough = true})
            firstLoader:SetAttrs({TOOLB_PassThrough = false})
        end
        comp:Unlock()
        
        -- Ask user about frame adjustment if frames are different
        if oldFrameNumber ~= newFrameNumber then
            local result = comp:AskUser("Frame Range Change", {
                { 
                    "Message",
                    "Text",
                    Default = string.format("Frame numbers changed from %d to %d.\nAdjust frames?", oldFrameNumber, newFrameNumber),
                    ReadOnly = true,
                }
            })
            shouldAdjustFrames = result ~= nil
        end
    end
    
    comp:Lock()

    StatusUtils.updateStatus(itm.StatusBar, "Updating paths...")
    -- Update each pipe in the group
    for i, pipe in ipairs(group.pipes) do
        StatusUtils.updateStatus(itm.StatusBar, string.format("Updating pipe %d of %d...", i, #group.pipes))
        if type(pipe) == "table" and pipe.toolID then
            local tool = comp:FindTool(pipe.toolID)
            if tool and tool:GetAttrs().TOOLS_RegID == "Loader" then
                comp:Lock()
                
                -- Get current path and determine its folder structure
                local currentPath = PathUtils.normalizePath(tool.Clip[fu.TIME_UNDEFINED])
                
                -- Extract the folder using PathUtils
                local currentFolder = PathUtils.getParentFolder(currentPath) or ""
                
                -- Find the matching new path based on the folder structure
                local newRelativePath = pathLookup[currentFolder]
                local newFullPath = nil
                
                if newRelativePath then
                    newFullPath = PathUtils.joinPath(rootPath, newRelativePath)
                elseif pathLookup[""] then  -- If no match found in subfolders, check root
                    newRelativePath = pathLookup[""]
                    newFullPath = PathUtils.joinPath(rootPath, newRelativePath)
                end
                
                if newFullPath then
                    -- Get original part info from stored data
                    local oldInfo = originalParts[tool:GetAttrs().TOOLS_Name]
                    
                    -- Update the loader path
                    PathUtils.updateLoaderPath(tool, newFullPath)
                    
                    -- Force reload of clip
                    local currentClip = tool.Clip[fu.TIME_UNDEFINED]
                    tool.Clip[fu.TIME_UNDEFINED] = ""  -- Clear the clip
                    tool.Clip[fu.TIME_UNDEFINED] = currentClip  -- Restore the clip 

                    -- Handle frame adjustment
                    if shouldAdjustFrames then
                        PathUtils.moveLoader(tool, newFrameNumber, comp)
                    elseif tool == firstLoader then
                        PathUtils.moveLoader(tool, oldFrameNumber, comp)
                    end

                    local newInfo = PathUtils.getPartOrChannelNameFromLoader(tool)

                    if newInfo then
                        if oldInfo and newInfo.name ~= oldInfo.name then
--[[                             ErrorHandler.info("PipeManager", string.format("New part name: '%s' (%s), Source Part Name: '%s'", 
                            newInfo.name, newInfo.type, newInfo.sourcePartName))

                            ErrorHandler.info("PipeManager", string.format("Old part name: '%s' (%s), Source Part Name: '%s'", 
                            oldInfo.name, oldInfo.type, oldInfo.sourcePartName)) ]]

                            -- Try to find the old part name in the new EXR using pre-built mappings
                            local foundMatch = false
                            local folder = currentFolder or ""
                            local correctPart = nil
                            
                            -- First try direct channel name mapping in current folder
                            if scanResults.partMappings[folder] then
                                local mappings = scanResults.partMappings[folder]
                                
                                -- Try direct channel name mapping
                                if mappings.new[oldInfo.name] then
                                    correctPart = mappings.new[oldInfo.name]
                                    ErrorHandler.info("PipeManager", string.format("Found direct mapping '%s' -> '%s' (folder: %s)", 
                                        oldInfo.name, correctPart, folder))
                                    foundMatch = true
                                -- Try reverse lookup through part name
                                elseif oldInfo.sourcePartName and mappings.new["part:" .. oldInfo.sourcePartName] then
                                    local channelName = mappings.new["part:" .. oldInfo.sourcePartName]
                                    correctPart = mappings.new[channelName]
                                    if correctPart then
                                        ErrorHandler.info("PipeManager", string.format("Found reverse mapping through part '%s' -> '%s' (folder: %s)", 
                                            oldInfo.sourcePartName, correctPart, folder))
                                        foundMatch = true
                                    end
                                end
                            end
                            
                            -- If not found and we're in a subfolder, try root mappings
                            if not foundMatch and folder ~= "" and scanResults.partMappings["root"] then
                                local rootMappings = scanResults.partMappings["root"]
                                
                                -- Try direct channel name mapping in root
                                if rootMappings.new[oldInfo.name] then
                                    correctPart = rootMappings.new[oldInfo.name]
                                    ErrorHandler.info("PipeManager", string.format("Found root mapping '%s' -> '%s'", 
                                        oldInfo.name, correctPart))
                                    foundMatch = true
                                -- Try reverse lookup through part name in root
                                elseif oldInfo.sourcePartName and rootMappings.new["part:" .. oldInfo.sourcePartName] then
                                    local channelName = rootMappings.new["part:" .. oldInfo.sourcePartName]
                                    correctPart = rootMappings.new[channelName]
                                    if correctPart then
                                        ErrorHandler.info("PipeManager", string.format("Found root reverse mapping through part '%s' -> '%s'", 
                                            oldInfo.sourcePartName, correctPart))
                                        foundMatch = true
                                    end
                                end
                            end
                            
                            -- Only set the part if we found a valid mapping
                            if foundMatch and correctPart then
                                tool:SetInput("Clip1.OpenEXRFormat.Part", correctPart)
                                -- Force a reload after changing the part
                                tool:SetAttrs({TOOLB_PassThrough = true})
                                tool:SetAttrs({TOOLB_PassThrough = false})
                            else
                                ErrorHandler.warning("PipeManager", string.format("Could not find matching part '%s' in new EXR", oldInfo.name))
                            end
                        end
                    end
                    
                     -- Update PipeData
                    local pipeData = tool:GetData("PipeData") or {}
                    pipeData.sourcePath = newFullPath
                    pipeData.rootPath = rootPath
                    pipeData.relativePath = newRelativePath
                    pipeData.sourceType = pipeData.sourceType or "Loader"
                    pipeData.sourceTool = pipeData.sourceTool or tool:GetAttrs().TOOLS_Name
                    tool:SetData("PipeData", pipeData) 
                    
                     -- Store updated pipe info for GroupData
                    table.insert(updatedPipes, {
                        toolID = pipe.toolID,
                        pipeName = pipe.pipeName,
                        relativePath = newRelativePath,
                        sourcePath = newFullPath
                    }) 

                else
                    ErrorHandler.warning("PipeManager", "No matching path found for: " .. currentPath)
                end
                
                comp:Unlock()
            end
        end
    end

    comp:Unlock()

    -- Store root mappings if it's the root sequence
    for folder, mapping in pairs(partMappings) do
        if folder == "" then
            partMappings["root"] = mapping
            ErrorHandler.info("PipeManager", "Stored root sequence mappings")
            break  -- Only need to store once
        end
    end
    
    -- First, collect all the updated paths for GroupData
    local updatedPipes = {}
    
    -- Create a set of all existing parts across all sequences
    local allExistingParts = {}
    for _, pipe in ipairs(group.pipes) do
        if type(pipe) == "table" and pipe.toolID then
            local tool = comp:FindTool(pipe.toolID)
            if tool and tool:GetAttrs().TOOLS_RegID == "Loader" then
                local partInfo = PathUtils.getPartOrChannelNameFromLoader(tool)
                if partInfo then
                    allExistingParts[partInfo.name] = true
                    ErrorHandler.info("PipeManager", string.format("Found existing part: %s", partInfo.name))
                end
            end
        end
    end
    
    StatusUtils.updateStatus(itm.StatusBar, "Checking for new parts...")
    
    -- Process new parts for each sequence
    for folder, mapping in pairs(partMappings) do
        if mapping.old and mapping.new then
            -- Find new parts by comparing with all existing parts
            local newParts = {}
            for displayName, partID in pairs(mapping.new) do
                -- Skip the part: prefix entries and only process actual part mappings
                if not displayName:match("^part:") and not allExistingParts[displayName] then
                    table.insert(newParts, {
                        displayName = displayName,
                        partID = partID
                    })
                    ErrorHandler.info("PipeManager", string.format("Found new part in sequence '%s': %s (ID: %s)", 
                        folder, displayName, partID))
                end
            end
            
            if #newParts > 0 then
                ErrorHandler.info("PipeManager", string.format("Found %d new parts in sequence '%s'", #newParts, folder))
                
                -- Find a base loader from the current sequence first
                local baseLoader = nil
                local mainSequenceLoader = nil
                
                -- First, try to find the main sequence loader
                for _, pipe in ipairs(group.pipes) do
                    if type(pipe) == "table" and pipe.toolID then
                        local tool = comp:FindTool(pipe.toolID)
                        if tool and tool:GetAttrs().TOOLS_RegID == "Loader" then
                            local currentPath = PathUtils.normalizePath(tool.Clip[fu.TIME_UNDEFINED])
                            
                            -- Check if this is a main sequence loader
                            if not currentPath:match("/cryptomatte/") and not currentPath:match("/data/") then
                                local partsTable = PathUtils.getLoaderParts(tool)
                                if partsTable then
                                    local partMappings = PathUtils.GetPartMappings(tool, partsTable)
                                    local hasCryptoPartsOnly = true
                                    
                                    -- Check if this loader has any non-cryptomatte parts
                                    for displayName, _ in pairs(partMappings) do
                                        if not displayName:match("^part:") and not displayName:match("crypto") then
                                            hasCryptoPartsOnly = false
                                            break
                                        end
                                    end
                                    
                                    -- Use this loader if it has non-cryptomatte parts
                                    if not hasCryptoPartsOnly then
                                        mainSequenceLoader = tool
                                        ErrorHandler.info("PipeManager", string.format("Found main sequence loader for base: %s", 
                                            tool:GetAttrs().TOOLS_Name))
                                        break
                                    end
                                end
                            end
                        end
                    end
                end
                
                -- For root sequence, use the main sequence loader
                if folder == "" and mainSequenceLoader then
                    baseLoader = mainSequenceLoader
                    ErrorHandler.info("PipeManager", string.format("Using main sequence loader as base: %s", 
                        baseLoader:GetAttrs().TOOLS_Name))
                else
                    -- For other sequences, try to find a loader in the same folder
                    for _, pipe in ipairs(group.pipes) do
                        if type(pipe) == "table" and pipe.toolID then
                            local tool = comp:FindTool(pipe.toolID)
                            if tool and tool:GetAttrs().TOOLS_RegID == "Loader" then
                                local currentPath = PathUtils.normalizePath(tool.Clip[fu.TIME_UNDEFINED])
                                local currentFolder = PathUtils.getParentFolder(currentPath) or ""
                                
                                -- Try to match the folder
                                if currentFolder == folder then
                                    baseLoader = tool
                                    ErrorHandler.info("PipeManager", string.format("Using base loader from matching sequence: %s", 
                                        tool:GetAttrs().TOOLS_Name))
                                    break
                                end
                            end
                        end
                    end
                    
                    -- If still no loader found, use the main sequence loader as fallback
                    if not baseLoader and mainSequenceLoader then
                        baseLoader = mainSequenceLoader
                        ErrorHandler.info("PipeManager", string.format("Using main sequence loader as fallback base: %s", 
                            baseLoader:GetAttrs().TOOLS_Name))
                    end
                end
                
                if baseLoader then
                    ErrorHandler.info("PipeManager", string.format("Creating new loaders based on: %s", baseLoader:GetAttrs().TOOLS_Name))
                    
                    -- Create new loaders for each new part
                    for _, partInfo in ipairs(newParts) do
                        -- Skip if we've already processed this part
                        if allExistingParts[partInfo.displayName] then
                            ErrorHandler.info("PipeManager", string.format("Skipping already processed part: %s", partInfo.displayName))
                            goto continue_part
                        end
                        allExistingParts[partInfo.displayName] = true
                        
                        comp:Lock()
                        
                        ErrorHandler.info("PipeManager", string.format("Creating new loader for part '%s' (ID: %s) based on %s", 
                            partInfo.displayName, partInfo.partID, baseLoader:GetAttrs().TOOLS_Name))
                        
                        -- Clone the loader using flow selection
                        flow = comp.CurrentFrame.FlowView
                        flow:Select()  -- Clear current selection
                        comp:Copy(baseLoader)
                        comp:Paste()
                        local newLoader = comp.ActiveTool
                        
                        if newLoader then
                            -- Set a proper name for the loader
                            local loaderName = partInfo.displayName
                            
                            newLoader:SetAttrs({
                                TOOLS_Name = loaderName,
                                TOOLB_NameSet = true  -- Mark the name as manually set
                            })
                            
                            -- Update the loader path to the correct sequence
                            local newFullPath = PathUtils.joinPath(rootPath, pathLookup[folder] or pathLookup[""])
                            PathUtils.updateLoaderPath(newLoader, newFullPath)
                            
                            -- Set the part and verify
                            newLoader:SetInput("Clip1.OpenEXRFormat.Part", partInfo.partID)
                            
                            -- Verify the part was set correctly
                            local currentPart = newLoader:GetInput("Clip1.OpenEXRFormat.Part")
                            if currentPart ~= partInfo.partID then
                                ErrorHandler.warning("PipeManager", string.format("Part not set correctly. Retrying... (Expected: %s, Got: %s)", 
                                    partInfo.partID, tostring(currentPart)))
                                
                                -- Try setting the part again after a reload
                                newLoader:SetAttrs({TOOLB_PassThrough = false})
                                bmd.wait(0.1)
                                newLoader:SetInput("Clip1.OpenEXRFormat.Part", partInfo.partID)
                                newLoader:SetAttrs({TOOLB_PassThrough = true})
                                bmd.wait(0.1)
                            end
                            
                            -- Verify the final part setting
                            local finalPart = newLoader:GetInput("Clip1.OpenEXRFormat.Part")
                            if finalPart == partInfo.partID then
                                ErrorHandler.info("PipeManager", string.format("Successfully set part to: %s", partInfo.partID))
                            else
                                ErrorHandler.error("PipeManager", string.format("Failed to set part. (Expected: %s, Got: %s)", 
                                    partInfo.partID, tostring(finalPart)))
                            end
                            
                            -- Position the new loader with incremental offset
                            local x, y = flow:GetPos(baseLoader)
                            local index = 0
                            for i, part in ipairs(newParts) do
                                if part.displayName == partInfo.displayName then
                                    index = i
                                    break
                                end
                            end
                            flow:SetPos(newLoader, x + index, y + 1)
                            
                            -- Add to group's pipes and updatedPipes
                            local pipeName = "Pipe_" .. partInfo.displayName
                            
                            local newPipeInfo = {
                                toolID = newLoader:GetAttrs().TOOLS_Name,
                                pipeName = pipeName,
                                relativePath = pathLookup[folder] or pathLookup[""],
                                sourcePath = mapping.newPath
                            }
                            table.insert(group.pipes, newPipeInfo)
                            table.insert(updatedPipes, newPipeInfo)
                            
                            -- Set up PipeData using consistent path handling
                            local pipeData = {
                                sourceTool = newLoader:GetAttrs().TOOLS_Name,
                                sourceType = "Loader",
                                rootPath = rootPath,
                                relativePath = pathLookup[folder] or pathLookup[""],
                                sourcePath = mapping.newPath
                            }
                            newLoader:SetData("PipeData", pipeData)
                            
                            -- Immediately update the Pipe tool's GroupData
                            local allPipes = comp:GetToolList(false, "Fuse.Pipe")
                            for _, pipe in ipairs(allPipes) do
                                local groupData = pipe:GetData("GroupData")
                                if groupData and groupData.name == group.name then
                                    -- Add the new pipe to the GroupData
                                    table.insert(groupData.pipes, newPipeInfo)
                                    pipe:SetData("GroupData", groupData)
                                    
                                    -- Force reload of the Pipe tool to refresh UI
                                    pipe:SetData("WasOnceInitialized", false)
                                    pipe:SetAttrs({
                                        TOOLB_ControlsExpanded = true,
                                        TOOLB_PassThrough = false
                                    })
                                    pipe.ScriptReload[fu.TIME_UNDEFINED] = 1
                                    bmd.wait(0.1)
                                    pipe:SetAttrs({TOOLS_TileColor = {R = 0.584313725490196, G = 0.294117647058824, B = 0.803921568627451}})
                                end
                            end
                            
                            ErrorHandler.info("PipeManager", string.format("Successfully created loader for part: %s", partInfo.displayName))
                        else
                            ErrorHandler.error("PipeManager", "Failed to create new loader - paste failed")
                        end
                        
                        comp:Unlock()
                        ::continue_part::
                    end
                else
                    ErrorHandler.warning("PipeManager", string.format("Could not find base loader for sequence '%s'", folder))
                end
            end
        end
    end

    StatusUtils.updateStatus(itm.StatusBar, "Finalizing updates...")
 
    -- Sort pipes alphabetically by pipeName
    local function sortPipes(a, b)
        if not a or not b or not a.pipeName or not b.pipeName then
            return false
        end
        -- Remove "Pipe_" prefix for comparison
        local nameA = a.pipeName:match("^Pipe_(.+)") or a.pipeName
        local nameB = b.pipeName:match("^Pipe_(.+)") or b.pipeName
        return nameA:lower() < nameB:lower()
    end
    
    table.sort(group.pipes, sortPipes)
    table.sort(updatedPipes, sortPipes)
    
    -- Update GroupData for all pipes in the group
    local allPipes = comp:GetToolList(false, "Fuse.Pipe")
    for _, pipe in ipairs(allPipes) do
        local groupData = pipe:GetData("GroupData")
        if groupData and groupData.name == group.name then
            -- Create a lookup table for existing pipes to avoid duplicates
            local existingPipes = {}
            for _, existingPipe in ipairs(groupData.pipes) do
                existingPipes[existingPipe.toolID] = true
            end
            
            -- Add new pipes only if they don't already exist
            for _, newPipe in ipairs(updatedPipes) do
                if not existingPipes[newPipe.toolID] then
                    table.insert(groupData.pipes, newPipe)
                end
            end

            -- Sort pipes alphabetically by pipeName
            table.sort(groupData.pipes, sortPipes)
            
            pipe:SetData("GroupData", groupData)
            
            -- Also update PipeData for the Pipe tool itself
            local pipeData = pipe:GetData("PipeData")
            if pipeData then
                pipeData.rootPath = rootPath  -- Update root path here as well
                pipe:SetData("PipeData", pipeData)
            end

                        -- Force reload of the Pipe tool to refresh UI
            pipe:SetData("WasOnceInitialized", false)
            pipe:SetAttrs({
                TOOLB_ControlsExpanded = true,
                TOOLB_PassThrough = false
            })
            pipe.ScriptReload[fu.TIME_UNDEFINED] = 1
            bmd.wait(0.1)
            pipe:SetAttrs({TOOLS_TileColor = {R = 0.584313725490196, G = 0.294117647058824, B = 0.803921568627451}})
        end
    end

    comp:EndUndo()
    
    -- Refresh the display
    UpdateGroupDisplay(group.name)
    StatusUtils.updateStatus(itm.StatusBar, 'Paths updated successfully')

    ErrorHandler.info("PipeManager", "Path update complete")
end

function ValidateAllPaths()
    ErrorHandler.info("PipeManager", "\n=== Validating All Paths ===")
    local invalidPaths = {}
    local comp = fu:GetCurrentComp()
    
    local currentGroup = itm.GroupSelect.CurrentText
    ErrorHandler.info("PipeManager", "Current Group: " .. (currentGroup or "none"))
    
    if not currentGroup or currentGroup == "" then
        ErrorHandler.error("PipeManager", "No group selected")
        itm.StatusBar.Text = 'No group selected'
        return
    end
    
    local groups = GetAllPipeGroups()
    local group = groups[currentGroup]
    if not group then
        ErrorHandler.error("PipeManager", "Group not found")
        itm.StatusBar.Text = 'Group not found'
        return
    end
    
    -- Validate paths directly from group data
    for _, pipe in ipairs(group.pipes) do
        if type(pipe) == "table" and pipe.toolID then
            local tool = comp:FindTool(pipe.toolID)
            if tool then
                local fullPath = ""
                
                -- Try to get path from tool if it's a Loader
                if tool:GetAttrs().TOOLS_RegID == "Loader" then
                    fullPath = tool.Clip[fu.TIME_UNDEFINED]
                    -- If the path is empty, try to get from PipeData
                    if fullPath == "" then
                        local pipeData = tool:GetData("PipeData")
                        if pipeData and pipeData.sourcePath then
                            fullPath = pipeData.sourcePath
                        end
                    end
                else
                    -- For non-Loader tools, try to get from connected Loader
                    local connectedOutput = tool.Input and tool.Input:GetConnectedOutput()
                    if connectedOutput then
                        local sourceTool = connectedOutput:GetTool()
                        if sourceTool and sourceTool:GetAttrs().TOOLS_RegID == "Loader" then
                            fullPath = sourceTool.Clip[fu.TIME_UNDEFINED]
                        end
                    end
                    
                    -- If still no path, try PipeData
                    if fullPath == "" then
                        local pipeData = tool:GetData("PipeData")
                        if pipeData and pipeData.sourcePath then
                            fullPath = pipeData.sourcePath
                        end
                    end
                end
                
                -- Validate the path using PathUtils
                local isValid = PathUtils.validatePathExists(fullPath)
                if not isValid then
                    table.insert(invalidPaths, {
                        name = pipe.pipeName,
                        path = fullPath
                    })
                end
                
                -- Find and update the corresponding item in the tree
                for i = 0, itm.PipeList:TopLevelItemCount() - 1 do
                    local item = itm.PipeList:TopLevelItem(i)
                    if item and item.Text[0] == pipe.pipeName then
                        item.Text[3] = isValid and 'Valid' or 'Missing'
                        break
                    end
                end
            end
        end
    end
    
    if #invalidPaths > 0 then
        local msg = 'Invalid paths found:\n\n'
        for _, invalid in ipairs(invalidPaths) do
            msg = msg .. invalid.name .. ': ' .. invalid.path .. '\n'
        end
        itm.StatusBar.Text = #invalidPaths .. ' invalid paths found'
        ErrorHandler.warning("PipeManager", msg)
    else
        itm.StatusBar.Text = 'All paths are valid'
        ErrorHandler.info("PipeManager", "All paths are valid")
    end
end


-- Add new helper functions
function GetCurrentVersion(groupData)
    if not groupData or not groupData.loaders or #groupData.loaders == 0 then
        return nil
    end
    
    -- Try to find version from the first loader with a valid path
    for _, loader in ipairs(groupData.loaders) do
        if loader.pathInfo and loader.pathInfo.version then
            return loader.pathInfo.version
        end
    end
    
    return nil
end

-- Add function to adjust column widths based on content
function AdjustColumnWidths()
    -- Adjust PipeList columns
    local maxWidths = {150, 300, 400, 100}  -- Default minimum widths
    
    -- Calculate maximum content width for each column
    for i = 0, itm.PipeList:TopLevelItemCount() - 1 do
        local item = itm.PipeList:TopLevelItem(i)
        for col = 0, 3 do
            local content = item.Text[col]
            if content then
                -- Approximate width based on character count (adjust multiplier as needed)
                local width = content:len() * 7  -- 7 pixels per character approximation
                maxWidths[col + 1] = math.max(maxWidths[col + 1], width)
            end
        end
    end
    
    -- Apply new widths with some padding
    for col = 0, 3 do
        itm.PipeList.ColumnWidth[col] = maxWidths[col + 1] + 20  -- Add 20px padding
    end
    
    -- Adjust GroupInfo columns
    local infoMaxWidths = {200, 400}  -- Default minimum widths
    
    -- Calculate maximum content width for GroupInfo
    for i = 0, itm.GroupInfo:TopLevelItemCount() - 1 do
        local item = itm.GroupInfo:TopLevelItem(i)
        for col = 0, 1 do
            local content = item.Text[col]
            if content then
                local width = content:len() * 7
                infoMaxWidths[col + 1] = math.max(infoMaxWidths[col + 1], width)
            end
        end
    end
    
    -- Apply new widths to GroupInfo
    for col = 0, 1 do
        itm.GroupInfo.ColumnWidth[col] = infoMaxWidths[col + 1] + 20
    end
end

-- Enhanced file matching
function FindMatchingFile(newRoot, originalPath)
    ErrorHandler.info("PipeManager", "\n=== Finding Matching File ===")
    ErrorHandler.info("PipeManager", "New Root: " .. newRoot)
    ErrorHandler.info("PipeManager", "Original Path: " .. originalPath)

    -- Normalize paths
    newRoot = newRoot:gsub("\\", "/")
    originalPath = originalPath:gsub("\\", "/")
    
    -- Ensure root ends with separator
    if not newRoot:match("/$") then
        newRoot = newRoot .. "/"
    end

    -- Extract the relative path structure from the original path
    local relativePath = originalPath:match(".+/([^/]+/[^/]+%.exr)$")
    if not relativePath then
        ErrorHandler.error("PipeManager", "Could not extract relative path from: " .. originalPath)
        return nil
    end

    -- Update version number in the filename
    local newVersion = newRoot:match("V(%d+)")
    if newVersion then
        relativePath = relativePath:gsub("V%d+", "V" .. newVersion)
    end

    -- Construct the new full path
    local newPath = newRoot .. relativePath
    ErrorHandler.info("PipeManager", "Looking for file: " .. newPath)
    
    -- Check if the file exists using PathUtils
    if PathUtils.validatePathExists(newPath) then
        ErrorHandler.info("PipeManager", "Found file: " .. newPath)
        return newPath
    end

    ErrorHandler.error("PipeManager", "File not found: " .. newPath)
    return nil
end

-- Replace GetFullPath function with PathUtils version
function GetFullPath(pipeData)
    return PathUtils.constructFullPath(pipeData)
end

-- Helper function to count table entries
function table.count(t)
    local count = 0
    for _ in pairs(t) do count = count + 1 end
    return count
end

-- Initial Update
RefreshDisplay()

-- Show Window
win:Show()
disp:RunLoop()
win:Hide() 