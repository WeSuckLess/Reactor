-- ============================================================================
-- modules
-- ============================================================================

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vGradientToScriptVal"
DATATYPE = "ScriptVal"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = "ScriptVal",
    REGID_InputDataType = "Gradient",
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Gradient\\ScriptVal",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Create a ScriptVal object from a Gradient.",
    REGS_OpIconString = FUSE_NAME,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]
    self:RemoveControlPage("Controls")
    self:AddControlPage("Color", {CTID_DIB_ID  = "Icons.Tools.Tabs.Color"})

    InGradient = self:AddInput("Gradient", "Gradient", {
        LINKID_DataType = "Gradient",
        INPID_InputControl = "GradientControl",
        LINK_Main = 1
    })

    InShowDump = self:AddInput("Show Dump", "ShowDump", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })


    OutScriptVal = self:AddOutput("ScriptVal", "ScriptVal", {
        LINKID_DataType = "ScriptVal",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
end

function Process(req)
    -- [[ Creates the output. ]]
    local tbl = {}
    local gradient = InGradient:GetValue(req)
    local show_dump = InShowDump:GetValue(req).Value

    -- Get the number of color entries in the gradient
    totalColors = gradient:GetColorCount() - 1

    for index = 0, totalColors, 1 do
        local color = gradient:GetColor(index)
        table.insert(tbl, {P = tonumber(index / totalColors), R = tonumber(color.R), G = tonumber(color.G), B = tonumber(color.B), A = tonumber(color.A)})
    end

    if show_dump == 1 then
        print("\n----------------------")
        print("[" .. tostring(self.Name) .. "]")
        local txt_str = bmd.writestring(tbl)
        dump(txt_str)
    end

    OutScriptVal:Set(req, ScriptValParam(tbl))
end
