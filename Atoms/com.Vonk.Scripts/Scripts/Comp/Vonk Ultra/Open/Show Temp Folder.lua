-- "Show Temp Folder" menu item

local path = app:MapPath("Temp:/Vonk/")

print("\n[Show Temp Folder] ", path)
if bmd.direxists(path) == false then
    bmd.createdir(path)
    print("[Created Temp Folder] ", path)
end

bmd.openfileexternal("Open", path)
