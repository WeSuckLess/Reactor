-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vPointMix"
DATATYPE = "Point"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Point\\Operators",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Performs linear interpolation between two Fusion Point objects.",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.Tracker",
})

function Create()
    -- [[ Creates the user interface. ]]
    InPoint1 = self:AddInput("Point1", "Point1", {
        LINKID_DataType = "Point",
        INPID_InputControl = "OffsetControl",
        INPID_PreviewControl = 'CrosshairControl',
        INP_DoNotifyChanged = true,
        INP_DefaultX = 0.5,
        INP_DefaultY = 0.5,
        LINK_Main = 1,
    })
    
    InPoint2 = self:AddInput("Point2", "Point2", {
        LINKID_DataType = "Point",
        INPID_InputControl = "OffsetControl",
        INPID_PreviewControl = 'CrosshairControl',
        INP_DoNotifyChanged = true,
        INP_DefaultX = 0.5,
        INP_DefaultY = 0.5,
        LINK_Main = 2,
    })
    
    InWeight = self:AddInput("Weight", "Weight" , {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        INP_MinScale = -1,
        INP_MaxScale = 1,
        INP_Default = 0,
        LINK_Main = 3,
    })
    
    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutPoint = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Point",
        INPID_InputControl = "OffsetControl",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end
        -- InPoint1:SetAttrs({LINK_Visible = visible})
        -- InPoint2:SetAttrs({LINK_Visible = visible})
        InWeight:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local p1 = InPoint1:GetValue(req)
    local p2 = InPoint2:GetValue(req)
    local weight = InWeight:GetValue(req).Value

    local x1 = tonumber(p1.X)
    local y1 = tonumber(p1.Y)

    local x2 = tonumber(p2.X)
    local y2 = tonumber(p2.Y)

    local outX = x1 * ( 1 - weight ) + x2 * weight
    local outY = y1 * ( 1 - weight ) + y2 * weight

    local out = Point(outX, outY)
    OutPoint:Set(req, out)
end
