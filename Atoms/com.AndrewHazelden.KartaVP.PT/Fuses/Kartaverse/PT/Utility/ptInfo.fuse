-- ============================================================================
-- modules
-- ============================================================================
local json = self and require("dkjson") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "ptInfo"
DATATYPE = "ScriptVal"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\PT\\Utility",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "View the PT Datastream in the Inspector.",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.LightTrim",
})

function Create()
    -- [[ Creates the user interface. ]]
    InShowDump = self:AddInput("Show Dump", "ShowDump", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InUISeparator1 = self:AddInput("UISeparator1", "UISeparator1", {
        IC_Visible = true,
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
    })

    InDisplayLines = self:AddInput("Display Lines", "DisplayLines", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = true,
        INP_MaxScale = 100,
        INP_MinAllowed = 1,
        INP_Default = 15,
        LINK_Visible = true,
        INP_Passive = true,
        INP_DoNotifyChanged  = true,
    })
    InWrapLines = self:AddInput("Wrap Lines", "WrapLines", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InUISeparator2 = self:AddInput("UISeparator2", "UISeparator2", {
        IC_Visible = true,
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
    })

    InScriptVal = self:AddInput("ScriptVal", "ScriptVal", {
        LINKID_DataType = "ScriptVal",
        INPID_InputControl = "ScriptValListControl",
        IC_NoLabel = true,
        LC_Rows = 15,
        INP_Passive = true,
        INP_External = true,
        LINK_Main = 1
    })

    InJSON = self:AddInput("JSON" , "JSON" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        IC_NoLabel = true,
        TEC_ReadOnly = true,
        TECS_Language = "lua",
        INP_Passive = true,
        INP_External = true,
        TEC_Lines = 15
    })

    -- The output node connection where data is pushed out of the fuse
    OutScriptVal = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "ScriptVal",
        LINK_Main = 1
    })
end


function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InDisplayLines then
        local lines = InDisplayLines:GetSource(time, REQF_SecondaryTime).Value
        InScriptVal:SetAttrs({LC_Rows = lines})
        InJSON:SetAttrs({TEC_Lines = lines})
        -- Toggle the visibility to refresh the inspector view
        InScriptVal:SetAttrs({IC_Visible = false})
        InScriptVal:SetAttrs({IC_Visible = true})
        InJSON:SetAttrs({IC_Visible = false})
        InJSON:SetAttrs({IC_Visible = true})
    elseif inp == InWrapLines then
        local wrap
        if param.Value == 1.0 then wrap = true else wrap = false end
        InJSON:SetAttrs({TEC_Wrap = wrap})

        -- Toggle the visibility to refresh the inspector view
        InJSON:SetAttrs({IC_Visible = false})
        InJSON:SetAttrs({IC_Visible = true})
    end
end


function OnConnected(inp, old, new)
    if inp == InScriptVal and new ~= nil then
        -- New connection
    else
        InJSON:SetSource(Text(""), 0)
    end
end


function OnAddToFlow()
     InJSON:SetSource(Text(""), 0)
end


function Save()
    InJSON:SetSource(Text(""), 0)
end


function Process(req)
    -- [[ Creates the output. ]]
    local tbl = InScriptVal:GetValue(req):GetValue()
    local show_dump = InShowDump:GetValue(req).Value

    if show_dump == 1 then
        print("\n----------------------")
        print("[" .. tostring(self.Name) .. "]")
        local txt_str = bmd.writestring(tbl)
        dump(txt_str)
    end

    local json_str = json.encode(tbl, {indent = true})
    InJSON:SetSource(Text(json_str), 0)

    OutScriptVal:Set(req, ScriptValParam(tbl))
end
