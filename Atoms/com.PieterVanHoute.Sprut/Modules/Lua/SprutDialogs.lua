-- initialise UIManager
local ui = fu.UIManager
local disp = bmd.UIDispatcher(ui)

-- error dialog
-- Display a UI Manager 3 second popup for notifications
function SprutMsgDialog(title, msg, butt)
	local width,height = 450,100
   
	win = disp:AddWindow({
		ID = "SprutWin",
		WindowTitle = title,
		Geometry = {715, 490, width, height},
		Spacing = 10,
 
		ui:VGroup{
			ID = "root",
			ui:Label{
				ID = "MessageText",
				Text = msg,
				ReadOnly = true,
				Alignment = {
					AlignHCenter = true,
					AlignTop = true
				},
			},
			-- Add your GUI elements here:
			ui:Button{
				ID = 'SprutButt',
				Text = butt,
			},
		},
	})
   
	print("\n" .. tostring(title) .. '\n' .. tostring(msg) .. "\n")
	
	itm = win:GetItems()
   
	-- The window was closed
	function win.On.SprutWin.Close(ev)
		disp:ExitLoop()
	end
	
	function win.On.SprutButt.Clicked(ev)
		disp:ExitLoop()
	end
   
	win:Show()
	disp:RunLoop()
	win:Hide()
end